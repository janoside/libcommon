package com.janoside.simpledb;

import org.springframework.beans.factory.InitializingBean;

import com.janoside.security.encryption.Encryptor;
import com.janoside.security.encryption.EncryptorAware;
import com.xerox.amazonws.sdb.SimpleDB;

public class SimpleDbConnection implements EncryptorAware, InitializingBean {
	
	private SimpleDB simpleDB;
	
	private Encryptor encryptor;
	
	private String encryptedAwsAccessId;
	
	private String encryptedAwsSecretKey;
	
	private String server;
	
	private Integer port;
	
	private boolean secure;
	
	public void afterPropertiesSet() {
		if (port != null) {
			this.simpleDB = new SimpleDB(
					this.encryptor.decrypt(this.encryptedAwsAccessId),
					this.encryptor.decrypt(this.encryptedAwsSecretKey),
					this.secure,
					this.server,
					this.port);
			
		} else if (server != null) {
			this.simpleDB = new SimpleDB(
					this.encryptor.decrypt(this.encryptedAwsAccessId),
					this.encryptor.decrypt(this.encryptedAwsSecretKey),
					this.secure,
					this.server);
			
		} else {
			this.simpleDB = new SimpleDB(
					this.encryptor.decrypt(this.encryptedAwsAccessId),
					this.encryptor.decrypt(this.encryptedAwsSecretKey),
					this.secure);
		}
	}
	
	public SimpleDB getSimpleDB() {
		return this.simpleDB;
	}
	
	public void setEncryptor(Encryptor encryptor) {
		this.encryptor = encryptor;
	}
	
	public void setEncryptedAwsAccessId(String encryptedAwsAccessId) {
		this.encryptedAwsAccessId = encryptedAwsAccessId;
	}
	
	public void setEncryptedAwsSecretKey(String encryptedAwsSecretKey) {
		this.encryptedAwsSecretKey = encryptedAwsSecretKey;
	}
	
	public String getServer() {
		return this.server;
	}
	
	public void setServer(String server) {
		this.server = server;
	}
	
	public int getPort() {
		return this.port;
	}
	
	public void setPort(int port) {
		this.port = port;
	}
	
	public boolean isSecure() {
		return this.secure;
	}
	
	public void setSecure(boolean secure) {
		this.secure = secure;
	}
}