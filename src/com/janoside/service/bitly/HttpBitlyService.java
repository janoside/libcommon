package com.janoside.service.bitly;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.util.StringUtils;

import com.janoside.http.HttpClient;
import com.janoside.json.JsonArray;
import com.janoside.json.JsonObject;
import com.janoside.security.encryption.Encryptor;
import com.janoside.security.encryption.EncryptorAware;
import com.janoside.util.UrlUtil;

public class HttpBitlyService implements BitlyService, EncryptorAware {
	
	private HttpClient httpClient;
	
	private Encryptor encryptor;
	
	private String apiBaseUrl;
	
	private String domain;
	
	private String login;
	
	private String encryptedApiKey;
	
	private int apiTimeout;
	
	public HttpBitlyService() {
		this.domain = "bit.ly";
		this.apiTimeout = 10000;
		this.apiBaseUrl = "http://api.bit.ly/v3";
	}
	
	public String shortenUrl(String url) {
		StringBuilder requestUrl = new StringBuilder(this.apiBaseUrl + "/shorten?format=json");
		requestUrl.append("&longUrl=" + UrlUtil.encode(url));
		
		if (StringUtils.hasText(this.domain)) {
			requestUrl.append("&domain=" + UrlUtil.encode(this.domain));
		}
		
		if (StringUtils.hasText(this.login)) {
			requestUrl.append("&login=" + UrlUtil.encode(this.login));
		}
		
		if (StringUtils.hasText(this.encryptedApiKey)) {
			requestUrl.append("&apiKey=" + UrlUtil.encode(this.encryptor.decrypt(this.encryptedApiKey)));
		}
		
		JsonObject jsonResponse = new JsonObject(this.httpClient.get(requestUrl.toString(), this.apiTimeout));
		
		int statusCode = jsonResponse.getInt("status_code");
		
		if ((statusCode < 200) || (statusCode > 299)) {
			if (jsonResponse.has("status_txt")) {
				throw new RuntimeException("bit.ly request failed with status code " + statusCode + ": " + jsonResponse.getString("status_txt"));
				
			} else {
				throw new RuntimeException("bit.ly request failed with status code " + statusCode);
			}
		}
		
		return jsonResponse.getJsonObject("data").getString("url");
	}
	
	public List<Integer> getDailyUrlClicks(String url) {
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put("shortUrl", url);
		parameters.put("login", this.login);
		parameters.put("apiKey", this.encryptor.decrypt(this.encryptedApiKey));
		
		String jsonContent = this.httpClient.get(
				"http://api.bit.ly/v3/clicks_by_day",
				parameters,
				10000);
		
		JsonObject json = new JsonObject(jsonContent);
		
		ArrayList<Integer> views = new ArrayList<Integer>();
		
		JsonArray clicksArray = json.getJsonObject("data").getJsonArray("clicks_by_day").getJsonObject(0).getJsonArray("clicks");
		for (int i = 0; i < clicksArray.length(); i++) {
			JsonObject data = clicksArray.getJsonObject(i);
			
			views.add(0, data.getInt("clicks"));
		}
		
		return views;
	}
	
	public int getTotalUrlClicks(String url) {
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put("shortUrl", url);
		parameters.put("login", this.login);
		parameters.put("apiKey", this.encryptor.decrypt(this.encryptedApiKey));
		
		String jsonContent = this.httpClient.get(
				"http://api.bit.ly/v3/clicks",
				parameters,
				10000);
		
		JsonObject json = new JsonObject(jsonContent);
		
		return json.getJsonObject("data").getJsonArray("clicks").getJsonObject(0).getInt("global_clicks");
	}
	
	public void setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
	}
	
	public void setEncryptor(Encryptor encryptor) {
		this.encryptor = encryptor;
	}
	
	public void setApiBaseUrl(String apiBaseUrl) {
		this.apiBaseUrl = apiBaseUrl;
	}
	
	public void setDomain(String domain) {
		this.domain = domain;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public void setEncryptedApiKey(String encryptedApiKey) {
		this.encryptedApiKey = encryptedApiKey;
	}
	
	public void setApiTimeout(int apiTimeout) {
		this.apiTimeout = apiTimeout;
	}
}