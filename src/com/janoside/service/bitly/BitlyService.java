package com.janoside.service.bitly;

import java.util.List;

public interface BitlyService {
	
	List<Integer> getDailyUrlClicks(String url);
	
	String shortenUrl(String url);
	
	int getTotalUrlClicks(String url);
}