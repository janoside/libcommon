package com.janoside.stats;

import java.util.concurrent.TimeUnit;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InfluxDbWrapper {
	
	private static final Logger logger = LoggerFactory.getLogger(InfluxDbStatTracker.class);
	
	private InfluxDB influxDb;
	
	private String url;
	
	private String username;
	
	private String password;
	
	private int batchMaxTime;
	
	private int batchMaxPointCount;
	
	public InfluxDbWrapper() {
		this.batchMaxTime = 5000;
		this.batchMaxPointCount = 5000;
	}
	
	public boolean isReady() {
		return this.setupIfNeeded();
	}
	
	private boolean setupIfNeeded() {
		if (this.influxDb == null) {
			synchronized (this) {
				if (this.influxDb == null) {
					this.influxDb = InfluxDBFactory.connect(this.url, this.username, this.password);
					
					// set up batching params
					this.influxDb.enableBatch(this.batchMaxPointCount, // flush every X points...
							this.batchMaxTime, // at least every Y millis
							TimeUnit.MILLISECONDS);
				}
			}
		}
		
		return (this.influxDb != null);
	}
	
	public InfluxDB getInfluxDb() {
		return this.influxDb;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public void setBatchMaxTime(int batchMaxTime) {
		this.batchMaxTime = batchMaxTime;
	}
	
	public void setBatchMaxPointCount(int batchMaxPointCount) {
		this.batchMaxPointCount = batchMaxPointCount;
	}
}