package com.janoside.stats;

import com.janoside.environment.Environment;
import com.janoside.environment.EnvironmentAware;

public class StatsdStatTracker extends BaseStatTracker implements EnvironmentAware {
	
	private StatsdClient statsdClient;
	
	private Environment environment;
	
	private String globalPrefix;
	
	public StatsdStatTracker() {
		this.globalPrefix = "janoside.";
	}
	
	protected void trackEventInternal(String name, int count) {
		this.statsdClient.increment(this.globalPrefix + name, count);
	}
	
	protected void trackPerformanceInternal(String name, long millis) {
		this.statsdClient.timing(this.globalPrefix + name, (int) millis);
	}
	
	protected void trackValueInternal(String name, float value) {
		this.statsdClient.gauge(this.globalPrefix + name, (int) (value * 100));
	}
	
	public void setStatsdClient(StatsdClient statsdClient) {
		this.statsdClient = statsdClient;
	}
	
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}
	
	public void setGlobalPrefix(String globalPrefix) {
		this.globalPrefix = globalPrefix;
		
		if (!this.globalPrefix.endsWith(".")) {
			this.globalPrefix = (globalPrefix + ".");
		}
	}
}