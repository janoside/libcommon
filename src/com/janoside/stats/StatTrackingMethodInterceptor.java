package com.janoside.stats;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.aopalliance.intercept.MethodInvocation;

import com.janoside.aop.FilteringMethodInterceptor;
import com.janoside.util.StatTrackerUtil;

public class StatTrackingMethodInterceptor extends FilteringMethodInterceptor implements StatTrackerAware {
	
	private StatTracker statTracker;
	
	private Map<Method, String> statNamesByMethod;
	
	private String statDirectory;
	
	public StatTrackingMethodInterceptor() {
		this.statNamesByMethod = new ConcurrentHashMap<Method, String>();
		this.statDirectory = "uncategorized-stats";
	}
	
	protected Object invokeInternal(MethodInvocation invocation) throws Throwable {
		String statName = this.getStatName(invocation.getMethod());
		
		try {
			this.statTracker.trackThreadPerformanceStart(statName);
			
			this.statTracker.trackEvent(statName + ".attempt");
			
			Object result = invocation.proceed();
			
			this.statTracker.trackEvent(statName + ".success");
			
			return result;
			
		} catch (Throwable t) {
			this.statTracker.trackEvent(statName + ".exception");
			
			throw t;
			
		} finally {
			this.statTracker.trackThreadPerformanceEnd(statName);
		}
	}
	
	private String getStatName(Method method) {
		if (!this.statNamesByMethod.containsKey(method)) {
			String statName = String.format("%s.%s", this.statDirectory, StatTrackerUtil.normalizeNameComponent(method.getName()));
			
			this.statNamesByMethod.put(method, statName);
		}
		
		return this.statNamesByMethod.get(method);
	}
	
	public void setStatTracker(StatTracker statTracker) {
		this.statTracker = statTracker;
	}
	
	public void setStatDirectory(String statDirectory) {
		this.statDirectory = statDirectory;
	}
}
