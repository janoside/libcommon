package com.janoside.stats;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.influxdb.dto.Point;
import org.influxdb.dto.Point.Builder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.janoside.environment.Environment;
import com.janoside.environment.EnvironmentAware;
import com.janoside.util.RandomUtil;
import com.janoside.util.SystemTimeSource;
import com.janoside.util.TimeSource;
import com.janoside.util.TimeSourceAware;

public class InfluxDbStatTracker extends BaseStatTracker implements TimeSourceAware, EnvironmentAware {
	
	private static final Logger logger = LoggerFactory.getLogger(InfluxDbStatTracker.class);
	
	private InfluxDbWrapper influxDbWrapper;
	
	private TimeSource timeSource;
	
	private Environment environment;
	
	private Map<String, String> defaultTags;
	
	private String dbName;
	
	private String retentionPolicy;
	
	private float samplingRate;
	
	private boolean active;
	
	public InfluxDbStatTracker() {
		this.timeSource = new SystemTimeSource();
		this.retentionPolicy = "default";
		this.samplingRate = 1.0f;
		this.active = false;
	}
	
	protected void trackEventInternal(String name, int count, Map<String, String> tags) {
		try {
			if (!this.isActiveAndReady()) {
				return;
			}
			
			Builder pointBuilder = this.buildBasicPoint(name, tags);
			pointBuilder.addField("count", count);
			
			Point point = pointBuilder.build();
			
			this.influxDbWrapper.getInfluxDb().write(this.dbName, this.retentionPolicy, point);
			
		} catch (Throwable t) {
			logger.error("Failed tracking event in InfluxDB: name={}, count={}, tags={}", name, count, this.defaultTags, t);
		}
	}
	
	protected void trackPerformanceInternal(String name, long millis, Map<String, String> tags) {
		try {
			if (!this.isActiveAndReady()) {
				return;
			}
			
			Builder pointBuilder = this.buildBasicPoint(name, tags);
			pointBuilder.addField("performance", millis);
			
			Point point = pointBuilder.build();
			
			this.influxDbWrapper.getInfluxDb().write(this.dbName, this.retentionPolicy, point);
			
		} catch (Throwable t) {
			logger.error("Failed tracking performance in InfluxDB: name={}, performance={}, tags={}", name, millis, this.defaultTags, t);
		}
	}
	
	protected void trackValueInternal(String name, float value, Map<String, String> tags) {
		try {
			if (!this.isActiveAndReady()) {
				return;
			}
			
			Builder pointBuilder = this.buildBasicPoint(name, tags);
			pointBuilder.addField("value", value);
			
			Point point = pointBuilder.build();
			
			this.influxDbWrapper.getInfluxDb().write(this.dbName, this.retentionPolicy, point);
			
		} catch (Throwable t) {
			logger.error("Failed tracking value in InfluxDB: name={}, value={}, tags={}", name, value, this.defaultTags, t);
		}
	}
	
	private Builder buildBasicPoint(String name, Map<String, String> tags) {
		Builder pointBuilder = Point.measurement(name)
				.time(this.timeSource.getCurrentTime(), TimeUnit.MILLISECONDS)
				.tag(this.defaultTags);
		
		if (tags != null && !tags.isEmpty()) {
			pointBuilder.tag(tags);
		}
		
		return pointBuilder;
	}
	
	private boolean isActiveAndReady() {
		if (!this.active) {
			return false;
		}
		
		if (!this.influxDbWrapper.isReady()) {
			return false;
		}
		
		if (!this.setupIfNeeded()) {
			return false;
		}
		
		return (RandomUtil.randomFloat(0, 1) <= this.samplingRate);
	}
	
	private boolean setupIfNeeded() {
		if (this.defaultTags == null && this.environment != null) {
			synchronized (this) {
				if (this.defaultTags == null) {
					String hwId = environment.getHardwareId();
					if (hwId.endsWith(".astar.mobi")) {
						hwId = hwId.substring(0, hwId.length() - ".astar.mobi".length());
					}
					
					final String hardwareId = hwId;
					final String scv = environment.getSourcecodeVersion();
					this.defaultTags = new HashMap<String, String>() {{
						put("app", environment.getAppName());
						put("version", scv.length() > 8 ? scv.substring(0, 8) : scv);
						put("server", hardwareId);
					}};
				}
			}
		}
		
		return (this.defaultTags != null);
	}
	
	public void setInfluxDbWrapper(InfluxDbWrapper influxDbWrapper) {
		this.influxDbWrapper = influxDbWrapper;
	}
	
	public void setTimeSource(TimeSource timeSource) {
		this.timeSource = timeSource;
	}
	
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}
	
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	
	public void setRetentionPolicy(String retentionPolicy) {
		this.retentionPolicy = retentionPolicy;
	}
	
	public float getSamplingRate() {
		return this.samplingRate;
	}
	
	public void setSamplingRate(float samplingRate) {
		this.samplingRate = samplingRate;
	}
	
	public boolean isActive() {
		return this.active;
	}
	
	public void setActive(boolean active) {
		this.active = active;
	}
}