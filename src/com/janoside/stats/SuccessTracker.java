package com.janoside.stats;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

@ManagedResource(objectName = "Janoside:name=SuccessTracker")
public class SuccessTracker {
	
	private EventTracker successTracker;
	
	private EventTracker failureTracker;
	
	private String name;
	
	/**
	 * The minimum total count (successes and failures) for which
	 * the SuccessTracker will publish its results (via getSuccessRate()).
	 * Until this count is met getSuccessRate() will return 1.0f;
	 */
	private int minPublishCount;
	
	public SuccessTracker() {
		this.successTracker = new EventTracker();
		this.failureTracker = new EventTracker();
		this.name = super.toString();
		this.minPublishCount = 10;
	}
	
	public void reset() {
		this.successTracker.reset();
		this.failureTracker.reset();
	}
	
	public void success() {
		this.successTracker.increment();
	}
	
	public void failure() {
		this.failureTracker.increment();
	}
	
	@ManagedAttribute
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@ManagedAttribute
	public float getSuccessRate() {
		if (this.getTotalCount() >= this.minPublishCount && (this.successTracker.getCount() > 0 || this.failureTracker.getCount() > 0)) {
			return ((float) this.successTracker.getCount() / (this.successTracker.getCount() + this.failureTracker.getCount()));
			
		} else {
			return 1.0f;
		}
	}
	
	@ManagedAttribute
	public long getTotalCount() {
		return (this.successTracker.getCount() + this.failureTracker.getCount());
	}
	
	@ManagedAttribute
	public long getSuccessCount() {
		return this.successTracker.getCount();
	}
	
	@ManagedAttribute
	public long getFailureCount() {
		return this.failureTracker.getCount();
	}
	
	@ManagedAttribute
	public int getRateMeasureWindowSize() {
		return this.successTracker.getRateMeasureWindowSize();
	}
	
	public void setRateMeasureWindowSize(int rateMeasureWindowSize) {
		this.successTracker.setRateMeasureWindowSize(rateMeasureWindowSize);
		this.failureTracker.setRateMeasureWindowSize(rateMeasureWindowSize);
	}
	
	@ManagedAttribute
	public int getMinPublishCount() {
		return this.minPublishCount;
	}
	
	public void setMinPublishCount(int minPublishCount) {
		this.minPublishCount = minPublishCount;
	}
}