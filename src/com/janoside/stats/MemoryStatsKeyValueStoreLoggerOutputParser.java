package com.janoside.stats;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.janoside.csv.CsvFieldExtractor;
import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.storage.KeyValueStore;
import com.janoside.util.DateUtil;
import com.janoside.util.StringUtil;

public class MemoryStatsKeyValueStoreLoggerOutputParser implements ExceptionHandlerAware {
	
	private KeyValueStore<byte[]> keyValueStore;
	
	private ExecutorService executorService;
	
	private CsvFieldExtractor csvFieldExtractor;
	
	private ExceptionHandler exceptionHandler;
	
	public MemoryStatsKeyValueStoreLoggerOutputParser() {
		this.executorService = Executors.newFixedThreadPool(25);
		this.csvFieldExtractor = new CsvFieldExtractor();
	}
	
	public SortedMap<String, SortedMap<Date, Long>> parseEventData(final String directory, Date start, Date end, final long resolution) {
		List<Date> dates = DateUtil.getDatesBetween(start, end, resolution);
		
		TreeMap<String, SortedMap<Date, Long>> data = new TreeMap<String, SortedMap<Date, Long>>();
		
		Map<Date, List<byte[]>> dataListsByTimeSlot = this.getDataByDates(dates, directory, "events.csv", resolution);
		
		for (Map.Entry<Date, List<byte[]>> entry : dataListsByTimeSlot.entrySet()) {
			for (byte[] dataItem : entry.getValue()) {
				String eventReportCsv = StringUtil.utf8StringFromBytes(dataItem);
				
				List<Map<String, String>> csv = this.csvFieldExtractor.getCsvFields(eventReportCsv, ",", false, false);
				
				for (Map<String, String> row : csv) {
					String name = row.get("name").trim();
					String countStr = row.get(" count").trim();
					long count = Long.parseLong(countStr);
					
					if (count == 0) {
						continue;
					}
					
					if (!data.containsKey(name)) {
						data.put(name, new TreeMap<Date, Long>());
						
						for (Date date : dates) {
							data.get(name).put(date, 0L);
						}
					}
					
					data.get(name).put(entry.getKey(), data.get(name).get(entry.getKey()) + count);
				}
			}
		}
		
		return data;
	}
	
	public SortedMap<String, SortedMap<Date, Map<String, Long>>> parsePerformanceData(String directory, Date start, Date end, long resolution) {
		List<Date> dates = DateUtil.getDatesBetween(start, end, resolution);
		
		TreeMap<String, SortedMap<Date, Map<String, Long>>> data = new TreeMap<String, SortedMap<Date, Map<String, Long>>>();
		
		Map<Date, List<byte[]>> dataListsByTimeSlot = this.getDataByDates(dates, directory, "performances.csv", resolution);
		
		for (Map.Entry<Date, List<byte[]>> entry : dataListsByTimeSlot.entrySet()) {
			for (byte[] dataItem : entry.getValue()) {
				String performanceReportCsv = StringUtil.utf8StringFromBytes(dataItem);
				
				List<Map<String, String>> csv = this.csvFieldExtractor.getCsvFields(performanceReportCsv, ",", false, false);
				
				for (Map<String, String> row : csv) {
					String name = row.get("name").trim();
					String countStr = row.get(" count").trim();
					long count = Long.parseLong(countStr);
					String avgStr = row.get(" average (ms)").trim();
					long avg = (long) Float.parseFloat(avgStr);
					String slowestStr = row.get(" slowest (ms)").trim();
					long slowest = (long) Float.parseFloat(slowestStr);
					
					if (!data.containsKey(name)) {
						data.put(name, new TreeMap<Date, Map<String, Long>>());
						
						for (Date date : dates) {
							data.get(name).put(date, new HashMap<String, Long>() {
								{
									put("average", 0L);
									put("slowest", 0L);
									put("count", 0L);
								}
							});
						}
					}
					
					Map<String, Long> map = data.get(name).get(entry.getKey());
					
					if (slowest > map.get("slowest")) {
						map.put("slowest", slowest);
					}
					
					long total = map.get("count") * map.get("average");
					total += count * avg;
					
					long totalCount = map.get("count") + count;
					
					map.put("average", total / totalCount);
					map.put("count", totalCount);
				}
			}
		}
		
		return data;
	}
	
	public SortedMap<String, SortedMap<Date, Map<String, Number>>> parseValueData(String directory, Date start, Date end, long resolution) {
		List<Date> dates = DateUtil.getDatesBetween(start, end, resolution);
		
		TreeMap<String, SortedMap<Date, Map<String, Number>>> data = new TreeMap<String, SortedMap<Date, Map<String, Number>>>();
		
		Map<Date, List<byte[]>> dataListsByTimeSlot = this.getDataByDates(dates, directory, "values.csv", resolution);
		
		for (Map.Entry<Date, List<byte[]>> entry : dataListsByTimeSlot.entrySet()) {
			for (byte[] dataItem : entry.getValue()) {
				String valueReportCsv = StringUtil.utf8StringFromBytes(dataItem);
				
				List<Map<String, String>> csv = this.csvFieldExtractor.getCsvFields(valueReportCsv, ",", false, false);
				
				for (Map<String, String> row : csv) {
					String name = row.get("name").trim();
					String countStr = row.get("count").trim();
					long count = Long.parseLong(countStr);
					String avgStr = row.get("avg").trim();
					float avg = Float.parseFloat(avgStr);
					String maxStr = row.get("max").trim();
					final float max = Float.parseFloat(maxStr);
					String minStr = row.get("min").trim();
					final float min = Float.parseFloat(minStr);
					
					if (!data.containsKey(name)) {
						data.put(name, new TreeMap<Date, Map<String, Number>>());
						
						for (Date date : dates) {
							data.get(name).put(date, new HashMap<String, Number>() {
								{
									put("average", 0.0f);
									put("max", max);
									put("min", min);
									put("count", 0);
								}
							});
						}
					}
					
					Map<String, Number> map = data.get(name).get(entry.getKey());
					
					if (max > map.get("max").floatValue()) {
						map.put("max", max);
					}
					
					if (min < map.get("min").floatValue()) {
						map.put("min", min);
					}
					
					float total = (int)(map.get("count").intValue() * map.get("average").floatValue());
					total += count * avg;
					
					long totalCount = (long) map.get("count").intValue() + count;
					
					map.put("average", total / totalCount);
					map.put("count", totalCount);
				}
			}
		}
		
		return data;
	}
	
	private Map<Date, List<byte[]>> getDataByDates(final List<Date> dates, final String directory, final String keySuffix, final long resolution) {
		final ConcurrentHashMap<Date, List<byte[]>> dataListsByTimeSlot = new ConcurrentHashMap<Date, List<byte[]>>();
		
		final CountDownLatch latch = new CountDownLatch(dates.size());
		for (final Date date : dates) {
			dataListsByTimeSlot.put(date, new ArrayList<byte[]>());
			
			this.executorService.execute(new Runnable() {
				public void run() {
					try {
						Date d = new Date(date.getTime());
						while (d.getTime() - date.getTime() < resolution) {
							String key = String.format(
									"%s/%s/%s/%s",
									directory,
									DateUtil.format("yyyy-MM-dd", d),
									DateUtil.format("HHmm", d),
									keySuffix);
							
							byte[] dataSet = keyValueStore.get(key);
							
							if (key.equals("janoside/192.168.1.2/Webadmin/2013-11-14/1138/events.csv")) {
								System.out.println("cheese");
							}
							
							if (dataSet != null) {
								dataListsByTimeSlot.get(date).add(dataSet);
							}
							
							d.setTime(d.getTime() + 60000);
						}
					} catch (Throwable t) {
						exceptionHandler.handleException(t);
						
					} finally {
						latch.countDown();
					}
				}
			});
		}
		
		try {
			latch.await();
			
		} catch (InterruptedException ie) {
			this.exceptionHandler.handleException(ie);
		}
		
		return dataListsByTimeSlot;
	}
	
	public void setKeyValueStore(KeyValueStore<byte[]> keyValueStore) {
		this.keyValueStore = keyValueStore;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
}