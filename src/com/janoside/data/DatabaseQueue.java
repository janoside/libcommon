package com.janoside.data;

import java.util.List;

import com.janoside.beans.ObjectSource;
import com.janoside.queue.ObjectQueue;

public class DatabaseQueue<T extends Model> implements ObjectQueue<T> {
	
	private EntityPersister<T> entityPersister;
	
	private ObjectSource<T> dequeueObjectSource;
	
	public void enqueue(T object) {
		this.entityPersister.persist(object);
	}
	
	public List<T> dequeue(int count) {
		return this.dequeueObjectSource.getObjects(count);
	}
	
	public void clear() {
		throw new UnsupportedOperationException("clear() is unsupported for DatabaseQueues");
	}
	
	public int getSize() {
		return 0;
	}
	
	public void setEntityPersister(EntityPersister<T> entityPersister) {
		this.entityPersister = entityPersister;
	}
	
	public void setDequeueObjectSource(ObjectSource<T> dequeueObjectSource) {
		this.dequeueObjectSource = dequeueObjectSource;
	}
}