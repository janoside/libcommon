package com.janoside.data;

public interface EntityDeleter<T> {
	
	void delete(T entity);
}