package com.janoside.data;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * Interface for delegating the correction of database constraint violations. Used by the
 * default EntityPersister implementation to check each model object before persistence is
 * attempted.
 *
 * @param <T> - Type of model object being checked/corrected.
 */
public interface ConstraintViolationResolver<T extends Model> {
	
	/**
	 * Resolves any constraint violations detected in the given model object.
	 * @param hibernateDaoSupport - Access to data source
	 * @param dataObject - Model object with potention constraint violations
	 * @return - Whether corrective action was necessary
	 */
	boolean resolveConstraintViolations(HibernateDaoSupport hibernateDaoSupport, T dataObject);
}