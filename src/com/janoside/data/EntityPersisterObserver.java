package com.janoside.data;

public interface EntityPersisterObserver<T> {
	
	void beforeWrite(EntityPersister<T> entityPersister, T entity);
	
	void afterWrite(EntityPersister<T> entityPersister, T entity);
}