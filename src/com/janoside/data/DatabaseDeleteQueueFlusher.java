package com.janoside.data;

import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.queue.ObjectConsumer;

@ManagedResource(objectName = "Janoside:name=DatabaseDeleteQueueFlusher")
public class DatabaseDeleteQueueFlusher implements ObjectConsumer<Model> {

	private EntityDeleter<Model> entityDeleter;
	
	public void consume(Model object) {
		this.entityDeleter.delete(object);
	}

	public void setEntityDeleter(EntityDeleter<Model> entityDeleter) {
		this.entityDeleter = entityDeleter;
	}
}