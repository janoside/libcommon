package com.janoside.data;

import java.util.Timer;
import java.util.TimerTask;

import org.hibernate.SessionFactory;
import org.hibernate.stat.QueryStatistics;
import org.hibernate.stat.Statistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.hash.Md5Hasher;
import com.janoside.stats.StatTracker;
import com.janoside.stats.StatTrackerAware;

public class HibernateSessionFactoryStatMonitor implements StatTrackerAware, ExceptionHandlerAware, InitializingBean {
	
	private static final Logger logger = LoggerFactory.getLogger(HibernateSessionFactoryStatMonitor.class);
	
	private SessionFactory sessionFactory;
	
	private StatTracker statTracker;
	
	private ExceptionHandler exceptionHandler;
	
	private Md5Hasher queryHasher;
	
	private String statDirectory;
	
	private long period;
	
	public HibernateSessionFactoryStatMonitor() {
		this.queryHasher = new Md5Hasher();
		this.period = 60000;
	}
	
	public void afterPropertiesSet() {
		new Timer("HibernateSessionFactoryStatMonitor-GatherTimer-" + Integer.toHexString(this.hashCode())).scheduleAtFixedRate(new TimerTask() {
				public void run() {
					try {
						gatherStats();
						
					} catch (Throwable t) {
						exceptionHandler.handleException(t);
					}
				}
			},
			this.period,
			this.period);
		
		
		// turn hibernate stat tracking on if it's not
		Statistics stats = this.sessionFactory.getStatistics();
		
		if (!stats.isStatisticsEnabled()) {
			stats.setStatisticsEnabled(true);
		}
	}
	
	public void gatherStatsManually() {
		this.gatherStats();
	}
	
	private void gatherStats() {
		Statistics stats = this.sessionFactory.getStatistics();
		
		// general
		this.gatherStatIfNonZero(".connections", (int) stats.getConnectCount());
		this.gatherStatIfNonZero(".transactions", (int) stats.getTransactionCount());
		this.gatherStatIfNonZero(".successful-transactions", (int) stats.getSuccessfulTransactionCount());
		this.gatherStatIfNonZero(".queries", (int) stats.getQueryExecutionCount());
		this.gatherStatIfNonZero(".flushes", (int) stats.getFlushCount());
		this.gatherStatIfNonZero(".sessions-opened", (int) stats.getSessionOpenCount());
		this.gatherStatIfNonZero(".sessions-closed", (int) stats.getSessionCloseCount());
		this.gatherStatIfNonZero(".optimistic-failures", (int) stats.getOptimisticFailureCount());
		this.gatherStatIfNonZero(".prepared-statement", (int) stats.getPrepareStatementCount());
		
		// entities
		this.gatherStatIfNonZero(".entity-loads", (int) stats.getEntityLoadCount());
		this.gatherStatIfNonZero(".entity-deletes", (int) stats.getEntityDeleteCount());
		this.gatherStatIfNonZero(".entity-inserts", (int) stats.getEntityInsertCount());
		this.gatherStatIfNonZero(".entity-fetches", (int) stats.getEntityFetchCount());
		this.gatherStatIfNonZero(".entity-updates", (int) stats.getEntityUpdateCount());
		
		// second-level cache
		this.gatherStatIfNonZero(".second-level-cache.hit-count", (int) stats.getSecondLevelCacheHitCount());
		this.gatherStatIfNonZero(".second-level-cache.miss-count", (int) stats.getSecondLevelCacheMissCount());
		this.gatherStatIfNonZero(".second-level-cache.put-count", (int) stats.getSecondLevelCachePutCount());
		
		// query cache
		this.gatherStatIfNonZero(".query-cache.hit-count", (int) stats.getQueryCacheHitCount());
		this.gatherStatIfNonZero(".query-cache.miss-count", (int) stats.getQueryCacheMissCount());
		this.gatherStatIfNonZero(".query-cache.put-count", (int) stats.getQueryCachePutCount());
		
		// individual queries
		for (String queryId : stats.getQueries()) {
			try {
				QueryStatistics queryStats = stats.getQueryStatistics(queryId);
				
				String queryHash = this.queryHasher.hash(queryId).substring(0, 10);
				
				this.gatherStatIfNonZero(".queries." + queryHash + ".execution-count", (int) queryStats.getExecutionCount());
				this.gatherStatIfNonZero(".queries." + queryHash + ".row-count", (int) queryStats.getExecutionRowCount());
				this.gatherStatIfNonZero(".queries." + queryHash + ".cache-hit-count", (int) queryStats.getCacheHitCount());
				this.gatherStatIfNonZero(".queries." + queryHash + ".cache-miss-count", (int) queryStats.getCacheMissCount());
				
				this.statTracker.trackPerformance(this.statDirectory + ".queries." + queryHash + ".average-time", queryStats.getExecutionAvgTime());
				this.statTracker.trackPerformance(this.statDirectory + ".queries." + queryHash + ".max-time", queryStats.getExecutionMaxTime());
				this.statTracker.trackPerformance(this.statDirectory + ".queries." + queryHash + ".total-time", queryStats.getExecutionCount() * queryStats.getExecutionAvgTime());
				
				logger.info("Query stats: queryHash=" + queryHash + ", query=" + queryId);
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
		}
		
		if (stats.getQueryExecutionMaxTime() > 0) {
			this.statTracker.trackPerformance(this.statDirectory + ".slowest-query", stats.getQueryExecutionMaxTime());
		}
		
		// log the slowest query for optional analysis
		logger.info("Slowest query (" + stats.getQueryExecutionMaxTime() + "ms): " + stats.getQueryExecutionMaxTimeQueryString());
		
		// reset
		stats.clear();
	}
	
	private void gatherStatIfNonZero(String name, int value) {
		if (value > 0) {
			this.statTracker.trackEvent(this.statDirectory + name, value);
		}
	}
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public void setStatTracker(StatTracker statTracker) {
		this.statTracker = statTracker;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setStatDirectory(String statDirectory) {
		this.statDirectory = statDirectory;
	}
	
	public void setPeriod(long period) {
		this.period = period;
	}
}