package com.janoside.data;

public interface ModelCreator<T extends Model> {
	
	T create();
}