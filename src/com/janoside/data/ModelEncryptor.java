package com.janoside.data;

public interface ModelEncryptor {
	
	void encrypt(Model model);
	
	void decrypt(Model model);
}