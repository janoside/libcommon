package com.janoside.data;

public interface EntityPersister<T> {

	void persist(T entity);
}