package com.janoside.data;

import com.janoside.queue.ObjectConsumer;

public class EntityPersisterObjectConsumer<T> implements ObjectConsumer<T> {
	
	private EntityPersister<T> entityPersister;
	
	public void consume(T object) {
		this.entityPersister.persist(object);
	}
	
	public void setEntityPersister(EntityPersister<T> entityPersister) {
		this.entityPersister = entityPersister;
	}
}