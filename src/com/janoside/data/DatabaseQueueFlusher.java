package com.janoside.data;

import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.criteria.BooleanCriteria;
import com.janoside.criteria.Criteria;
import com.janoside.queue.ObjectConsumer;

@ManagedResource(objectName = "Janoside:name=DatabaseQueueFlusher")
public class DatabaseQueueFlusher implements ObjectConsumer<Model> {
	
	private EntityPersister<Model> entityPersister;
	
	/**
	 * List of Criteria that, if any of which are met, causes data to be
	 * dropped (lost) instead of persisted. These Criteria should be
	 * configured carefully.
	 */
	private Criteria dropDataCriteria;
	
	public DatabaseQueueFlusher() {
		this.dropDataCriteria = new BooleanCriteria(false);
	}
	
	public void consume(Model object) {
		if (this.dropDataCriteria.isMet()) {
			return;
		}
		
		this.entityPersister.persist(object);
	}
	
	public void setEntityPersister(EntityPersister<Model> entityPersister) {
		this.entityPersister = entityPersister;
	}
	
	public void setDropDataCriteria(Criteria dropDataCriteria) {
		this.dropDataCriteria = dropDataCriteria;
	}
}