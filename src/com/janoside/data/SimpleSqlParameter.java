package com.janoside.data;

import java.io.Serializable;

@SuppressWarnings("serial")
public class SimpleSqlParameter implements Serializable {
	
	private Object value;
	
	private int sqlDataType;
	
	public SimpleSqlParameter(Object value, int sqlDataType) {
		this.value = value;
		this.sqlDataType = sqlDataType;
	}
	
	public Object getValue() {
		return value;
	}
	
	public void setValue(Object value) {
		this.value = value;
	}
	
	public int getSqlDataType() {
		return sqlDataType;
	}
	
	public void setSqlDataType(int sqlDataType) {
		this.sqlDataType = sqlDataType;
	}
}