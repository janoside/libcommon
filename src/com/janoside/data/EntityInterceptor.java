package com.janoside.data;

import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;

import org.hibernate.CallbackException;
import org.hibernate.EmptyInterceptor;
import org.hibernate.Transaction;

import com.janoside.app.Application;
import com.janoside.util.SystemTimeSource;
import com.janoside.util.TimeSource;
import com.janoside.util.TimeSourceAware;

public class EntityInterceptor extends EmptyInterceptor implements
		TimeSourceAware {
private TimeSource timeSource;
	
	public EntityInterceptor() {
		this.timeSource = new SystemTimeSource();
	}
	
	public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState, String[] propertyNames, org.hibernate.type.Type[] types) {
		return prepareToPersist(entity, id, currentState, propertyNames, types);
	}
	
	public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, org.hibernate.type.Type[] types) {
		return prepareToPersist(entity, id, state, propertyNames, types);
	}
	
	protected boolean prepareToPersist(Object entity, Serializable id, Object[] state, String[] propertyNames, org.hibernate.type.Type[] types) {
		boolean changed = false;
		
		if (entity instanceof Model) {
			Model dataObject = (Model) entity;
			if (dataObject.getCreatedAt() == null) {
				dataObject.setCreatedAt(new Date(this.timeSource.getCurrentTime()));
				updateState("createdAt", dataObject.getCreatedAt(), state, propertyNames);
			}
			
			if (dataObject.getCreatedBy() == 0) {
				dataObject.setCreatedBy((int) Application.getInstance().getId());
				updateState("createdBy", dataObject.getCreatedBy(), state, propertyNames);
			}
			
			dataObject.setUpdatedAt(new Date(this.timeSource.getCurrentTime()));
			updateState("updatedAt", dataObject.getUpdatedAt(), state, propertyNames);
			
			dataObject.setUpdatedBy((int) Application.getInstance().getId());
			updateState("updatedBy", dataObject.getUpdatedBy(), state, propertyNames);
			
			changed = true;
		}
		
		return changed;
	}
	
	public boolean onLoad(Object entity, Serializable id, Object[] state, String[] propertyNames, org.hibernate.type.Type[] types) {
		return false;
	}
	
	public String onPrepareStatement(String sql) {
		return sql;
	}
	
	public void afterTransactionCompletion(Transaction transaction) {
	}
	
	public void postFlush(Iterator iterator) throws CallbackException {
	}
	
	protected void updateState(String propertyToUpdate, Object value, Object[] state, String[] propertyNames) {
		for (int i = 0; i < propertyNames.length; i++) {
			if (propertyNames[i].equals(propertyToUpdate)) {
				state[i] = value;
				break;
			}
		}
	}
	
	public void setTimeSource(TimeSource timeSource) {
		this.timeSource = timeSource;
	}

}
