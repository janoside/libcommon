package com.janoside.exception;

import java.lang.Thread.UncaughtExceptionHandler;

import org.springframework.beans.factory.InitializingBean;

public class UncaughtExceptionsCatcher implements UncaughtExceptionHandler, ExceptionHandlerAware, InitializingBean {
	
	private ExceptionHandler exceptionHandler;
	
	private UncaughtExceptionHandler replacedHandler;
	
	public void afterPropertiesSet() {
		this.replacedHandler = Thread.getDefaultUncaughtExceptionHandler();
		
		Thread.setDefaultUncaughtExceptionHandler(this);
	}
	
	public void uncaughtException(Thread t, Throwable e) {
		this.exceptionHandler.handleException(e);
		
		if (this.replacedHandler != null) {
			this.replacedHandler.uncaughtException(t, e);
		}
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
}