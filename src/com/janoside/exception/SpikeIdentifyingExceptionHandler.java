package com.janoside.exception;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.health.ErrorTracker;
import com.janoside.health.HealthMonitor;
import com.janoside.stats.DataSeries;
import com.janoside.util.DateUtil;

@ManagedResource(objectName = "Janoside:name=SpikeIdentifyingExceptionHandler")
public class SpikeIdentifyingExceptionHandler implements ExceptionHandler, HealthMonitor {
	
	private DataSeries<Integer> dataSeries;
	
	private AtomicInteger pendingExceptionCount;
	
	/*
	 * The amount (ratio) above the moving average the latest value must be
	 * in order to be identified as a spike.
	 */
	private double spikeRatioThreshold;
	
	private int minSpikeMagnitude;
	
	public SpikeIdentifyingExceptionHandler() {
		this.dataSeries = new DataSeries<Integer>();
		this.dataSeries.setWindowSize(15);
		
		this.pendingExceptionCount = new AtomicInteger(0);
		this.spikeRatioThreshold = 0.5;
		
		this.minSpikeMagnitude = 100;
		
		new Timer().scheduleAtFixedRate(new TimerTask() {
				public void run() {
					SpikeIdentifyingExceptionHandler.this.logPendingExceptions();
				}
			},
			DateUtil.MinuteMillis,
			DateUtil.MinuteMillis);
	}
	
	public void handleException(Throwable t) {
		this.pendingExceptionCount.getAndIncrement();
	}
	
	public void reportInternalErrors(ErrorTracker errorTracker) {
		if (this.dataSeries.isSpikingUpward(this.spikeRatioThreshold)) {
			if (this.dataSeries.getLatestValue().doubleValue() >= this.minSpikeMagnitude) {
				int magnitude = this.dataSeries.getLatestValue().intValue();
				double ratio = this.dataSeries.getLatestValue().doubleValue() / this.dataSeries.getAverage();
				
				errorTracker.trackError(String.format("Exception spike identified, spikeMagnitude=%d, spikeRatioAboveAverage=%.2f", magnitude, ratio));
			}
		}
	}
	
	private void logPendingExceptions() {
		this.dataSeries.addValue(this.pendingExceptionCount.getAndSet(0));
	}
	
	@ManagedAttribute
	public double getSpikeRatioThreshold() {
		return this.spikeRatioThreshold;
	}
	
	@ManagedAttribute
	public void setSpikeRatioThreshold(double spikeRatioThreshold) {
		this.spikeRatioThreshold = spikeRatioThreshold;
	}
	
	@ManagedAttribute
	public int getMinSpikeMagnitude() {
		return this.minSpikeMagnitude;
	}
	
	@ManagedAttribute
	public void setMinSpikeMagnitude(int minSpikeMagnitude) {
		this.minSpikeMagnitude = minSpikeMagnitude;
	}
}