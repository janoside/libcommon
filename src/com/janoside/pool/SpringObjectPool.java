package com.janoside.pool;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.spring.SpringObjectFactory;

@ManagedResource(objectName = "Janoside:name=SpringObjectPool")
public class SpringObjectPool<T extends Poolable> extends ManagedObjectPool<T> implements BeanFactoryAware {
	
	private SpringObjectFactory<T> springObjectFactory;
	
	public SpringObjectPool() {
		this.springObjectFactory = new SpringObjectFactory<T>();
		this.setFactory(this.springObjectFactory);
	}
	
	public void setBeanFactory(BeanFactory beanFactory) {
		this.springObjectFactory.setBeanFactory(beanFactory);
	}
	
	public void setPrototypeBeanId(String prototypeBeanId) {
		this.springObjectFactory.setPrototypeBeanId(prototypeBeanId);
	}
}