package com.janoside.list;

import java.util.Map;

public class ListItem {
	
	private Map<String, String> propertyValues;
	
	private String type;
	
	private String id;
	
	public Map<String, String> getPropertyValues() {
		return propertyValues;
	}
	
	public void setPropertyValues(Map<String, String> propertyValues) {
		this.propertyValues = propertyValues;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
}