package com.janoside.list;

import java.util.List;
import java.util.Map;

import com.janoside.data.SimpleSqlDataSource;
import com.janoside.paging.Page;
import com.janoside.paging.SortDirection;
import com.janoside.paging.SortField;
import com.janoside.util.SqlUtility;

public class DatabaseListService implements ListService {
	
	private SimpleSqlDataSource dataSource;
	
	public List<ListItem> getItems(String type, Map<String, String> propertyValues, Page page) {
		StringBuilder buffer = new StringBuilder();
		
		buffer.append("select\n\tdistinct ip0.item_id\nfrom\n\titem_properties ip0\nleft join\n\titem_types it\non\n\tip0.item_type_id=it.id ");
		
		int joinCount = 0;
		for (int i = 0; i < propertyValues.size(); i++) {
			joinCount++;
			
			buffer.append("\nleft join\n\titem_properties ip" + joinCount + "\non\n\tip0.item_type_id=ip" + joinCount + ".item_type_id\nand\n\tip0.item_id=ip" + joinCount + ".item_id");
		}
		
		if (page.getSortFields() != null && !page.getSortFields().isEmpty()) {
			for (int i = 0; i < page.getSortFields().size(); i++) {
				joinCount++;
				
				buffer.append("\nleft join\n\titem_properties ip" + joinCount + "\non\n\tip0.item_type_id=ip" + joinCount + ".item_type_id\nand\n\tip0.item_id=ip" + joinCount + ".item_id");
			}
		}
		
		buffer.append("\nwhere\n\tit.name='" + SqlUtility.escape(type) + "'");
		
		joinCount = 0;
		for (Map.Entry<String, String> entry : propertyValues.entrySet()) {
			joinCount++;
			
			buffer.append("\nand\n\tip" + joinCount + ".property_name='" + SqlUtility.escape(entry.getKey()) + "'\nand\n\tip" + joinCount + ".property_value='" + SqlUtility.escape(entry.getValue()) + "'");
		}
		
		if (page.getSortFields() != null && !page.getSortFields().isEmpty()) {
			int joinCountStart = joinCount;
			
			for (SortField sortField : page.getSortFields()) {
				joinCount++;
				
				buffer.append("\nand\n\tip" + joinCount + ".property_name='" + SqlUtility.escape(sortField.getName()) + "'");
			}
			
			joinCount = joinCountStart;
			for (SortField sortField : page.getSortFields()) {
				joinCount++;
				
				buffer.append("\norder by\n\tip" + joinCount + ".property_value " + (sortField.getSortDirection() == SortDirection.Descending ? "desc" : "asc"));
			}
			
			buffer.append("\nlimit\n\t" + page.getCount() + "\noffset\n\t" + page.getStart());
		}
		
		System.out.println(buffer.toString());
		
		return null;
	}
	
	public int getItemCount(String type, Map<String, String> propertyValues) {
		return 0;
	}
	
	public void saveItem(ListItem item) {
	}
}