package com.janoside.beans;

import java.util.Map;

import com.janoside.json.JsonObject;

public class StringKeyStringValueMapParser implements ObjectParser<Map<String, String>> {
	
	public Map<String, String> parse(String value) {
		return new JsonObject(value).toStringMap();
	}
}