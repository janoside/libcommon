package com.janoside.beans;

public interface ObjectStringDescriber<T> {
	
	String describe(T object);
}