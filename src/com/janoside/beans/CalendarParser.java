package com.janoside.beans;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class CalendarParser implements ObjectParser<Calendar> {
	
	private DateFormat dateFormat;
	
	public Calendar parse(String value) {
		try {
			Date date = this.dateFormat.parse(value);
			
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			
			return calendar;
			
		} catch (ParseException pe) {
			throw new RuntimeException(pe);
		}
	}
	
	public void setDateFormat(DateFormat dateFormat) {
		this.dateFormat = dateFormat;
	}
}