package com.janoside.beans;

public class LongParser implements ObjectParser<Long> {
	
	public Long parse(String value) {
		return Long.parseLong(value);
	}
}