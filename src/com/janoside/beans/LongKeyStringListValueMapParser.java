package com.janoside.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Parses strings of the form (([0-9]+)=\[((.*),)+\]) into <code>Map&lt;Integer, List&lt;String&gt;&gt;</code>.
 */
public class LongKeyStringListValueMapParser implements ObjectParser<Map<Long, List<String>>> {
	
	public Map<Long, List<String>> parse(String input) {
		String value = input.trim();
		
		HashMap<Long, List<String>> map = new HashMap<Long, List<String>>();
		
		while (value.length() > 0) {
			// key
			long longValue = Long.parseLong(value.substring(0, value.indexOf("=")).trim());
			
			ArrayList<String> stringList = new ArrayList<String>();
			
			boolean done = false;
			value = value.substring(value.indexOf("[") + 1).trim();
			while (!done) {
				if (!value.startsWith("\"")) {
					throw new IllegalArgumentException("Invalid map format");
				}
				
				value = value.substring(1);
				
				stringList.add(value.substring(0, value.indexOf("\"")));
				
				value = value.substring(value.indexOf("\"") + 1).trim();
				
				char nextChar = value.charAt(0);
				
				if (nextChar != ']' && nextChar != ',') {
					throw new IllegalArgumentException("Invalid map format");
				}
				
				done = (nextChar == ']');
				
				if (!done) {
					// move past comma
					value = value.substring(1).trim();
				}
			}
			
			map.put(longValue, stringList);
			
			value = value.substring(value.indexOf("]") + 1).trim();
			value = value.substring(value.indexOf(",") + 1).trim();
		}
		
		return map;
	}
}