package com.janoside.beans;

public class BooleanParser implements ObjectParser<Boolean> {
	
	public Boolean parse(String value) {
		return Boolean.parseBoolean(value);
	}
}