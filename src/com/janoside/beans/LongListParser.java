package com.janoside.beans;

import java.util.ArrayList;
import java.util.List;

import com.janoside.json.JsonArray;

public class LongListParser implements ObjectParser<List<Long>> {
	
	public List<Long> parse(String value) {
		ArrayList<Long> list = new ArrayList<Long>();
		
		JsonArray json = new JsonArray(value);
		for (int i = 0; i < json.length(); i++) {
			list.add(json.getLong(i));
		}
		
		return list;
	}
}