package com.janoside.beans;

import java.beans.PropertyDescriptor;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.commons.beanutils.PropertyUtils;

import com.janoside.json.JsonArray;
import com.janoside.json.JsonObject;

@SuppressWarnings("serial")
public class JsonObjectStringDescriber implements ObjectStringDescriber<Object> {
	
	private static final HashSet<Class<?>> primitiveTypes = new HashSet<Class<?>>() {{
		add(Integer.class);
		add(Float.class);
		add(Number.class);
		add(Double.class);
		add(Boolean.class);
		add(String.class);
		add(Byte.class);
		add(Long.class);
		add(Number.class);
	}};
	
	private static final HashMap<Class<?>, PropertyDescriptor[]> propertyDescriptorsByClass = new HashMap<Class<?>, PropertyDescriptor[]>();
	
	private DateFormat dateFormatter;
	
	private boolean showClass;
	
	private boolean showFullClass;
	
	public JsonObjectStringDescriber() {
		this.dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		this.showClass = false;
		this.showFullClass = false;
	}
	
	public String describe(Object object) {
		ArrayList<Object> objectGraph = new ArrayList<Object>();
		
		return this.getObjectDescription(object, objectGraph).toString();
	}
	
	private Object getObjectDescription(Object object, Collection<Object> objectGraph) {
		if (!objectGraph.contains(object)) {
			objectGraph.add(object);
		}
		
		if (object == null) {
			return "null";
			
		} else if (object instanceof Class<?>) {
			return "Class(" + ((Class<?>) object).getName() + ")";
			
		} else if (object instanceof Collection<?>) {
			JsonArray propertyValueList = new JsonArray();
			
			for (Object element : ((Collection<?>) object)) {
				if (element == object) {
					propertyValueList.put("self");
					
				} else {
					propertyValueList.put(this.getObjectDescription(element, objectGraph));
				}
			}
			
			return propertyValueList;
			
		} else if (object instanceof Date) {
			return this.dateFormatter.format((Date) object);
			
		} else if (object instanceof Calendar) {
			return this.dateFormatter.format(((Calendar) object).getTime());
			
		} else if (JsonObjectStringDescriber.primitiveTypes.contains(object.getClass())) {
			return object;
			
		} else {
			return this.getJsonDescription(object, objectGraph);
		}
	}
	
	private JsonObject getJsonDescription(Object object, Collection<Object> objectGraph) {
		PropertyDescriptor[] propertyDescriptors = JsonObjectStringDescriber.propertyDescriptorsByClass.get(object.getClass());
		
		if (propertyDescriptors == null) {
			propertyDescriptors = PropertyUtils.getPropertyDescriptors(object.getClass());
			
			JsonObjectStringDescriber.propertyDescriptorsByClass.put(object.getClass(), propertyDescriptors);
		}
		
		HashMap<String, Object> propertyValues = new HashMap<String, Object>();
		
		for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
			Object propertyValue = null;
			boolean failure = false;
			
			try {
				propertyValue = PropertyUtils.getProperty(object, propertyDescriptor.getName());
				
			} catch (Exception e) {
				// exceptions here are inherent to the objects being described
				// therefore, this is not an exceptional case and there's no need to track
				failure = true;
			}
			
			if (failure) {
				propertyValues.put(propertyDescriptor.getName(), "error");
				
			} else if (propertyValue == null) {
				propertyValues.put(propertyDescriptor.getName(), "null");
				
			} else if (objectGraph.contains(propertyValue)) {
				// this is where we avoid stack overflows, grab simple description from existing object graph
				propertyValues.put(propertyDescriptor.getName(), "ref(" + propertyValue.getClass().getName() + ")");
				
			} else if (propertyValue instanceof Class<?>) {
				if (this.showClass) {
					if (this.showFullClass) {
						propertyValues.put(propertyDescriptor.getName(), ((Class<?>) propertyValue).getName());
						
					} else {
						propertyValues.put(propertyDescriptor.getName(), ((Class<?>) propertyValue).getSimpleName());
					}
				}
			} else if (propertyValue instanceof Collection<?>) {
				JsonArray propertyValueList = new JsonArray();
				
				for (Object element : ((Collection<?>) propertyValue)) {
					propertyValueList.put(this.getObjectDescription(element, objectGraph));
				}
				
				propertyValues.put(propertyDescriptor.getName(), propertyValueList);
				
			} else if (propertyValue instanceof Date) {
				propertyValues.put(propertyDescriptor.getName(), this.dateFormatter.format((Date) propertyValue));
				
			} else if (propertyValue instanceof Calendar) {
				propertyValues.put(propertyDescriptor.getName(), this.dateFormatter.format(((Calendar) propertyValue).getTime()));
				
			} else if (JsonObjectStringDescriber.primitiveTypes.contains(propertyValue.getClass())) {
				propertyValues.put(propertyDescriptor.getName(), propertyValue);
				
			} else {
				propertyValues.put(propertyDescriptor.getName(), this.getObjectDescription(propertyValue, objectGraph));
			}
		}
		
		return new JsonObject(propertyValues);
	}
	
	public void setDateFormatter(DateFormat dateFormatter) {
		this.dateFormatter = dateFormatter;
	}
	
	public void setShowClass(boolean showClass) {
		this.showClass = showClass;
	}
	
	public void setShowFullClass(boolean showFullClass) {
		this.showFullClass = showFullClass;
	}
}