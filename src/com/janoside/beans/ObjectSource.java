package com.janoside.beans;

import java.util.List;

public interface ObjectSource<T> {
	
	List<T> getObjects(int count);
}