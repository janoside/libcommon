package com.janoside.beans;

import java.util.regex.Pattern;

public class PatternParser implements ObjectParser<Pattern> {
	
	public Pattern parse(String value) {
		return Pattern.compile(value, Pattern.CASE_INSENSITIVE);
	}
}