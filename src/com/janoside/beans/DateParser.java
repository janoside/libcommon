package com.janoside.beans;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

public class DateParser implements ObjectParser<Date> {
	
	private DateFormat dateFormat;
	
	public Date parse(String value) {
		try {
			return this.dateFormat.parse(value);
			
		} catch (ParseException pe) {
			throw new RuntimeException(pe);
		}
	}
	
	public void setDateFormat(DateFormat dateFormat) {
		this.dateFormat = dateFormat;
	}
}