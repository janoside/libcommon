package com.janoside.factory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;

import com.janoside.util.ClassUtils;

public class ClassNameKeyedObjectFactory implements KeyedObjectFactory, InitializingBean {
	
	/**
	 * Map of the form {"ClassName"="com.package.ClassName"}
	 */
	private Map<String, String> classNamesByType;
	
	/**
	 * List of package names from which all classes will be registered.
	 */
	private List<String> packages;
	
	public ClassNameKeyedObjectFactory() {
		this.packages = new ArrayList<String>();
	}
	
	public <T> T createObject(String type) {
		if (!this.classNamesByType.containsKey(type)) {
			throw new RuntimeException("No class name registration for type name '" + type + "'");
		}
		
		try {
			return (T) Class.forName(this.classNamesByType.get(type)).newInstance();
			
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}
	
	public void afterPropertiesSet() {
		for (String p : this.packages) {
			Class[] packageClasses = ClassUtils.getClasses(p);
			
			for (Class c : packageClasses) {
				this.classNamesByType.put(c.getSimpleName(), c.getCanonicalName());
			}
		}
	}
	
	public void setClassNamesByType(Map<String, String> classNamesByType) {
		this.classNamesByType = classNamesByType;
	}
	
	public void setPackages(List<String> packages) {
		this.packages = packages;
	}
}