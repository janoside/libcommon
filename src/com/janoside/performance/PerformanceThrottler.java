package com.janoside.performance;

import java.util.Timer;
import java.util.TimerTask;

import com.janoside.cache.MemoryCache;
import com.janoside.stats.MemoryStats;

public class PerformanceThrottler {
	
	private MemoryStats memoryStats;
	
	private MemoryCache<Boolean> blockedActionNames;
	
	private Timer timer;
	
	private long performanceThreshold;
	
	private long throttleLength;
	
	public PerformanceThrottler() {
		this.memoryStats = new MemoryStats();
		this.blockedActionNames = new MemoryCache<Boolean>();
		this.performanceThreshold = 10000;
		this.throttleLength = 120000;
		
		this.timer = new Timer();
		this.timer.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				for (String actionName : memoryStats.getPerformances().keySet()) {
					long avgMs = (long) (1000 * memoryStats.getPerformances().get(actionName).getAverage());
					
					if (avgMs >= performanceThreshold) {
						blockedActionNames.put(actionName, Boolean.TRUE, throttleLength);
					}
				}
			}
		}, 60000, 60000);
	}
	
	public boolean canPerform(String action) {
		return (this.blockedActionNames.get(action) != null);
	}
	
	public void countPerformanceStart(String name) {
		this.memoryStats.countPerformanceStart(name);
	}
	
	public void countPerformanceEnd(String name) {
		this.memoryStats.countPerformanceEnd(name);
	}
	
	public void setPerformanceThreshold(long performanceThreshold) {
		this.performanceThreshold = performanceThreshold;
	}
	
	public void setThrottleLength(long throttleLength) {
		this.throttleLength = throttleLength;
	}
}