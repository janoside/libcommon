package com.janoside.spring;

import java.util.HashMap;
import java.util.Map;

public class InjectableHashMap<K, V> extends HashMap<K, V> {
	
	public void setValues(Map<K, V> values) {
		super.clear();
		super.putAll(values);
	}
}