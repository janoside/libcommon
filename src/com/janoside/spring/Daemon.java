package com.janoside.spring;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

@ManagedResource(objectName = "Janoside:name=Daemon")
public class Daemon {

	private static Daemon instance;
	
	private AbstractApplicationContext context;
	
	private Object shutdown;
	
	public static void main(String[] args) throws Exception {
		instance = new Daemon();
		
		synchronized (instance.getShutdown()) {
			while (!instance.isShutdown()) {
				instance.getShutdown().wait();
			}
		}
	}
	
	public Daemon() {
		this.shutdown = new Object();
		this.context = new ClassPathXmlApplicationContext("applicationContext.xml");
		this.context.registerShutdownHook();
	}
	
	@ManagedOperation
	public void shutdown() {
		this.shutdown = "shutdown";
		this.shutdown.notifyAll();
	}
	
	protected Object getShutdown() {
		return this.shutdown;
	}
	
	@ManagedAttribute
	public boolean isShutdown() {
		return ("shutdown".equals(this.shutdown));
	}
}