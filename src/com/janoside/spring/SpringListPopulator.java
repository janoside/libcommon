package com.janoside.spring;

import java.util.List;

import com.janoside.app.Application;
import com.janoside.app.ApplicationObserver;
import com.janoside.collections.ListPopulator;

public class SpringListPopulator<T> implements ApplicationObserver {
	
	private ListPopulator<T> listPopulator;
	
	private List<T> list;
	
	public void onApplicationInitialized(Application application) {
		this.listPopulator.populate(list);
	}
	
	public void setListPopulator(ListPopulator<T> listPopulator) {
		this.listPopulator = listPopulator;
	}
	
	public void setList(List<T> list) {
		this.list = list;
	}
}