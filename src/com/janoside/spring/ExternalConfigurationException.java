package com.janoside.spring;

@SuppressWarnings("serial")
public class ExternalConfigurationException extends RuntimeException {
	
	public ExternalConfigurationException(String message, Exception cause) {
		super(message, cause);
	}
	
	public ExternalConfigurationException(String message) {
		super(message);
	}
}