package com.janoside.spring;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

public class ExternalConfigurationNamespaceHandler extends NamespaceHandlerSupport {
	
	public void init() {
		registerBeanDefinitionDecorator("external-property", new ExternalConfigurationBeanDefinitionDecorator());
	}
}