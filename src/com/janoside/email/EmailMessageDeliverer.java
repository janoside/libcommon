package com.janoside.email;

import java.util.Map;
import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.util.StringUtils;

import com.janoside.environment.Environment;
import com.janoside.environment.EnvironmentAware;
import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.message.Message;
import com.janoside.message.MessageDeliverer;
import com.janoside.security.encryption.Encryptor;
import com.janoside.security.encryption.EncryptorAware;
import com.janoside.util.ArrayUtils;

@ManagedResource(objectName = "Janoside:name=Emailer")
public class EmailMessageDeliverer implements MessageDeliverer, EnvironmentAware, EncryptorAware, ExceptionHandlerAware, InitializingBean {
	
	private static final Logger logger = LoggerFactory.getLogger(SpringEmailer.class);
	
	private JavaMailSenderImpl mailSender;
	
	private Environment environment;
	
	private Encryptor encryptor;
	
	private ExceptionHandler exceptionHandler;
	
	private String username;
	
	private String encryptedPassword;
	
	private String displayName;
	
	private long successfulEmailCount;
	
	private long failedEmailCount;
	
	public EmailMessageDeliverer() {
		Properties props = System.getProperties();
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.auth", "true");
		
		this.mailSender = new JavaMailSenderImpl();
		this.mailSender.setHost("smtp.gmail.com");
		this.mailSender.setPort(587);
		this.mailSender.setJavaMailProperties(props);
		
		this.successfulEmailCount = 0;
		this.failedEmailCount = 0;
	}

	public void deliver(Message message) {
		try {
			if (message.getAttachments().isEmpty()) {
				SimpleMailMessage smMessage = new SimpleMailMessage();
				
				smMessage.setText(message.getBody());
				smMessage.setSubject(message.getSubject());
				smMessage.setFrom(this.displayName);
				smMessage.setTo(message.getToRecipients().toArray(new String[]{}));
				smMessage.setCc(message.getCcRecipients().toArray(new String[]{}));
				smMessage.setBcc(message.getBccRecipients().toArray(new String[]{}));
				
				if (StringUtils.hasText(System.getProperty("email.debug.address")) && !this.environment.getName().equals("production")) {
					smMessage.setText("\nDebug email:\n----------\nOriginal To: " + ArrayUtils.toString(smMessage.getTo()) + "\nOriginal CC: " + ArrayUtils.toString(smMessage.getCc()) + "\nOriginal BCC: " + ArrayUtils.toString(smMessage.getBcc()) + "\n----------\n\n" + smMessage.getText());
					smMessage.setTo(System.getProperty("email.debug.address"));
					smMessage.setCc(new String[]{});
					smMessage.setBcc(new String[]{});
				}
				
				this.mailSender.send(smMessage);
				
			} else {
				MimeMessage mimeMessage = this.mailSender.createMimeMessage();
				
				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
				
				for (String to : message.getToRecipients()) {
					mimeMessageHelper.addTo(to);
				}
				
				for (String cc : message.getCcRecipients()) {
					mimeMessageHelper.addCc(cc);
				}
				
				for (String bcc : message.getBccRecipients()) {
					mimeMessageHelper.addBcc(bcc);
				}
				
				for (Map.Entry<String, byte[]> entry : message.getAttachments().entrySet()) {
					mimeMessageHelper.addAttachment(entry.getKey(), new ByteArrayResource(entry.getValue()));
				}
				
				mimeMessageHelper.setFrom(this.displayName);
				mimeMessageHelper.setSubject(message.getSubject());
				mimeMessageHelper.setText(message.getBody());
				
				if (StringUtils.hasText(System.getProperty("email.debug.address")) && !this.environment.getName().equals("production")) {
					StringBuilder originalRecipientsBuffer = new StringBuilder();
					for (int i = 0; i < mimeMessageHelper.getMimeMessage().getAllRecipients().length; i++) {
						if (i > 0) {
							originalRecipientsBuffer.append(", ");
						}
						
						originalRecipientsBuffer.append(mimeMessageHelper.getMimeMessage().getAllRecipients()[i]);
					}
					
					mimeMessageHelper.setText("\nDebug email:\n----------\nOriginal Recipients: " + originalRecipientsBuffer + "\n----------\n\n" + mimeMessageHelper.getMimeMessage().getContent());
					mimeMessageHelper.setTo(System.getProperty("email.debug.address"));
					mimeMessageHelper.setCc(new String[]{});
					mimeMessageHelper.setBcc(new String[]{});
				}
				
				this.mailSender.send(mimeMessage);
			}
			
			logger.info(String.valueOf(message));
			
			this.successfulEmailCount++;
			
		} catch (Throwable t) {
			logger.error(String.valueOf(message), t);
			
			this.failedEmailCount++;
			
			this.exceptionHandler.handleException(t);
		}
	}
	
	public void afterPropertiesSet() {
		this.mailSender.setUsername(this.username);
		this.mailSender.setPassword(this.encryptor.decrypt(this.encryptedPassword));
		
		if (!StringUtils.hasText(this.displayName)) {
			this.displayName = this.username;
		}
	}
	
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}
	
	public void setEncryptor(Encryptor encryptor) {
		this.encryptor = encryptor;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public String getUsername() {
		return this.username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}
	
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	@ManagedAttribute
	public long getSuccessfulEmailCount() {
		return this.successfulEmailCount;
	}
	
	@ManagedAttribute
	public long getFailedEmailCount() {
		return this.failedEmailCount;
	}
	
	@ManagedAttribute
	public float getEmailSuccessRate() {
		return ((float) this.successfulEmailCount / (this.successfulEmailCount + this.failedEmailCount));
	}
}