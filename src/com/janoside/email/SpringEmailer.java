package com.janoside.email;

import java.util.Map;
import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.util.StringUtils;

import com.janoside.environment.Environment;
import com.janoside.environment.EnvironmentAware;
import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.security.encryption.Encryptor;
import com.janoside.security.encryption.EncryptorAware;
import com.janoside.util.ArrayUtils;

@ManagedResource(objectName = "Janoside:name=Emailer")
public class SpringEmailer implements Emailer, EnvironmentAware, EncryptorAware, ExceptionHandlerAware, InitializingBean {
	
	private static final Logger logger = LoggerFactory.getLogger(SpringEmailer.class);
	
	private JavaMailSenderImpl mailSender;
	
	private Environment environment;
	
	private Encryptor encryptor;
	
	private ExceptionHandler exceptionHandler;
	
	private String username;
	
	private String encryptedPassword;
	
	private String displayName;
	
	private long successfulEmailCount;
	
	private long failedEmailCount;
	
	public SpringEmailer() {
		Properties props = System.getProperties();
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.auth", "true");
		
		this.mailSender = new JavaMailSenderImpl();
		this.mailSender.setHost("smtp.gmail.com");
		this.mailSender.setPort(587);
		this.mailSender.setJavaMailProperties(props);
		
		this.successfulEmailCount = 0;
		this.failedEmailCount = 0;
	}

	public void sendEmail(Email email) {
		try {
			if (email.getAttachments().isEmpty()) {
				SimpleMailMessage message = new SimpleMailMessage();
				
				message.setText(email.getBody());
				message.setSubject(email.getSubject());
				message.setFrom(this.displayName);
				message.setTo(email.getToRecipients().toArray(new String[]{}));
				message.setCc(email.getCcRecipients().toArray(new String[]{}));
				message.setBcc(email.getBccRecipients().toArray(new String[]{}));
				
				// is a debug-email-address system-property configured?
				if (StringUtils.hasText(System.getProperty("email.debug.address")) && !this.environment.getName().equals("production")) {
					message.setText("\nDebug email:\n----------\nOriginal To: " + ArrayUtils.toString(message.getTo()) + "\nOriginal CC: " + ArrayUtils.toString(message.getCc()) + "\nOriginal BCC: " + ArrayUtils.toString(message.getBcc()) + "\n----------\n\n" + message.getText());
					message.setTo(System.getProperty("email.debug.address"));
					message.setCc(new String[]{});
					message.setBcc(new String[]{});
				}
				
				this.mailSender.send(message);
				
			} else {
				MimeMessage message = this.mailSender.createMimeMessage();
				
				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(message, true);
				
				for (String to : email.getToRecipients()) {
					mimeMessageHelper.addTo(to);
				}
				
				for (String cc : email.getCcRecipients()) {
					mimeMessageHelper.addCc(cc);
				}
				
				for (String bcc : email.getBccRecipients()) {
					mimeMessageHelper.addBcc(bcc);
				}
				
				for (Map.Entry<String, byte[]> entry : email.getAttachments().entrySet()) {
					mimeMessageHelper.addAttachment(entry.getKey(), new ByteArrayResource(entry.getValue()));
				}
				
				mimeMessageHelper.setFrom(this.displayName);
				mimeMessageHelper.setSubject(email.getSubject());
				mimeMessageHelper.setText(email.getBody());
				
				// is a debug-email-address system-property configured?
				if (StringUtils.hasText(System.getProperty("email.debug.address")) && !this.environment.getName().equals("production")) {
					StringBuilder originalRecipientsBuffer = new StringBuilder();
					for (int i = 0; i < mimeMessageHelper.getMimeMessage().getAllRecipients().length; i++) {
						if (i > 0) {
							originalRecipientsBuffer.append(", ");
						}
						
						originalRecipientsBuffer.append(mimeMessageHelper.getMimeMessage().getAllRecipients()[i]);
					}
					
					mimeMessageHelper.setText("\nDebug email:\n----------\nOriginal Recipients: " + originalRecipientsBuffer + "\n----------\n\n" + mimeMessageHelper.getMimeMessage().getContent());
					mimeMessageHelper.setTo(System.getProperty("email.debug.address"));
					mimeMessageHelper.setCc(new String[]{});
					mimeMessageHelper.setBcc(new String[]{});
				}
				
				this.mailSender.send(message);
			}
			
			logger.info(String.valueOf(email));
			
			this.successfulEmailCount++;
			
		} catch (Throwable t) {
			logger.error(String.valueOf(email), t);
			
			this.failedEmailCount++;
			
			this.exceptionHandler.handleException(t);
		}
	}
	
	public void afterPropertiesSet() {
		this.mailSender.setUsername(this.username);
		this.mailSender.setPassword(this.encryptor.decrypt(this.encryptedPassword));
		
		if (!StringUtils.hasText(this.displayName)) {
			this.displayName = this.username;
		}
	}
	
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}
	
	public void setEncryptor(Encryptor encryptor) {
		this.encryptor = encryptor;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public String getUsername() {
		return this.username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}
	
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	@ManagedAttribute
	public long getSuccessfulEmailCount() {
		return this.successfulEmailCount;
	}
	
	@ManagedAttribute
	public long getFailedEmailCount() {
		return this.failedEmailCount;
	}
	
	@ManagedAttribute
	public float getEmailSuccessRate() {
		return ((float) this.successfulEmailCount / (this.successfulEmailCount + this.failedEmailCount));
	}
}