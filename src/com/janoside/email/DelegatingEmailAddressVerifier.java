package com.janoside.email;

import java.util.List;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;

public class DelegatingEmailAddressVerifier implements EmailAddressVerifier, ExceptionHandlerAware {
	
	private ExceptionHandler exceptionHandler;
	
	private List<EmailAddressVerifier> emailAddressVerifiers;
	
	public boolean emailExists(String email) {
		for (EmailAddressVerifier emailAddressVerifier : this.emailAddressVerifiers) {
			try {
				if (emailAddressVerifier.emailExists(email)) {
					return true;
				}
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
		}
		
		return false;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setEmailAddressVerifiers(List<EmailAddressVerifier> emailAddressVerifiers) {
		this.emailAddressVerifiers = emailAddressVerifiers;
	}
}