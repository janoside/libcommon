package com.janoside.email;

import com.janoside.queue.ObjectConsumer;

public class EmailSendingConsumer implements ObjectConsumer<Email> {
	
	private Emailer emailer;
	
	public void consume(Email email) {
		this.emailer.sendEmail(email);
	}
	
	public void setEmailer(Emailer emailer) {
		this.emailer = emailer;
	}
}