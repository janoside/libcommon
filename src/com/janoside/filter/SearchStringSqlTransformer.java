package com.janoside.filter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.janoside.transform.ObjectTransformer;

public class SearchStringSqlTransformer implements ObjectTransformer<Map<String, List<String>>, String> {
	
	private static final String[] booleanOperators = new String[] {"OR", "AND"};
	
	private Map<String, String> parameterNameMap;
	
	private char wildcardChar;
	
	private boolean useLike;
	
	public SearchStringSqlTransformer() {
		this.parameterNameMap = new HashMap<String, String>();
		this.wildcardChar = '*';
		this.useLike = true;
	}
	
	public String transform(Map<String, List<String>> map) {
		return this.transform(map, "`");
	}
	
	public String transform(Map<String, List<String>> map, String quoteChar) {
		StringBuilder buffer = new StringBuilder();
		
		int index = 0;
		for (Map.Entry<String, List<String>> entry : map.entrySet()) {
			for (String item : entry.getValue()) {
				boolean itemHandled = false;
				
				for (String booleanOperator : booleanOperators) {
					String[] itemParts = item.split(" " + booleanOperator + " ");
					
					if (itemParts.length > 1) {
						itemHandled = true;
						
						if (index > 0) {
							buffer.append(" and ");
						}
						
						buffer.append("(");
						
						boolean negate = entry.getKey().startsWith("-");
						
						int innerIndex = 0;
						for (String itemPart : itemParts) {
							if (innerIndex > 0) {
								// if negating: OR -> AND in groupings
								if (negate && booleanOperator.equalsIgnoreCase("or")) {
									buffer.append(" AND ");
									
								} else {
									buffer.append(" " + booleanOperator + " ");
								}
							}
							
							buffer.append(quoteChar);
							
							if (this.parameterNameMap.containsKey(entry.getKey())) {
								buffer.append(this.parameterNameMap.get(entry.getKey()).replaceAll(Pattern.quote("."), String.format("%s.%s", quoteChar, quoteChar)));
								
							} else {
								if (negate) {
									buffer.append(entry.getKey().substring(1).replaceAll(Pattern.quote("."), String.format("%s.%s", quoteChar, quoteChar)));
									
								} else {
									buffer.append(entry.getKey().replaceAll(Pattern.quote("."), String.format("%s.%s", quoteChar, quoteChar)));
								}
							}
							
							buffer.append(quoteChar);
							
							if (this.useLike) {
								if (negate) {
									buffer.append(" not like ");
									
								} else {
									buffer.append(" like ");
								}
								
								if (!itemPart.contains(String.valueOf(this.wildcardChar))) {
									itemPart = this.wildcardChar + itemPart + this.wildcardChar;
								}
								
								buffer.append("'");
								buffer.append(itemPart.replace(this.wildcardChar, '%'));
								buffer.append("'");
								
							} else {
								throw new UnsupportedOperationException("Only useLike=true is currently supported");
							}
							
							innerIndex++;
						}
						
						buffer.append(")");
						
					}
				}
				
				if (!itemHandled) {
					if (index > 0) {
						buffer.append(" and ");
					}
					
					boolean negate = false;
					
					buffer.append(quoteChar);
					
					if (this.parameterNameMap.containsKey(entry.getKey())) {
						buffer.append(this.parameterNameMap.get(entry.getKey()).replaceAll(Pattern.quote("."), String.format("%s.%s", quoteChar, quoteChar)));
						
					} else {
						if (entry.getKey().startsWith("-")) {
							negate = true;
							
							buffer.append(entry.getKey().substring(1).replaceAll(Pattern.quote("."), String.format("%s.%s", quoteChar, quoteChar)));
							
						} else {
							buffer.append(entry.getKey().replaceAll(Pattern.quote("."), String.format("%s.%s", quoteChar, quoteChar)));
						}
					}
					
					buffer.append(quoteChar);
					
					if (this.useLike) {
						if (negate) {
							buffer.append(" not like ");
							
						} else {
							buffer.append(" like ");
						}
						
						if (!item.contains(String.valueOf(this.wildcardChar))) {
							item = this.wildcardChar + item + this.wildcardChar;
						}
						
						buffer.append("'");
						buffer.append(item.replace(this.wildcardChar, '%'));
						buffer.append("'");
						
					} else {
						throw new UnsupportedOperationException("Only useLike=true is currently supported");
					}
				}
				
				index++;
			}
		}
		
		return buffer.toString();
	}
	
	public void setParameterNameMap(Map<String, String> parameterNameMap) {
		this.parameterNameMap = parameterNameMap;
	}
	
	public void setWildcardChar(char wildcardChar) {
		this.wildcardChar = wildcardChar;
	}
	
	public void setUseLike(boolean useLike) {
		this.useLike = useLike;
	}
}