package com.janoside.filter;

import java.util.Date;
import java.util.List;
import java.util.SortedMap;

import com.janoside.util.DateUtil;
import com.janoside.util.TimeSource;
import com.janoside.util.TimeSourceAware;
import com.janoside.util.Tuple;

/**
 * Util class that uses a SearchStringParser internally and simplifies use of common parameters.
 */
public class UtilSearchStringParser implements TimeSourceAware {
	
	private SearchStringParser searchStringParser;
	
	private TimeSource timeSource;
	
	public UtilSearchStringParser() {
		this.searchStringParser = new SearchStringParser();
	}
	
	public Tuple<Date, Date> getStartEndDates(String filter) {
		SortedMap<String, List<String>> parameterMap = this.searchStringParser.parse(filter);
		
		Date start = new Date(this.timeSource.getCurrentTime() - DateUtil.HourMillis);
		Date end = new Date(this.timeSource.getCurrentTime());
		
		if (parameterMap.containsKey("since")) {
			String startString = parameterMap.remove("since").get(0);
			
			start = DateUtil.parseAsDateOrRelativeTimeSpan(startString, this.timeSource);
		}
		
		if (parameterMap.containsKey("start")) {
			String startString = parameterMap.remove("start").get(0);
			
			start = DateUtil.parseAsDateOrRelativeTimeSpan(startString, this.timeSource);
		}
		
		if (parameterMap.containsKey("end")) {
			String endString = parameterMap.remove("end").get(0);
			
			end = DateUtil.parseAsDateOrRelativeTimeSpan(endString, this.timeSource);
		}
		
		return new Tuple<Date, Date>(start, end);
	}
	
	public void setTimeSource(TimeSource timeSource) {
		this.timeSource = timeSource;
	}
}