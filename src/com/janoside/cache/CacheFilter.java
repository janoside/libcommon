package com.janoside.cache;

public interface CacheFilter<T> {
	
	boolean isValidEntry(ObjectCache<T> cache, String key, T value);
}