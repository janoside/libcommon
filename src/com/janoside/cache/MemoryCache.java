package com.janoside.cache;

import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import com.janoside.storage.MemoryKeyValueStore;

public class MemoryCache<T> implements ObjectCache<T> {
	
	private MemoryKeyValueStore<ExpiringWrapper<T>> store;
	
	private Timer timer;
	
	public MemoryCache() {
		this.store = new MemoryKeyValueStore<ExpiringWrapper<T>>();
		this.timer = new Timer("MemoryCache-CleanupTimer-" + Integer.toHexString(this.hashCode()));
	}
	
	public void put(final String key, T value, long lifetime) {
		ExpiringWrapper<T> expiringWrapper = new ExpiringWrapper<T>();
		expiringWrapper.setValue(value);
		expiringWrapper.setExpiration(System.currentTimeMillis() + lifetime);
		
		this.timer.schedule(new TimerTask() {
			public void run() {
				// check that it's expired, remove if it is
				get(key);
			}
		}, new Date(System.currentTimeMillis() + lifetime + 1));
		
		this.store.put(key, expiringWrapper);
	}
	
	public void put(String key, T value) {
		ExpiringWrapper<T> expiringWrapper = new ExpiringWrapper<T>();
		expiringWrapper.setValue(value);
		expiringWrapper.setExpiration(Long.MAX_VALUE);
		
		this.store.put(key, expiringWrapper);
	}
	
	public T get(String key) {
		ExpiringWrapper<T> expiringWrapper = this.store.get(key);
		
		if (expiringWrapper != null) {
			if (System.currentTimeMillis() < expiringWrapper.getExpiration()) {
				return expiringWrapper.getValue();
				
			} else {
				this.remove(key);
			}
		}
		
		return null;
	}
	
	public void remove(String key) {
		this.store.remove(key);
	}
	
	public void clear() {
		this.store.clear();
	}
	
	public int getSize() {
		Set<String> keys = this.store.getKeys();
		
		int count = 0;
		for (String key : keys) {
			if (this.get(key) != null) {
				count++;
			}
		}
		
		return count;
	}
	
	public void setValues(Map<String, T> values) {
		for (Map.Entry<String, T> entry : values.entrySet()) {
			this.put(entry.getKey(), entry.getValue());
		}
	}
}