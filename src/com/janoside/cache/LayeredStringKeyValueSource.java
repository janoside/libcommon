package com.janoside.cache;

import java.util.ArrayList;
import java.util.List;

import com.janoside.keyvalue.KeyValueSource;

/**
 * KeyValueSource<String> implementation that interatively transforms the
 * input key using each of its internal KeyValueSource<String> objects.
 * Yes, order matters.
 */
public class LayeredStringKeyValueSource implements CachingKeyValueSource<String> {
	
	private List<KeyValueSource<String>> internalKeyValueSources;
	
	public String get(String key) {
		String value = key;
		
		for (KeyValueSource<String> internalKeyValueSource : this.internalKeyValueSources) {
			value = internalKeyValueSource.get(value);
			
			if (value == null) {
				return null;
			}
		}
		
		return value;
	}
	
	public String getNoCache(String key) {
		String value = key;
		
		for (KeyValueSource<String> internalKeyValueSource : this.internalKeyValueSources) {
			if (internalKeyValueSource instanceof CachingKeyValueSource<?>) {
				value = ((CachingKeyValueSource<String>) internalKeyValueSource).getNoCache(value);
				
			} else {
				value = internalKeyValueSource.get(value);
			}
			
			if (value == null) {
				return null;
			}
		}
		
		return value;
	}
	
	public String getOnlyCache(String key) {
		String value = key;
		
		for (KeyValueSource<String> internalKeyValueSource : this.internalKeyValueSources) {
			if (internalKeyValueSource instanceof CachingKeyValueSource<?>) {
				value = ((CachingKeyValueSource<String>) internalKeyValueSource).getOnlyCache(value);
				
			} else {
				value = internalKeyValueSource.get(value);
			}
			
			if (value == null) {
				return null;
			}
		}
		
		return value;
	}
	
	public void setInternalKeyValueSources(List<KeyValueSource<String>> internalKeyValueSources) {
		this.internalKeyValueSources = new ArrayList<KeyValueSource<String>>(internalKeyValueSources);
	}
}