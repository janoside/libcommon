package com.janoside.cache;

import com.janoside.keyvalue.KeyValueSource;

public interface CachingKeyValueSource<T> extends KeyValueSource<T> {
	
	T getOnlyCache(String key);
	
	T getNoCache(String key);
}