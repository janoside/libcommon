package com.janoside.cache;

import java.util.Collections;
import java.util.Map;

import org.apache.commons.collections.map.LRUMap;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.util.SystemTimeSource;
import com.janoside.util.TimeSource;
import com.janoside.util.TimeSourceAware;

@ManagedResource(objectName = "Janoside:name=LruMemoryCache")
public class LruMemoryCache<T> implements ObjectCache<T>, TimeSourceAware {
	
	private Map<String, ExpiringWrapper<T>> lruMap;
	
	private TimeSource timeSource;
	
	public LruMemoryCache() {
		this.timeSource = new SystemTimeSource();
		
		this.setMaxSize(1000);
	}
	
	public void put(String key, T value, long lifetime) {
		ExpiringWrapper<T> wrapper = new ExpiringWrapper<T>();
		wrapper.setValue(value);
		wrapper.setExpiration(this.timeSource.getCurrentTime() + lifetime);
		
		this.lruMap.put(key, wrapper);
	}
	
	public void put(String key, T value) {
		ExpiringWrapper<T> wrapper = new ExpiringWrapper<T>();
		wrapper.setValue(value);
		wrapper.setExpiration(Long.MAX_VALUE);
		
		this.lruMap.put(key, wrapper);
	}
	
	public T get(String key) {
		ExpiringWrapper<T> expiringWrapper = this.lruMap.get(key);
		
		if (expiringWrapper != null) {
			if (this.timeSource.getCurrentTime() < expiringWrapper.getExpiration()) {
				return expiringWrapper.getValue();
				
			} else {
				this.remove(key);
			}
		}
		
		return null;
	}
	
	public void remove(String key) {
		this.lruMap.remove(key);
	}
	
	public void clear() {
		this.lruMap.clear();
	}
	
	@ManagedAttribute
	public int getSize() {
		return this.lruMap.size();
	}
	
	public boolean contains(String key) {
		return this.lruMap.containsKey(key);
	}
	
	public void setTimeSource(TimeSource timeSource) {
		this.timeSource = timeSource;
	}
	
	public void setMaxSize(int maxSize) {
		LRUMap internalLruMap = new LRUMap(maxSize);
		
		this.lruMap = Collections.synchronizedMap(internalLruMap);
	}
}