package com.janoside.cache;

public interface CacheEventObserver<T> {
	
	void onPut(ObjectCache<T> cache, String key, T value);

	void onGetHit(ObjectCache<T> cache, String key, T value);
	
	void onGetMiss(ObjectCache<T> cache, String key);
	
	void onRemove(ObjectCache<T> cache, String key);
	
	void onClear(ObjectCache<T> cache);
}