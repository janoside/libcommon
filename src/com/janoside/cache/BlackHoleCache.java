package com.janoside.cache;

public class BlackHoleCache<T> implements ObjectCache<T> {
	
	public void put(String key, T value) {
	}
	
	public void put(String key, T object, long lifetime) {
	}
	
	public T get(String key) {
		return null;
	}
	
	public void remove(String key) {
	}
	
	public void clear() {
	}
	
	public int getSize() {
		return 0;
	}
}