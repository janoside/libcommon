package com.janoside.cache;

import java.util.ArrayList;
import java.util.List;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.keyvalue.KeyValueSource;

public class ManagedKeyValueSource<T> implements KeyValueSource<T>, ExceptionHandlerAware {
	
	private KeyValueSource<T> internalKeyValueSource;
	
	private List<KeyValueSourceEventObserver<T>> keyValueSourceEventObservers;
	
	private ExceptionHandler exceptionHandler;
	
	public ManagedKeyValueSource() {
		this.keyValueSourceEventObservers = new ArrayList<KeyValueSourceEventObserver<T>>();
	}
	
	public T get(String key) {
		for (KeyValueSourceEventObserver<T> observer : this.keyValueSourceEventObservers) {
			try {
				observer.onKeyValueRequested(this.internalKeyValueSource, key);
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
		}
		
		return this.internalKeyValueSource.get(key);
	}
	
	public void setInternalKeyValueSource(KeyValueSource<T> internalKeyValueSource) {
		this.internalKeyValueSource = internalKeyValueSource;
	}
	
	public void setKeyValueSourceEventObservers(List<KeyValueSourceEventObserver<T>> keyValueSourceEventObservers) {
		this.keyValueSourceEventObservers = keyValueSourceEventObservers;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
}