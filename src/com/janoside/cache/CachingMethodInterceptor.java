package com.janoside.cache;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import org.aopalliance.intercept.MethodInvocation;
import org.springframework.util.StringUtils;

import com.janoside.aop.FilteringMethodInterceptor;
import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;

public class CachingMethodInterceptor extends FilteringMethodInterceptor implements ExceptionHandlerAware {
	
	private ObjectCache cache;
	
	private ExceptionHandler exceptionHandler;
	
	public CachingMethodInterceptor() {
		this.cache = new LruMemoryCache();
		((LruMemoryCache) this.cache).setMaxSize(1000);
	}
	
	protected Object invokeInternal(MethodInvocation invocation) throws Throwable {
		Method method = invocation.getMethod();
		Object[] args = invocation.getArguments();
		
		// check if we should cache this call
		if (!this.shouldCache(method, args)) {
			// no caching, just pass through
			return invocation.proceed();
		}
		
		// caching logic
		String cacheKey = this.getCacheKey(method, args);
		long cacheTime = this.getCacheKeyLifespan(cacheKey, method, args);
		
		Object value = null;
		try {
			value = this.cache.get(cacheKey);
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
		}
		
		if (value != null) {
			this.cacheHit(cacheKey, method, args);
			
			return value;
			
		} else {
			this.cacheMiss(cacheKey, method, args);
			
			value = invocation.proceed();
			
			if (value != null) {
				try {
					if (cacheTime > 0) {
						this.cache.put(cacheKey, value, cacheTime);
						
					} else {
						this.cache.put(cacheKey, value);
					}
				} catch (Throwable t) {
					this.exceptionHandler.handleException(t);
				}
			}
			
			return value;
		}
	}
	
	protected Object getValueWithoutCache(Object targetObject, Method method, Object[] args) {
		try {
			return method.invoke(targetObject, args);
			
		} catch (IllegalAccessException iae) {
			throw new RuntimeException(iae);
			
		} catch (InvocationTargetException ite) {
			throw new RuntimeException(ite);
		}
	}
	
	protected String getCacheKey(Method method, Object[] args) {
		ArrayList<String> keyComponents = new ArrayList<String>();
		for (Object arg : args) {
			keyComponents.add(this.getCacheKeyComponent(method, arg));
		}
		
		return String.format(
				"%s-%d-%s",
				method.getName(),
				method.toGenericString().hashCode(),
				StringUtils.collectionToDelimitedString(keyComponents, "-"));
	}
	
	protected String getCacheKeyComponent(Method method, Object arg) {
		return String.valueOf(arg);
	}
	
	protected long getCacheKeyLifespan(String cacheKey, Method method, Object[] args) {
		return 0;
	}
	
	protected boolean shouldCache(Method method, Object[] args) {
		return false;
	}
	
	protected void cacheHit(String cacheKey, Method method, Object[] args) {
	}
	
	protected void cacheMiss(String cacheKey, Method method, Object[] args) {
	}
	
	protected final void removeCacheKey(String cacheKey) {
		this.cache.remove(cacheKey);
	}
	
	public void setCache(ObjectCache cache) {
		this.cache = cache;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
}