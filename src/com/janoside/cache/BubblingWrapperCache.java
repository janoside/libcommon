package com.janoside.cache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

@ManagedResource(objectName = "Janoside:name=BubblingWrapperCache")
public class BubblingWrapperCache<T> implements ObjectCache<T> {
	
	private ObjectCache<T> internalCache;
	
	private Map<String, ManagedObjectCacheKeyAccess> lastAccessesByKey;
	
	private SortedSet<ManagedObjectCacheKeyAccess> keyLastAccesses;
	
	private Map<String, ManagedObjectCacheKeyExpiration> expirationsByKey;
	
	private SortedSet<ManagedObjectCacheKeyExpiration> keyExpirations;
	
	private int maxSize;
	
	public BubblingWrapperCache() {
		this.lastAccessesByKey = new ConcurrentHashMap<String, ManagedObjectCacheKeyAccess>();
		this.keyLastAccesses = Collections.synchronizedSortedSet(new TreeSet<ManagedObjectCacheKeyAccess>());
		this.expirationsByKey = new ConcurrentHashMap<String, ManagedObjectCacheKeyExpiration>();
		this.keyExpirations = Collections.synchronizedSortedSet(new TreeSet<ManagedObjectCacheKeyExpiration>());
		
		this.maxSize = Integer.MAX_VALUE;
	}
	
	public void put(String key, T value, long lifetime) {
		this.internalCache.put(key, value, lifetime);
		
		ManagedObjectCacheKeyExpiration keyExpiration = null;
		
		if (this.expirationsByKey.containsKey(key)) {
			keyExpiration = this.expirationsByKey.get(key);
			
			this.keyExpirations.remove(keyExpiration);
			
			keyExpiration.setExpiration(System.currentTimeMillis() + lifetime);
			
			this.keyExpirations.add(keyExpiration);
			
		} else {
			keyExpiration = new ManagedObjectCacheKeyExpiration(key, System.currentTimeMillis() + lifetime);
			
			this.expirationsByKey.put(key, keyExpiration);
			this.keyExpirations.add(keyExpiration);
		}
		
		this.touch(key);
		this.trimToSize();
	}
	
	public void put(String key, T value) {
		this.internalCache.put(key, value);
		
		this.touch(key);
		this.trimToSize();
	}
	
	@ManagedOperation
	public T get(String key) {
		T object = this.internalCache.get(key);
		
		if (object != null) {
			if (this.expirationsByKey.containsKey(key)) {
				if (this.expirationsByKey.get(key).getExpiration() > System.currentTimeMillis()) {
					this.touch(key);
					
				} else {
					object = null;
				}
			} else {
				this.touch(key);
			}
		}
		
		return object;
	}
	
	@ManagedOperation
	public void remove(String key) {
		this.internalCache.remove(key);
		
		ManagedObjectCacheKeyAccess keyAccess = null;
		if (this.lastAccessesByKey.containsKey(key)) {
			keyAccess = this.lastAccessesByKey.remove(key);
			
			this.keyLastAccesses.remove(keyAccess);
		}
		
		ManagedObjectCacheKeyExpiration keyExpiration = null;
		if (this.expirationsByKey.containsKey(key)) {
			keyExpiration = this.expirationsByKey.remove(key);
			
			this.keyExpirations.remove(keyExpiration);
		}
	}
	
	@ManagedOperation
	public void clear() {
		this.internalCache.clear();
		this.lastAccessesByKey.clear();
		this.keyLastAccesses.clear();
		this.expirationsByKey.clear();
		this.keyExpirations.clear();
	}
	
	private void touch(String key) {
		ManagedObjectCacheKeyAccess keyAccess = null;
		
		if (this.lastAccessesByKey.containsKey(key)) {
			keyAccess = this.lastAccessesByKey.get(key);
			
			this.keyLastAccesses.remove(keyAccess);
			
			keyAccess.setLastAccess(System.currentTimeMillis());
			
			this.keyLastAccesses.add(keyAccess);
			
		} else {
			keyAccess = new ManagedObjectCacheKeyAccess(key, System.currentTimeMillis());
			
			this.lastAccessesByKey.put(key, keyAccess);
			this.keyLastAccesses.add(keyAccess);
		}
	}
	
	@ManagedOperation
	public void trimToSize() {
		while (this.internalCache.getSize() > this.maxSize) {
			ManagedObjectCacheKeyAccess keyAccess = this.keyLastAccesses.last();
			
			this.keyLastAccesses.remove(keyAccess);
			this.lastAccessesByKey.remove(keyAccess.getKey());
			this.internalCache.remove(keyAccess.getKey());
		}
	}
	
	@ManagedOperation
	public void removeExpiredEntries() {
		while (!this.keyExpirations.isEmpty() && this.keyExpirations.last().getExpiration() <= System.currentTimeMillis()) {
			ManagedObjectCacheKeyExpiration keyExpiration = this.keyExpirations.last();
			
			this.keyExpirations.remove(keyExpiration);
			this.expirationsByKey.remove(keyExpiration.getKey());
			
			ManagedObjectCacheKeyAccess keyAccess = this.lastAccessesByKey.get(keyExpiration.getKey());

			this.keyLastAccesses.remove(keyAccess);
			this.lastAccessesByKey.remove(keyAccess.getKey());
			
			this.internalCache.remove(keyExpiration.getKey());
		}
	}
	
	@ManagedOperation
	public ArrayList<String> viewKeys() {
		ArrayList<String> keyList = new ArrayList<String>(this.lastAccessesByKey.keySet());
		Collections.sort(keyList);
		
		return keyList;
	}
	
	@ManagedOperation
	public ArrayList<String> viewValues() {
		ArrayList<String> keyList = new ArrayList<String>(this.lastAccessesByKey.keySet());
		Collections.sort(keyList);
		
		ArrayList<String> keyValueList = new ArrayList<String>(keyList.size());
		for (String key : keyList) {
			keyValueList.add(key + "=" + this.internalCache.get(key));
		}
		
		return keyValueList;
	}
	
	public void setInternalCache(ObjectCache<T> internalCache) {
		this.internalCache = internalCache;
	}
	
	@ManagedAttribute
	public int getSize() {
		return this.internalCache.getSize();
	}
	
	@ManagedAttribute
	public int getMaxSize() {
		return this.maxSize;
	}
	
	@ManagedAttribute
	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
		
		this.trimToSize();
	}
	
	private class ManagedObjectCacheKeyAccess implements Comparable<ManagedObjectCacheKeyAccess> {

		private String key;
		
		private long lastAccess;
		
		public ManagedObjectCacheKeyAccess(String key, long lastAccess) {
			this.key = key;
			this.lastAccess = lastAccess;
		}
		
		public int compareTo(ManagedObjectCacheKeyAccess mocka) {
			if (mocka.getLastAccess() != this.lastAccess) {
				return (int) (mocka.getLastAccess() - this.lastAccess);
				
			} else {
				return mocka.getKey().compareTo(this.key);
			}
		}
		
		public String getKey() {
			return this.key;
		}
		
		public long getLastAccess() {
			return this.lastAccess;
		}
		
		public void setLastAccess(long lastAccess) {
			this.lastAccess = lastAccess;
		}
		
		@Override
		public boolean equals(Object object) {
			if (object == null || object.getClass() != this.getClass()) {
				return false;
				
			} else if (object == this) {
				return true;
				
			} else {
				return this.key.equals(((ManagedObjectCacheKeyAccess) object).getKey());
			}
		}
		
		@Override
		public int hashCode() {
			return this.key.hashCode();
		}
		
		@Override
		public String toString() {
			return String.format("(%s, %s, %s)", this.hashCode(), this.key, (System.currentTimeMillis() - this.lastAccess)); 
		}
	}
	
	private class ManagedObjectCacheKeyExpiration implements Comparable<ManagedObjectCacheKeyExpiration> {
		
		private String key;
		
		private long expiration;
		
		public ManagedObjectCacheKeyExpiration(String key, long expiration) {
			this.key = key;
			this.expiration = expiration;
		}
		
		public int compareTo(ManagedObjectCacheKeyExpiration mocke) {
			if (mocke.getExpiration() != this.expiration) {
				return (int) (mocke.getExpiration() - this.expiration);
				
			} else {
				return mocke.getKey().compareTo(this.key);
			}
		}
		
		public String getKey() {
			return this.key;
		}
		
		public long getExpiration() {
			return this.expiration;
		}
		
		public void setExpiration(long expiration) {
			this.expiration = expiration;
		}
		
		@Override
		public boolean equals(Object object) {
			if (object == null || object.getClass() != this.getClass()) {
				return false;
				
			} else if (object == this) {
				return true;
				
			} else {
				return this.key.equals(((ManagedObjectCacheKeyExpiration) object).getKey());
			}
		}
		
		@Override
		public int hashCode() {
			return this.key.hashCode();
		}
		
		@Override
		public String toString() {
			return String.format("(%s, %s, %s)", this.hashCode(), this.key, (this.expiration - System.currentTimeMillis())); 
		}
	}
}