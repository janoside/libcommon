package com.janoside.cache;

public interface CacheValueChangeObserver<T> {
	
	void onCacheValueChanged(ObjectCache<T> cache, String key, T oldValue, T newValue);
}