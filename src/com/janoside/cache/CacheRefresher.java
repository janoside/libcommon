package com.janoside.cache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.keyvalue.KeyValueSource;
import com.janoside.stats.EventTracker;
import com.janoside.thread.ManagedThread;

/**
 * Thread that periodically "refreshes" a target <code>ObjectCache</code> using
 * a specified <code>KeyValueSource</code>. <code>CacheRefresher</code>s can
 * be used in the following ways:
 * 
 * 1. set as a <code>CacheEventObserver</code> on their target cache, in this case
 * all keys in the target cache will be automatically refreshed
 * 2. set as a <code>KeyValueSourceEventObserver</code> on a <code>KeyValueSource</code> of interest
 * 3. manually given keys which should be refreshed, via <code>addWatchedKey(String)</code>
 * and <code>removeWatchedKey(String)</code>
 * 4. any combination thereof
 *
 * @param <T> - the content type of the target cache
 */
@ManagedResource(objectName = "Janoside:name=CacheRefresher")
public class CacheRefresher<T> extends ManagedThread implements CacheEventObserver<T>, KeyValueSourceEventObserver<T> {
	
	private ObjectCache<T> cache;
	
	private KeyValueSource<T> keyValueSource;
	
	private Set<String> keys;
	
	private EventTracker refreshTracker;
	
	private EventTracker refreshFailureTracker;
	
	public CacheRefresher() {
		this.keys = Collections.synchronizedSet(new HashSet<String>());
		
		this.refreshTracker = new EventTracker();
		this.refreshFailureTracker = new EventTracker();
	}
	
	protected void runInternal() {
		ArrayList<String> keysToBeRemoved = new ArrayList<String>();
		
		synchronized (this.keys) {
			for (String key : this.keys) {
				try {
					T value = this.keyValueSource.get(key);
					
					if (value == null) {
						keysToBeRemoved.add(key);
						
					} else {
						this.cache.put(key, value);
						
						this.refreshTracker.increment();
					}
				} catch (Throwable t) {
					this.exceptionHandler.handleException(t);
					
					this.refreshFailureTracker.increment();
				}
			}
		}
		
		for (String keyToBeRemoved : keysToBeRemoved) {
			this.cache.remove(keyToBeRemoved);
		}
	}
	
	public void onPut(ObjectCache<T> cache, String key, T value) {
		this.keys.add(key);
	}
	
	public void onGetHit(ObjectCache<T> cache, String key, T value) {
		this.keys.add(key);
	}

	public void onGetMiss(ObjectCache<T> cache, String key) {
		this.keys.remove(key);
	}
	
	public void onRemove(ObjectCache<T> cache, String key) {
	}
	
	public void onClear(ObjectCache<T> cache) {
	}
	
	public void onKeyValueRequested(KeyValueSource<T> keyValueSource, String key) {
		this.keys.add(key);
	}
	
	public void addWatchedKey(String key) {
		this.keys.add(key);
	}
	
	public void removeWatchedKey(String key) {
		this.keys.remove(key);
	}
	
	public void setCache(ObjectCache<T> cache) {
		this.cache = cache;
	}
	
	public void setKeyValueSource(KeyValueSource<T> keyValueSource) {
		this.keyValueSource = keyValueSource;
	}
	
	@ManagedOperation
	public List<String> viewKeys() {
		ArrayList<String> keyList = new ArrayList<String>(this.keys);
		
		Collections.sort(keyList);
		
		return keyList;
	}
	
	@ManagedAttribute
	public long getRefreshCount() {
		return this.refreshTracker.getCount();
	}
	
	@ManagedAttribute
	public long getRefreshFailureCount() {
		return this.refreshFailureTracker.getCount();
	}
	
	@ManagedAttribute
	public int getSecondsSinceRefresh() {
		return this.refreshTracker.getSecondsSinceLastOccurrence();
	}
	
	@ManagedAttribute
	public int getSecondsSinceRefreshFailure() {
		return this.refreshFailureTracker.getSecondsSinceLastOccurrence();
	}
}