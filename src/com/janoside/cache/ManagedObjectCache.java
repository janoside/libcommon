package com.janoside.cache;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.exception.StandardErrorExceptionHandler;
import com.janoside.health.ErrorTracker;
import com.janoside.health.EventTrackerHealthMonitor;
import com.janoside.health.HealthMonitor;
import com.janoside.health.SuccessTrackerHealthMonitor;
import com.janoside.stats.BlackHoleStatTracker;
import com.janoside.stats.EventTracker;
import com.janoside.stats.StatTracker;
import com.janoside.stats.StatTrackerAware;
import com.janoside.stats.SuccessTracker;
import com.janoside.text.TextTransformer;
import com.janoside.text.UnchangingTextTransformer;

@ManagedResource(objectName = "Janoside:name=ManagedObjectCache")
public class ManagedObjectCache<T> implements ObjectCache<T>, ExceptionHandlerAware, HealthMonitor, StatTrackerAware {
	
	private static final Logger logger = LoggerFactory.getLogger(ManagedObjectCache.class);
	
	private ObjectCache<T> internalCache;
	
	private List<CacheFilter<T>> filters;
	
	private List<CacheEventObserver<T>> cacheEventObservers;
	
	private TextTransformer keyTransformer;
	
	private SuccessTracker successTracker;
	
	private SuccessTracker hitRateTracker;
	
	private EventTracker failureTracker;
	
	private EventTrackerHealthMonitor failureTrackerHealthMonitor;
	
	private SuccessTrackerHealthMonitor successTrackerHealthMonitor;
	
	private ExceptionHandler exceptionHandler;
	
	private StatTracker statTracker;
	
	private String name;
	
	public ManagedObjectCache() {
		this.filters = new ArrayList<CacheFilter<T>>();
		this.cacheEventObservers = new ArrayList<CacheEventObserver<T>>();
		this.keyTransformer = new UnchangingTextTransformer();
		this.successTracker = new SuccessTracker();
		this.hitRateTracker = new SuccessTracker();
		this.failureTracker = new EventTracker();
		this.exceptionHandler = new StandardErrorExceptionHandler();
		this.statTracker = new BlackHoleStatTracker();
		
		this.failureTrackerHealthMonitor = new EventTrackerHealthMonitor();
		this.failureTrackerHealthMonitor.setEventTracker(this.failureTracker);
		this.failureTrackerHealthMonitor.setErrorThreshold(100);
		
		this.successTrackerHealthMonitor = new SuccessTrackerHealthMonitor();
		this.successTrackerHealthMonitor.setSuccessTracker(this.successTracker);
		this.successTrackerHealthMonitor.setErrorThreshold(0.75f);
	}
	
	public void reportInternalErrors(ErrorTracker errorTracker) {
		this.failureTrackerHealthMonitor.reportInternalErrors(errorTracker);
		this.successTrackerHealthMonitor.reportInternalErrors(errorTracker);
	}
	
	public void put(String key, T value) {
		this.statTracker.trackThreadPerformanceStart("cache." + this.name + ".put");
		
		try {
			for (CacheFilter<T> filter : this.filters) {
				if (!filter.isValidEntry(this, key, value)) {
					return;
				}
			}
			
			String normalizedKey = this.keyTransformer.transform(key);
			
			this.internalCache.put(normalizedKey, value);
			
			for (CacheEventObserver<T> observer : this.cacheEventObservers) {
				try {
					observer.onPut(this, normalizedKey, value);
					
				} catch (Throwable t) {
					this.exceptionHandler.handleException(t);
				}
			}
			
			this.successTracker.success();
			
		} catch (Throwable t) {
			this.successTracker.failure();
			this.failureTracker.increment();
			
			logger.warn("cache." + this.name + ") failed PUT(key, value), key=" + key);
			
			throw new RuntimeException("cache." + this.name + ") failed PUT(key, value)", t);
			
		} finally {
			this.statTracker.trackThreadPerformanceEnd("cache." + this.name + ".put");
		}
	}
	
	public void put(String key, T value, long lifetime) {
		this.statTracker.trackThreadPerformanceStart("cache." + this.name + ".put.expiration");
		
		try {
			for (CacheFilter<T> filter : this.filters) {
				if (!filter.isValidEntry(this, key, value)) {
					return;
				}
			}
			
			String normalizedKey = this.keyTransformer.transform(key);
			
			this.internalCache.put(normalizedKey, value, lifetime);
			
			for (CacheEventObserver<T> observer : this.cacheEventObservers) {
				try {
					observer.onPut(this, normalizedKey, value);
					
				} catch (Throwable t) {
					this.exceptionHandler.handleException(t);
				}
			}
			
			this.successTracker.success();
			
		} catch (Throwable t) {
			this.successTracker.failure();
			this.failureTracker.increment();
			
			logger.warn("cache." + this.name + ") failed PUT(key, value, lifetime), key=" + key);
			
			throw new RuntimeException("cache." + this.name + ") failed PUT(key, value, lifetime)", t);
			
		} finally {
			this.statTracker.trackThreadPerformanceEnd("cache." + this.name + ".put.expiration");
		}
	}
	
	@ManagedOperation
	public T get(String key) {
		long startTime = System.currentTimeMillis();
		this.statTracker.trackThreadPerformanceStart("cache." + this.name + ".get");
		
		boolean cacheHit = false;
		
		try {
			String normalizedKey = this.keyTransformer.transform(key);
			
			T object = this.internalCache.get(normalizedKey);
			
			if (object != null) {
				for (CacheEventObserver<T> observer : this.cacheEventObservers) {
					try {
						observer.onGetHit(this, normalizedKey, object);
						
					} catch (Throwable t) {
						this.exceptionHandler.handleException(t);
					}
				}
				
				cacheHit = true;
				
			} else {
				for (CacheEventObserver<T> observer : this.cacheEventObservers) {
					try {
						observer.onGetMiss(this, normalizedKey);
						
					} catch (Throwable t) {
						this.exceptionHandler.handleException(t);
					}
				}
			}
			
			if (cacheHit) {
				this.hitRateTracker.success();
				this.statTracker.trackPerformance("cache." + this.name + ".get_hit", System.currentTimeMillis() - startTime);
				
			} else {
				this.hitRateTracker.failure();
				this.statTracker.trackPerformance("cache." + this.name + ".get_miss", System.currentTimeMillis() - startTime);
			}

			this.successTracker.success();
			
			return object;
			
		} catch (Throwable t) {
			this.successTracker.failure();
			this.failureTracker.increment();
			
			logger.warn("cache." + this.name + ") failed GET(key), key=" + key);
			
			throw new RuntimeException("cache." + this.name + ") failed GET(key)", t);
			
		} finally {
			this.statTracker.trackThreadPerformanceEnd("cache." + this.name + ".get");
		}
	}
	
	@ManagedOperation
	public void remove(String key) {
		this.statTracker.trackThreadPerformanceStart("cache." + this.name + ".remove");
		
		try {
			String normalizedKey = this.keyTransformer.transform(key);
			
			this.internalCache.remove(normalizedKey);
			
			for (CacheEventObserver<T> observer : this.cacheEventObservers) {
				try {
					observer.onRemove(this, normalizedKey);
					
				} catch (Throwable t) {
					this.exceptionHandler.handleException(t);
				}
			}
			
			this.successTracker.success();
			
		} catch (Throwable t) {
			this.successTracker.failure();
			this.failureTracker.increment();
			
			logger.warn("cache." + this.name + ") failed REMOVE(key), key=" + key);
			
			throw new RuntimeException("cache." + this.name + ") failed REMOVE(key)", t);
			
		} finally {
			this.statTracker.trackThreadPerformanceEnd("cache." + this.name + ".remove");
		}
	}
	
	@ManagedOperation
	public void clear() {
		this.internalCache.clear();
		
		for (CacheEventObserver<T> observer : this.cacheEventObservers) {
			try {
				observer.onClear(this);
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
		}
	}
	
	@ManagedOperation
	public void clearStats() {
		this.hitRateTracker.reset();
	}
	
	public void setInternalCache(ObjectCache<T> internalCache) {
		this.internalCache = internalCache;
	}
	
	public void setKeyTransformer(TextTransformer keyTransformer) {
		this.keyTransformer = keyTransformer;
	}
	
	public void setCacheFilters(List<CacheFilter<T>> filters) {
		this.filters = new ArrayList<CacheFilter<T>>(filters);
	}
	
	public void setCacheEventObservers(List<CacheEventObserver<T>> cacheEventObservers) {
		this.cacheEventObservers = cacheEventObservers;
	}
	
	public void setSuccessTracker(SuccessTracker successTracker) {
		this.successTracker = successTracker;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}

	public void setStatTracker(StatTracker statTracker) {
		this.statTracker = statTracker;
	}
	
	@ManagedAttribute
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
		this.failureTracker.setName("FailureTracker(ManagedObjectCache(" + this.name + "))");
		this.successTracker.setName("SuccessTracker(ManagedObjectCache(" + this.name + "))");
		this.hitRateTracker.setName("HitRateTracker(ManagedObjectCache(" + this.name + "))");
	}
	
	public void setSuccessRateErrorThreshold(float successRateErrorThreshold) {
		this.successTrackerHealthMonitor.setErrorThreshold(successRateErrorThreshold);
	}
	
	@ManagedAttribute
	public float getCacheHitRatio() {
		return this.hitRateTracker.getSuccessRate();
	}
	
	@ManagedAttribute
	public float getSuccessRate() {
		return this.successTracker.getSuccessRate();
	}
	
	@ManagedAttribute
	public int getSize() {
		return this.internalCache.getSize();
	}
	
	@ManagedAttribute
	public long getHitCount() {
		return this.hitRateTracker.getSuccessCount();
	}
	
	@ManagedAttribute
	public long getMissCount() {
		return this.hitRateTracker.getFailureCount();
	}
	
	public void setFailureErrorThreshold(int failureErrorThreshold) {
		this.failureTrackerHealthMonitor.setErrorThreshold(failureErrorThreshold);
	}
}