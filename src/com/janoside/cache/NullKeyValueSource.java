package com.janoside.cache;

import com.janoside.keyvalue.KeyValueSource;

public class NullKeyValueSource<T> implements KeyValueSource<T> {
	
	public T get(String key) {
		return null;
	}
}