package com.janoside.cache;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ExpiringWrapper<T> implements Serializable {
	
	private T value;
	
	private long expiration;
	
	public T getValue() {
		return this.value;
	}
	
	public void setValue(T value) {
		this.value = value;
	}
	
	public long getExpiration() {
		return this.expiration;
	}
	
	public void setExpiration(long expiration) {
		this.expiration = expiration;
	}
}