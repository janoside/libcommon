package com.janoside.queue;

@SuppressWarnings("serial")
public class PhysicsException extends RuntimeException {
	
	public PhysicsException(String message) {
		super(message);
	}
}