package com.janoside.queue;

import org.springframework.jmx.export.annotation.ManagedResource;

@ManagedResource(objectName = "Janoside:name=ManagedMemoryQueue")
public class ManagedMemoryQueue<T> extends ManagedObjectQueue<T> {
	
	public ManagedMemoryQueue() {
		this.setInternalQueue(new MemoryQueue<T>());
	}
}