package com.janoside.queue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MemoryQueue<T> implements ObjectQueue<T> {
	
	private List<T> objects;
	
	public MemoryQueue() {
		this.objects = Collections.synchronizedList(new ArrayList<T>());
	}
	
	public void enqueue(T object) {
		this.objects.add(object);
	}
	
	public List<T> dequeue(int count) {
		if (count <= 0) {
			return new ArrayList<T>();
		}
		
		ArrayList<T> returnList = new ArrayList<T>(count);
			
		int index = 0;
		while (index < count && this.objects.size() > 0) {
			T object = this.objects.remove(0);
			
			returnList.add(object);
			
			index++;
		}
		
		return returnList;
	}
	
	public void clear() {
		this.objects.clear();
	}
	
	public int getSize() {
		return this.objects.size();
	}
}