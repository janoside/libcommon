package com.janoside.queue;

import org.springframework.jmx.export.annotation.ManagedResource;

@ManagedResource(objectName = "Janoside:name=MonitoredObjectQueue")
public class MonitoredObjectQueue<T> extends ManagedObjectQueue<T> {
}