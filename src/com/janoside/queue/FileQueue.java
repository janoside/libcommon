package com.janoside.queue;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.json.JsonObject;
import com.janoside.util.FileUtil;

public class FileQueue<T extends Serializable> implements ObjectQueue<T>, ExceptionHandlerAware {
	
	private ExecutorService executorService;
	
	private ExceptionHandler exceptionHandler;
	
	private JsonObject manifest;
	
	private String directory;
	
	public FileQueue() {
		this.executorService = Executors.newFixedThreadPool(5);
	}
	
	public void enqueue(T object) {
		FileUtil.writeObjectToFile(object, this.directory + Long.toString(this.getEnqueueIndex()));
		
		synchronized (this.manifest) {
			this.manifest.put("enqueueIndex", this.manifest.getInt("enqueueIndex") + 1);
			
			this.saveManifest();
		}
	}
	
	public List<T> dequeue(int count) {
		ArrayList<T> objects = new ArrayList<T>(count);
		for (int i = 0; i < count; i++) {
			final File file = new File(this.directory + Long.toString(this.getDequeueIndex() + i));
			if (!file.exists()) {
				break;
			}
			
			objects.add((T) FileUtil.readObjectFromFile(file));
			
			this.executorService.execute(new Runnable() {
				public void run() {
					try {
						file.delete();
						
					} catch (Throwable t) {
						exceptionHandler.handleException(t);
					}
				}
			});
		}
		
		synchronized (this.manifest) {
			this.manifest.put("dequeueIndex", this.manifest.getInt("dequeueIndex") + count);
			
			this.saveManifest();
		}
		
		return objects;
	}
	
	public void clear() {
	}
	
	public int getSize() {
		return this.getEnqueueIndex() - this.getDequeueIndex();
	}
	
	private JsonObject getManifest() {
		if (this.manifest == null) {
			File manifestFile = new File(this.directory + "manifest");
			if (!manifestFile.exists()) {
				JsonObject manifest = new JsonObject();
				manifest.put("enqueueIndex", 0);
				manifest.put("dequeueIndex", 0);
				
				FileUtil.writeObjectToFile(manifest.toString(), manifestFile);
			}
			
			this.manifest = new JsonObject((String) FileUtil.readObjectFromFile(this.directory + "manifest"));
		}
		
		return this.manifest;
	}
	
	private int getEnqueueIndex() {
		return this.getManifest().getInt("enqueueIndex");
	}
	
	private int getDequeueIndex() {
		return this.getManifest().getInt("dequeueIndex");
	}
	
	private void saveManifest() {
		FileUtil.writeObjectToFile(this.manifest.toString(), this.directory + "manifest");
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setDirectory(String directory) {
		this.directory = directory;
		
		if (!this.directory.endsWith("/")) {
			this.directory = this.directory + "/";
		}
	}
}