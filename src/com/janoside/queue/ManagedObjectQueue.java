package com.janoside.queue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.health.HealthMonitor;
import com.janoside.health.ErrorTracker;
import com.janoside.stats.EventTracker;

@ManagedResource(objectName = "Janoside:name=ManagedObjectQueue")
public class ManagedObjectQueue<T> implements ObjectQueue<T>, ObjectQueueObserver<T>, HealthMonitor, ExceptionHandlerAware {
	
	private ObjectQueue<T> internalQueue;
	
	private ArrayList<ObjectQueueObserver<T>> managedObjectQueueObservers;
	
	private EventTracker enqueueTracker;
	
	private EventTracker dequeueTracker;
	
	private ExceptionHandler exceptionHandler;
	
	private String name;
	
	private long blockedObjectCount;
	
	private long expectedEnqueuePeriod;
	
	private int maxSize;
	
	private int sizeErrorThreshold;
	
	private boolean overflowing;
	
	public ManagedObjectQueue() {
		this.managedObjectQueueObservers = new ArrayList<ObjectQueueObserver<T>>();
		this.managedObjectQueueObservers.add(this);
		
		this.enqueueTracker = new EventTracker();
		this.dequeueTracker = new EventTracker();
		this.name = "time-" + System.currentTimeMillis();
		this.expectedEnqueuePeriod = Integer.MAX_VALUE;
		this.maxSize = Integer.MAX_VALUE;
		this.sizeErrorThreshold = 20000;
		this.overflowing = false;
	}
	
	public void enqueue(T object) {
		if (!this.isValidToBeEnqueued(object)) {
			this.blockedObjectCount++;
			
			return;
		}
		
		if (this.getSize() >= this.maxSize) {
			this.overflowing = true;
			
			throw new RuntimeException("ObjectQueue(" + this.name + ") max size (" + this.maxSize + ") reached, unable to add new object");
		}
		
		this.internalQueue.enqueue(object);
		
		for (ObjectQueueObserver<T> observer : this.managedObjectQueueObservers) {
			try {
				observer.onObjectAdded(this, object);
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
		}
	}
	
	public List<T> dequeue(int count) {
		List<T> returnList = this.internalQueue.dequeue(count);
		
		for (T object : returnList) {
			for (ObjectQueueObserver<T> observer : this.managedObjectQueueObservers) {
				try {
					observer.onObjectRemoved(this, object);
					
				} catch (Throwable t) {
					this.exceptionHandler.handleException(t);
				}
			}
		}
		
		return returnList;
	}
	
	@ManagedOperation
	public void clear() {
		this.internalQueue.clear();
	}
	
	public void onObjectAdded(ObjectQueue<T> objectQueue, T object) {
		this.enqueueTracker.increment();
	}
	
	public void onObjectRemoved(ObjectQueue<T> objectQueue, T object) {
		this.dequeueTracker.increment();
	}
	
	protected boolean isValidToBeEnqueued(T object) {
		return true;
	}
	
	public void reportInternalErrors(ErrorTracker errorTracker) {
		if (this.getSize() > this.sizeErrorThreshold) {
			errorTracker.trackError(this.toString() + " size > " + this.sizeErrorThreshold);
		}
		
		if (this.dequeueTracker.getSecondsSinceLastOccurrence() > 60000 && this.getSize() > 0) {
			errorTracker.trackError(this.toString() + " has stopped being consumed");
		}
		
		if (this.enqueueTracker.getSecondsSinceLastOccurrence() > (this.expectedEnqueuePeriod * 1000) && this.getSize() == 0) {
			errorTracker.trackError(this.toString() + " has stopped getting new items");
		}
		
		if (this.overflowing) {
			errorTracker.trackError(this.toString() + " is overflowing and dropping data");
			
			this.overflowing = false;
		}
	}
	
	public void setInternalQueue(ObjectQueue<T> internalQueue) {
		this.internalQueue = internalQueue;
	}
	
	public void addManagedObjectQueueObserver(ObjectQueueObserver<T> managedObjectQueueObserver) {
		this.managedObjectQueueObservers.add(managedObjectQueueObserver);
	}
	
	public void setManagedObjectQueueObservers(List<ObjectQueueObserver<T>> managedObjectQueueObservers) {
		this.managedObjectQueueObservers.addAll(managedObjectQueueObservers);
		
		// remove dups
		this.managedObjectQueueObservers = new ArrayList<ObjectQueueObserver<T>>(new HashSet<ObjectQueueObserver<T>>(this.managedObjectQueueObservers));
		
		// make sure we are first
		if (this.managedObjectQueueObservers.get(0) != this) {
			this.managedObjectQueueObservers.remove(this);
			this.managedObjectQueueObservers.add(0, this);
		}
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	@ManagedAttribute
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@ManagedAttribute
	public long getSecondsSinceEnqueue() {
		return this.enqueueTracker.getSecondsSinceLastOccurrence();
	}
	
	@ManagedAttribute
	public long getSecondsSinceDequeue() {
		return this.dequeueTracker.getSecondsSinceLastOccurrence();
	}
	
	@ManagedAttribute
	public long getQueuedObjectCount() {
		return this.enqueueTracker.getCount();
	}
	
	@ManagedAttribute
	public long getBlockedObjectCount() {
		return this.blockedObjectCount;
	}
	
	@ManagedAttribute
	public long getExpectedEnqueuePeriod() {
		return this.expectedEnqueuePeriod;
	}
	
	public void setExpectedEnqueuePeriod(long expectedEnqueuePeriod) {
		this.expectedEnqueuePeriod = expectedEnqueuePeriod;
	}
	
	@ManagedAttribute
	public float getEnqueueRate() {
		return this.enqueueTracker.getRate();
	}
	
	@ManagedAttribute
	public float getDequeueRate() {
		return this.dequeueTracker.getRate();
	}
	
	@ManagedAttribute
	public float getOverflowRate() {
		if (this.dequeueTracker.getCount() > 0) {
			if (this.enqueueTracker.getRate() > this.dequeueTracker.getRate()) {
				return this.enqueueTracker.getRate() - this.dequeueTracker.getRate();
				
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	}
	
	@ManagedAttribute
	public int getSize() {
		return this.internalQueue.getSize();
	}
	
	@ManagedAttribute
	public int getMaxSize() {
		return this.maxSize;
	}
	
	@ManagedAttribute
	public int getSizeErrorThreshold() {
		return this.sizeErrorThreshold;
	}
	
	public void setSizeErrorThreshold(int sizeErrorThreshold) {
		this.sizeErrorThreshold = sizeErrorThreshold;
	}
	
	@ManagedAttribute
	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}
	
	@ManagedAttribute
	public float getBlockRate() {
		if ((this.blockedObjectCount + this.enqueueTracker.getCount()) == 0) {
			return 0;
			
		} else {
			return ((float) this.blockedObjectCount / (this.blockedObjectCount + this.enqueueTracker.getCount()));
		}
	}
	
	public String toString() {
		return "ManagedObjectQueue(" + this.name + ")";
	}
	
	public int hashCode() {
		return this.toString().hashCode();
	}
}