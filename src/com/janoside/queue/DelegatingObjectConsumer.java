package com.janoside.queue;

import java.util.List;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;

public class DelegatingObjectConsumer<T> implements ObjectConsumer<T>, ExceptionHandlerAware {
	
	private List<ObjectConsumer<T>> internalConsumers;
	
	private ExceptionHandler exceptionHandler;
	
	public void consume(T object) {
		for (ObjectConsumer<T> internalConsumer : this.internalConsumers) {
			try {
				internalConsumer.consume(object);
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
		}
	}
	
	public void setInternalConsumers(List<ObjectConsumer<T>> internalConsumers) {
		this.internalConsumers = internalConsumers;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
}