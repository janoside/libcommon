package com.janoside.queue;

import java.util.List;

import org.springframework.jmx.export.annotation.ManagedResource;

@ManagedResource(objectName = "Janoside:name=BlackHole")
public class BlackHole<T> implements ObjectQueue<T> {
	
	public void enqueue(T object) {
	}
	
	public List<T> dequeue(int count) {
		throw new PhysicsException("Event horizon crossed, there is no return");
	}
	
	public void clear() {
	}
	
	public int getSize() {
		return 0;
	}
}