package com.janoside.queue;

import java.util.List;

import com.janoside.criteria.Criteria;

public class CriteriaSwitchingQueue<T> implements ObjectQueue<T> {
	
	private Criteria criteria;
	
	private ObjectQueue<T> criteriaMetQueue;
	
	private ObjectQueue<T> criteriaUnmetQueue;
	
	public void enqueue(T object) {
		if (this.criteria.isMet()) {
			this.criteriaMetQueue.enqueue(object);
			
		} else {
			this.criteriaUnmetQueue.enqueue(object);
		}
	}
	
	public List<T> dequeue(int count) {
		if (this.criteria.isMet()) {
			return this.criteriaMetQueue.dequeue(count);
			
		} else {
			return this.criteriaUnmetQueue.dequeue(count);
		}
	}
	
	public void clear() {
		if (this.criteria.isMet()) {
			this.criteriaMetQueue.clear();
			
		} else {
			this.criteriaUnmetQueue.clear();
		}
	}
	
	public int getSize() {
		if (this.criteria.isMet()) {
			return this.criteriaMetQueue.getSize();
			
		} else {
			return this.criteriaUnmetQueue.getSize();
		}
	}
	
	public void setCriteria(Criteria criteria) {
		this.criteria = criteria;
	}
	
	public void setCriteriaMetQueue(ObjectQueue<T> criteriaMetQueue) {
		this.criteriaMetQueue = criteriaMetQueue;
	}
	
	public void setCriteriaUnmetQueue(ObjectQueue<T> criteriaUnmetQueue) {
		this.criteriaUnmetQueue = criteriaUnmetQueue;
	}
}