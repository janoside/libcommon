package com.janoside.queue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClassSeparatingQueue<T> implements ObjectQueue<T> {
	
	private Map<Class<?>, ObjectQueue<T>> internalQueuesByClass;
	
	private ObjectQueue<T> defaultQueue;
	
	public void enqueue(T object) {
		Class<?> objectClass = object.getClass();
		
		if (this.internalQueuesByClass.containsKey(objectClass)) {
			this.internalQueuesByClass.get(objectClass).enqueue(object);
			
		} else if (this.defaultQueue != null) {
			this.defaultQueue.enqueue(object);
			
		} else {
			throw new IllegalArgumentException("Unexpected type " + objectClass + " enqueued in " + this + ", configure an internal queue for this class or configure a default queue");
		}
	}
	
	public List<T> dequeue(int count) {
		throw new UnsupportedOperationException("ClassSeparatingQueues should not be dequeued from, instead dequeue from its internal queues");
	}
	
	public void clear() {
		throw new UnsupportedOperationException("ClassSeparatingQueues should not be cleared, instead clear its internal queues");
	}
	
	public int getSize() {
		return 0;
	}
	
	public void setInternalQueuesByClassName(Map<String, ObjectQueue<T>> internalQueuesByClassName) throws ClassNotFoundException {
		this.internalQueuesByClass = new HashMap<Class<?>, ObjectQueue<T>>();
		
		for (Map.Entry<String, ObjectQueue<T>> entry : internalQueuesByClassName.entrySet()) {
			Class<?> internalQueueClass = Class.forName(entry.getKey());
			
			this.internalQueuesByClass.put(internalQueueClass, entry.getValue());
		}
	}
	
	public void setDefaultQueue(ObjectQueue<T> defaultQueue) {
		this.defaultQueue = defaultQueue;
	}
}