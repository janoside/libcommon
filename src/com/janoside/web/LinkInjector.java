package com.janoside.web;

import java.util.Collection;
import java.util.HashSet;

import org.springframework.util.StringUtils;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;

public class LinkInjector implements ExceptionHandlerAware {
	
	private TitleEncoder titleEncoder;
	
	private ExceptionHandler exceptionHandler;
	
	public String format(String input, Collection<String> keywords, String baseUrl) {
		try {
			if (!StringUtils.hasText(input) || keywords == null || keywords.isEmpty()) {
				return input;
			}
			
			HashSet<String> keywordSet = new HashSet<String>(keywords);
			
			StringBuilder buffer = new StringBuilder(input.length());
			
			HashSet<String> usedKeywords = new HashSet<String>();
			
			int charIndex = 0;
			while (charIndex < input.length()) {
				boolean match = false;
				
				for (String keyword : keywordSet) {
					if (!usedKeywords.contains(keyword) && charIndex <= (input.length() - keyword.length()) && input.substring(charIndex, charIndex + keyword.length()).toLowerCase().equals(keyword)) {
						buffer.append("<a href=\"" + baseUrl + this.titleEncoder.encode(keyword) + "\">").append(input.substring(charIndex, charIndex + keyword.length())).append("</a>");
						
						match = true;
						charIndex += keyword.length();
						
						usedKeywords.add(keyword);
					}
				}
	
				if (!match && (charIndex < input.length())) {
					buffer.append(input.charAt(charIndex));
						
					charIndex++;
				}
			}
			
			return buffer.toString();
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
			
			return input;
		}
	}
	
	public void setTitleEncoder(TitleEncoder titleEncoder) {
		this.titleEncoder = titleEncoder;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
}