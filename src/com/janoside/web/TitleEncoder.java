package com.janoside.web;

import java.util.HashMap;
import java.util.regex.Pattern;

import com.janoside.text.CharacterTranslatingTextTransformer;
import com.janoside.util.UrlUtil;

public class TitleEncoder {
	
	private final static char[] escapeChars = {'/', '\\'};
	
	private CharacterTranslatingTextTransformer encodeTransformer;
	
	private CharacterTranslatingTextTransformer decodeTransformer;
	
	public TitleEncoder() {
		this.encodeTransformer = new CharacterTranslatingTextTransformer();
		HashMap<Character, Character> encodeTranslationMap = new HashMap<Character, Character>();
		encodeTranslationMap.put('+', '-');
		encodeTranslationMap.put('-', '+');
		this.encodeTransformer.setTranslationMap(encodeTranslationMap);
		
		this.decodeTransformer = new CharacterTranslatingTextTransformer();
		HashMap<Character, Character> decodeTranslationMap = new HashMap<Character, Character>();
		decodeTranslationMap.put(' ', '-');
		decodeTranslationMap.put('-', ' ');
		this.decodeTransformer.setTranslationMap(decodeTranslationMap);
	}

	public String encode(String title) {
		String encodedTitle = title.toLowerCase().replaceAll(Pattern.quote("&") + "#([0-9]+);", "&amp;#$1;");
		
		for (char escapeChar : escapeChars) {
			encodedTitle = encodedTitle.replace(Character.toString(escapeChar), "&#" + ((int) escapeChar) + ";");
		}
		
		encodedTitle = this.encodeTransformer.transform(UrlUtil.encode(encodedTitle));
		
		return encodedTitle;
	}
	
	public String decode(String encodedTitle, boolean urlEncoded) {
		String decodedTitle = encodedTitle;

		if (urlEncoded) {
			decodedTitle = UrlUtil.decode(decodedTitle);
		}
		
		decodedTitle = this.decodeTransformer.transform(decodedTitle);
		
		for (char escapeChar : escapeChars) {
			decodedTitle = decodedTitle.replace("&#" + ((int) escapeChar) + ";", Character.toString(escapeChar));
		}
		
		decodedTitle = decodedTitle.replaceAll(Pattern.quote("&") + "amp;#([0-9]+);", "&#$1;");
		
		return decodedTitle;
	}
}