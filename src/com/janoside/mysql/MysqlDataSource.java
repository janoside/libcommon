package com.janoside.mysql;

import java.beans.PropertyVetoException;

import com.janoside.data.JdbcDataSource;
import com.mchange.v2.c3p0.ComboPooledDataSource;

public class MysqlDataSource extends JdbcDataSource {
	
	private ComboPooledDataSource pooledDataSource;
	
	public MysqlDataSource(String jdbcUrl, String username, String password) throws PropertyVetoException {
		this.pooledDataSource = new ComboPooledDataSource();
		this.pooledDataSource.setDriverClass("com.mysql.jdbc.Driver");
		this.pooledDataSource.setJdbcUrl(jdbcUrl);
		this.pooledDataSource.setUser(username);
		this.pooledDataSource.setPassword(password);
		
		this.setDataSource(this.pooledDataSource);
	}
	
	public MysqlDataSource(String host, String database, String username, String password) {
		try {
			String jdbcUrl = "jdbc:mysql://" + host + ":3306/" + database + "?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&noAccessToProcedureBodies=true";
			
			this.pooledDataSource = new ComboPooledDataSource();
			this.pooledDataSource.setDriverClass("com.mysql.jdbc.Driver");
			this.pooledDataSource.setJdbcUrl(jdbcUrl);
			this.pooledDataSource.setUser(username);
			this.pooledDataSource.setPassword(password);
			
			this.setDataSource(this.pooledDataSource);
			
		} catch (PropertyVetoException pve) {
			throw new RuntimeException(pve);
		}
	}
	
	public void closeConnections() {
		this.pooledDataSource.close();
	}
}