package com.janoside.transform;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.janoside.json.JsonObject;

public class JsonDeserializer<T> implements ObjectTransformer<JsonObject, T> {
	
	private static final Gson gson = new Gson();
	
	public T transform(JsonObject value) {
		if (value == null) {
			return null;
		}
		
		try {
			return (T) gson.fromJson(value.getJsonObject("object").toString(), Class.forName(value.getString("objectType")));
			
		} catch (JsonParseException e) {
			
			throw new RuntimeException("Failed to parse json string", e);
		} catch (ClassNotFoundException e) {
			
			throw new RuntimeException("Class not found", e);
		}
	}
}