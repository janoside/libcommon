package com.janoside.transform;

import com.google.gson.Gson;
import com.janoside.json.JsonObject;

public class JsonSerializer<T> implements ObjectTransformer<T, JsonObject> {
	
	private static final Gson gson = new Gson();
	
	public JsonObject transform(T value) {
		JsonObject jsonObject = new JsonObject();
		jsonObject.put("objectType", value.getClass().getName());
		jsonObject.put("object", new JsonObject(gson.toJson(value)));
		
		return jsonObject;
	}
}