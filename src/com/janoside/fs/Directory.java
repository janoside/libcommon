package com.janoside.fs;

import java.io.File;
import java.io.IOException;

public class Directory {
	
	private String directoryPath;
	
	public File getFile(String filename) {
		return new File(this.directoryPath + filename);
	}
	
	public String getDirectoryPath() {
		return this.directoryPath;
	}
	
	public void setDirectoryPath(String directoryPath) throws IOException {
		String directory = directoryPath.trim();
		
		File directoryTest = new File(directory);
		
		if (!directoryTest.exists()) {
			if (directoryTest.mkdir()) {
				if (!directoryTest.isDirectory()) {
					throw new RuntimeException("Invalid directoryPath: " + directory);
				}
			} else {
				throw new RuntimeException("Unable to create directory for directoryPath: " + directory);
			}
		} else {
			if (!directoryTest.isDirectory()) {
				throw new RuntimeException("Invalid directoryPath: " + directory);
			}
		}
		
		// ensure ends with "/" to simplify filepath concatenation
		this.directoryPath = directory;
		if (!this.directoryPath.endsWith("/")) {
			this.directoryPath = (this.directoryPath + "/");
		}
	}
	
	public String toString() {
		return "Directory(" + this.directoryPath + ")";
	}
}
