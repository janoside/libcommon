package com.janoside.message;

import java.util.ArrayList;
import java.util.List;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.util.StringUtils;

import com.janoside.stats.EventTracker;

@ManagedResource(objectName = "Janoside:name=Service")
public class Service {
	
	private EventTracker hitTracker;
	
	private EventTracker missTracker;
	
	private List<ServiceObserver> serviceObservers;
	
	private String type;
	
	private String name;
	
	private boolean available;
	
	public Service() {
		this.hitTracker = new EventTracker();
		this.missTracker = new EventTracker();
		
		this.serviceObservers = new ArrayList<ServiceObserver>();
		
		// innocent until proven guilty, must to be assumed "available" for startup proceedings
		this.available = true;
	}
	
	private void onHit() {
		this.hitTracker.increment();
		
		if (!this.available) {
			for (ServiceObserver observer : this.serviceObservers) {
				observer.onServiceBecameAvailable(this);
			}
			
			this.available = true;
		}
	}
	
	private void onMiss() {
		this.missTracker.increment();
		
		if (this.available) {
			for (ServiceObserver observer : this.serviceObservers) {
				observer.onServiceBecameUnavailable(this);
			}
			
			this.available = false;
		}
	}
	
	public void addServiceObserver(ServiceObserver serviceObserver) {
		if (!this.serviceObservers.contains(serviceObserver)) {
			this.serviceObservers.add(serviceObserver);
		}
	}
	
	public void setServiceObservers(List<ServiceObserver> serviceObservers) {
		for (ServiceObserver serviceObserver : serviceObservers) {
			this.addServiceObserver(serviceObserver);
		}
	}
	
	@ManagedAttribute
	public String getType() {
		return this.type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	@ManagedAttribute
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@ManagedAttribute
	public long getSecondsSinceUnavailable() {
		return this.missTracker.getSecondsSinceLastOccurrence();
	}
	
	@ManagedAttribute
	public float getUptime() {
		return ((float) this.hitTracker.getCount()) / (this.hitTracker.getCount() + this.missTracker.getCount());
	}
	
	@ManagedAttribute
	public long getHitCount() {
		return this.hitTracker.getCount();
	}
	
	@ManagedAttribute
	public long getMissCount() {
		return this.missTracker.getCount();
	}
	
	@ManagedAttribute
	public boolean isAvailable() {
		return this.available;
	}
	
	protected void setAvailable(boolean available) {
		if (available) {
			this.onHit();
			
		} else {
			this.onMiss();
		}
	}
	
	@Override
	public String toString() {
		return "Service(" + (this.type + (StringUtils.hasText(this.name) ? (", " + this.name) : "")) + ")";
	}
}