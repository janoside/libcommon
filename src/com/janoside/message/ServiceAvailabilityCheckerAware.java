package com.janoside.message;

public interface ServiceAvailabilityCheckerAware {

	void setServiceAvailabilityChecker(ServiceAvailabilityChecker serviceAvailabilityChecker);
}