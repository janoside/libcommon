package com.janoside.monitor;

import com.janoside.stats.StatTracker;

public class ActiveThreadCountMonitor extends SimpleValueMonitor {
	
	protected void run(StatTracker statTracker) {
		statTracker.trackValue("hardware.threads.active-count", Thread.activeCount());
	}
}