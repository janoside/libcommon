package com.janoside.monitor;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.janoside.jmx.JmxUtil;
import com.janoside.stats.StatTracker;

public class TomcatConnectionCountMonitor extends SimpleValueMonitor {
	
	private Pattern objectNamesPattern;
	
	public TomcatConnectionCountMonitor() {
		this.objectNamesPattern = Pattern.compile("Catalina:type=ThreadPool,name=\"http-[a-z]+-([0-9]+)\"");
	}
	
	protected void run(StatTracker statTracker) {
		List<String> objectNames = JmxUtil.getObjectNamesByPattern(this.objectNamesPattern);
		
		HashMap<String, Set<String>> map = new HashMap<String, Set<String>>();
		for (String objectName : objectNames) {
			map.put(objectName, new HashSet<String>() {{
				add("connectionCount");
			}});
		}
		
		Map<String, Map<String, Object>> data = JmxUtil.getAttributeValues(map);
		
		for (Map.Entry<String, Map<String, Object>> entry : data.entrySet()) {
			Matcher matcher = this.objectNamesPattern.matcher(entry.getKey());
			
			if (matcher.matches()) {
				int port = Integer.parseInt(matcher.group(1));
				int connectionCount = Integer.parseInt(entry.getValue().get("connectionCount").toString());
				
				if (connectionCount > 0) {
					statTracker.trackValue("tomcat.thread-pool." + port + ".connection-count", connectionCount);
				}
			}
		}
	}
}