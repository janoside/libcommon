package com.janoside.monitor;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.janoside.jmx.JmxUtil;
import com.janoside.stats.StatTracker;

public class C3P0PendingTasksMonitor extends SimpleValueMonitor {
	
	private Pattern objectNamesPattern;
	
	public C3P0PendingTasksMonitor() {
		this.objectNamesPattern = Pattern.compile("com.mchange.v2.c3p0:type=PooledDataSource\\[(.*)\\]");
	}
	
	protected void run(StatTracker statTracker) {
		List<String> objectNames = JmxUtil.getObjectNamesByPattern(this.objectNamesPattern);
		
		HashMap<String, Set<String>> map = new HashMap<String, Set<String>>();
		for (String objectName : objectNames) {
			map.put(objectName, new HashSet<String>() {{
				add("threadPoolNumTasksPending");
			}});
		}
		
		Map<String, Map<String, Object>> data = JmxUtil.getAttributeValues(map);
		
		for (Map.Entry<String, Map<String, Object>> entry : data.entrySet()) {
			Matcher matcher = this.objectNamesPattern.matcher(entry.getKey());
			
			if (matcher.matches()) {
				String uuid = matcher.group(1);
				int numTasksPending = Integer.parseInt(entry.getValue().get("threadPoolNumTasksPending").toString());
				
				if (numTasksPending > 0) {
					statTracker.trackValue("c3p0.data-source." + uuid + ".num-tasks-pending", numTasksPending);
				}
			}
		}
	}
}