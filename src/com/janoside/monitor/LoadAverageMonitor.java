package com.janoside.monitor;

import java.lang.management.ManagementFactory;

import com.janoside.stats.StatTracker;

public class LoadAverageMonitor extends SimpleValueMonitor {
	
	protected void run(StatTracker statTracker) {
		float loadAverage = (float) ManagementFactory.getOperatingSystemMXBean().getSystemLoadAverage();
		
		if (loadAverage >= 0) {
			int availableProcessors = ManagementFactory.getOperatingSystemMXBean().getAvailableProcessors();
			
			float loadRatio = loadAverage / (float) availableProcessors;
			
			statTracker.trackValue("hardware.cpu.load-average", loadAverage);
			statTracker.trackValue("hardware.cpu.load-average-over-available-processors", loadRatio);
		}
	}
}