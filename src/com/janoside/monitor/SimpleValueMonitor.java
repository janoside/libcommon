package com.janoside.monitor;

import org.springframework.beans.factory.InitializingBean;

import com.janoside.environment.Environment;
import com.janoside.environment.EnvironmentAware;
import com.janoside.scheduling.BasicScheduler;
import com.janoside.scheduling.Scheduler;
import com.janoside.scheduling.SchedulerAware;
import com.janoside.stats.StatTracker;
import com.janoside.stats.StatTrackerAware;

public abstract class SimpleValueMonitor implements SchedulerAware, StatTrackerAware, EnvironmentAware, InitializingBean {
	
	private Scheduler scheduler;
	
	private StatTracker statTracker;
	
	private Environment environment;
	
	private long period;
	
	public SimpleValueMonitor() {
		this.scheduler = new BasicScheduler();
		this.period = 60000;
	}
	
	public void afterPropertiesSet() {
		this.scheduler.scheduleTaskAtConstantRate(new Runnable() {
				public void run() {
					SimpleValueMonitor.this.run(statTracker);
				}
			},
			this.period,
			this.period);
	}
	
	protected abstract void run(StatTracker statTracker);
	
	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}
	
	public void setStatTracker(StatTracker statTracker) {
		this.statTracker = statTracker;
	}
	
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}
	
	public void setPeriod(long period) {
		this.period = period;
	}
}