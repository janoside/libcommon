package com.janoside.http;

import java.util.Map;

import org.apache.commons.httpclient.NameValuePair;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.stats.SuccessTracker;

@ManagedResource(objectName = "Janoside:name=RetryingHttpClient")
public class RetryingHttpClient implements HttpClient, ExceptionHandlerAware {
	
	private HttpClient httpClient;
	
	private ExceptionHandler exceptionHandler;
	
	private SuccessTracker firstAttemptSuccessTracker;
	
	private SuccessTracker anyAttemptSuccessTracker;
	
	private long retryPeriod;
	
	private int retryCount;
	
	public RetryingHttpClient() {
		this.firstAttemptSuccessTracker = new SuccessTracker();
		this.anyAttemptSuccessTracker = new SuccessTracker();
		this.retryPeriod = 250;
		this.retryCount = 1;
	}
	
	public String get(String url) {
		int attempt = 0;
		while (attempt <= this.retryCount) {
			try {
				return this.httpClient.get(url);
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
			
			attempt++;
			
			if (attempt < this.retryCount) {
				try {
					Thread.sleep(this.retryPeriod);
					
				} catch (InterruptedException ie) {
					this.exceptionHandler.handleException(ie);
				}
			}
		}
		
		this.anyAttemptSuccessTracker.failure();
		
		throw new RuntimeException("Failed json call");
	}
	
	public String get(String url, long timeout) {
		int attempt = 0;
		while (attempt <= this.retryCount) {
			try {
				return this.httpClient.get(url, timeout);
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
			
			attempt++;
			
			if (attempt < this.retryCount) {
				try {
					Thread.sleep(this.retryPeriod);
					
				} catch (InterruptedException ie) {
					this.exceptionHandler.handleException(ie);
				}
			}
		}
		
		this.anyAttemptSuccessTracker.failure();
		
		throw new RuntimeException("Failed json call");
	}
	
	public String get(String url, Map<String, String> parameters, long timeout) {
		int attempt = 0;
		while (attempt <= this.retryCount) {
			try {
				return this.httpClient.get(url, parameters, timeout);
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
			
			attempt++;
			
			if (attempt < this.retryCount) {
				try {
					Thread.sleep(this.retryPeriod);
					
				} catch (InterruptedException ie) {
					this.exceptionHandler.handleException(ie);
				}
			}
		}
		
		this.anyAttemptSuccessTracker.failure();
		
		throw new RuntimeException("Failed json call");
	}
	
	public byte[] getBytes(String url, long timeout) {
		int attempt = 0;
		while (attempt <= this.retryCount) {
			try {
				return this.httpClient.getBytes(url, timeout);
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
			
			attempt++;
			
			if (attempt < this.retryCount) {
				try {
					Thread.sleep(this.retryPeriod);
					
				} catch (InterruptedException ie) {
					this.exceptionHandler.handleException(ie);
				}
			}
		}
		
		this.anyAttemptSuccessTracker.failure();
		
		throw new RuntimeException("Failed json call");
	}
	
	public String post(String url, Map<String, String> parameters, long timeout) {
		return this.httpClient.post(url, parameters, timeout);
	}
	
	public String post(String url, Map<String, String> parameters) {
		return this.httpClient.post(url, parameters);
	}
	
	public String post(String url, NameValuePair[] data) {
		return this.httpClient.post(url, data);
	}
	
	public String post(String url, NameValuePair[] data, long timeout) {
		return this.httpClient.post(url, data, timeout);
	}
	
	public String post(String url, Map<String, String> requestHeaders, String postData, String contentType, String encoding, long timeout) {
		return this.httpClient.post(url, requestHeaders, postData, contentType, encoding, timeout);
	}
	
	public String post(String url, String postData, String contentType, String encoding, long timeout) {
		return this.httpClient.post(url, postData, contentType, encoding, timeout);
	}
	
	public String post(String url, String postData, long timeout) {
		return this.httpClient.post(url, postData, timeout);
	}
	
	public String post(String url, String postData) {
		return this.httpClient.post(url, postData);
	}
	
	public void setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	@ManagedAttribute
	public long getRetryPeriod() {
		return this.retryPeriod;
	}
	
	public void setRetryPeriod(long retryPeriod) {
		this.retryPeriod = retryPeriod;
	}
	
	@ManagedAttribute
	public int getRetryCount() {
		return this.retryCount;
	}
	
	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}
	
	@ManagedAttribute
	public float getFirstAttemptSuccessRate() {
		return this.firstAttemptSuccessTracker.getSuccessRate();
	}
	
	@ManagedAttribute
	public float getAnyAttemptSuccessRate() {
		return this.anyAttemptSuccessTracker.getSuccessRate();
	}
}