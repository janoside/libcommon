package com.janoside.http;

import java.util.Map;

import org.apache.commons.httpclient.NameValuePair;

import com.janoside.security.RateLimiter;

public class RateLimitingHttpClient implements HttpClient {
	
	private HttpClient httpClient;
	
	private RateLimiter rateLimiter;
	
	public RateLimitingHttpClient() {
		this.rateLimiter = new RateLimiter();
		this.rateLimiter.setPeriod(60000);
		this.rateLimiter.setMaxOccurrencesPerPeriod(60);
	}
	
	public String get(String url, long timeout) {
		if (this.rateLimiter.canPerform()) {
			return this.httpClient.get(url, timeout);
			
		} else {
			return "";
		}
	}
	
	public String get(String url, Map<String, String> parameters, long timeout) {
		if (this.rateLimiter.canPerform()) {
			return this.httpClient.get(url, parameters, timeout);
			
		} else {
			return "";
		}
	}
	
	public String get(String url) {
		if (this.rateLimiter.canPerform()) {
			return this.get(url);
			
		} else {
			return "";
		}
	}
	
	public byte[] getBytes(String url, long timeout) {
		if (this.rateLimiter.canPerform()) {
			return this.httpClient.getBytes(url, timeout);
			
		} else {
			return new byte[0];
		}
	}
	
	public String post(String url, Map<String, String> parameters, long timeout) {
		if (this.rateLimiter.canPerform()) {
			return this.httpClient.post(url, parameters, timeout);
			
		} else {
			return "";
		}
	}
	
	public String post(String url, Map<String, String> parameters) {
		if (this.rateLimiter.canPerform()) {
			return this.httpClient.post(url, parameters);
			
		} else {
			return "";
		}
	}
	
	public String post(String url, NameValuePair[] data) {
		if (this.rateLimiter.canPerform()) {
			return this.httpClient.post(url, data);
			
		} else {
			return "";
		}
	}
	
	public String post(String url, NameValuePair[] data, long timeout) {
		if (this.rateLimiter.canPerform()) {
			return this.httpClient.post(url, data, timeout);
			
		} else {
			return "";
		}
	}
	
	public String post(String url, Map<String, String> requestHeaders, String postData, String contentType, String encoding, long timeout) {
		if (this.rateLimiter.canPerform()) {
			return this.httpClient.post(url, requestHeaders, postData, contentType, encoding, timeout);
			
		} else {
			return "";
		}
	}
	
	public String post(String url, String postData, String contentType, String encoding, long timeout) {
		if (this.rateLimiter.canPerform()) {
			return this.httpClient.post(url, postData, contentType, encoding, timeout);
			
		} else {
			return "";
		}
	}
	
	public String post(String url, String postData, long timeout) {
		if (this.rateLimiter.canPerform()) {
			return this.httpClient.post(url, postData, timeout);
			
		} else {
			return "";
		}
	}
	
	public String post(String url, String postData) {
		if (this.rateLimiter.canPerform()) {
			return this.httpClient.post(url, postData);
			
		} else {
			return "";
		}
	}
	
	public void setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
	}
	
	public void setPeriod(long period) {
		this.rateLimiter.setPeriod(period);
	}
	
	public void setMaxOccurrencesPerPeriod(int maxOccurrencesPerPeriod) {
		this.rateLimiter.setMaxOccurrencesPerPeriod(maxOccurrencesPerPeriod);
	}
}