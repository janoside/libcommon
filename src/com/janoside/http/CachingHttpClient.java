package com.janoside.http;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import org.apache.commons.httpclient.NameValuePair;
import org.springframework.util.StringUtils;

import com.janoside.cache.ObjectCache;
import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;

public class CachingHttpClient implements HttpClient, ExceptionHandlerAware {
	
	private HttpClient httpClient;
	
	private ObjectCache<String> cache;
	
	private ExceptionHandler exceptionHandler;
	
	public String get(String url, long timeout) {
		String value = null;
		
		try {
			value = this.cache.get(url);
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
		}
		
		if (value != null) {
			return value;
			
		} else {
			try {
				String content = this.httpClient.get(url, timeout);
				
				if (StringUtils.hasText(content)) {
					try {
						this.cache.put(url, content);
						
					} catch (Throwable t) {
						this.exceptionHandler.handleException(t);
					}
				}
				
				return content;
				
			} catch (Exception e) {
				this.exceptionHandler.handleException(e);
				
				return null;
			}
		}
	}
	
	public String get(String url, Map<String, String> parameters, long timeout) {
		ArrayList<String> parameterNames = new ArrayList<String>(parameters.keySet());
		Collections.sort(parameterNames);
		
		StringBuilder buffer = new StringBuilder();
		for (String parameterName : parameterNames) {
			buffer.append(parameterName);
			buffer.append("=");
			buffer.append(parameters.get(parameterName));
			buffer.append("&");
		}
		
		String fullUrl = url + buffer.toString();
		
		String value = null;
		
		try {
			value = this.cache.get(fullUrl);
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
		}
		
		if (value != null) {
			return value;
			
		} else {
			try {
				String content = this.httpClient.get(url, parameters, timeout);
				
				if (StringUtils.hasText(content)) {
					try {
						this.cache.put(fullUrl, content);
						
					} catch (Throwable t) {
						this.exceptionHandler.handleException(t);
					}
				}
				
				return content;
				
			} catch (Exception e) {
				this.exceptionHandler.handleException(e);
				
				return null;
			}
		}
	}
	
	public String get(String url) {
		return this.get(url, 0);
	}
	
	public byte[] getBytes(String url, long timeout) {
		return this.httpClient.getBytes(url, timeout);
	}
	
	public String post(String url, Map<String, String> parameters, long timeout) {
		return this.httpClient.post(url, parameters, timeout);
	}
	
	public String post(String url, Map<String, String> parameters) {
		return this.httpClient.post(url, parameters);
	}
	
	public String post(String url, NameValuePair[] data) {
		return this.httpClient.post(url, data);
	}
	
	public String post(String url, NameValuePair[] data, long timeout) {
		return this.httpClient.post(url, data, timeout);
	}
	
	public String post(String url, Map<String, String> requestHeaders, String postData, String contentType, String encoding, long timeout) {
		return this.httpClient.post(url, requestHeaders, postData, contentType, encoding, timeout);
	}
	
	public String post(String url, String postData, String contentType, String encoding, long timeout) {
		return this.httpClient.post(url, postData, contentType, encoding, timeout);
	}
	
	public String post(String url, String postData, long timeout) {
		return this.httpClient.post(url, postData, timeout);
	}
	
	public String post(String url, String postData) {
		return this.httpClient.post(url, postData);
	}
	
	public void setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
	}
	
	public void setCache(ObjectCache<String> cache) {
		this.cache = cache;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
}