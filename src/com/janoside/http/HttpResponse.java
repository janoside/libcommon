package com.janoside.http;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("serial")
public class HttpResponse implements Serializable {
	
	private Map<String, String> headers;
	
	private String body;
	
	private int statusCode;
	
	public HttpResponse() {
		this.headers = new HashMap<String, String>();
	}
	
	public Map<String, String> getHeaders() {
		return this.headers;
	}
	
	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}
	
	public String getBody() {
		return this.body;
	}
	
	public void setBody(String body) {
		this.body = body;
	}
	
	public int getStatusCode() {
		return this.statusCode;
	}
	
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getHeader(String header) {
		return this.headers.get(header);
	}
	
	public boolean isSuccess() {
		return (this.statusCode >= 200) && (this.statusCode < 300);
	}
}