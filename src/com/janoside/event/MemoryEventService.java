package com.janoside.event;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import com.janoside.util.Tuple;

public class MemoryEventService implements EventService {
	
	private Map<Tuple<EventListener, Pattern>, Pattern> eventNamePatternsByEventListener;
	
	public MemoryEventService() {
		this.eventNamePatternsByEventListener = new HashMap<Tuple<EventListener, Pattern>, Pattern>();
	}
	
	public void eventOccurred(Object source, String eventName) {
		for (Map.Entry<Tuple<EventListener, Pattern>, Pattern> entry : this.eventNamePatternsByEventListener.entrySet()) {
			if (entry.getValue().matcher(eventName).matches()) {
				entry.getKey().getItem1().afterEventOccurred(source, eventName);
			}
		}
	}
	
	public void registerEventListener(EventListener eventListener, Pattern eventNamePattern) {
		this.eventNamePatternsByEventListener.put(new Tuple<EventListener, Pattern>(eventListener, eventNamePattern), eventNamePattern);
	}
	
	public void registerEventListener(EventListener eventListener) {
		this.registerEventListener(eventListener, Pattern.compile(".*"));
	}
	
	public void unregisterEventListener(EventListener eventListener, Pattern eventNamePattern) {
		this.eventNamePatternsByEventListener.remove(new Tuple<EventListener, Pattern>(eventListener, eventNamePattern));
	}
	
	public void unregisterEventListener(EventListener eventListener) {
		this.unregisterEventListener(eventListener, Pattern.compile(".*"));
	}
}