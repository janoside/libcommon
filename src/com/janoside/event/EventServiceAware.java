package com.janoside.event;

public interface EventServiceAware {
	
	void setEventService(EventService eventService);
}