package com.janoside.event;

import java.util.regex.Pattern;

public interface EventService {
	
	void eventOccurred(Object source, String eventName);
	
	void registerEventListener(EventListener eventListener, Pattern eventNamePattern);
	
	void registerEventListener(EventListener eventListener);
	
	void unregisterEventListener(EventListener eventListener, Pattern eventNamePattern);
	
	void unregisterEventListener(EventListener eventListener);
}