package com.janoside.image;

public enum ResizeMode {
	Constrain,
	Crop,
	Stretch
}