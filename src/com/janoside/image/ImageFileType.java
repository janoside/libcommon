package com.janoside.image;

public enum ImageFileType {
	Jpeg,
	Png,
	Gif
}