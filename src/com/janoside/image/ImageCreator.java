package com.janoside.image;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Locale;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.exception.StandardErrorExceptionHandler;

public class ImageCreator implements ExceptionHandlerAware {
	
	private static final Logger logger = LoggerFactory.getLogger(ImageCreator.class);
	
	private ExceptionHandler exceptionHandler;
	
	public ImageCreator() {
		this.exceptionHandler = new StandardErrorExceptionHandler();
	}
	
	public BufferedImage createImageFromByteArray(byte[] bytes) {
		ByteArrayInputStream inputStream = null;
		
		try {
			inputStream = new ByteArrayInputStream(bytes);
			
			return ImageIO.read(inputStream);
			
		} catch (IOException e) {
			throw new RuntimeException("Failed to read image from byte array", e);
			
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
					
				} catch (Throwable t) {
					this.exceptionHandler.handleException(t);
				}
			}
		}
	}
	
	public byte[] createImageFile(BufferedImage image, ImageFileType fileType, float compressionQuality) {
		ImageOutputStream imageOutputStream = null;
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		
		try {
			imageOutputStream = ImageIO.createImageOutputStream(outputStream);
			
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		if (imageOutputStream == null) {
			logger.error("Null ImageOutputStream, image=" + image + ", fileType=" + fileType + ", compressionQuality=" + compressionQuality);
			
			throw new RuntimeException("Null ImageOutputStream");
		}
		
		ImageWriter imageWriter = null;
		ImageWriteParam imageWriteParam = null;
		
		if (fileType == ImageFileType.Jpeg) {
			imageWriter = this.getImageWriterByFormatName("jpeg");
			
			imageWriteParam = new JPEGImageWriteParam(Locale.getDefault());
			((JPEGImageWriteParam) imageWriteParam).setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
			((JPEGImageWriteParam) imageWriteParam).setCompressionQuality(compressionQuality);
			
		} else if (fileType == ImageFileType.Gif) {
			imageWriter = this.getImageWriterByFormatName("gif");
			
		} else if (fileType == ImageFileType.Png) {
			imageWriter = this.getImageWriterByFormatName("png");
			
		} else {
			// nothing, keep imageWriter null if we don't know what to do with it, that way
			// we're sure to get into the next try, whose finally closes the streams
		}
		
		try {
			imageWriter.setOutput(imageOutputStream);
			
			imageWriter.write(
					null,
					new IIOImage(image, null, null),
					imageWriteParam);
			
			imageOutputStream.flush();
			outputStream.flush();
			
			return outputStream.toByteArray();
			
		} catch (IOException e) {
			throw new RuntimeException("Failed to write " + fileType + " to stream", e);
			
		} finally {
			try {
				imageWriter.dispose();
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
			
			if (imageOutputStream != null) {
				try {
					imageOutputStream.close();
					
				} catch (Throwable t) {
					this.exceptionHandler.handleException(t);
				}
			}
			
			if (outputStream != null) {
				try {
					outputStream.close();
					
				} catch (Throwable t) {
					this.exceptionHandler.handleException(t);
				}
			}
		}
	}
	
	private ImageWriter getImageWriterByFormatName(String formatName) {
		Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName(formatName);
		
		if (writers.hasNext()) {
			return writers.next();
			
		} else {
			throw new RuntimeException("Could not get an ImageWriter for format " + formatName);
		}
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
}