package com.janoside.image;

public enum ImagePosition {
	TopLeft,
	TopMiddle,
	TopRight,
	CenterLeft,
	CenterMiddle,
	CenterRight,
	BottomLeft,
	BottomMiddle,
	BottomRight
}