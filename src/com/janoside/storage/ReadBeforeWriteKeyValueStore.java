package com.janoside.storage;

import java.util.Comparator;

/**
 * A KeyValueStore implementation that will read before writing, 
 * and if the key-to-be-written's value would not be changed 
 * (from the point of view of a configurable internal Comparator)
 * then the actual "put" operation on the internal store is not 
 * performed.
 * 
 * @author janoside
 *
 * @param <T>
 */
public class ReadBeforeWriteKeyValueStore<T> implements KeyValueStore<T> {
	
	private KeyValueStore<T> store;
	
	private Comparator<T> comparator;
	
	public ReadBeforeWriteKeyValueStore() {
		this.comparator = new Comparator<T>() {
			public int compare(T t1, T t2) {
				if (t1.equals(t2)) {
					return 0;
					
				} else {
					return -1;
				}
			}
		};
	}
	
	public void put(String key, T value) {
		T existingValue = this.store.get(key);
		
		if (existingValue == null || this.comparator.compare(value, existingValue) != 0) {
			this.store.put(key, value);
		}
	}
	
	public T get(String key) {
		return this.store.get(key);
	}
	
	public void remove(String key) {
		this.store.remove(key);
	}
	
	public void setStore(KeyValueStore<T> store) {
		this.store = store;
	}
	
	public void setComparator(Comparator<T> comparator) {
		this.comparator = comparator;
	}
}