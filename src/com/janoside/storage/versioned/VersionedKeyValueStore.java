package com.janoside.storage.versioned;

import java.util.List;
import java.util.Map;

public interface VersionedKeyValueStore<T, PagingType> {
	
	List<VersionedObject<T>> getVersions(String key, PagingType page);
	
	VersionedObject<T> getLatestVersion(String key);
	
	VersionedObject<T> getVersion(String key, String versionId);
	
	void put(String key, T value, Map<String, Object> metadata);
	
	void removeVersion(String key, String versionId);
	
	void remove(String key);
}