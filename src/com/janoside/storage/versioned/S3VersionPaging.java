package com.janoside.storage.versioned;

public class S3VersionPaging {
	
	/**
	 * The last versionId from the previous "Page" of results.
	 */
	private String lastVersionId;
	
	private int count;
	
	public String getLastVersionId() {
		return lastVersionId;
	}
	
	public void setLastVersionId(String lastVersionId) {
		this.lastVersionId = lastVersionId;
	}
	
	public int getCount() {
		return count;
	}
	
	public void setCount(int count) {
		this.count = count;
	}
}