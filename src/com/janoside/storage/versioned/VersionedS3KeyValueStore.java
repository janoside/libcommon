package com.janoside.storage.versioned;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.jets3t.service.S3Service;
import org.jets3t.service.S3ServiceException;
import org.jets3t.service.VersionOrDeleteMarkersChunk;
import org.jets3t.service.acl.AccessControlList;
import org.jets3t.service.acl.GroupGrantee;
import org.jets3t.service.acl.Permission;
import org.jets3t.service.model.BaseVersionOrDeleteMarker;
import org.jets3t.service.model.S3Bucket;
import org.jets3t.service.model.S3Object;
import org.jets3t.service.utils.ServiceUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.serialization.Serializer;
import com.janoside.util.MimeUtil;
import com.janoside.util.StreamUtil;

public class VersionedS3KeyValueStore<T> implements VersionedKeyValueStore<T, S3VersionPaging>, ExceptionHandlerAware, InitializingBean {
	
	private static final Logger logger = LoggerFactory.getLogger(VersionedS3KeyValueStore.class);
	
	private List<VersionedKeyValueStoreObserver<T, S3VersionPaging>> observers;
	
	private S3Service s3Service;
	
	private S3Bucket s3Bucket;
	
	private Serializer<T, byte[]> valueSerializer;
	
	private ExceptionHandler exceptionHandler;
	
	private String bucketName;
	
	public VersionedS3KeyValueStore() {
		this.observers = new ArrayList<VersionedKeyValueStoreObserver<T, S3VersionPaging>>();
	}
	
	public List<VersionedObject<T>> getVersions(String key, S3VersionPaging paging) {
		try {
			VersionOrDeleteMarkersChunk chunk = this.s3Service.listVersionedObjectsChunked(
					this.s3Bucket.getName(),
					key,
					"/",
					paging.getCount(),
					paging.getLastVersionId() == null ? null : key,
					paging.getLastVersionId(),
					false);
			
			ArrayList<VersionedObject<T>> versions = new ArrayList<VersionedObject<T>>(chunk.getItemCount());
			for (BaseVersionOrDeleteMarker version : chunk.getItems()) {
				VersionedObject<T> versionedObject = new VersionedObject<T>();
				versionedObject.setVersionId(version.getVersionId());
				
				if (!version.isDeleteMarker()) {
					S3Object s3Object = this.s3Service.getVersionedObject(
							version.getVersionId(),
							this.s3Bucket.getName(),
							key);
					
					byte[] valueBytes = StreamUtil.streamToByteArray(s3Object.getDataInputStream());
					T value = this.valueSerializer.deserialize(valueBytes);
					
					versionedObject.setMetadata(s3Object.getMetadataMap());
					versionedObject.setValue(value);
				}
				
				versions.add(versionedObject);
			}
			
			return versions;
			
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}
	
	public VersionedObject<T> getLatestVersion(String key) {
		try {
			S3Object s3Object = this.s3Service.getObject(this.bucketName, key);
			
			return this.getVersionedObject(s3Object);
			
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}
	
	public VersionedObject<T> getVersion(String key, String versionId) {
		try {
			S3Object s3Object = this.s3Service.getVersionedObject(
					versionId,
					this.bucketName,
					key);
			
			return this.getVersionedObject(s3Object);
			
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}
	
	public void put(String key, T value, Map<String, Object> metadata) {
		byte[] valueBytes = this.valueSerializer.serialize(value);
		
		String keyExtension = "bin";
		if (key.contains(".")) {
			keyExtension = key.substring(key.lastIndexOf('.') + 1);
		}
		
		S3Object s3Object = new S3Object(this.s3Bucket, key);
		s3Object.setContentType(MimeUtil.getMimeType(keyExtension));
		
		ByteArrayInputStream inputStream = new ByteArrayInputStream(valueBytes);
		
		try {
			s3Object.setDataInputStream(inputStream);
			s3Object.setContentLength(valueBytes.length);
			
			try {
				s3Object.setMd5Hash(ServiceUtils.computeMD5Hash(inputStream));
				inputStream.reset();
				
			} catch (NoSuchAlgorithmException e) {
				throw new RuntimeException("Whoa! MD5 does not exist!", e);
				
			} catch (IOException e) {
				throw new RuntimeException("MD5 Hash failed", e);
			}
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
					
				} catch (IOException ioe) {
					logger.error("Error uploading object to S3: key={}, value={}, versionId={}, metadata={}", key, value);
					
					throw new RuntimeException("Error uploading object to S3.", ioe);
				}
			}
		}
		
		if (metadata.containsKey("public")) {
			boolean makePublic = Boolean.parseBoolean(String.valueOf(metadata.get("public")));
			
			if (makePublic) {
				AccessControlList acl = new AccessControlList();
				acl.setOwner(this.s3Bucket.getOwner());
				
				acl.grantPermission(GroupGrantee.ALL_USERS, Permission.PERMISSION_READ);
				
				s3Object.setAcl(acl);
			}
		}
		
		if (metadata != null) {
			for (Entry<String, Object> entry : metadata.entrySet()) {
				if (entry.getValue() instanceof Date) {
					s3Object.addMetadata(entry.getKey(), (Date) entry.getValue());
					
				} else {
					s3Object.addMetadata(entry.getKey(), String.valueOf(entry.getValue()));
				}
			}
		}
		
		try {
			this.s3Service.putObject(this.s3Bucket, s3Object);
			
			for (VersionedKeyValueStoreObserver<T, S3VersionPaging> observer : this.observers) {
				try {
					observer.onPut(this, key, value, metadata);
					
				} catch (Throwable t) {
					this.exceptionHandler.handleException(t);
				}
			}
		} catch (S3ServiceException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void removeVersion(String key, String versionId) {
		try {
			this.s3Service.deleteVersionedObject(versionId, this.bucketName, key);
			
			for (VersionedKeyValueStoreObserver<T, S3VersionPaging> observer : this.observers) {
				try {
					observer.onRemoveVersion(this, key, versionId);
					
				} catch (Throwable t) {
					this.exceptionHandler.handleException(t);
				}
			}
		} catch (S3ServiceException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void remove(String key) {
		try {
			this.s3Service.deleteObject(this.bucketName, key);
			
			for (VersionedKeyValueStoreObserver<T, S3VersionPaging> observer : this.observers) {
				try {
					observer.onRemove(this, key);
					
				} catch (Throwable t) {
					this.exceptionHandler.handleException(t);
				}
			}
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}
	
	public void afterPropertiesSet() {
		try {
			this.s3Bucket = this.s3Service.getBucket(this.bucketName);
			
		} catch (S3ServiceException e) {
			throw new RuntimeException(e);
		}
	}
	
	private VersionedObject<T> getVersionedObject(S3Object s3Object) throws Exception{
		byte[] valueBytes = StreamUtil.streamToByteArray(s3Object.getDataInputStream());
		T value = this.valueSerializer.deserialize(valueBytes);
		
		VersionedObject<T> versionedObject = new VersionedObject<T>();
		versionedObject.setVersionId(String.valueOf(s3Object.getMetadataMap().get("version-id")));
		versionedObject.setMetadata(s3Object.getMetadataMap());
		versionedObject.setValue(value);
		
		return versionedObject;
	}
	
	public void setObservers(List<VersionedKeyValueStoreObserver<T, S3VersionPaging>> observers) {
		this.observers = observers;
	}
	
	public void setS3Service(S3Service s3Service) {
		this.s3Service = s3Service;
	}
	
	public void setValueSerializer(Serializer<T, byte[]> valueSerializer) {
		this.valueSerializer = valueSerializer;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}
}