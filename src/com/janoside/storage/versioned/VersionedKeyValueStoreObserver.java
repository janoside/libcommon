package com.janoside.storage.versioned;

import java.util.Map;

public interface VersionedKeyValueStoreObserver<T, PagingType> {
	
	void onPut(VersionedKeyValueStore<T, PagingType> store, String key, T value, Map<String, Object> metadata);
	
	void onRemove(VersionedKeyValueStore<T, PagingType> store, String key);
	
	void onRemoveVersion(VersionedKeyValueStore<T, PagingType> store, String key, String versionId);
}