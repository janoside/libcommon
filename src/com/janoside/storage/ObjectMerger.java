package com.janoside.storage;

public interface ObjectMerger<T> {

	T mergeObjects(T oldObject, T newObject);
}