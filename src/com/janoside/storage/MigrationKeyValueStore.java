package com.janoside.storage;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;

public class MigrationKeyValueStore<T> implements KeyValueStore<T>, ExceptionHandlerAware {
	
	private KeyValueStore<T> source;
	
	private KeyValueStore<T> destination;
	
	private ExceptionHandler exceptionHandler;
	
	public T get(String key) {
		try {
			T value = this.destination.get(key);
			
			if (value != null) {
				return value;
			}
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
		}
		
		try {
			T value = this.source.get(key);
			
			if (value != null) {
				try {
					this.destination.put(key, value);
					
				} catch (Throwable t) {
					this.exceptionHandler.handleException(t);
				}
				
				return value;
			}
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
		}
		
		return null;
	}
	
	public void put(String key, T value) {
		this.destination.put(key, value);
	}
	
	public void remove(String key) {
		try {
			this.destination.remove(key);
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
		}
		
		try {
			this.source.remove(key);
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
		}
	}
	
	public void setSource(KeyValueStore<T> source) {
		this.source = source;
	}
	
	public void setDestination(KeyValueStore<T> destination) {
		this.destination = destination;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
}