package com.janoside.storage;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UTFDataFormatException;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.s3.S3Bucket;
import com.janoside.util.StreamUtil;

public class StringS3KeyValueStore implements KeyValueStore<String>, KeySearcher, ExceptionHandlerAware {
	
	private static final Logger logger = LoggerFactory.getLogger(StringS3KeyValueStore.class);
	
	private S3Bucket s3Bucket;
	
	private ExceptionHandler exceptionHandler;
	
	private String keyFileExtension;
	
	public StringS3KeyValueStore() {
		this.keyFileExtension = "txt";
	}
	
	public Set<String> getKeys(String prefix) {
		return new HashSet<String>(this.s3Bucket.getKeys(prefix, false, false));
	}
	
	public void put(String key, String value) {
		DataOutputStream dataOutputStream = null;
		
		try {
			this.s3Bucket.uploadFile(
					this.finalKey(key),
					value.getBytes("UTF-8"),
					false,
					"text/plain");
			
		} catch (Throwable t) {
			if (t instanceof UTFDataFormatException) {
				logger.error("UTF error when putting key in " + this + ": key=" + key + ", value=" + value, t);
			}
			
			throw new RuntimeException("Failed to put string in S3", t);
			
		} finally {
			if (dataOutputStream != null) {
				try {
					dataOutputStream.close();
					
				} catch (IOException ioe) {
					this.exceptionHandler.handleException(ioe);
				}
			}
		}
	}
	
	public String get(String key) {
		try {
			InputStream inputStream = this.s3Bucket.getFileContents(this.finalKey(key));
			
			if (inputStream == null) {
				return null;
			}
			
			String value = StreamUtil.convertStreamToString(inputStream, "UTF-8");
			
			return value;
			
		} catch (Throwable t) {
			throw new RuntimeException("Failed to read string from S3", t);
		}
	}
	
	public void remove(String key) {
		this.s3Bucket.deleteFile(this.finalKey(key));
	}
	
	private String finalKey(String key) {
		return String.format("%s%s", key, StringUtils.hasText(this.keyFileExtension) ? String.format(".%s", this.keyFileExtension) : "");
	}
	
	public void setS3Bucket(S3Bucket s3Bucket) {
		this.s3Bucket = s3Bucket;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setKeyFileExtension(String keyFileExtension) {
		this.keyFileExtension = keyFileExtension;
	}
}