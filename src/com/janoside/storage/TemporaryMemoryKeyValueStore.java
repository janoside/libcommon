package com.janoside.storage;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;

public class TemporaryMemoryKeyValueStore<T> implements KeyValueStore<T>, ExceptionHandlerAware {
	
	private static final Logger logger = LoggerFactory.getLogger(TemporaryMemoryKeyValueStore.class);
	
	private KeyValueStore<T> store;
	
	private MemoryKeyValueStore<T> temporaryStore;
	
	private ExecutorService executorService;
	
	private ExceptionHandler exceptionHandler;
	
	public TemporaryMemoryKeyValueStore() {
		this.executorService = Executors.newFixedThreadPool(100);
	}
	
	public void put(String key, T value) {
		if (this.temporaryStore != null) {
			this.temporaryStore.put(key, value);
			
		} else {
			this.store.put(key, value);
		}
	}
	
	public T get(String key) {
		if (this.temporaryStore != null) {
			return this.temporaryStore.get(key);
			
		} else {
			return this.store.get(key);
		}
	}
	
	public void remove(String key) {
		if (this.temporaryStore != null) {
			this.temporaryStore.remove(key);
			
		} else {
			this.store.remove(key);
		}
	}
	
	public void beginWriteTransaction() {
		this.temporaryStore = new MemoryKeyValueStore<T>();
	}
	
	public void commitWriteTranaction() {
		final AtomicInteger counter = new AtomicInteger(1);
		
		final CountDownLatch latch = new CountDownLatch(this.temporaryStore.getKeys().size());
		
		final int count = this.temporaryStore.getKeys().size();
		for (final String key : this.temporaryStore.getKeys()) {
			this.executorService.execute(new Runnable() {
				public void run() {
					try {
						logger.info("Writing key " + counter.getAndIncrement() + " of " + count);
						
						store.put(key, temporaryStore.get(key));
						
					} catch (Throwable t) {
						exceptionHandler.handleException(t);
						
					} finally {
						latch.countDown();
					}
				}
			});
		}
		
		try {
			latch.await();
			
		} catch (InterruptedException ie) {
			this.exceptionHandler.handleException(ie);
		}
		
		this.temporaryStore = null;
	}
	
	public void setStore(KeyValueStore<T> store) {
		this.store = store;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
}