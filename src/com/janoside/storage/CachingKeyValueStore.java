package com.janoside.storage;

import java.util.Set;

import com.janoside.cache.ObjectCache;
import com.janoside.collections.LruSet;
import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;

public class CachingKeyValueStore<T> implements KeyValueStore<T>, ExceptionHandlerAware {
	
	private KeyValueStore<T> store;
	
	private ObjectCache<T> cache;
	
	private Set<String> emptyKeysSet;
	
	private ExceptionHandler exceptionHandler;
	
	private long cacheKeyLifespan;
	
	private boolean cacheEnabled;
	
	private boolean cacheOnPut;
	
	private boolean cacheOnGet;
	
	public CachingKeyValueStore() {
		this.emptyKeysSet = new LruSet<String>();
		this.cacheEnabled = true;
		this.cacheOnPut = true;
		this.cacheOnGet = true;
	}
	
	public void put(String key, T value) {
		this.store.put(key, value);
		
		if (this.cacheEnabled && this.cacheOnPut) {
			try {
				if (this.emptyKeysSet.contains(key)) {
					this.emptyKeysSet.remove(key);
				}
				
				if (this.cacheKeyLifespan > 0) {
					this.cache.put(
							key,
							value,
							this.cacheKeyLifespan);
					
				} else {
					this.cache.put(
							key,
							value);
				}
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
		}
	}
	
	public T get(String key) {
		if (this.cacheEnabled && this.cacheOnGet) {
			T value = null;
			
			try {
				value = this.cache.get(key);
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
			
			if (value == null) {
				try {
					if (this.emptyKeysSet.contains(key)) {
						return null;
					}
				} catch (Throwable t) {
					this.exceptionHandler.handleException(t);
				}
				
				value = this.store.get(key);
				
				if (value != null) {
					try {
						if (this.cacheKeyLifespan > 0) {
							this.cache.put(
									key,
									value,
									this.cacheKeyLifespan);
							
						} else {
							this.cache.put(
									key,
									value);
						}
					} catch (Throwable t) {
						this.exceptionHandler.handleException(t);
					}
				} else {
					this.emptyKeysSet.add(key);
				}
			} else if (this.emptyKeysSet.contains(key)) {
				this.emptyKeysSet.remove(key);
			}
			
			return value;
			
		} else {
			return this.store.get(key);
		}
	}
	
	public void remove(String key) {
		this.store.remove(key);
		
		if (this.cacheEnabled) {
			try {
				this.cache.remove(key);
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
			
			this.emptyKeysSet.remove(key);
		}
	}
	
	public void setStore(KeyValueStore<T> store) {
		this.store = store;
	}
	
	public void setCache(ObjectCache<T> cache) {
		this.cache = cache;
	}
	
	public void setEmptyKeysSet(Set<String> emptyKeysSet) {
		this.emptyKeysSet = emptyKeysSet;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setCacheKeyLifespan(long cacheKeyLifespan) {
		this.cacheKeyLifespan = cacheKeyLifespan;
	}
	
	public boolean isCacheEnabled() {
		return this.cacheEnabled;
	}
	
	public void setCacheEnabled(boolean cacheEnabled) {
		this.cacheEnabled = cacheEnabled;
	}
	
	public boolean isCacheOnPut() {
		return this.cacheOnPut;
	}
	
	public void setCacheOnPut(boolean cacheOnPut) {
		this.cacheOnPut = cacheOnPut;
	}
	
	public boolean isCacheOnGet() {
		return this.cacheOnGet;
	}
	
	public void setCacheOnGet(boolean cacheOnGet) {
		this.cacheOnGet = cacheOnGet;
	}
}