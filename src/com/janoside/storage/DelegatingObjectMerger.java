package com.janoside.storage;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@SuppressWarnings("unchecked")
public class DelegatingObjectMerger implements ObjectMerger {

	private Map<String, List<ObjectMerger>> objectMergerMap;
	
	public DelegatingObjectMerger() {
		this.objectMergerMap = new ConcurrentHashMap<String, List<ObjectMerger>>();
	}
	
	public Object mergeObjects(Object oldObject, Object newObject) {
		Object mergedObject = newObject;
		String objectName = newObject.getClass().getName();
		
		if (this.objectMergerMap.containsKey(objectName)) {
			List<ObjectMerger> objectMergers = this.objectMergerMap.get(objectName);
			
			for (ObjectMerger objectMerger : objectMergers) {
				mergedObject = objectMerger.mergeObjects(oldObject, mergedObject);
			}
		}
		
		return mergedObject;
	}
	
	public void setObjectMergerMap(Map<String, List<ObjectMerger>> objectMergerMap) {
		this.objectMergerMap = objectMergerMap;
	}
}
