package com.janoside.storage;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.janoside.cache.ObjectCache;
import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.status.StatusService;
import com.janoside.status.StatusServiceAware;
import com.janoside.transaction.TransactionObserver;

public class TransactionCachingKeyValueStore<T> implements KeyValueStore<T>, TransactionObserver, StatusServiceAware, ExceptionHandlerAware {
	
	private static final Logger logger = LoggerFactory.getLogger(TransactionCachingKeyValueStore.class);
	
	private KeyValueStore<T> store;
	
	private ObjectCache<T> cache;
	
	private StatusService statusService;
	
	private Set<String> dirtyKeys;
	
	private Set<String> removedKeys;
	
	private ExecutorService executorService;
	
	private ExceptionHandler exceptionHandler;
	
	private Pattern transactionNamePattern;
	
	private AtomicInteger cachedPutCounter;
	
	private AtomicInteger cachedGetCounter;
	
	private AtomicInteger cachedRemoveCounter;
	
	private boolean active;
	
	public TransactionCachingKeyValueStore() {
		this.executorService = Executors.newFixedThreadPool(25);
		this.transactionNamePattern = Pattern.compile("", Pattern.CASE_INSENSITIVE);
		this.dirtyKeys = Collections.synchronizedSet(new HashSet<String>());
		this.removedKeys = Collections.synchronizedSet(new HashSet<String>());
		this.cachedPutCounter = new AtomicInteger(0);
		this.cachedGetCounter = new AtomicInteger(0);
		this.cachedRemoveCounter = new AtomicInteger(0);
		this.active = false;
	}
	
	public void put(String key, T value) {
		if (this.active) {
			this.cache.put(key, value);
			
			this.dirtyKeys.add(key);
			this.cachedPutCounter.getAndIncrement();
			
		} else {
			this.store.put(key, value);
		}
	}
	
	public T get(String key) {
		if (this.active) {
			T value = this.cache.get(key);
			
			if (value != null) {
				this.cachedGetCounter.getAndIncrement();
				
				return value;
				
			} else {
				return this.store.get(key);
			}
		} else {
			return this.store.get(key);
		}
	}
	
	public void remove(String key) {
		if (this.active) {
			this.cache.remove(key);
			
			this.removedKeys.add(key);
			this.cachedRemoveCounter.getAndIncrement();
			
		} else {
			this.store.remove(key);
		}
	}
	
	public void onTransactionBegun(String name) {
		if (this.transactionNamePattern.matcher(name).matches()) {
			this.cachedPutCounter.getAndSet(0);
			this.cachedGetCounter.getAndSet(0);
			this.cachedRemoveCounter.getAndSet(0);
			
			this.active = true;
		}
	}
	
	public void onTransactionCommitted(String name) {
		if (this.transactionNamePattern.matcher(name).matches()) {
			logger.info("Committing transaction: store=" + this.store + ", tranaction=" + name);
			
			final String actionName = "Transaction(" + name + ").commit";
			
			HashSet<String> tempDirtyKeys = new HashSet<String>(this.dirtyKeys);
			this.dirtyKeys.clear();
			
			HashSet<String> tempRemovedKeys = new HashSet<String>(this.removedKeys);
			this.removedKeys.clear();
			
			this.statusService.startAction(actionName, tempDirtyKeys.size() + tempRemovedKeys.size());
			
			final AtomicInteger dirtyCounter = new AtomicInteger(0);
			
			final CountDownLatch latch = new CountDownLatch(tempDirtyKeys.size() + tempRemovedKeys.size());
			
			final int dirtyCount = tempDirtyKeys.size();
			for (final String key : tempDirtyKeys) {
				this.executorService.execute(new Runnable() {
					public void run() {
						try {
							logger.trace("Updating key " + dirtyCounter.getAndIncrement() + " of " + dirtyCount);
							
							store.put(key, cache.get(key));
							
						} catch (Throwable t) {
							exceptionHandler.handleException(t);
							
						} finally {
							latch.countDown();
							
							statusService.onItemCompleted(actionName);
						}
					}
				});
			}
			
			final AtomicInteger removedCounter = new AtomicInteger(0);
			
			final int removedCount = tempRemovedKeys.size();
			for (final String key : tempRemovedKeys) {
				this.executorService.execute(new Runnable() {
					public void run() {
						try {
							logger.trace("Removing key " + removedCounter.getAndIncrement() + " of " + removedCount);
							
							store.remove(key);
							
						} catch (Throwable t) {
							exceptionHandler.handleException(t);
							
						} finally {
							latch.countDown();
							
							statusService.onItemCompleted(actionName);
						}
					}
				});
			}
			
			try {
				latch.await();
				
			} catch (InterruptedException ie) {
				this.exceptionHandler.handleException(ie);
			}
			
			logger.info("Committed transaction: store=" + this.store + ", tranaction=" + name + ", updated=" + dirtyCounter.get() + ", removed=" + removedCounter.get() + ", cached-get=" + this.cachedGetCounter.get() + ", cached-put=" + this.cachedPutCounter.get() + ", cached-remove=" + this.cachedRemoveCounter.get());
			
			this.active = false;
		}
	}
	
	public void setStore(KeyValueStore<T> store) {
		this.store = store;
	}
	
	public void setCache(ObjectCache<T> cache) {
		this.cache = cache;
	}
	
	public void setStatusService(StatusService statusService) {
		this.statusService = statusService;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setTransactionNamePatternString(String transactionNamePatternString) {
		this.transactionNamePattern = Pattern.compile(transactionNamePatternString, Pattern.CASE_INSENSITIVE);
	}
	
	public void setThreadCount(int threadCount) {
		this.executorService = Executors.newFixedThreadPool(threadCount);
	}
	
	public String toString() {
		if (this.store != null) {
			return this.store.toString();
			
		} else {
			return super.toString();
		}
	}
	
	public int hashCode() {
		return super.hashCode();
	}
	
	public boolean equals(Object o) {
		return this == o;
	}
}