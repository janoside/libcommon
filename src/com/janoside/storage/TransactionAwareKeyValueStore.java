package com.janoside.storage;

import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.transaction.TransactionObserver;

public class TransactionAwareKeyValueStore<T> implements KeyValueStore<T>, TransactionObserver, ExceptionHandlerAware {
	
	private static final Logger logger = LoggerFactory.getLogger(TransactionAwareKeyValueStore.class);
	
	private KeyValueStore<T> store;
	
	private MemoryKeyValueStore<T> temporaryStore;
	
	private ExecutorService executorService;
	
	private ExceptionHandler exceptionHandler;
	
	private Pattern transactionNamePattern;
	
	private boolean activeOnRead;
	
	public TransactionAwareKeyValueStore() {
		this.executorService = Executors.newFixedThreadPool(100);
		this.transactionNamePattern = Pattern.compile(".*", Pattern.CASE_INSENSITIVE);
		this.activeOnRead = true;
	}
	
	public void put(String key, T value) {
		if (this.temporaryStore != null) {
			this.temporaryStore.put(key, value);
			
		} else {
			this.store.put(key, value);
		}
	}
	
	public T get(String key) {
		if (this.activeOnRead && this.temporaryStore != null) {
			return this.temporaryStore.get(key);
			
		} else {
			return this.store.get(key);
		}
	}
	
	public void remove(String key) {
		if (this.temporaryStore != null) {
			this.temporaryStore.remove(key);
			
		} else {
			this.store.remove(key);
		}
	}
	
	public void onTransactionBegun(String name) {
		if (this.transactionNamePattern.matcher(name).matches()) {
			this.temporaryStore = new MemoryKeyValueStore<T>();
		}
	}
	
	public void onTransactionCommitted(String name) {
		if (this.transactionNamePattern.matcher(name).matches()) {
			final AtomicInteger counter = new AtomicInteger(1);
			
			Set<String> keys = this.temporaryStore.getKeys();
			
			final CountDownLatch latch = new CountDownLatch(keys.size());
			
			final int count = this.temporaryStore.getKeys().size();
			for (final String key : keys) {
				this.executorService.execute(new Runnable() {
					public void run() {
						try {
							logger.info("Writing key " + counter.getAndIncrement() + " of " + count);
							
							store.put(key, temporaryStore.get(key));
							
						} catch (Throwable t) {
							exceptionHandler.handleException(t);
							
						} finally {
							latch.countDown();
						}
					}
				});
			}
			
			try {
				latch.await();
				
			} catch (InterruptedException ie) {
				this.exceptionHandler.handleException(ie);
			}
			
			this.temporaryStore = null;
		}
	}
	
	public void setStore(KeyValueStore<T> store) {
		this.store = store;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setTransactionNamePatternString(String transactionNamePatternString) {
		this.transactionNamePattern = Pattern.compile(transactionNamePatternString, Pattern.CASE_INSENSITIVE);
	}
	
	public boolean isActiveOnRead() {
		return this.activeOnRead;
	}
	
	public void setActiveOnRead(boolean activeOnRead) {
		this.activeOnRead = activeOnRead;
	}
}