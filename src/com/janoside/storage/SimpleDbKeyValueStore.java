package com.janoside.storage;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.janoside.simpledb.SimpleDbConnection;
import com.janoside.stats.StatTracker;
import com.janoside.stats.StatTrackerAware;
import com.xerox.amazonws.sdb.Domain;
import com.xerox.amazonws.sdb.Item;
import com.xerox.amazonws.sdb.ItemAttribute;
import com.xerox.amazonws.sdb.ListDomainsResult;
import com.xerox.amazonws.sdb.SDBException;

public class SimpleDbKeyValueStore<T> implements KeyValueStore<T>, StatTrackerAware {
	
	private static final Gson gson = new Gson();
	
	private static final Logger logger = LoggerFactory.getLogger(SimpleDbKeyValueStore.class);
	
	private SimpleDbConnection simpleDbConnection;
	
	private Domain domain;
	
	private StatTracker statTracker;
	
	private String domainName;

	public void put(String key, T value) {
		try {
			this.statTracker.trackThreadPerformanceStart("simpledb.put");
			
			ArrayList<ItemAttribute> itemAttributes = new ArrayList<ItemAttribute>(2);
			itemAttributes.add(new ItemAttribute("json", gson.toJson(value), true));
			itemAttributes.add(new ItemAttribute("type", value.getClass().getName(), true));
			
			Item item = this.domain.getItem(key);
			item.putAttributes(itemAttributes);
			
		} catch (SDBException e) {
			logger.error("Error setting key/value in SimpleDB, key=" + key, e);
			
			throw new RuntimeException("Error setting key/value in SimpleDB", e);
			
		} finally {
			this.statTracker.trackThreadPerformanceEnd("simpledb.put");
		}
	}
	
	public void remove(String key) {
		try {
			this.statTracker.trackThreadPerformanceStart("simpledb.delete");
			
			this.domain.deleteItem(key);
			
		} catch (SDBException e) {
			logger.error("Error removing key from SimpleDB, key=" + key, e);
			
			throw new RuntimeException("Error removing key from SimpleDB", e);
			
		} finally {
			this.statTracker.trackThreadPerformanceEnd("simpledb.delete");
		}
	}
	
	public T get(String key) {
		try {
			this.statTracker.trackThreadPerformanceStart("simpledb.get");
			
			Item item = this.domain.getItem(key);
			
			String json = null;
			String type = null;
			
			for (ItemAttribute itemAttribute : item.getAttributes()) {
				if (itemAttribute.getName().equals("json")) {
					json = itemAttribute.getValue();
					
				} else if (itemAttribute.getName().equals("type")) {
					type = itemAttribute.getValue();
				}
			}
			
			if ((json == null) || (type == null)) {
				return null;
				
			} else {
				return (T) gson.fromJson(json, Class.forName(type));
			}
		} catch (Throwable t) {
			logger.error("Error getting key from SimpleDB, key=" + key, t);
			
			throw new RuntimeException("Error getting key from SimpleDB", t);
			
		} finally {
			this.statTracker.trackThreadPerformanceEnd("simpledb.get");
		}
	}
	
	public void setSimpleDbConnection(SimpleDbConnection simpleDbConnection) {
		this.simpleDbConnection = simpleDbConnection;
	}
	
	public void setStatTracker(StatTracker statTracker) {
		this.statTracker = statTracker;
	}
	
	public void setDomainName(String domainName) {
		if ((this.domainName == null) || !this.domainName.equals(domainName)) {
			try {
				boolean domainExists = false;
				ListDomainsResult listDomainsResult = this.simpleDbConnection.getSimpleDB().listDomains();

				for (Domain domain : listDomainsResult.getDomainList()) {
					if (domain.getName().equals(domainName)) {
						this.domain = domain;
						domainExists = true;
					}
				}
				
				if (!domainExists) {
					this.domain = this.simpleDbConnection.getSimpleDB().createDomain(domainName);
				}
			} catch (SDBException e) {
				throw new RuntimeException("Error setting SimpleDB domain", e);
			}
		}
		
		this.domainName = domainName;
	}
}