package com.janoside.storage;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.json.JsonObject;
import com.janoside.keyvalue.KeyValueList;
import com.janoside.status.StatusService;
import com.janoside.status.StatusServiceAware;
import com.janoside.thread.CountdownLatchJoinable;
import com.janoside.thread.Joinable;
import com.janoside.transaction.TransactionObserver;

public class PersistentTransactionWritingKeyValueStore<T> implements KeyValueStore<T>, TransactionObserver, StatusServiceAware, ExceptionHandlerAware {
	
	private static final Logger logger = LoggerFactory.getLogger(PersistentTransactionWritingKeyValueStore.class);
	
	private KeyValueStore<T> store;
	
	private KeyValueStore<T> transactionStore;
	
	private StatusService statusService;
	
	private KeyValueList dirtyKeys;
	
	private KeyValueList removedKeys;
	
	private ExecutorService executorService;
	
	private ExceptionHandler exceptionHandler;
	
	private Joinable commitJoinable;
	
	private Pattern transactionNamePattern;
	
	private boolean active;
	
	public PersistentTransactionWritingKeyValueStore() {
		this.executorService = Executors.newFixedThreadPool(25);
		this.transactionNamePattern = Pattern.compile(".*", Pattern.CASE_INSENSITIVE);
		this.active = false;
	}
	
	public void put(String key, T value) {
		if (this.commitJoinable != null) {
			this.commitJoinable.join();
		}
		
		if (this.active) {
			this.transactionStore.put(key, value);
			
			this.dirtyKeys.add(new JsonObject().put("key", key));
			
		} else {
			this.store.put(key, value);
		}
	}
	
	public T get(String key) {
		if (this.active) {
			T value = this.transactionStore.get(key);
			
			if (value != null) {
				return value;
				
			} else {
				return this.store.get(key);
			}
		} else {
			return this.store.get(key);
		}
	}
	
	public void remove(String key) {
		if (this.commitJoinable != null) {
			this.commitJoinable.join();
		}
		
		if (this.active) {
			this.transactionStore.remove(key);
			
			this.removedKeys.add(new JsonObject().put("key", key));
			
		} else {
			this.store.remove(key);
		}
	}
	
	public void onTransactionBegun(String name) {
		if (this.transactionNamePattern.matcher(name).matches()) {
			this.active = true;
		}
	}
	
	public void onTransactionCommitted(String name) {
		if (this.transactionNamePattern.matcher(name).matches()) {
			CountDownLatch latch = new CountDownLatch(1);
			
			this.commitJoinable = new CountdownLatchJoinable(latch);
			
			try {
				ArrayList<String> dirtyKeysBatch = new ArrayList<String>();
				for (JsonObject json : this.dirtyKeys) {
					dirtyKeysBatch.add(json.getString("key"));
					
					if (dirtyKeysBatch.size() == 1000) {
						this.updateDirtyKeys(dirtyKeysBatch);
						
						dirtyKeysBatch.clear();
					}
				}
				
				this.updateDirtyKeys(dirtyKeysBatch);
				
				ArrayList<String> removedKeysBatch = new ArrayList<String>();
				for (JsonObject json : this.removedKeys) {
					removedKeysBatch.add(json.getString("key"));
					
					if (removedKeysBatch.size() == 1000) {
						this.removeKeys(removedKeysBatch);
						
						removedKeysBatch.clear();
					}
				}
				
				this.removeKeys(removedKeysBatch);
				
				this.active = false;
				
			} finally {
				latch.countDown();
			}
		}
	}
	
	private void updateDirtyKeys(List<String> dirtyKeysList) {
		final CountDownLatch latch = new CountDownLatch(dirtyKeysList.size());
		
		try {
			for (final String key : dirtyKeysList) {
				this.executorService.execute(new Runnable() {
					public void run() {
						try {
							store.put(key, transactionStore.get(key));
							
						} catch (Throwable t) {
							exceptionHandler.handleException(t);
							
						} finally {
							latch.countDown();
						}
					}
				});
			}
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
		}
	}
	
	private void removeKeys(List<String> removedKeys) {
		final CountDownLatch latch = new CountDownLatch(removedKeys.size());
		
		try {
			for (final String key : removedKeys) {
				this.executorService.execute(new Runnable() {
					public void run() {
						try {
							store.remove(key);
							
						} catch (Throwable t) {
							exceptionHandler.handleException(t);
							
						} finally {
							latch.countDown();
						}
					}
				});
			}
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
		}
	}
	
	public void setStore(KeyValueStore<T> store) {
		this.store = store;
	}
	
	public void setTransactionStore(KeyValueStore<T> transactionStore) {
		this.transactionStore = transactionStore;
	}
	
	public void setStatusService(StatusService statusService) {
		this.statusService = statusService;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setTransactionNamePatternString(String transactionNamePatternString) {
		this.transactionNamePattern = Pattern.compile(transactionNamePatternString, Pattern.CASE_INSENSITIVE);
	}
}