package com.janoside.storage;

import com.janoside.json.JsonObject;

public class JsonKeyValueStore implements KeyValueStore<JsonObject> {
	
	private KeyValueStore<String> stringStore;
	
	public void put(String key, JsonObject value) {
		this.stringStore.put(key, value.toString());
	}
	
	public JsonObject get(String key) {
		String string = this.stringStore.get(key);
		
		if (string == null) {
			return null;
			
		} else {
			return new JsonObject(string);
		}
	}
	
	public void remove(String key) {
		this.stringStore.remove(key);
	}
	
	public void setStringStore(KeyValueStore<String> stringStore) {
		this.stringStore = stringStore;
	}
}