package com.janoside.storage;

public class ReadWriteSplitKeyValueStore<T> implements KeyValueStore<T> {
	
	private KeyValueStore<T> readStore;
	
	private KeyValueStore<T> writeStore;
	
	public void put(String key, T value) {
		this.writeStore.put(key, value);
	}
	
	public T get(String key) {
		return this.readStore.get(key);
	}
	
	public void remove(String key) {
		this.writeStore.remove(key);
	}
	
	public void setReadStore(KeyValueStore<T> readStore) {
		this.readStore = readStore;
	}
	
	public void setWriteStore(KeyValueStore<T> writeStore) {
		this.writeStore = writeStore;
	}
}