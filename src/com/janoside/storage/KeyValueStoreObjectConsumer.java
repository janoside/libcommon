package com.janoside.storage;

import java.util.ArrayList;
import java.util.List;

import com.janoside.queue.ObjectConsumer;
import com.janoside.transform.ObjectTransformer;

public class KeyValueStoreObjectConsumer<T> implements ObjectConsumer<T> {

	private ObjectTransformer<T, String> objectKeyTransformer;
	
	private KeyValueStore<T> keyValueStore;
	
	private List<ObjectMerger<T>> objectMergers;
	
	public KeyValueStoreObjectConsumer() {
		this.objectMergers = new ArrayList<ObjectMerger<T>>();
	}
	
	public void consume(T object) {
		String key = this.objectKeyTransformer.transform(object);
		T objectToPut = object;
		
		if (!this.objectMergers.isEmpty()) {
			T existingObject = this.keyValueStore.get(key);
			
			if (existingObject != null) {
				for (ObjectMerger<T> objectMerger : this.objectMergers) {
					objectToPut = objectMerger.mergeObjects(existingObject, objectToPut);
				}
			}
		}
		
		this.keyValueStore.put(key, objectToPut);
	}

	public void setObjectKeyTransformer(ObjectTransformer<T, String> objectKeyTransformer) {
		this.objectKeyTransformer = objectKeyTransformer;
	}
	
	public void setKeyValueStore(KeyValueStore<T> keyValueStore) {
		this.keyValueStore = keyValueStore;
	}
	
	public void setObjectMergers(List<ObjectMerger<T>> objectMergers) {
		this.objectMergers = objectMergers;
	}
}
