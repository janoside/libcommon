package com.janoside.storage;

import java.util.Date;

import com.janoside.util.DateUtil;

/**
 * Implementation of KeyValueStore that automatically backs up previous
 * versions of keys when they are overwritten or removed.
 * 
 * @author janoside
 * @param <T>
 */
public class VersioningKeyValueStore<T> implements KeyValueStore<T> {
	
	private KeyValueStore<T> store;
	
	private boolean backupsActive;
	
	public VersioningKeyValueStore() {
		this.backupsActive = true;
	}
	
	public void put(String key, T value) {
		if (this.backupsActive) {
			T previousValue = this.store.get(key);
			
			if (previousValue != null) {
				this.store.put(key + "-" + DateUtil.format("yyyy-MM-dd HH:mm:ss", new Date()), previousValue);
			}
		}
		
		this.store.put(key, value);
	}
	
	public T get(String key) {
		return this.store.get(key);
	}
	
	public void remove(String key) {
		if (this.backupsActive) {
			T previousValue = this.store.get(key);
			
			if (previousValue != null) {
				this.store.put(key + "-" + DateUtil.format("yyyy-MM-dd HH:mm:ss", new Date()), previousValue);
			}
		}
		
		this.store.remove(key);
	}
	
	public void setStore(KeyValueStore<T> store) {
		this.store = store;
	}
	
	public void setBackupsActive(boolean backupsActive) {
		this.backupsActive = backupsActive;
	}
}