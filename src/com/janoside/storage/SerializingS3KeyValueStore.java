package com.janoside.storage;

import java.io.InputStream;
import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.s3.S3Bucket;
import com.janoside.serialization.ObjectStreamByteArraySerializer;
import com.janoside.serialization.Serializer;
import com.janoside.stats.StatTracker;
import com.janoside.stats.StatTrackerAware;
import com.janoside.util.MimeUtil;
import com.janoside.util.StreamUtil;

public class SerializingS3KeyValueStore<T extends Serializable> implements KeyValueStore<T>, ExceptionHandlerAware, StatTrackerAware {
	
	private static final Logger logger = LoggerFactory.getLogger(SerializingS3KeyValueStore.class);
	
	private S3Bucket s3Bucket;
	
	private Serializer<T, byte[]> serializer;
	
	private ExceptionHandler exceptionHandler;
	
	private StatTracker statTracker;
	
	public SerializingS3KeyValueStore() {
		ObjectStreamByteArraySerializer<T> serializer = new ObjectStreamByteArraySerializer<T>();
		serializer.setExceptionHandler(new ExceptionHandler() {
			public void handleException(Throwable t) {
				exceptionHandler.handleException(t);
			}
		});
		
		this.serializer = serializer;
	}
	
	public void put(String key, T value) {
		String mimeType = MimeUtil.getMimeType(value);
		
		byte[] data = this.serializer.serialize(value);
		
		logger.trace("S3 Operation - Upload: bucket=" + this.s3Bucket.getName() + ", key=" + key + ", value.length=" + data.length);
		
		this.s3Bucket.uploadFile(
				key,
				data,
				false,
				mimeType);
		
		this.statTracker.trackEvent("s3.put");
	}
	
	@SuppressWarnings("unchecked")
	public T get(String key) {
		try {
			logger.trace("S3 Operation - Download: bucket=" + this.s3Bucket.getName() + ", key=" + key);
			
			InputStream inputStream = this.s3Bucket.getFileContents(key);
			
			if (inputStream == null) {
				return null;
			}
			
			byte[] data = StreamUtil.streamToByteArray(inputStream);
			
			T value = this.serializer.deserialize(data);
			
			this.statTracker.trackEvent("s3.get");
			
			return value;
			
		} catch (Throwable t) {
			throw new RuntimeException("Failed to read from S3", t);
		}
	}
	
	public void remove(String key) {
		logger.trace("S3 Operation - Delete: bucket=" + this.s3Bucket.getName() + ", key=" + key);
		
		this.s3Bucket.deleteFile(key);
		
		this.statTracker.trackEvent("s3.delete");
	}
	
	public void handleException(Throwable t) {
		this.exceptionHandler.handleException(t);
	}
	
	public void setS3Bucket(S3Bucket s3Bucket) {
		this.s3Bucket = s3Bucket;
	}
	
	public void setSerializer(Serializer<T, byte[]> serializer) {
		this.serializer = serializer;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setStatTracker(StatTracker statTracker) {
		this.statTracker = statTracker;
	}
	
	public String toString() {
		if (this.s3Bucket != null) {
			return "S3Store(" + this.s3Bucket.getName() + ")";
			
		} else {
			return super.toString();
		}
	}
	
	public int hashCode() {
		return super.hashCode();
	}
	
	public boolean equals(Object o) {
		return this == o;
	}
}