package com.janoside.storage;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.janoside.cache.ObjectCache;
import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;

public class BufferCachingKeyValueStore<T> implements KeyValueStore<T>, ExceptionHandlerAware {
	
	private static final Logger logger = LoggerFactory.getLogger(BufferCachingKeyValueStore.class);
	
	private KeyValueStore<T> store;
	
	private ObjectCache<T> cache;
	
	private ReentrantReadWriteLock lock;
	
	private Set<String> dirtyKeys;
	
	private Set<String> removedKeys;
	
	private ExecutorService executorService;
	
	private ExceptionHandler exceptionHandler;
	
	private Timer timer;
	
	private Future commitFuture;
	
	private long maxBufferAge;
	
	private long bufferStartTime;
	
	private int bufferMaxItemCount;
	
	public BufferCachingKeyValueStore() {
		this.lock = new ReentrantReadWriteLock();
		this.executorService = Executors.newFixedThreadPool(50);
		this.dirtyKeys = Collections.synchronizedSet(new HashSet<String>());
		this.removedKeys = Collections.synchronizedSet(new HashSet<String>());
		this.maxBufferAge = 30000;
		this.bufferMaxItemCount = 1000;
		
		this.timer = new Timer();
		this.timer.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				if ((System.currentTimeMillis() - bufferStartTime) >= maxBufferAge) {
					commitBuffer();
				}
			}
		}, this.maxBufferAge, this.maxBufferAge);
	}
	
	public void put(String key, T value) {
		this.lock.writeLock().lock();
		
		try {
			this.cache.put(key, value);
			
			if (this.dirtyKeys.isEmpty() && this.removedKeys.isEmpty() && this.bufferStartTime == 0) {
				this.bufferStartTime = System.currentTimeMillis();
			}
			
			this.dirtyKeys.add(key);
			
			if ((this.dirtyKeys.size() + this.removedKeys.size()) >= this.bufferMaxItemCount) {
				this.commitBufferAsync();
			}
		} finally {
			this.lock.writeLock().unlock();
		}
	}
	
	public T get(String key) {
		this.lock.readLock().lock();
		
		try {
			T result = this.cache.get(key);
			
			if (result != null) {
				return result;
				
			} else {
				return this.store.get(key);
			}
		} finally {
			this.lock.readLock().unlock();
		}
	}
	
	public void remove(String key) {
		this.lock.writeLock().lock();
		
		try {
			this.cache.remove(key);
			
			if (this.dirtyKeys.isEmpty() && this.removedKeys.isEmpty() && this.bufferStartTime == 0) {
				this.bufferStartTime = System.currentTimeMillis();
			}
			
			this.removedKeys.add(key);
			
			if ((this.dirtyKeys.size() + this.removedKeys.size()) >= this.bufferMaxItemCount) {
				this.commitBufferAsync();
			}
		} finally {
			this.lock.writeLock().unlock();
		}
	}
	
	private void commitBufferAsync() {
		this.commitFuture = this.executorService.submit(new Runnable() {
			public void run() {
				try {
					commitBuffer();
					
				} catch (Throwable t) {
					exceptionHandler.handleException(t);
				}
			}
		});
	}
	
	private void commitBuffer() {
		this.lock.writeLock().lock();
		
		try {
			HashSet<String> tempDirtyKeys = new HashSet<String>();
			tempDirtyKeys.addAll(this.dirtyKeys);
			
			this.dirtyKeys.clear();
			
			HashSet<String> tempRemovedKeys = new HashSet<String>();
			tempRemovedKeys.addAll(this.removedKeys);
			
			this.removedKeys.clear();
			
			final AtomicInteger dirtyCounter = new AtomicInteger(1);
			
			final CountDownLatch latch = new CountDownLatch(tempDirtyKeys.size() + tempRemovedKeys.size());
			
			final int dirtyCount = tempDirtyKeys.size();
			for (final String key : tempDirtyKeys) {
				this.executorService.execute(new Runnable() {
					public void run() {
						try {
							logger.info("Updating key " + dirtyCounter.getAndIncrement() + " of " + dirtyCount);
							
							store.put(key, cache.get(key));
							
						} catch (Throwable t) {
							exceptionHandler.handleException(t);
							
						} finally {
							latch.countDown();
						}
					}
				});
			}
			
			final AtomicInteger removedCounter = new AtomicInteger(1);
			
			final int removedCount = tempRemovedKeys.size();
			for (final String key : tempRemovedKeys) {
				this.executorService.execute(new Runnable() {
					public void run() {
						try {
							logger.info("Removing key " + removedCounter.getAndIncrement() + " of " + removedCount);
							
							store.remove(key);
							
						} catch (Throwable t) {
							exceptionHandler.handleException(t);
							
						} finally {
							latch.countDown();
						}
					}
				});
			}
			
			try {
				latch.await();
				
			} catch (InterruptedException ie) {
				this.exceptionHandler.handleException(ie);
			}
			
			this.bufferStartTime = 0;
			
		} finally {
			this.lock.writeLock().unlock();
		}
	}
	
	public void waitForCommit() {
		try {
			this.commitFuture.get();
			
		} catch (Throwable t) {
			throw new RuntimeException("Error while waiting for commit");
		}
	}
	
	public void setStore(KeyValueStore<T> store) {
		this.store = store;
	}
	
	public void setCache(ObjectCache<T> cache) {
		this.cache = cache;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public int getBufferMaxItemCount() {
		return this.bufferMaxItemCount;
	}
	
	public int getBufferItemCount() {
		return (this.dirtyKeys.size() + this.removedKeys.size());
	}
}