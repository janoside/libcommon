package com.janoside.storage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.health.ErrorTracker;
import com.janoside.health.HealthMonitor;
import com.janoside.health.SuccessTrackerHealthMonitor;
import com.janoside.stats.StatTracker;
import com.janoside.stats.StatTrackerAware;
import com.janoside.stats.SuccessTracker;
import com.janoside.text.TextTransformer;
import com.janoside.text.UnchangingTextTransformer;

@ManagedResource(objectName = "Janoside:name=ManagedKeyValueStore")
public class ManagedKeyValueStore<T> implements KeyValueStore<T>, HealthMonitor, StatTrackerAware {
	
	private static final Logger logger = LoggerFactory.getLogger(ManagedKeyValueStore.class);
	
	private KeyValueStore<T> store;
	
	private TextTransformer keyTransformer;
	
	private SuccessTracker successTracker;
	
	private SuccessTrackerHealthMonitor successTrackerHealthMonitor;
	
	private StatTracker statTracker;
	
	private String name;
	
	public ManagedKeyValueStore() {
		this.keyTransformer = new UnchangingTextTransformer();
		this.successTracker = new SuccessTracker();
		
		this.successTrackerHealthMonitor = new SuccessTrackerHealthMonitor();
		this.successTrackerHealthMonitor.setSuccessTracker(this.successTracker);
		this.successTrackerHealthMonitor.setErrorThreshold(0);
	}
	
	public void put(String key, T value) {
		this.statTracker.trackThreadPerformanceStart("store." + this.name + ".put");
		
		try {
			String transformedKey = this.keyTransformer.transform(key);
			
			this.store.put(transformedKey, value);
			
			this.successTracker.success();
			
		} catch (Throwable t) {
			this.successTracker.failure();
			
			logger.warn("ManagedKeyValueStore(" + this.name + ") failed PUT(key, value), key=" + key);
			
			throw new RuntimeException("ManagedKeyValueStore(" + this.name + ") failed PUT(key, value)", t);
			
		} finally {
			this.statTracker.trackThreadPerformanceEnd("store." + this.name + ".put");
		}
	}
	
	public T get(String key) {
		this.statTracker.trackThreadPerformanceStart("store." + this.name + ".get");
		
		try {
			String transformedKey = this.keyTransformer.transform(key);
			
			T value = this.store.get(transformedKey);
			
			this.successTracker.success();
			
			return value;
			
		} catch (Throwable t) {
			this.successTracker.failure();
			
			logger.warn("ManagedKeyValueStore(" + this.name + ") failed GET(key), key=" + key);
			
			throw new RuntimeException("ManagedKeyValueStore(" + this.name + ") failed GET(key)", t);
			
		} finally {
			this.statTracker.trackThreadPerformanceEnd("store." + this.name + ".get");
		}
	}
	
	public void remove(String key) {
		this.statTracker.trackThreadPerformanceStart("store." + this.name + ".remove");
		
		try {
			String transformedKey = this.keyTransformer.transform(key);
			
			this.store.remove(transformedKey);
			
			this.successTracker.success();
			
		} catch (Throwable t) {
			this.successTracker.failure();
			
			logger.warn("ManagedKeyValueStore(" + this.name + ") failed REMOVE(key), key=" + key);
			
			throw new RuntimeException("ManagedKeyValueStore(" + this.name + ") failed REMOVE(key)", t);
			
		} finally {
			this.statTracker.trackThreadPerformanceEnd("store." + this.name + ".remove");
		}
	}
	
	public void reportInternalErrors(ErrorTracker errorTracker) {
		this.successTrackerHealthMonitor.reportInternalErrors(errorTracker);
	}
	
	public void setStore(KeyValueStore<T> store) {
		this.store = store;
	}
	
	public void setKeyTransformer(TextTransformer keyTransformer) {
		this.keyTransformer = keyTransformer;
	}
	
	public void setStatTracker(StatTracker statTracker) {
		this.statTracker = statTracker;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}