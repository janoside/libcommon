package com.janoside.storage;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.janoside.json.JsonObject;

public class GenericJsonKeyValueStore<T> implements KeyValueStore<T> {

	private KeyValueStore<JsonObject> jsonKeyValueStore;
	
	private static final Gson gson = new Gson();
	
	public void put(String key, T value) {
		JsonObject jsonObject = new JsonObject();
		jsonObject.put("objectType", value.getClass().getName());
		jsonObject.put("object", new JsonObject(gson.toJson(value)));
		this.jsonKeyValueStore.put(key, jsonObject);
	}

	public void remove(String key) {
		this.jsonKeyValueStore.remove(key);
	}

	@SuppressWarnings("unchecked")
	public T get(String key) {
		JsonObject jsonObject = this.jsonKeyValueStore.get(key);
		if (jsonObject == null) {
			return null;
		}
		
		try {
			return (T) gson.fromJson(jsonObject.getJsonObject("object").toString(), Class.forName(jsonObject.getString("objectType")));
			
		} catch (JsonParseException e) {
			
			throw new RuntimeException("Failed to parse json string", e);
		} catch (ClassNotFoundException e) {
			
			throw new RuntimeException("Class not found", e);
		}
	}

	public void setJsonKeyValueStore(KeyValueStore<JsonObject> jsonKeyValueStore) {
		this.jsonKeyValueStore = jsonKeyValueStore;
	}
}
