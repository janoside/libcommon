package com.janoside.storage;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.stats.StatTracker;
import com.janoside.stats.StatTrackerAware;

@ManagedResource(objectName = "Janoside:name=TimingOutKeyValueStore")
public class TimingOutKeyValueStore<T> implements KeyValueStore<T>, StatTrackerAware {
	
	private static final Logger logger = LoggerFactory.getLogger(TimingOutKeyValueStore.class);
	
	private KeyValueStore<T> store;
	
	private ExecutorService executorService;
	
	private StatTracker statTracker;
	
	private String name;
	
	private long timeout;
	
	public TimingOutKeyValueStore() {
		this.executorService = Executors.newCachedThreadPool();
		this.timeout = 30000;
	}
	
	public void put(String key, T value) {
		this.store.put(key, value);
	}
	
	public T get(final String key) {
		Callable<T> task = new Callable<T>() {
			public T call() {
				return store.get(key);
			}
		};
		
		Future<T> future = this.executorService.submit(task);
		
		try {
			return future.get(this.timeout, TimeUnit.MILLISECONDS);
			
		} catch (TimeoutException te) {
			logger.warn("Timeout on KeyValueStore(" + this.name + "), key=" + key);
			
			this.statTracker.trackEvent("store." + this.name + ".timeout");
			
		} catch (InterruptedException ie) {
			throw new RuntimeException(ie);
			
		} catch (ExecutionException ee) {
			throw new RuntimeException(ee);
		}
		
		return null;
	}
	
	public void remove(String key) {
		this.store.remove(key);
	}
	
	public void setStore(KeyValueStore<T> store) {
		this.store = store;
	}
	
	public void setStatTracker(StatTracker statTracker) {
		this.statTracker = statTracker;
	}
	
	@ManagedAttribute
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@ManagedAttribute
	public long getTimeout() {
		return this.timeout;
	}
	
	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}
}