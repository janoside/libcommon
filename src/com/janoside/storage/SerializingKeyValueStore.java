package com.janoside.storage;

import com.janoside.transform.ObjectTransformer;

public class SerializingKeyValueStore<FromType, ToType> implements KeyValueStore<FromType> {
	
	private KeyValueStore<ToType> store;
	
	private ObjectTransformer<FromType, ToType> serializer;
	
	private ObjectTransformer<ToType, FromType> deserializer;
	
	public FromType get(String key) {
		return this.deserializer.transform(this.store.get(key));
	}
	
	public void put(String key, FromType value) {
		this.store.put(key, this.serializer.transform(value));
	}
	
	public void remove(String key) {
		this.store.remove(key);
	}
	
	public void setStore(KeyValueStore<ToType> store) {
		this.store = store;
	}
	
	public void setSerializer(ObjectTransformer<FromType, ToType> serializer) {
		this.serializer = serializer;
	}
	
	public void setDeserializer(ObjectTransformer<ToType, FromType> deserializer) {
		this.deserializer = deserializer;
	}
}