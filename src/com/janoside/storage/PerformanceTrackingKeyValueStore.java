package com.janoside.storage;

import com.janoside.stats.StatTracker;
import com.janoside.stats.StatTrackerAware;

public class PerformanceTrackingKeyValueStore<T> implements KeyValueStore<T>, StatTrackerAware {
	
	private KeyValueStore<T> store;
	
	private StatTracker statTracker;
	
	private String name;
	
	public void put(String key, T value) {
		try {
			this.statTracker.trackThreadPerformanceStart("store." + this.name + ".put");
			
			this.store.put(key, value);
			
		} finally {
			this.statTracker.trackThreadPerformanceEnd("store." + this.name + ".put");
		}
	}
	
	public T get(String key) {
		try {
			this.statTracker.trackThreadPerformanceStart("store." + this.name + ".get");
			
			return this.store.get(key);
			
		} finally {
			this.statTracker.trackThreadPerformanceEnd("store." + this.name + ".get");
		}
	}
	
	public void remove(String key) {
		try {
			this.statTracker.trackThreadPerformanceStart("store." + this.name + ".remove");
			
			this.store.remove(key);
			
		} finally {
			this.statTracker.trackThreadPerformanceEnd("store." + this.name + ".remove");
		}
	}
	
	public void setStore(KeyValueStore<T> store) {
		this.store = store;
	}
	
	public void setStatTracker(StatTracker statTracker) {
		this.statTracker = statTracker;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		if (this.store != null) {
			return this.store.toString();
			
		} else {
			return super.toString();
		}
	}
	
	public int hashCode() {
		return super.hashCode();
	}
	
	public boolean equals(Object o) {
		return this == o;
	}
}