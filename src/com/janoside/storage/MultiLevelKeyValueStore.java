package com.janoside.storage;

import java.util.ArrayList;
import java.util.List;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;

/**
 * KeyValueStore implementation that keeps a prioritized list of other KeyValueStores. The highest "priority"
 * stores are earlier in the list, and if an operation is successful using a higher-priority store the operation
 * is not attempted on an others.
 * 
 * This dynamic is generally designed to accomodate situations where operations on "cheaper" stores (either in terms
 * of performance or actual cost) can offset operations to "expensive" stores, but in which the expensive stores are
 * more reliable by some measure.
 * 
 * Note that over time, for non-volatile stores, this implementation will effectively migrate all data from
 * lower-priority stores into the highest-priority store.
 * 
 * @author janoside
 *
 * @param <T>
 */
public class MultiLevelKeyValueStore<T> implements KeyValueStore<T>, ExceptionHandlerAware {
	
	private ExceptionHandler exceptionHandler;
	
	private List<KeyValueStore<T>> stores;
	
	/**
	 * Iterates the internal list of KeyValueStores in order, until one of them returns a non-null
	 * value for the given key. The value is then copied upward into the higher-priority stores
	 * that did not return the value on first attempt, stopping when the first one successfully
	 * handles the put().
	 */
	public T get(final String key) {
		ArrayList<KeyValueStore<T>> storesToCopyTo = new ArrayList<KeyValueStore<T>>(this.stores.size());
		
		for (KeyValueStore<T> store : this.stores) {
			try {
				final T value = store.get(key);
				
				if (value != null) {
					for (KeyValueStore<T> storeToCopyTo : storesToCopyTo) {
						try {
							storeToCopyTo.put(key, value);
							
							break;
							
						} catch (Throwable t) {
							exceptionHandler.handleException(t);
						}
					}
					
					return value;
					
				} else {
					storesToCopyTo.add(store);
				}
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
		}
		
		return null;
	}
	
	public void put(String key, T value) {
		for (KeyValueStore<T> store : this.stores) {
			try {
				store.put(key, value);
				
				return;
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
		}
	}
	
	public void remove(String key) {
		for (KeyValueStore<T> store : this.stores) {
			try {
				store.remove(key);
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
		}
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setStores(List<KeyValueStore<T>> stores) {
		this.stores = new ArrayList<KeyValueStore<T>>(stores);
	}
}