package com.janoside.storage;

import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.data.JdbcDataSource;
import com.janoside.data.SimpleSqlParameter;

/**
 * KeyValueStore built on a database table of the following form:
 * 
 * ------------------------------------------------------------------------
 * | id | key | value | created_at | created_by | updated_at | updated_by |
 * ------------------------------------------------------------------------
 *
 */
@ManagedResource(objectName = "Janoside:name=MysqlKeyValueStore")
public class MysqlKeyValueStore implements KeyValueStore<String> {
	
	private JdbcDataSource dataSource;
	
	private DateFormat dateFormat;
	
	private String tableName;
	
	private String selectIdSql;
	
	private String selectValueSql;
	
	private String insertSql;
	
	private String updateSql;
	
	private String deleteSql;
	
	private int appId;
	
	private boolean storePreviousVersions;
	
	public MysqlKeyValueStore() {
		this.dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		this.appId = 0;
		this.storePreviousVersions = false;
	}
	
	@SuppressWarnings("serial")
	@ManagedOperation
	public void put(final String key, final String value) {
		final long existingId = this.getIdForKey(key);
		
		if (existingId == -1) {
			this.dataSource.executeUpdate(
					this.insertSql,
					new HashMap<Integer, SimpleSqlParameter>() {{
						put(1, new SimpleSqlParameter(key, Types.VARCHAR));
						put(2, new SimpleSqlParameter(value, Types.LONGVARCHAR));
					}});
			
		} else {
			if (this.storePreviousVersions) {
				final String previousValue = this.get(key);
				
				this.dataSource.executeUpdate(
						this.insertSql,
						new HashMap<Integer, SimpleSqlParameter>() {{
							put(1, new SimpleSqlParameter(key + "-" + dateFormat.format(new Date()), Types.VARCHAR));
							put(2, new SimpleSqlParameter(previousValue, Types.LONGVARCHAR));
						}});
			}
			
			this.dataSource.executeUpdate(
					this.updateSql,
					new HashMap<Integer, SimpleSqlParameter>() {{
						put(1, new SimpleSqlParameter(value, Types.LONGVARCHAR));
						put(2, new SimpleSqlParameter(existingId, Types.BIGINT));
					}});
		}
	}
	
	@SuppressWarnings("serial")
	@ManagedOperation
	public String get(final String key) {
		List<Object[]> data = this.dataSource.getData(
				this.selectValueSql,
				new HashMap<Integer, SimpleSqlParameter>() {{
					put(1, new SimpleSqlParameter(key, Types.VARCHAR));
				}});
		
		if (data == null || data.isEmpty()) {
			return null;
			
		} else {
			return data.get(0)[0].toString();
		}
	}
	
	@SuppressWarnings("serial")
	@ManagedOperation
	public void remove(final String key) {
		if (this.storePreviousVersions) {
			final String value = this.get(key);
			
			this.dataSource.executeUpdate(
					this.insertSql,
					new HashMap<Integer, SimpleSqlParameter>() {{
						put(1, new SimpleSqlParameter(key + "-" + dateFormat.format(new Date()), Types.VARCHAR));
						put(2, new SimpleSqlParameter(value, Types.LONGVARCHAR));
					}});
		}
		
		this.dataSource.executeUpdate(
				this.deleteSql,
				new HashMap<Integer, SimpleSqlParameter>() {{
					put(1, new SimpleSqlParameter(key, Types.VARCHAR));
				}});
	}
	
	@SuppressWarnings("serial")
	private long getIdForKey(final String key) {
		List<Object[]> data = this.dataSource.getData(
				this.selectIdSql,
				new HashMap<Integer, SimpleSqlParameter>() {{
					put(1, new SimpleSqlParameter(key, Types.VARCHAR));
				}});
		
		if (data == null || data.isEmpty()) {
			return -1;
			
		} else {
			return Long.parseLong(data.get(0)[0].toString());
		}
	}
	
	public void setDataSource(JdbcDataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	public void setTableName(String tableName) {
		this.tableName = tableName;
		this.selectIdSql = "select `id` from `" + this.tableName + "` where `key`=?;";
		this.selectValueSql = "select `value` from `" + this.tableName + "` where `key`=?;";
		this.insertSql = "insert into `" + this.tableName + "` values (null, ?, ?, now(), " + this.appId + ", now(), " + this.appId + ");";
		this.updateSql = "update `" + this.tableName + "` set `value`=?, `updated_at`=now(), `updated_by`=" + this.appId + " where `id`=?;";
		this.deleteSql = "delete from `" + this.tableName + "` where `key`=?;";
	}
	
	public void setStorePreviousVersions(boolean storePreviousVersions) {
		this.storePreviousVersions = storePreviousVersions;
	}
}