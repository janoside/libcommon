package com.janoside.s3;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.jets3t.service.S3Service;
import org.jets3t.service.S3ServiceException;
import org.jets3t.service.ServiceException;
import org.jets3t.service.StorageObjectsChunk;
import org.jets3t.service.acl.AccessControlList;
import org.jets3t.service.impl.rest.httpclient.RestS3Service;
import org.jets3t.service.model.S3Object;
import org.jets3t.service.model.StorageObject;
import org.jets3t.service.security.AWSCredentials;
import org.jets3t.service.utils.ServiceUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.StringUtils;

import com.janoside.security.encryption.Encryptor;
import com.janoside.security.encryption.EncryptorAware;

public class Jets3tS3Bucket implements S3Bucket, InitializingBean, EncryptorAware {
	
	private static final Logger logger = LoggerFactory.getLogger(Jets3tS3Bucket.class);
	
	private S3Service s3Service;
	
	private org.jets3t.service.model.S3Bucket s3Bucket;
	
	private Encryptor encryptor;
	
	private String accessKey;
	
	private String encryptedAccessKey;
	
	private String secretKey;
	
	private String encryptedSecretKey;
	
	private String bucketName;
	
	public void initialize() {
		try {
			if (!StringUtils.hasText(this.accessKey) && StringUtils.hasText(this.encryptedAccessKey)) {
				this.accessKey = this.encryptor.decrypt(this.encryptedAccessKey);
			}
			
			if (!StringUtils.hasText(this.secretKey) && StringUtils.hasText(this.encryptedSecretKey)) {
				this.secretKey = this.encryptor.decrypt(this.encryptedSecretKey);
			}
			
			this.s3Service = new RestS3Service(new AWSCredentials(this.accessKey, this.secretKey));
			this.s3Service.getJetS3tProperties().setProperty("s3service.https-only", "false");
			this.s3Service.getJetS3tProperties().setProperty("s3service.max-thread-count", "75");
			this.s3Service.getJetS3tProperties().setProperty("httpclient.connection-timeout-ms", "30000");
			this.s3Service.getJetS3tProperties().setProperty("httpclient.max-connections", "200");
			this.s3Service.getJetS3tProperties().setProperty("xmlparser.sanitize-listings", "false");
			
			this.s3Bucket = this.s3Service.getBucket(this.bucketName);
			
			if (this.s3Bucket == null) {
				org.jets3t.service.model.S3Bucket bucket = new org.jets3t.service.model.S3Bucket();
				bucket.setName(this.bucketName);
				bucket.setCreationDate(new Date());
				
				this.s3Service.createBucket(bucket);
				
				do {
					this.s3Bucket = this.s3Service.getBucket(this.bucketName);
					
				} while (this.s3Bucket == null);
			}
		} catch (S3ServiceException e) {
			throw new RuntimeException("Error connecting to S3." + this.getName(), e);
		}
	}
	
	public void afterPropertiesSet() throws Exception {
		this.initialize();
	}
	
	public List<S3Object> getAllFiles() {
		try {
			return Arrays.asList(this.s3Service.listObjects(this.s3Bucket.getName()));
			
		} catch (S3ServiceException e) {
			throw new RuntimeException("Failed to get all file info", e);
		}
	}
	
	public List<String> getDirectories(String prefix) {
		try {
			StorageObjectsChunk soc = this.s3Service.listObjectsChunked(this.bucketName, prefix, "/", Integer.MAX_VALUE, null, true);
			
			ArrayList<String> dirs = new ArrayList<String>(soc.getCommonPrefixes().length);
			for (String dir : soc.getCommonPrefixes()) {
				String cleanDir = dir.substring(prefix.length()); 
				if (cleanDir.endsWith("/")) {
					cleanDir = cleanDir.substring(0, cleanDir.length() - 1);
				}
				
				dirs.add(cleanDir);
			}
			
			return dirs;
			
		} catch (ServiceException e) {
			throw new RuntimeException("Failed to get all file info", e);
		}
	}
	
	public List<String> getKeys(String prefix) {
		try {
			StorageObjectsChunk soc = this.s3Service.listObjectsChunked(
					this.bucketName,
					prefix,
					"/",
					Integer.MAX_VALUE,
					null,
					true);
			
			ArrayList<String> keys = new ArrayList<String>(soc.getObjects().length);
			for (StorageObject storageObject : soc.getObjects()) {
				keys.add(storageObject.getKey());
			}
			
			return keys;
			
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}
	
	public List<String> getKeys(String prefix, boolean removeLiteralPrefix, boolean stripPrefixFromKeys) {
		try {
			StorageObjectsChunk soc = this.s3Service.listObjectsChunked(
					this.bucketName,
					prefix,
					"/",
					Integer.MAX_VALUE,
					null,
					true);
			
			ArrayList<String> keys = new ArrayList<String>(soc.getObjects().length);
			for (StorageObject storageObject : soc.getObjects()) {
				String key = storageObject.getKey();
				String cleanKey = key;
				if (stripPrefixFromKeys) {
					cleanKey = cleanKey.substring(prefix.length());
				}
				
				if (key.equals(prefix)) {
					if (!removeLiteralPrefix) {
						if (stripPrefixFromKeys) {
							keys.add(cleanKey);
						}
					}
				} else {
					keys.add(cleanKey);
				}
			}
			
			return keys;
			
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}
	
	public StorageObject getFileInfo(String key) {
		try {
			return this.s3Service.getObjectDetails(this.s3Bucket.getName(), key);
			
		} catch (S3ServiceException e) {
			throw new RuntimeException("Failed to get file info for key", e);
			
		} catch (ServiceException se) {
			throw new RuntimeException("Failed to get file info for key", se);
		}
	}
	
	public InputStream getFileContents(String key) {
		try {
			S3Object s3Object = this.s3Service.getObject(this.s3Bucket.getName(), key);
			
			if (s3Object != null) {
				return s3Object.getDataInputStream();
				
			} else {
				return null;
			}
		} catch (S3ServiceException e) {
			if (e.getS3ErrorCode() != null && e.getS3ErrorCode().equals("NoSuchKey")) {
				return null;
				
			} else {
				logger.error("S3ServiceException while reading key from S3." + this.getName() + ", key=" + key);
				
				throw new RuntimeException("S3ServiceException while reading key from S3." + this.getName(), e);
			}
		} catch (ServiceException se) {
			if (se.getErrorCode() != null && se.getErrorCode().equals("NoSuchKey")) {
				return null;
				
			} else {
				logger.error("ServiceException while reading key from S3." + this.getName() + ", key=" + key);
				
				throw new RuntimeException("ServiceException while reading key from S3." + this.getName(), se);
			}
		}
	}
	
	public AccessControlList getFileAcl(String key) {
		try {
			return this.s3Service.getObjectAcl(this.s3Bucket.getName(), key);
			
		} catch (S3ServiceException e) {
			throw new RuntimeException("Failed to get file info for key", e);
			
		} catch (ServiceException se) {
			throw new RuntimeException("Failed to get file info for key", se);
		}
	}
	
	public String getName() {
		return this.bucketName;
	}
	
	public boolean contains(String key) {
		try {
			return this.s3Service.isObjectInBucket(this.bucketName, key);
			
		} catch (S3ServiceException s3se) {
			throw new RuntimeException("Failed to check file existence", s3se);
			
		} catch (ServiceException se) {
			throw new RuntimeException("Failed to check file existence", se);
		}
	}
	
	private void uploadObject(S3Object s3Object, boolean makePublic, Map<String, Object> metadata, String contentType) {
		if (contentType != null) {
			s3Object.setContentType(contentType);
		}
		
		if (metadata != null) {
			for (Entry<String, Object> entry : metadata.entrySet()) {
				if (entry.getValue() instanceof Date) {
					s3Object.addMetadata(entry.getKey(), (Date) entry.getValue());
					
				} else {
					s3Object.addMetadata(entry.getKey(), String.valueOf(entry.getValue()));
				}
			}
		}
		
		// https://forums.aws.amazon.com/message.jspa?messageID=206225#214751
		if (makePublic) {
			s3Object.setAcl(AccessControlList.REST_CANNED_PUBLIC_READ);
			
		} else {
			s3Object.setAcl(AccessControlList.REST_CANNED_PRIVATE);
		}
		
		try {
			this.s3Service.putObject(this.s3Bucket, s3Object);
			
		} catch (S3ServiceException e) {
			logger.error("Error uploading object to S3." + this.getName() + ", key=" + s3Object.getKey());
			
			throw new RuntimeException("Error uploading object to S3." + this.getName(), e);
		}
	}
	
	public void uploadFile(String key, byte[] fileContents, boolean makePublic, Map<String, Object> metadata, String contentType) {
		S3Object s3Object = new S3Object(this.s3Bucket, key);
		ByteArrayInputStream inputStream = new ByteArrayInputStream(fileContents);
		
		try {
			s3Object.setDataInputStream(inputStream);
			s3Object.setContentLength(fileContents.length);
			
			try {
				s3Object.setMd5Hash(ServiceUtils.computeMD5Hash(inputStream));
				inputStream.reset();
				
			} catch (NoSuchAlgorithmException e) {
				throw new RuntimeException("Whoa! MD5 does not exist!", e);
				
			} catch (IOException e) {
				throw new RuntimeException("MD5 Hash failed", e);
			}
			
			this.uploadObject(s3Object, makePublic, metadata, contentType);
			
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
					
				} catch (IOException ioe) {
					throw new RuntimeException("Error uploading object to S3." + this.getName(), ioe);
				}
			}
		}
	}
	
	public void uploadFile(String key, byte[] fileContents, boolean makePublic, Map<String, Object> metadata) {
		this.uploadFile(key, fileContents, makePublic, metadata, null);
	}

	public void uploadFile(String key, byte[] fileContents, boolean makePublic, String contentType) {
		this.uploadFile(key, fileContents, makePublic, null, contentType);
	}
	
	public void uploadFile(String key, byte[] fileContents, boolean makePublic) {
		this.uploadFile(key, fileContents, makePublic, null, null);
	}
	
	public void uploadFile(String key, File file, boolean makePublic, Map<String, Object> metadata, String contentType) {
		try {
			S3Object s3Object = new S3Object(this.s3Bucket, file);
			s3Object.setKey(key);
			
			this.uploadObject(s3Object, makePublic, metadata, contentType);
			
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("Whoa! MD5 does not exist!", e);
			
		} catch (IOException e) {
			throw new RuntimeException("MD5 Hash failed", e);
		}
	}
	
	public void uploadFile(String key, File file, boolean makePublic, Map<String, Object> metadata) {
		this.uploadFile(key, file, makePublic, metadata, null);
	}
	
	public void uploadFile(String key, File file, boolean makePublic, String contentType) {
		this.uploadFile(key, file, makePublic, null, contentType);
	}
	
	public void uploadFile(String key, File file, boolean makePublic) {
		this.uploadFile(key, file, makePublic, null, null);
	}
	
	public void deleteFile(String key) {
		try {
			this.s3Service.deleteObject(this.s3Bucket, key);
			
		} catch (S3ServiceException s3se) {
			logger.error("Error while deleting file from S3." + this.getName() + ", key=" + key);
			
			throw new RuntimeException("Error while deleting file from S3." + this.getName(), s3se);
		}
	}
	
	public void deleteSelf() {
		try {
			this.s3Service.deleteBucket(this.s3Bucket.getName());
			
		} catch (S3ServiceException s3se) {
			throw new RuntimeException("Failed to delete bucket S3." + this.getName(), s3se);
			
		} catch (ServiceException se) {
			throw new RuntimeException("Failed to delete bucket S3." + this.getName(), se);
		}
	}
	
	public void setEncryptor(Encryptor encryptor) {
		this.encryptor = encryptor;
	}
	
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
	
	public void setEncryptedAccessKey(String encryptedAccessKey) {
		this.encryptedAccessKey = encryptedAccessKey;
	}
	
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
	
	public void setEncryptedSecretKey(String encryptedSecretKey) {
		this.encryptedSecretKey = encryptedSecretKey;
	}
	
	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}
}