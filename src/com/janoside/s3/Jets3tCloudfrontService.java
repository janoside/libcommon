package com.janoside.s3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.jets3t.service.CloudFrontService;
import org.jets3t.service.CloudFrontServiceException;
import org.jets3t.service.model.cloudfront.Invalidation;
import org.jets3t.service.security.AWSCredentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.janoside.security.encryption.Encryptor;
import com.janoside.security.encryption.EncryptorAware;

public class Jets3tCloudfrontService implements EncryptorAware, InitializingBean {
	
	private static final Logger logger = LoggerFactory.getLogger(Jets3tCloudfrontService.class);
	
	private CloudFrontService cloudfrontService;
	
	private Encryptor encryptor;
	
	private String encryptedAccessKey;
	
	private String encryptedSecretKey;
	
	private String distributionId;
	
	public void afterPropertiesSet() throws Exception {
		this.cloudfrontService = new CloudFrontService(
				new AWSCredentials(
						this.encryptor.decrypt(this.encryptedAccessKey),
						this.encryptor.decrypt(this.encryptedSecretKey)));
	}
	
	public Invalidation invalidateKeys(List<String> keys) {
		try {
			ArrayList<String> sortedKeys = new ArrayList<String>(keys);
			
			Collections.sort(sortedKeys);
			
			return this.cloudfrontService.invalidateObjects(
					this.distributionId,
					keys.toArray(new String[] {}),
					Long.toString(sortedKeys.toString().hashCode()));
			
		} catch (CloudFrontServiceException e) {
			logger.error("Error invalidating cloudfront keys, keys=" + keys);
			
			throw new RuntimeException("Error invalidating cloudfront keys", e);
		}
	}
	
	public void setEncryptor(Encryptor encryptor) {
		this.encryptor = encryptor;
	}
	
	public void setEncryptedAccessKey(String encryptedAccessKey) {
		this.encryptedAccessKey = encryptedAccessKey;
	}
	
	public void setEncryptedSecretKey(String encryptedSecretKey) {
		this.encryptedSecretKey = encryptedSecretKey;
	}
	
	public void setDistributionId(String distributionId) {
		this.distributionId = distributionId;
	}
}