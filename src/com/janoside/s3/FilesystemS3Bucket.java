package com.janoside.s3;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.jets3t.service.acl.AccessControlList;
import org.jets3t.service.model.S3Object;

import com.janoside.util.FileUtil;

public class FilesystemS3Bucket implements S3Bucket {
	
	private String directory;
	
	private String bucketName;
	
	public FilesystemS3Bucket() {
		this.directory = "/tmp/s3-buckets";
	}
	
	public List<S3Object> getAllFiles() {
		throw new UnsupportedOperationException();
	}
	
	public List<String> getDirectories(String prefix) {
		throw new UnsupportedOperationException();
	}
	
	public List<String> getKeys(String prefix) {
		throw new UnsupportedOperationException();
	}
	
	public List<String> getKeys(String prefix, boolean removeLiteralPrefix, boolean stripPrefixFromKeys) {
		throw new UnsupportedOperationException();
	}
	
	public S3Object getFileInfo(String key) {
		throw new UnsupportedOperationException();
	}
	
	public InputStream getFileContents(String key) {
		try {
			return new FileInputStream(this.getFilepath(key));
			
		} catch (FileNotFoundException fnfe) {
			throw new RuntimeException(fnfe);
		}
	}
	
	public AccessControlList getFileAcl(String key) {
		throw new UnsupportedOperationException();
	}
	
	public String getName() {
		return "FilesystemS3Bucket(" + this.directory + "/" + this.bucketName + ")";
	}
	
	public boolean contains(String key) {
		return new File(this.getFilepath(key)).exists();
	}
	
	public void uploadFile(String key, byte[] fileContents, boolean makePublic, Map<String, Object> metadata, String contentType) {
		FileUtil.writeBytes(this.getFilepath(key), fileContents);
	}
	
	public void uploadFile(String key, byte[] fileContents, boolean makePublic, Map<String, Object> metadata) {
		FileUtil.writeBytes(this.getFilepath(key), fileContents);
	}
	
	public void uploadFile(String key, byte[] fileContents, boolean makePublic, String contentType) {
		FileUtil.writeBytes(this.getFilepath(key), fileContents);
	}
	
	public void uploadFile(String key, byte[] fileContents, boolean makePublic) {
		FileUtil.writeBytes(this.getFilepath(key), fileContents);
	}
	
	public void uploadFile(String key, File file, boolean makePublic, Map<String, Object> metadata, String contentType) {
		throw new UnsupportedOperationException();
	}
	
	public void uploadFile(String key, File file, boolean makePublic, Map<String, Object> metadata) {
		throw new UnsupportedOperationException();
	}
	
	public void uploadFile(String key, File file, boolean makePublic, String contentType) {
		throw new UnsupportedOperationException();
	}
	
	public void uploadFile(String key, File file, boolean makePublic) {
		throw new UnsupportedOperationException();
	}
	
	public void deleteFile(String key) {
		File file = new File(this.getFilepath(key));
		
		if (!file.delete()) {
			throw new RuntimeException("Failed to delete key");
		}
	}
	
	private String getFilepath(String key) {
		return this.directory + "/" + this.bucketName + "/" + key;
	}
	
	public void setDirectory(String directory) {
		this.directory = directory;
	}
	
	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}
}