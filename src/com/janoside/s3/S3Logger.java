package com.janoside.s3;

import java.util.Date;

import com.janoside.environment.Environment;
import com.janoside.environment.EnvironmentAware;
import com.janoside.hash.Sha1StringHasher;
import com.janoside.storage.StringS3KeyValueStore;
import com.janoside.util.DateUtil;
import com.janoside.util.SystemTimeSource;
import com.janoside.util.TimeSource;

public class S3Logger implements EnvironmentAware {
	
	private StringS3KeyValueStore stringS3KeyValueStore;
	
	private TimeSource timeSource;
	
	private Environment environment;
	
	private Sha1StringHasher hasher;
	
	public S3Logger() {
		this.timeSource = new SystemTimeSource();
		this.hasher = new Sha1StringHasher();
	}
	
	public void log(String category, String data) {
		String path = String.format(
				"%s/%s/%s/%s",
				this.environment.getName(),
				category,
				DateUtil.format("yyyy/MM/dd/HH/mm", new Date(this.timeSource.getCurrentTime())),
				this.hasher.hash(data));
		
		this.stringS3KeyValueStore.put(path, data);
	}
	
	public void setStringS3KeyValueStore(StringS3KeyValueStore stringS3KeyValueStore) {
		this.stringS3KeyValueStore = stringS3KeyValueStore;
	}
	
	public void setTimeSource(TimeSource timeSource) {
		this.timeSource = timeSource;
	}
	
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}
}