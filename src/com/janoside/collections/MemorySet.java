package com.janoside.collections;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class MemorySet<T> implements ObjectSet<T> {
	
	private Set<T> internalSet;
	
	public MemorySet() {
		this.internalSet = Collections.synchronizedSet(new HashSet<T>());
	}
	
	public boolean contains(T object) {
		return this.internalSet.contains(object);
	}
	
	public void add(T object) {
		this.internalSet.add(object);
	}
	
	public T remove(T object) {
		this.internalSet.remove(object);
		
		return object;
	}
	
	public void clear() {
		this.internalSet.clear();
	}
	
	public int getSize() {
		return this.internalSet.size();
	}
	
	public Iterator<T> iterator() {
		return this.internalSet.iterator();
	}
}