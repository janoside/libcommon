package com.janoside.collections;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.map.LRUMap;

public class LruSet<KeyType> implements Set<KeyType> {
	
	private Map<KeyType, Boolean> internalMap;
	
	public LruSet() {
		this.internalMap = Collections.synchronizedMap(new LRUMap(1000));
	}
	
	public LruSet(int size) {
		this.internalMap = Collections.synchronizedMap(new LRUMap(size));
	}
	
	public boolean add(KeyType key) {
		boolean newItem = !this.internalMap.containsKey(key);
		
		this.internalMap.put(key, Boolean.TRUE);
		
		return newItem;
	}
	
	public boolean addAll(Collection<? extends KeyType> keys) {
		boolean somethingNew = false;
		
		for (KeyType key : keys) {
			if (this.add(key)) {
				somethingNew = true;
			}
		}
		
		return somethingNew;
	}
	
	public void clear() {
		this.internalMap.clear();
	}
	
	public boolean contains(Object o) {
		return this.internalMap.containsKey(o);
	}
	
	public boolean containsAll(Collection<?> c) {
		return this.internalMap.keySet().containsAll(c);
	}
	
	public boolean isEmpty() {
		return this.internalMap.isEmpty();
	}
	
	public Iterator<KeyType> iterator() {
		return this.internalMap.keySet().iterator();
	}
	
	public boolean remove(Object o) {
		return (this.internalMap.remove(o) == null);
	}
	
	public boolean removeAll(Collection<?> c) {
		boolean somethingChanged = false;
		
		for (Object o : c) {
			if (this.remove(o)) {
				somethingChanged = true;
			}
		}
		
		return somethingChanged;
	}
	
	public boolean retainAll(Collection<?> c) {
		return false;
	}
	
	public int size() {
		return this.internalMap.size();
	}
	
	public Object[] toArray() {
		return this.internalMap.keySet().toArray();
	}
	
	public <T> T[] toArray(T[] a) {
		return this.internalMap.keySet().toArray(a);
	}
}