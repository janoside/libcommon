package com.janoside.collections;

import java.util.Collections;
import java.util.Map;

import org.apache.commons.collections.map.LRUMap;

public class LruMap<KeyType, ValueType> {
	
	private Map<KeyType, ValueType> internalMap;
	
	public LruMap() {
		this.internalMap = Collections.synchronizedMap(new LRUMap(1000));
	}
	
	public LruMap(int size) {
		this.internalMap = Collections.synchronizedMap(new LRUMap(size));
	}
	
	public ValueType get(KeyType key) {
		return this.internalMap.get(key);
	}
	
	public void putAll(Map<? extends KeyType, ? extends ValueType> map) {
		this.internalMap.putAll(map);
	}
	
	public void put(KeyType key, ValueType value) {
		this.internalMap.put(key, value);
	}
	
	public ValueType remove(KeyType key) {
		return this.internalMap.remove(key);
	}
}