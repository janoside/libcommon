package com.janoside.collections;

import java.util.List;

public interface ListPopulator<T> {
	
	void populate(List<T> list);
}