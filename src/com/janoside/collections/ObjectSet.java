package com.janoside.collections;

public interface ObjectSet<T> extends Iterable<T> {
	
	boolean contains(T object);
	
	void add(T object);
	
	T remove(T object);
	
	void clear();
	
	int getSize();
}