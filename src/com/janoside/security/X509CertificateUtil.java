package com.janoside.security;

import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.jce.provider.X509CertificateObject;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.DefaultSignatureAlgorithmIdentifierFinder;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Date;

/**
 * Created by Alex on 1/12/15.
 */
public class X509CertificateUtil {
    public static X509Certificate generateSelfSignedX509Certificate(KeyPair rsaKeyPair, String issuer, String subject, Date notBefore, Date notAfter) {
        try {
            X500Name issuerX500 = new X500Name(issuer);
            BigInteger serialNumber = BigInteger.valueOf(new SecureRandom().nextInt());

            X500Name subjectX500 = new X500Name(subject);
            SubjectPublicKeyInfo publicKeyInfo = SubjectPublicKeyInfo.getInstance(rsaKeyPair.getPublic().getEncoded());

            X509v3CertificateBuilder certBuilder = new X509v3CertificateBuilder(
                    issuerX500,
                    serialNumber,
                    notBefore,
                    notAfter,
                    subjectX500,
                    publicKeyInfo);


            ContentSigner contentSigner = new JcaContentSignerBuilder("SHA1withRSA").build(rsaKeyPair.getPrivate());

            X509CertificateHolder certHolder = certBuilder.build(contentSigner);

            AlgorithmIdentifier signatureAlgorithmId = new DefaultSignatureAlgorithmIdentifierFinder().find("SHA1withRSA");

            ASN1EncodableVector v = new ASN1EncodableVector();

            v.add(certHolder.toASN1Structure().getTBSCertificate());
            v.add(signatureAlgorithmId);
            v.add(new DERBitString(certHolder.getSignature()));

            return new X509CertificateObject(Certificate.getInstance(new DERSequence(v)));

        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }
}
