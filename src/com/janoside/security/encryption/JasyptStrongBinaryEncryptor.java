package com.janoside.security.encryption;

import java.util.Random;

import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.util.binary.StrongBinaryEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;

import com.janoside.util.RandomUtil;

public class JasyptStrongBinaryEncryptor implements BinaryEncryptor {
	
private static final Logger logger = LoggerFactory.getLogger(JasyptStrongBinaryEncryptor.class);
	
	private org.jasypt.util.binary.BinaryEncryptor encryptor;
	
	private long successCount;
	
	private long failureCount;
	
	public JasyptStrongBinaryEncryptor() {
		this(RandomUtil.randomWord(50, new Random(JasyptStrongBinaryEncryptor.class.toString().hashCode())).toString());
	}
	
	public JasyptStrongBinaryEncryptor(String password) {
		this.encryptor = new StrongBinaryEncryptor();
		
		((StrongBinaryEncryptor) this.encryptor).setPassword(password);
		
		this.successCount = 0;
		this.failureCount = 0;
	}
	
	@ManagedOperation
	public byte[] encrypt(byte[] cleartext) {
		try {
			byte[] ciphertext = this.encryptor.encrypt(cleartext);
			
			this.successCount++;
			
			return ciphertext;
			
		} catch (EncryptionOperationNotPossibleException eonpe) {
			this.failureCount++;
			
			throw eonpe;
		}
	}

	@ManagedOperation
	public byte[] decrypt(byte[] input) {
		try {
			byte[] cleartext = this.encryptor.decrypt(input);
			
			this.successCount++;
			
			return cleartext;
			
		} catch (EncryptionOperationNotPossibleException eonpe) {
			logger.error("Unable to decrypt: " + input);
			
			this.failureCount++;
			
			throw eonpe;
		}
	}
	
	@ManagedAttribute
	public long getSuccessCount() {
		return this.successCount;
	}
	
	@ManagedAttribute
	public long getFailureCount() {
		return this.failureCount;
	}
	
	@ManagedAttribute
	public float getSuccessRate() {
		if ((this.successCount + this.failureCount) == 0) {
			return 0;
			
		} else {
			return ((float) this.successCount / (this.successCount + this.failureCount));
		}
	}
}