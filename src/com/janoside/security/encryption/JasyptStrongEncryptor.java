package com.janoside.security.encryption;

import java.util.Random;

import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.util.text.StrongTextEncryptor;
import org.jasypt.util.text.TextEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.util.RandomUtil;

@ManagedResource(objectName = "Janoside:name=Encryptor")
public class JasyptStrongEncryptor implements Encryptor {
	
	private static final Logger logger = LoggerFactory.getLogger(JasyptStrongEncryptor.class);
	
	private TextEncryptor textEncryptor;
	
	private long successCount;
	
	private long failureCount;
	
	public JasyptStrongEncryptor() {
		this(RandomUtil.randomWord(50, new Random(-593625044)).toString());
	}
	
	public JasyptStrongEncryptor(String password) {
		this.textEncryptor = new StrongTextEncryptor();
		((StrongTextEncryptor) this.textEncryptor).setPassword(password);
		
		this.successCount = 0;
		this.failureCount = 0;
	}
	
	@ManagedOperation
	public String encrypt(String input) {
		try {
			String ciphertext = this.textEncryptor.encrypt(input);
			
			this.successCount++;
			
			return ciphertext;
			
		} catch (EncryptionOperationNotPossibleException eonpe) {
			this.failureCount++;
			
			throw eonpe;
		}
	}

	@ManagedOperation
	public String decrypt(String input) {
		try {
			String cleartext = this.textEncryptor.decrypt(input);
			
			this.successCount++;
			
			return cleartext;
			
		} catch (EncryptionOperationNotPossibleException eonpe) {
			logger.error("Unable to decrypt: " + input);
			
			this.failureCount++;
			
			throw eonpe;
		}
	}
	
	public void setTextEncryptor(TextEncryptor textEncryptor) {
		this.textEncryptor = textEncryptor;
	}
	
	@ManagedAttribute
	public long getSuccessCount() {
		return this.successCount;
	}
	
	@ManagedAttribute
	public long getFailureCount() {
		return this.failureCount;
	}
	
	@ManagedAttribute
	public float getSuccessRate() {
		if ((this.successCount + this.failureCount) == 0) {
			return 0;
			
		} else {
			return ((float) this.successCount / (this.successCount + this.failureCount));
		}
	}
}