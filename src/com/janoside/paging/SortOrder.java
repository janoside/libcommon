package com.janoside.paging;

public enum SortOrder {
	Ascending,
	Descending
}