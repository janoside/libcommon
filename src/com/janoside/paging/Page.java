package com.janoside.paging;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class Page implements Serializable {
	
	public static Page fromUrlParameter(String urlParameter) {
		String[] parts = urlParameter.split(",");
		if (parts.length < 2) {
			throw new IllegalArgumentException("Invalid Page url parameter format");
		}
		
		ArrayList<SortField> sortFields = new ArrayList<SortField>();
		
		int sortFieldCount = (parts.length / 2) - 1;
		for (int i = 0; i < sortFieldCount; i++) {
			sortFields.add(new SortField(
					parts[2 * i],
					SortDirection.valueOf(parts[2 * i + 1])));
		}
		
		return new Page(
				sortFields,
				Integer.parseInt(parts[parts.length - 2]),
				Integer.parseInt(parts[parts.length - 1]));
	}
	
	private List<SortField> sortFields;
	
	private int start;
	
	private int count;
	
	public Page(Page p) {
		this.sortFields = new ArrayList<SortField>(p.getSortFields());
		this.start = p.start;
		this.count = p.count;
	}
	
	public Page(List<SortField> sortFields, int start, int count) {
		this.sortFields = new ArrayList<SortField>(sortFields);
		this.start = start;
		this.count = count;
	}
	
	public Page(String sortFieldName, SortDirection sortDirection, int start, int count) {
		SortField sortField = new SortField(sortFieldName, sortDirection);
		
		this.sortFields = new ArrayList<SortField>();
		this.sortFields.add(sortField);
		this.start = start;
		this.count = count;
	}
	
	public Page(int start, int count) {
		this.sortFields = new ArrayList<SortField>();
		this.start = start;
		this.count = count;
	}
	
	public Page() {
		this.sortFields = new ArrayList<SortField>();
	}
	
	public List<SortField> getSortFields() {
		return sortFields;
	}
	
	public void setSortFields(List<SortField> sortFields) {
		this.sortFields = sortFields;
	}
	
	public int getStart() {
		return start;
	}
	
	public void setStart(int start) {
		this.start = start;
	}
	
	public int getCount() {
		return count;
	}
	
	public void setCount(int count) {
		this.count = count;
	}
	
	public String toUrlParameter() {
		StringBuilder buffer = new StringBuilder();
		for (SortField sortField : this.sortFields) {
			buffer.append(sortField.getName());
			buffer.append(",");
			buffer.append(sortField.getSortDirection());
			buffer.append(",");
		}
		
		buffer.append(this.start);
		buffer.append(",");
		buffer.append(this.count);
		
		return buffer.toString();
	}
	
	public String toString() {
		return "Page(sortFields=" + this.sortFields + ", start=" + this.start + ", count=" + this.count + ")";
	}
	
	public int hashCode() {
		return this.toString().hashCode();
	}
	
	public boolean equals(Object o) {
		if (o == null) {
			return false;
			
		} else if (o instanceof Page) {
			return o.toString().equals(this.toString());
			
		} else {
			return false;
		}
	}
}
