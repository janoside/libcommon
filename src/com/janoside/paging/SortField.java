package com.janoside.paging;

import java.io.Serializable;

@SuppressWarnings("serial")
public class SortField implements Serializable {
	
	private String name;
	
	private SortDirection sortDirection;
	
	public SortField(String name, SortDirection sortDirection) {
		this.name = name;
		this.sortDirection = sortDirection;
	}
	
	public SortField() {
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public SortDirection getSortDirection() {
		return sortDirection;
	}
	
	public void setSortDirection(SortDirection sortDirection) {
		this.sortDirection = sortDirection;
	}
	
	public String toString() {
		return "SortField(" + this.name + ", " + this.sortDirection + ")";
	}
	
	public int hashCode() {
		return this.toString().hashCode();
	}
	
	public boolean equals(Object o) {
		if (o == null) {
			return false;
			
		} else if (o instanceof SortField) {
			return o.toString().equals(this.toString());
			
		} else {
			return false;
		}
	}
}