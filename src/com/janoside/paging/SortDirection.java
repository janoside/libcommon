package com.janoside.paging;

public enum SortDirection {
	Ascending,
	Descending
}