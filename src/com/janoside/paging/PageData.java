package com.janoside.paging;

import java.io.Serializable;
import java.util.List;

import com.google.gson.Gson;

@SuppressWarnings("serial")
public class PageData implements Serializable {
	
	private static final Gson gson = new Gson();

	private List<String> sortFields;

	private SortOrder sortOrder;

	private int start;

	private int count;

	public PageData() {
		this.start = 0;
		this.count = 10;
	}
	
	public List<String> getSortFields() {
		return sortFields;
	}
	
	public void setSortFields(List<String> sortFields) {
		this.sortFields = sortFields;
	}
	
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	
	public int getStart() {
		return start;
	}
	
	public void setStart(int start) {
		this.start = start;
	}
	
	public int getCount() {
		return count;
	}
	
	public void setCount(int count) {
		this.count = count;
	}
	
	public String toString() {
		return gson.toJson(this);
	}
	
	public int hashCode() {
		return this.toString().hashCode();
	}
	
	public boolean equals(Object o) {
		if (o == null) {
			return false;
			
		} else if (o instanceof PageData) {
			return ((PageData) o).toString().equals(this.toString());
			
		} else {
			return false;
		}
	}
}