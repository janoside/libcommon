package com.janoside.text;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.eclipse.mylyn.wikitext.core.parser.MarkupParser;
import org.eclipse.mylyn.wikitext.mediawiki.core.MediaWikiLanguage;

public class WikiHtmlTextTransformer implements TextTransformer {
	
	private Pattern linkPattern = Pattern.compile("(\\[([^\\]]+)\\|([^\\]]+)\\])", Pattern.CASE_INSENSITIVE);
	
	public String transform(String input) {
		MarkupParser markupParser = new MarkupParser();
		markupParser.setMarkupLanguage(new MediaWikiLanguage());
		
		String html = markupParser.parseToHtml(input).replaceAll("<b>", "<strong>").replaceAll("</b>", "</strong>");
		html = html.substring(html.indexOf("<body>") + "<body>".length());
		html = html.substring(0, html.lastIndexOf("</body>"));
		
		return html;
		
//		String output = this.formatLinks(input);
//		
//		output = StringUtil.formatOpenCloseTags(output, "'''''", "'''''", "<strong><i>", "</i></strong>");
//		output = StringUtil.formatOpenCloseTags(output, "'''", "'''", "<strong>", "</strong>");
//		output = StringUtil.formatOpenCloseTags(output, "''", "''", "<i>", "</i>");
//		
//		output = StringUtil.formatOpenCloseTags(output, "======", "======", "<h6>", "</h6>");
//		output = StringUtil.formatOpenCloseTags(output, "=====", "=====", "<h5>", "</h5>");
//		output = StringUtil.formatOpenCloseTags(output, "====", "====", "<h4>", "</h4>");
//		output = StringUtil.formatOpenCloseTags(output, "===", "===", "<h3>", "</h3>");
//		output = StringUtil.formatOpenCloseTags(output, "==", "==", "<h2>", "</h2>");
//		
//		return output;
	}
	
	private String formatLinks(String input) {
		String output = input;
		
		Matcher linkMatcher = linkPattern.matcher(output);
		while (linkMatcher.find()) {
			String group = linkMatcher.group(1);
			
			String url = linkMatcher.group(2);
			String text = linkMatcher.group(3);
			
			StringBuilder linkHtmlBuilder = new StringBuilder("<a href=\"");
			linkHtmlBuilder.append(url);
			linkHtmlBuilder.append("\" title=\"");
			linkHtmlBuilder.append(StringEscapeUtils.escapeHtml(text));
			linkHtmlBuilder.append("\">");
			linkHtmlBuilder.append(StringEscapeUtils.escapeHtml(text));
			linkHtmlBuilder.append("</a>");
			
			output = output.replaceAll(Pattern.quote(group), linkHtmlBuilder.toString());
		}
		
		return output;
	}
}