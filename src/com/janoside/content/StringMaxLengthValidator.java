package com.janoside.content;

public class StringMaxLengthValidator implements ContentValidator<String> {
	
	private int maxLength;
	
	public ValidationResult validate(String content) {
		ValidationResult validationResult = new ValidationResult();
		
		validationResult.setValid(content.length() <= this.maxLength);
		
		return validationResult;
	}
	
	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}
}