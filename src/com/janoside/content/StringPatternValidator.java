package com.janoside.content;

import java.util.regex.Pattern;

import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

/**
 * ContentValidator implementation for strings using a regex Pattern.
 * User sets both the Pattern used for comparison and whether a
 * match is blocked (set <code>patternBlocks=true</code>) or
 * allowed (set <code>patternBlocks=false</code>).
 * 
 * @author janoside
 */
@ManagedResource(objectName = "Janoside:name=StringPatternValidator")
public class StringPatternValidator implements ContentValidator<String> {
	
	private Pattern pattern;
	
	private boolean patternBlocks;
	
	@ManagedOperation
	public ValidationResult validate(String content) {
		ValidationResult validationResult = new ValidationResult();
		
		boolean matchesContent = this.pattern.matcher(content).matches();
		
		validationResult.setValid((this.patternBlocks) ? !matchesContent : matchesContent);
		
		return validationResult;
	}
	
	public void setPattern(Pattern pattern) {
		this.pattern = pattern;
	}
	
	public void setPatternString(String patternString) {
		this.pattern = Pattern.compile(patternString, Pattern.CASE_INSENSITIVE);
	}
	
	public void setPatternBlocks(boolean patternBlocks) {
		this.patternBlocks = patternBlocks;
	}
}