package com.janoside.content;

public interface ContentValidator<T> {

	ValidationResult validate(T content);
}