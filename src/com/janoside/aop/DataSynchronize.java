package com.janoside.aop;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface DataSynchronize {
	String lockNamePattern();
	String parameterTransformerClass() default "";
}