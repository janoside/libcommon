package com.janoside.aop;

import java.util.Set;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public abstract class FilteringMethodInterceptor implements MethodInterceptor {
	
	private Set<String> methodNames;
	
	public Object invoke(MethodInvocation invocation) throws Throwable {
		if (this.runFor(invocation)) {
			return this.invokeInternal(invocation);
			
		} else {
			return invocation.proceed();
		}
	}
	
	protected abstract Object invokeInternal(MethodInvocation invocation) throws Throwable;
	
	private boolean runFor(MethodInvocation invocation) {
		if (this.methodNames == null) {
			return true;
			
		} else {
			return this.methodNames.contains(invocation.getMethod().getName());
		}
	}
	
	public void setMethodNames(Set<String> methodNames) {
		this.methodNames = methodNames;
	}
}