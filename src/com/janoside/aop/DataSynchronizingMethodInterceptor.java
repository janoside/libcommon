package com.janoside.aop;

import java.util.List;
import java.util.regex.Pattern;

import org.aopalliance.intercept.MethodInvocation;

import com.janoside.lock.LockService;
import com.janoside.lock.LockServiceAware;
import com.janoside.lock.MemoryLockService;
import com.janoside.transform.ObjectTransformer;
import com.janoside.util.ClassUtils;
import com.janoside.util.StringUtil;

/*
 * Method interceptor that should be added "before" database-transaction-managing
 * interceptors (meaning the db-transaction-managing interceptors are wrapped
 * "inside" this one). The goal is to do data-aware "synchronizing" at the
 * application level to do the minimum possible "synchronizing".
 */
public class DataSynchronizingMethodInterceptor extends FilteringMethodInterceptor implements LockServiceAware {
	
	private static final ObjectTransformer<Object, String> DefaultParameterTransformer = new ObjectTransformer<Object, String>() {
		public String transform(Object o) {
			return String.valueOf(o);
		}
	};
	
	private LockService lockService;
	
	public DataSynchronizingMethodInterceptor() {
		this.lockService = new MemoryLockService();
	}
	
	protected Object invokeInternal(MethodInvocation invocation) throws Throwable {
		DataSynchronize dataSynchronizeAnnotation = invocation.getMethod().getAnnotation(DataSynchronize.class);
		
		if (dataSynchronizeAnnotation != null) {
			ObjectTransformer<Object, String> parameterTransformer = DefaultParameterTransformer;
			
			if (dataSynchronizeAnnotation.parameterTransformerClass().length() > 0) {
				parameterTransformer = (ObjectTransformer<Object, String>) ClassUtils.objectFromClassName(dataSynchronizeAnnotation.parameterTransformerClass());
			}
			
			List<String> lockNames = this.getLockNames(
					invocation,
					parameterTransformer,
					dataSynchronizeAnnotation.lockNamePattern());
			
			this.lockService.lockAll(lockNames);
			
			try {
				return invocation.proceed();
				
			} finally {
				this.lockService.unlockAll(lockNames);
			}
		} else {
			return invocation.proceed();
		}
	}
	
	private List<String> getLockNames(MethodInvocation invocation, ObjectTransformer<Object, String> parameterTransformer, String lockNamePattern) {
		String lockNamesList = lockNamePattern;
		
		for (int i = 0; i < invocation.getArguments().length; i++) {
			String patternI = String.format("{%d}", i);
			
			if (lockNamesList.contains(patternI)) {
				lockNamesList = lockNamesList.replaceAll(Pattern.quote(patternI), parameterTransformer.transform(invocation.getArguments()[i]));
			}
		}
		
		return StringUtil.split(lockNamesList, ",");
	}
	
	public void setLockService(LockService lockService) {
		this.lockService = lockService;
	}
}