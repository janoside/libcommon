package com.janoside.lock;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;

@ManagedResource(objectName = "Janoside:name=MemoryLockService")
public class MemoryLockService implements LockService, ExceptionHandlerAware {
	
	private static final Logger logger = LoggerFactory.getLogger(MemoryLockService.class);
	
	private ExceptionHandler exceptionHandler;
	
	private Map<String, Thread> ownerThreadsByLockName;
	
	public MemoryLockService() {
		this.ownerThreadsByLockName = new HashMap<String, Thread>();
	}
	
	@ManagedOperation
	public Set<String> getCurrentLocks() {
		return new HashSet<String>(this.ownerThreadsByLockName.keySet());
	}
	
	public void lockAll(Collection<String> lockNames) {
		LockUtil.lockAll(this, lockNames);
	}
	
	public void unlockAll(Collection<String> lockNames) {
		LockUtil.unlockAll(this, lockNames);
	}
	
	public void lock(String lockName) {
		synchronized (this.ownerThreadsByLockName) {
			if (Thread.currentThread().equals(this.ownerThreadsByLockName.get(lockName))) {
				logger.error("Attempt to acquire lock already owned, lockName=" + lockName);
				
				throw new RuntimeException("Attempt to acquire lock already owned");
			}
			
			while (this.ownerThreadsByLockName.containsKey(lockName)) {
				try {
					this.ownerThreadsByLockName.wait();
					
				} catch (InterruptedException e) {
					this.exceptionHandler.handleException(e);
				}
			}
			
			this.ownerThreadsByLockName.put(lockName, Thread.currentThread());
		}
	}
	
	public void unlock(String lockName) {
		synchronized (this.ownerThreadsByLockName) {
			if (!Thread.currentThread().equals(this.ownerThreadsByLockName.get(lockName))) {
				logger.error("Attempt to unlock lock that is not owned, lockName=" + lockName);
				
				throw new RuntimeException("Attempt to unlock lock that is not owned");
			}
			
			this.ownerThreadsByLockName.remove(lockName);
			
			this.ownerThreadsByLockName.notifyAll();
		}
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
}