package com.janoside.lock;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LockUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(LockUtil.class);
	
	public static void lockAll(LockService lockService, Collection<String> lockNames) {
		HashSet<String> acquiredLockNames = new HashSet<String>();
		
		for (String lockName : lockNames) {
			try {
				lockService.lock(lockName);
				
				acquiredLockNames.add(lockName);
				
			} catch (Throwable t) {
				LockUtil.unlockAll(lockService, acquiredLockNames);
				
				throw new RuntimeException(t);
			}
		}
		
		if (!acquiredLockNames.containsAll(lockNames)) {
			logger.error("Failure to acquire all locks, requested=" + lockNames);
			
			throw new RuntimeException("Failure to acquire all locks");
		}
	}
	
	public static void unlockAll(LockService lockService, Collection<String> lockNames) {
		HashSet<String> unlockedLockNames = new HashSet<String>();
		
		ArrayList<Throwable> throwables = new ArrayList<Throwable>();
		for (String lockName : lockNames) {
			try {
				lockService.unlock(lockName);
				
				unlockedLockNames.add(lockName);
				
			} catch (Throwable t) {
				throwables.add(t);
			}
		}
		
		if (!unlockedLockNames.containsAll(lockNames)) {
			logger.error("Failure to unlock all locks, requested=" + lockNames + ", unlocked=" + unlockedLockNames);
			
			throw new RuntimeException("Failure to unlock all locks");
		}
	}
}