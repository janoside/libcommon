package com.janoside.lock;

import java.util.Collection;
import java.util.Set;

public interface LockService {
	
	Set<String> getCurrentLocks();
	
	void lockAll(Collection<String> lockNames);
	
	void unlockAll(Collection<String> lockNames);
	
	void lock(String lockName);
	
	void unlock(String lockName);
}