package com.janoside.lock;

import java.util.Collection;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.StringUtils;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.scheduling.BasicScheduler;
import com.janoside.scheduling.Scheduler;
import com.janoside.scheduling.SchedulerAware;
import com.janoside.storage.KeyValueStore;
import com.janoside.thread.ThreadUtil;

public class KeyValueStoreLockService implements LockService, SchedulerAware, ExceptionHandlerAware, InitializingBean {
	
	private static final Logger logger = LoggerFactory.getLogger(KeyValueStoreLockService.class);
	
	private static final long LockExpirationPeriod = 10000;
	
	private KeyValueStore<String> store;
	
	private LockService localLockService;
	
	private Scheduler scheduler;
	
	private ExceptionHandler exceptionHandler;
	
	private String uuid;
	
	private long lockCheckPeriod;
	
	public KeyValueStoreLockService() {
		this.localLockService = new MemoryLockService();
		this.uuid = UUID.randomUUID().toString();
		this.lockCheckPeriod = 500;
		
		this.scheduler = new BasicScheduler();
	}
	
	public void afterPropertiesSet() {
		this.scheduler.scheduleTaskAtConstantRate(new Runnable() {
				public void run() {
					KeyValueStoreLockService.this.refreshOwnershipOfActiveLocks();
				}
			},
			0,
			LockExpirationPeriod / 4);
	}
	
	public Set<String> getCurrentLocks() {
		return this.localLockService.getCurrentLocks();
	}
	
	public void lockAll(Collection<String> lockNames) {
		LockUtil.lockAll(this, lockNames);
	}
	
	public void unlockAll(Collection<String> lockNames) {
		LockUtil.unlockAll(this, lockNames);
	}
	
	public void lock(String lockName) {
		// locks the lock for the current thread
		this.localLockService.lock(lockName);
		
		// acquires the lock for this "instance" in the global store
		this.lockInStore(lockName);
	}
	
	public void unlock(String lockName) {
		// unlocks the lock for this "instance" in the global store
		this.unlockInStore(lockName);
		
		// current thread lets go of the lock
		this.localLockService.unlock(lockName);
	}
	
	private void lockInStore(String lockName) {
		// keep trying to take control of the lock in the eyes of the "store"
		while (!this.isLockLocallyOwned(lockName)) {
			try {
				if (this.isLockExternallyOwned(lockName)) {
					try {
						ThreadUtil.sleep(this.lockCheckPeriod / 5);
						
					} catch (InterruptedException e) {
						this.exceptionHandler.handleException(e);
					}
					
					continue;
				}
				
				this.store.put(this.keyForLock(lockName), this.uuid);
				
				ThreadUtil.sleep(this.lockCheckPeriod);
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
		}
		
		this.store.put(this.keyForLockAlive(lockName), Long.toString(System.currentTimeMillis()));
	}
	
	private void unlockInStore(String lockName) {
		while (this.isLockLocallyOwned(lockName)) {
			try {
				this.store.remove(this.keyForLock(lockName));
				
				ThreadUtil.sleep(this.lockCheckPeriod);
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
		}
	}
	
	private boolean isLockLocallyOwned(String lockName) {
		String value = null;
		
		long sleepTime = 10;
		while (true) {
			try {
				value = this.store.get(this.keyForLock(lockName));
				
				return this.uuid.equals(value);
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
				
				try {
					// don't demolish the store if it's having problems
					ThreadUtil.sleep(sleepTime);
					sleepTime += 25;
					
				} catch (InterruptedException e) {
					this.exceptionHandler.handleException(t);
				}
			}
		}
	}
	
	private boolean isLockExternallyOwned(String lockName) {
		String value = null;
		
		long sleepTime = 10;
		while (true) {
			try {
				value = this.store.get(this.keyForLock(lockName));
				
				if (value == null || this.uuid.equals(lockName)) {
					return false;
				}
				
				String expiration = this.store.get(this.keyForLockAlive(lockName));
				
				if (!StringUtils.hasText(expiration)) {
					return false;
					
				} else {
					long millisAgo = (System.currentTimeMillis() - Long.parseLong(expiration));
					
					return (millisAgo < LockExpirationPeriod);
				}
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
				
				try {
					// don't demolish the store if it's having problems
					ThreadUtil.sleep(sleepTime);
					sleepTime += 25;
					
				} catch (InterruptedException e) {
					this.exceptionHandler.handleException(t);
				}
			}
		}
	}
	
	private void refreshOwnershipOfActiveLocks() {
		Set<String> ownedLocks = this.localLockService.getCurrentLocks();
		
		for (String lockName : ownedLocks) {
			try {
				this.store.put(this.keyForLockAlive(lockName), Long.toString(System.currentTimeMillis()));
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
		}
	}
	
	private String keyForLock(String lockName) {
		return "Lock-" + lockName;
	}
	
	private String keyForLockAlive(String lockName) {
		return "LockAlive-" + lockName;
	}
	
	public void setStore(KeyValueStore<String> store) {
		this.store = store;
	}
	
	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setLockCheckPeriod(long lockCheckPeriod) {
		this.lockCheckPeriod = lockCheckPeriod;
	}
}