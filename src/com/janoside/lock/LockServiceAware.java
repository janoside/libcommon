package com.janoside.lock;

public interface LockServiceAware {

	void setLockService(LockService lockService);
}