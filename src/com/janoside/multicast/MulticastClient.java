package com.janoside.multicast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class MulticastClient {
	
	private MulticastSocket multicastSocket;
	
	private String multicastAddress;
	
	private int multicastPort;
	
	public void multicast(Object message) {
		if (this.multicastSocket == null) {
			try {
				this.multicastSocket = new MulticastSocket(this.multicastPort);
				this.multicastSocket.joinGroup(InetAddress.getByName(this.multicastAddress));
				
			} catch (IOException e) {
				throw new RuntimeException("Failed to start multicast socket", e);
			}
		}
		
		try {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
			objectOutputStream.writeObject(message);
			
			byte[] outgoingPacketData = outputStream.toByteArray();
			
			DatagramPacket outgoingPacket = new DatagramPacket(outgoingPacketData, outgoingPacketData.length);
			outgoingPacket.setAddress(InetAddress.getByName(this.multicastAddress));
			outgoingPacket.setPort(this.multicastPort);
			
			this.multicastSocket.send(outgoingPacket);
			
		} catch (Exception e) {
			throw new RuntimeException("Failed to send DiscoveryRequest", e);
		}
	}
	
	public void setMulticastAddress(String multicastAddress) {
		this.multicastAddress = multicastAddress;
	}
	
	public void setMulticastPort(int multicastPort) {
		this.multicastPort = multicastPort;
	}
}