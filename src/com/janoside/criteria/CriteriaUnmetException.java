package com.janoside.criteria;

@SuppressWarnings("serial")
public class CriteriaUnmetException extends RuntimeException {
	
	public CriteriaUnmetException(Criteria criteria) {
		super(criteria.toString());
	}
}