package com.janoside.criteria;

import com.janoside.queue.ObjectQueue;

@SuppressWarnings("unchecked")
public class ObjectQueueMaxSizeCriteria implements Criteria {
	
	private ObjectQueue objectQueue;
	
	private int maxSize;
	
	public boolean isMet() {
		return (this.objectQueue.getSize() < this.maxSize);
	}
	
	public void setObjectQueue(ObjectQueue objectQueue) {
		this.objectQueue = objectQueue;
	}
	
	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}
	
	@Override
	public String toString() {
		return "ObjectQueueMaxSizeCriteria(objectQueue=" + this.objectQueue.toString() + ", maxSize=" + this.maxSize + ")";
	}
}