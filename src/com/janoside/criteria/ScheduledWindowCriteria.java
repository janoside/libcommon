package com.janoside.criteria;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.util.DateUtil;
import com.janoside.util.SystemTimeSource;
import com.janoside.util.TimeSource;

@ManagedResource(objectName = "Janoside:name=ScheduledWindowCriteria")
public class ScheduledWindowCriteria implements Criteria {
	
	private TimeSource timeSource;
	
	private DateFormat dateFormat;
	
	private DateFormat dateTimeFormat;
	
	private String windowStartTime;
	
	private String windowEndTime;
	
	public ScheduledWindowCriteria() {
		this.timeSource = new SystemTimeSource();
		this.dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		this.dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	}
	
	public boolean isMet() {
		String todayString = this.dateFormat.format(DateUtil.getToday());
		
		try {
			Date startTime = this.dateTimeFormat.parse(todayString + " " + this.windowStartTime);
			Date endTime = this.dateTimeFormat.parse(todayString + " " + this.windowEndTime);
			
			long startTimeMillis = startTime.getTime();
			long endTimeMillis = endTime.getTime();
			
			if (endTimeMillis < startTimeMillis) {
				endTimeMillis += DateUtil.DayMillis;
			}
			
			return (this.timeSource.getCurrentTime() > startTimeMillis && this.timeSource.getCurrentTime() < endTimeMillis);
			
		} catch (ParseException pe) {
			throw new RuntimeException("Failed parsing date to determine ScheduledWindow status", pe);
		}
	}
	
	public void setTimeSource(TimeSource timeSource) {
		this.timeSource = timeSource;
	}
	
	public void setWindowStartTime(String windowStartTime) {
		this.windowStartTime = windowStartTime;
	}
	
	public void setWindowEndTime(String windowEndTime) {
		this.windowEndTime = windowEndTime;
	}
	
	@ManagedAttribute
	public long getSecondsUntilWindowStart() {
		if (this.isMet()) {
			return -1;
			
		} else {
			String todayString = this.dateFormat.format(DateUtil.getToday());
			
			try {
				long startTimeMillis = this.dateTimeFormat.parse(todayString + " " + this.windowStartTime).getTime();
				if (startTimeMillis < this.timeSource.getCurrentTime()) {
					startTimeMillis += DateUtil.DayMillis;
				}
				
				return (startTimeMillis - this.timeSource.getCurrentTime()) / 1000;
				
			} catch (ParseException pe) {
				throw new RuntimeException("Failed parsing date to determine ScheduledWindow status", pe);
			}
		}
	}
	
	@ManagedAttribute
	public long getSecondsUntilWindowEnd() {
		if (!this.isMet()) {
			return -1;
			
		} else {
			String todayString = this.dateFormat.format(DateUtil.getToday());
			
			try {
				long endTimeMillis = this.dateTimeFormat.parse(todayString + " " + this.windowEndTime).getTime();
				if (endTimeMillis < this.timeSource.getCurrentTime()) {
					endTimeMillis += DateUtil.DayMillis;
				}
				
				return (endTimeMillis - this.timeSource.getCurrentTime()) / 1000;
				
			} catch (ParseException pe) {
				throw new RuntimeException("Failed parsing date to determine ScheduledWindow status", pe);
			}
		}
	}
	
	@Override
	public String toString() {
		return "ScheduledWindowCriteria(" + this.windowStartTime + ", " + this.windowEndTime + ")";
	}
}