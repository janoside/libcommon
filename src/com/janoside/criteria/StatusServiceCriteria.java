package com.janoside.criteria;

import java.util.ArrayList;
import java.util.regex.Pattern;

import com.janoside.status.Action;
import com.janoside.status.StatusService;
import com.janoside.status.StatusServiceAware;

public class StatusServiceCriteria implements Criteria, StatusServiceAware {
	
	private StatusService statusService;
	
	private Pattern activeActionNamePattern;
	
	public StatusServiceCriteria() {
		this.activeActionNamePattern = Pattern.compile(".*", Pattern.CASE_INSENSITIVE);
	}
	
	public boolean isMet() {
		ArrayList<Action> actions = new ArrayList<Action>();
		
		actions.addAll(this.statusService.getActiveActions());
		
		for (Action action : actions) {
			if (this.activeActionNamePattern.matcher(action.getName()).matches()) {
				return false;
			}
		}
		
		return true;
	}
	
	public void setStatusService(StatusService statusService) {
		this.statusService = statusService;
	}
	
	public void setActiveActionNamePatternString(String activeActionNamePatternString) {
		this.activeActionNamePattern = Pattern.compile(activeActionNamePatternString, Pattern.CASE_INSENSITIVE);
	}
}