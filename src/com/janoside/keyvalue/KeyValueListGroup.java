package com.janoside.keyvalue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.pool.SimpleObjectFactory;

@ManagedResource(objectName = "Janoside:name=KeyValueListGroup")
public class KeyValueListGroup {
	
	private SimpleObjectFactory<KeyValueList> factory;
	
	private Map<String, KeyValueList> cache;
	
	public KeyValueListGroup() {
		this.cache = new ConcurrentHashMap<String, KeyValueList>();
	}
	
	public KeyValueList getList(String key) {
		if (!this.cache.containsKey(key)) {
			KeyValueList keyValueList = this.factory.createObject();
			keyValueList.setName(key);
			
			this.cache.put(key, keyValueList);
		}
		
		return this.cache.get(key);
	}
	
	@ManagedOperation
	public List<String> printCacheKeys() {
		ArrayList<String> list = new ArrayList<String>(this.cache.keySet());
		
		Collections.sort(list);
		
		return list;
	}
	
	public void setFactory(SimpleObjectFactory<KeyValueList> factory) {
		this.factory = factory;
	}
}