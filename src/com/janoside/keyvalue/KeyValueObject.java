package com.janoside.keyvalue;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.janoside.json.JsonObject;

@SuppressWarnings("serial")
public class KeyValueObject implements Serializable {
	
	private static Gson gson = new Gson();
	
	public static <T> T fromJson(JsonObject json, Class<T> klass) {
		return gson.fromJson(json.toString(), klass);
	}
	
	protected Map<String, List<String>> properties;
	
	protected Map<String, String> uniqueProperties;
	
	protected String id;
	
	public KeyValueObject() {
		this.properties = new HashMap<String, List<String>>();
		this.uniqueProperties = new HashMap<String, String>();
	}
	
	public Map<String, List<String>> getProperties() {
		return properties;
	}
	
	public void setProperties(Map<String, List<String>> properties) {
		this.properties = properties;
	}
	
	public Map<String, String> getUniqueProperties() {
		return uniqueProperties;
	}
	
	public void setUniqueProperties(Map<String, String> uniqueProperties) {
		this.uniqueProperties = uniqueProperties;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public JsonObject toJson() {
		return new JsonObject(gson.toJson(this));
	}
	
	public String getProperty(String name) {
		if (!this.propertyExists(name)) {
			return null;
			
		} else {
			return this.properties.get(name).get(0);
		}
	}
	
	public String getUniqueProperty(String name) {
		if (!this.uniquePropertyExists(name)) {
			return null;
			
		} else {
			return this.uniqueProperties.get(name);
		}
	}
	
	public boolean propertyExists(String property) {
		return (this.properties.containsKey(property) && !this.properties.get(property).isEmpty());
	}
	
	public boolean propertyNotBlank(String property) {
		return (this.properties.containsKey(property) && !this.properties.get(property).isEmpty() && StringUtils.hasText(this.properties.get(property).get(0)));
	}
	
	public boolean uniquePropertyExists(String property) {
		return this.uniqueProperties.containsKey(property);
	}
	
	public boolean uniquePropertyNotBlank(String property) {
		return (this.uniqueProperties.containsKey(property) && StringUtils.hasText(this.uniqueProperties.get(property)));
	}
	
	public String toString() {
		return gson.toJson(this);
	}
	
	public int hashCode() {
		return this.toString().hashCode();
	}
	
	public boolean equals(Object o) {
		if (o == null) {
			return false;
			
		} else if (o instanceof KeyValueObject) {
			return ((KeyValueObject) o).getId().equals(this.id);
			
		} else {
			return false;
		}
	}
}