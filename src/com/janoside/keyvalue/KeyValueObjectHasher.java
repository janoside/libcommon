package com.janoside.keyvalue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.janoside.hash.Hasher;
import com.janoside.hash.Md5Hasher;
import com.janoside.json.JsonObject;

public class KeyValueObjectHasher implements Hasher<JsonObject, String> {
	
	private static final Logger logger = LoggerFactory.getLogger(KeyValueObjectHasher.class);
	
	private Md5Hasher md5Hasher;
	
	public KeyValueObjectHasher() {
		this.md5Hasher = new Md5Hasher();
	}
	
	public String hash(JsonObject json) {
		try {
			JsonObject cleanedJson = new JsonObject(json.toString());
			
			if (cleanedJson.has("properties")) {
				cleanedJson.getJsonObject("properties").remove("createdAt");
				cleanedJson.getJsonObject("properties").remove("updatedAt");
			}
			
			return this.md5Hasher.hash(cleanedJson.toSortedString());
			
		} catch (Throwable t) {
			logger.error("Error hashing KeyValueObject, json=" + json, t);
			
			throw new RuntimeException("Error hashing KeyValueObject", t);
		}
	}
}