package com.janoside.keyvalue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.exception.StandardErrorExceptionHandler;
import com.janoside.hash.Hasher;
import com.janoside.json.JsonObject;
import com.janoside.paging.Page;
import com.janoside.status.MemoryStatusService;
import com.janoside.status.StatusService;
import com.janoside.status.StatusServiceAware;
import com.janoside.storage.MultithreadedKeyValueStore;
import com.janoside.util.DateUtil;

public class KeyValueObjectService implements Hasher<KeyValueObject, String>, ExceptionHandlerAware, StatusServiceAware {
	
	private static final Logger logger = LoggerFactory.getLogger(KeyValueObjectService.class);
	
	protected MultithreadedKeyValueStore<JsonObject> store;
	
	protected KeyValueListGroup keyValueListGroup;
	
	protected ExecutorService executorService;
	
	protected ExceptionHandler exceptionHandler;
	
	private StatusService statusService;
	
	private List<KeyValueObjectObserver> observers;
	
	protected boolean createFullListForEachObjectType;
	
	public KeyValueObjectService() {
		this.executorService = Executors.newCachedThreadPool();
		this.exceptionHandler = new StandardErrorExceptionHandler();
		this.statusService = new MemoryStatusService();
		
		this.observers = new ArrayList<KeyValueObjectObserver>();
		this.createFullListForEachObjectType = true;
	}
	
	public <T extends KeyValueObject> Map<String, T> getObjects(List<String> ids, Class<T> klass) {
		String className = klass.getSimpleName();
		int keyPrefixLength = className.length() + 1;
		
		ArrayList<String> keys = new ArrayList<String>();
		for (String id : ids) {
			keys.add(klass.getSimpleName() + "-" + id);
		}
		
		Map<String, JsonObject> artistJsonMap = this.store.getAll(keys);
		
		HashMap<String, T> objects = new HashMap<String, T>();
		for (Map.Entry<String, JsonObject> entry : artistJsonMap.entrySet()) {
			objects.put(entry.getKey().substring(keyPrefixLength), (T) KeyValueObject.fromJson(entry.getValue(), klass));
		}
		
		return objects;
	}
	
	public <T extends KeyValueObject> List<T> getObjectsListByIds(final List<String> ids, Class<T> klass) {
		Map<String, T> map = this.getObjects(ids, klass);
		
		ArrayList<T> list = new ArrayList<T>(map.values());
		
		Collections.sort(list, new Comparator<T>() {
			public int compare(T o1, T o2) {
				return ids.indexOf(o1.getId()) - ids.indexOf(o2.getId());
			}
		});
		
		return list;
	}
	
	public <T extends KeyValueObject> List<T> getObjects(Page page, Class<T> klass) {
		final String className = klass.getSimpleName();
		
		List<JsonObject> jsonObjects = this.keyValueListGroup.getList(klass.getSimpleName() + "s").getItems(page);
		
		final ArrayList<String> ids = new ArrayList<String>();
		for (JsonObject jsonObject : jsonObjects) {
			ids.add(klass.getSimpleName() + "-" + jsonObject.getString("id"));
		}
		
		Map<String, JsonObject> jsonMap = this.store.getAll(ids);
		
		ArrayList<T> objects = new ArrayList<T>();
		for (Map.Entry<String, JsonObject> entry : jsonMap.entrySet()) {
			objects.add((T) KeyValueObject.fromJson(entry.getValue(), klass));
		}
		
		Collections.sort(objects, new Comparator<T>() {
			public int compare(T o1, T o2) {
				return ids.indexOf(className + "-" + o1.getId()) - ids.indexOf(className + "-" + o2.getId());
			}
		});
		
		return objects;
	}
	
	public <T extends KeyValueObject> List<T> getAllObjects(Class<T> klass) {
		Page page = new Page();
		page.setStart(0);
		page.setCount(this.getObjectCount(klass));
		
		return this.getObjects(page, klass);
	}
	
	public <T extends KeyValueObject> List<T> getObjectsByProperty(String propertyName, String propertyValue, Page page, Class<T> klass) {
		final String className = klass.getSimpleName();
		
		List<JsonObject> jsonObjects = this.keyValueListGroup.getList(className + "s-" + propertyName + "-" + propertyValue).getItems(page);
		
		final ArrayList<String> ids = new ArrayList<String>();
		for (JsonObject jsonObject : jsonObjects) {
			ids.add(className + "-" + jsonObject.getString("id"));
		}
		
		Map<String, JsonObject> jsonMap = this.store.getAll(ids);
		
		ArrayList<T> objects = new ArrayList<T>();
		for (Map.Entry<String, JsonObject> entry : jsonMap.entrySet()) {
			objects.add((T) KeyValueObject.fromJson(entry.getValue(), klass));
		}
		
		Collections.sort(objects, new Comparator<T>() {
			public int compare(T o1, T o2) {
				return ids.indexOf(className + "-" + o1.getId()) - ids.indexOf(className + "-" + o2.getId());
			}
		});
		
		return objects;
	}
	
	public <T extends KeyValueObject> T getObject(String id, Class<T> klass) {
		String className = klass.getSimpleName();
		
		JsonObject json = this.store.get(className + "-" + id);
		
		if (json != null) {
			return (T) KeyValueObject.fromJson(json, klass);
			
		} else {
			return null;
		}
	}
	
	public <T extends KeyValueObject> int getObjectCount(Class<T> klass) {
		String className = klass.getSimpleName();
		
		return this.keyValueListGroup.getList(className + "s").getItemCount();
	}
	
	public <T extends KeyValueObject> int getObjectCountByProperty(String propertyName, String propertyValue, Class<T> klass) {
		String className = klass.getSimpleName();
		
		KeyValueList list = this.keyValueListGroup.getList(className + "s-" + propertyName + "-" + propertyValue);
		
		return list.getItemCount();
	}
	
	public <T extends KeyValueObject> void save(T object) {
		this.save(object, false);
	}
	
	public <T extends KeyValueObject> void save(final T object, boolean forceFullRefresh) {
		T existingObject = null;
		
		JsonObject existingObjectJson = this.store.get(object.getClass().getSimpleName() + "-" + object.getId());
		if (existingObjectJson != null) {
			existingObject = (T) KeyValueObject.fromJson(existingObjectJson, object.getClass());
		}
		
		final boolean isNew = (existingObject == null);
		
		if (!isNew) {
			if (existingObject.propertyNotBlank("createdAt")) {
				object.getProperties().put("createdAt", existingObject.getProperties().get("createdAt"));
				
			} else {
				object.getProperties().put("createdAt", Arrays.asList(DateUtil.format("yyyy-MM-dd HH:mm:ss", new Date())));
			}
			
			String hash1 = this.hash(object);
			String hash2 = this.hash(existingObject);
			
			if (!hash1.equals(hash2)) {
				object.getProperties().put("updatedAt", Arrays.asList(DateUtil.format("yyyy-MM-dd HH:mm:ss", new Date())));
				
			} else if (existingObject.propertyNotBlank("updatedAt")) {
				object.getProperties().put("updatedAt", Arrays.asList(existingObject.getProperty("updatedAt")));
			}
		} else {
			object.getProperties().put("createdAt", Arrays.asList(DateUtil.format("yyyy-MM-dd HH:mm:ss", new Date())));
		}
		
		ArrayList<String> listIds = new ArrayList<String>();
		ArrayList<String> removeListIds = new ArrayList<String>();
		
		ArrayList<String> addUniqueProperties = new ArrayList<String>();
		ArrayList<String> removeUniqueProperties = new ArrayList<String>();
		
		HashMap<String, List<String>> allProperties = new HashMap<String, List<String>>();
		HashMap<String, String> allUniqueProperties = new HashMap<String, String>();
		
		allProperties.putAll(object.getProperties());
		allUniqueProperties.putAll(object.getUniqueProperties());
		
		HashMap<String, List<String>> allExistingProperties = new HashMap<String, List<String>>();
		HashMap<String, String> allExistingUniqueProperties = new HashMap<String, String>();
		
		if (!isNew) {
			allExistingProperties.putAll(existingObject.getProperties());
			allExistingUniqueProperties.putAll(existingObject.getUniqueProperties());
		}
		
		JsonObject jsonRepresentation = object.toJson();
		Iterator<String> keyIterator = jsonRepresentation.keys();
		while (keyIterator.hasNext()) {
			String key = keyIterator.next();
			String value = String.valueOf(jsonRepresentation.get(key));
			
			if (!allProperties.containsKey(key) && StringUtils.hasText(value)) {
				allProperties.put(key, Arrays.asList(value));
			}
		}
		
		if (isNew || forceFullRefresh) {
			for (Map.Entry<String, List<String>> entry : allProperties.entrySet()) {
				for (String value : entry.getValue()) {
					if (this.isValidListEntry(object.getClass(), entry.getKey(), value)) {
						listIds.add(object.getClass().getSimpleName() + "s-" + entry.getKey() + "-" + value);
					}
				}
			}
			
			for (Map.Entry<String, String> entry : object.getUniqueProperties().entrySet()) {
				addUniqueProperties.add(object.getClass().getSimpleName() + "-UniqueProperty-" + entry.getKey() + "-" + entry.getValue());
			}
			
			if (this.createFullListForEachObjectType) {
				listIds.add(object.getClass().getSimpleName() + "s");
			}
		} else {
			jsonRepresentation = existingObject.toJson();
			keyIterator = jsonRepresentation.keys();
			while (keyIterator.hasNext()) {
				String key = keyIterator.next();
				String value = String.valueOf(jsonRepresentation.get(key));
				
				if (!allExistingProperties.containsKey(key) && StringUtils.hasText(value)) {
					allExistingProperties.put(key, Arrays.asList(value));
				}
			}
			
			for (Map.Entry<String, List<String>> entry : allProperties.entrySet()) {
				for (String value : entry.getValue()) {
					if (!allExistingProperties.containsKey(entry.getKey())) {
						allExistingProperties.put(entry.getKey(), new ArrayList<String>());
					}
					
					if (!allExistingProperties.get(entry.getKey()).contains(value)) {
						if (this.isValidListEntry(object.getClass(), entry.getKey(), value)) {
							listIds.add(object.getClass().getSimpleName() + "s-" + entry.getKey() + "-" + value);
						}
					}
				}
			}
			
			// identify newly added unique properties
			for (Map.Entry<String, String> entry : object.getUniqueProperties().entrySet()) {
				if (!existingObject.getUniqueProperties().containsKey(entry.getKey())) {
					addUniqueProperties.add(object.getClass().getSimpleName() + "-UniqueProperty-" + entry.getKey() + "-" + entry.getValue());
				}
			}
			
			// identify newly deleted unique properties
			for (Map.Entry<String, String> entry : existingObject.getUniqueProperties().entrySet()) {
				if (!object.getUniqueProperties().containsKey(entry.getKey())) {
					removeUniqueProperties.add(object.getClass().getSimpleName() + "-UniqueProperty-" + entry.getKey() + "-" + entry.getValue());
				}
			}
			
			// remove other lists that we dont have anymore
			for (Map.Entry<String, List<String>> entry : allExistingProperties.entrySet()) {
				for (String value : entry.getValue()) {
					if (this.isValidListEntry(object.getClass(), entry.getKey(), value)) {
						if (!allProperties.containsKey(entry.getKey()) || !allProperties.get(entry.getKey()).contains(value)) {
							removeListIds.add(object.getClass().getSimpleName() + "s-" + entry.getKey() + "-" + value);
						}
					}
				}
			}
		}
		
		final CountDownLatch latch = new CountDownLatch(
				listIds.size() + 
				removeListIds.size() + 
				addUniqueProperties.size() +
				removeUniqueProperties.size());
		
		for (final String listId : listIds) {
			this.executorService.execute(new Runnable() {
				public void run() {
					try {
						KeyValueList list = keyValueListGroup.getList(listId);
						
						JsonObject valueToAdd = new JsonObject().put("id", object.getId());
						
						if (true) { //isNew || !list.contains(valueToAdd)) {
							list.add(valueToAdd);
						}
					} catch (Throwable t) {
						logger.error("Error adding item to list, object=" + object + ", listId=" + listId);
						
						exceptionHandler.handleException(new RuntimeException("Error adding item to list", t));
						
					} finally {
						latch.countDown();
					}
				}
			});
		}
		
		for (final String listId : removeListIds) {
			this.executorService.execute(new Runnable() {
				public void run() {
					try {
						KeyValueList list = keyValueListGroup.getList(listId);
						
						list.remove(new JsonObject().put("id", object.getId()));
						
					} catch (Throwable t) {
						logger.error("Error removing item from list, object=" + object + ", listId=" + listId);
						
						exceptionHandler.handleException(new RuntimeException("Error removing item from list", t));
						
					} finally {
						latch.countDown();
					}
				}
			});
		}
		
		final JsonObject idObject = new JsonObject().put("id", object.getId());
		for (final String addUniqueProperty : addUniqueProperties) {
			this.executorService.execute(new Runnable() {
				public void run() {
					try {
						store.put(addUniqueProperty, idObject);
						
					} catch (Throwable t) {
						logger.error("Error adding unique property, uniqueProperty=" + addUniqueProperty + ", idObject=" + idObject);
						
						exceptionHandler.handleException(new RuntimeException("Error adding unique property for object", t));
						
					} finally {
						latch.countDown();
					}
				}
			});
		}
		
		for (final String removeUniqueProperty : removeUniqueProperties) {
			this.executorService.execute(new Runnable() {
				public void run() {
					try {
						store.remove(removeUniqueProperty);
						
					} catch (Throwable t) {
						logger.error("Error removing unique property, uniqueProperty=" + removeUniqueProperty + ", idObject=" + idObject);
						
						exceptionHandler.handleException(new RuntimeException("Error removing unique property for object", t));
						
					} finally {
						latch.countDown();
					}
				}
			});
		}
		
		try {
			latch.await();
			
		} catch (InterruptedException ie) {
			this.exceptionHandler.handleException(ie);
		}
		
		/*
		HashMap<String, JsonObject> objectsByKey = new HashMap<String, JsonObject>();
		
		// handle unique properties
		JsonObject idJsonObject = new JsonObject().put("id", object.getId());
		for (Map.Entry<String, String> entry : object.getUniqueProperties().entrySet()) {
			objectsByKey.put(object.getClass().getSimpleName() + "-UniqueProperty-" + entry.getKey() + "-" + entry.getValue(), idJsonObject);
		}
		*/
		
		this.store.put(object.getClass().getSimpleName() + "-" + object.getId(), object.toJson());
		
		for (KeyValueObjectObserver observer : this.observers) {
			try {
				if (isNew) {
					observer.onNewObject(object);
					
				} else {
					observer.onUpdateObject(object);
				}
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T extends KeyValueObject> void delete(final T object) {
		T existingObject = null;
		
		JsonObject existingObjectJson = this.store.get(object.getClass().getSimpleName() + "-" + object.getId());
		if (existingObjectJson != null) {
			existingObject = (T) KeyValueObject.fromJson(existingObjectJson, object.getClass());
		}
		
		final boolean isNew = (existingObject == null);
		
		HashSet<String> removeListIds = new HashSet<String>();
		HashSet<String> removeUniqueProperties = new HashSet<String>();
		
		HashMap<String, List<String>> allProperties = new HashMap<String, List<String>>();
		allProperties.putAll(object.getProperties());
		
		HashMap<String, List<String>> allExistingProperties = new HashMap<String, List<String>>();
		
		if (!isNew) {
			allExistingProperties.putAll(existingObject.getProperties());
		}
		
		JsonObject jsonRepresentation = object.toJson();
		Iterator<String> keyIterator = jsonRepresentation.keys();
		while (keyIterator.hasNext()) {
			String key = keyIterator.next();
			String value = String.valueOf(jsonRepresentation.get(key));
			
			if (!allProperties.containsKey(key) && StringUtils.hasText(value)) {
				allProperties.put(key, Arrays.asList(value));
			}
		}
		
		for (Map.Entry<String, List<String>> entry : allProperties.entrySet()) {
			for (String value : entry.getValue()) {
				if (this.isValidListEntry(object.getClass(), entry.getKey(), value)) {
					removeListIds.add(object.getClass().getSimpleName() + "s-" + entry.getKey() + "-" + value);
				}
			}
		}
		
		for (Map.Entry<String, String> entry : object.getUniqueProperties().entrySet()) {
			removeUniqueProperties.add(object.getClass().getSimpleName() + "-UniqueProperty-" + entry.getKey() + "-" + entry.getValue());
		}
		
		removeListIds.add(object.getClass().getSimpleName() + "s");
		
		if (!isNew) {
			jsonRepresentation = existingObject.toJson();
			keyIterator = jsonRepresentation.keys();
			while (keyIterator.hasNext()) {
				String key = keyIterator.next();
				String value = String.valueOf(jsonRepresentation.get(key));
				
				if (!allExistingProperties.containsKey(key) && StringUtils.hasText(value)) {
					allExistingProperties.put(key, Arrays.asList(value));
				}
			}
			
			for (Map.Entry<String, List<String>> entry : allExistingProperties.entrySet()) {
				for (String value : entry.getValue()) {
					if (this.isValidListEntry(object.getClass(), entry.getKey(), value)) {
						removeListIds.add(object.getClass().getSimpleName() + "s-" + entry.getKey() + "-" + value);
					}
				}
			}
			
			for (Map.Entry<String, String> entry : existingObject.getUniqueProperties().entrySet()) {
				String key = object.getClass().getSimpleName() + "-UniqueProperty-" + entry.getKey() + "-" + entry.getValue();
				
				if (!removeUniqueProperties.contains(key)) {
					removeUniqueProperties.add(key);
				}
			}
		}
		
		final CountDownLatch latch = new CountDownLatch(removeListIds.size() + removeUniqueProperties.size());
		
		for (final String listId : removeListIds) {
			this.executorService.execute(new Runnable() {
				public void run() {
					try {
						statusService.startAction("KVS.delete(" + object.getId() + ").removeFromList(" + listId + ")", 1);
						
						KeyValueList list = keyValueListGroup.getList(listId);
						
						list.remove(new JsonObject().put("id", object.getId()));
						
					} catch (Throwable t) {
						logger.error("Error removing item from list, object=" + object + ", listId=" + listId);
						
						exceptionHandler.handleException(new RuntimeException("Error removing item from list", t));
						
					} finally {
						latch.countDown();
						
						statusService.onItemCompleted("KVS.delete(" + object.getId() + ").removeFromList(" + listId + ")");
					}
				}
			});
		}
		
		for (final String removeUniqueProperty : removeUniqueProperties) {
			this.executorService.execute(new Runnable() {
				public void run() {
					try {
						store.remove(removeUniqueProperty);
						
					} catch (Throwable t) {
						logger.error("Error removing unique property, uniqueProperty=" + removeUniqueProperty);
						
						exceptionHandler.handleException(new RuntimeException("Error removing unique property", t));
						
					} finally {
						latch.countDown();
					}
				}
			});
		}
		
		try {
			latch.await();
			
		} catch (InterruptedException ie) {
			this.exceptionHandler.handleException(ie);
		}
		
		this.store.remove(object.getClass().getSimpleName() + "-" + object.getId());
	}
	
	public String hash(KeyValueObject object) {
		JsonObject json = object.toJson();
		
		json.getJsonObject("properties").remove("createdAt");
		json.getJsonObject("properties").remove("updatedAt");
		
		List<String> dynamicProperties = this.getDynamicProperties(object.getClass());
		for (String dynamicProperty : dynamicProperties) {
			json.getJsonObject("properties").remove(dynamicProperty);
		}
		
		return Integer.toString(json.toTabbedString().hashCode());
	}
	
	protected List<String> getDynamicProperties(Class<?> c) {
		return new ArrayList<String>();
	}
	
	protected boolean isValidListEntry(Class<?> c, String listName, String listValue) {
		if (listName == null || listValue == null) {
			return false;
		}
		
		if (listName.equals("id") || listName.equals("properties") || listName.equals("uniqueProperties")) {
			return false;
		}
		
		if (listName.equals("name")) {
			return false;
		}
		
		if (listName.equals("createdAt") || listName.equals("updatedAt")) {
			return false;
		}
		
		if (listName.toLowerCase().contains("encrypted")) {
			return false;
		}
		
		if (listValue.toLowerCase().startsWith("http://")) {
			return false;
		}
		
		if (listValue.length() > 64) {
			return false;
		}
		
		// block timestamps, super dumb
		if (listValue.matches("\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}")) {
			return false;
		}
		
		// block uuids, really stupid
		if (listValue.matches("\\w{8}-\\w{4}-\\w{4}-\\w{4}-\\w{12}")) {
			return false;
		}
		
		// block emails, garbage
		if (listValue.matches("^[\\w\\.-]+@[\\w\\-]+\\.+[A-Za-z]{2,4}$")) {
			return false;
		}
		
		return true;
	}
	
	public void setStore(MultithreadedKeyValueStore<JsonObject> store) {
		this.store = store;
	}
	
	public void setKeyValueListGroup(KeyValueListGroup keyValueListGroup) {
		this.keyValueListGroup = keyValueListGroup;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setStatusService(StatusService statusService) {
		this.statusService = statusService;
	}
	
	public void setObservers(List<KeyValueObjectObserver> observers) {
		this.observers = observers;
	}
}