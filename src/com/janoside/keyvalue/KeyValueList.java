package com.janoside.keyvalue;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.json.JsonArray;
import com.janoside.json.JsonObject;
import com.janoside.paging.Page;
import com.janoside.paging.SortDirection;
import com.janoside.paging.SortOrder;
import com.janoside.status.MemoryStatusService;
import com.janoside.status.StatusService;
import com.janoside.status.StatusServiceAware;
import com.janoside.storage.KeyValueStore;
import com.janoside.util.RandomUtil;

public class KeyValueList implements Iterable<JsonObject>, ExceptionHandlerAware, StatusServiceAware {
	
	private static final Logger logger = LoggerFactory.getLogger(KeyValueList.class);
	
	public static String keySeparator = ":";
	
	static {
		if (File.separatorChar == '\\') {
			keySeparator = "_";
			
		} else {
			keySeparator = ":";
		}
	}
	
	private ExecutorService executorService;
	
	private KeyValueStore<JsonObject> store;
	
	private ExceptionHandler exceptionHandler;
	
	private StatusService statusService;
	
	private String name;
	
	private long timeout;
	
	private int pageSize;
	
	public KeyValueList() {
		this.executorService = Executors.newCachedThreadPool();
		this.statusService = new MemoryStatusService();
		this.timeout = 30000;
		this.pageSize = 100;
	}
	
	/**
	 * currentPageIndex is the counter that describes the ordinal of the
	 * next element to be added to the page that is currently being 
	 * built. currentPageId is the ordinal of the page that is
	 * currently being built in the list of all of the pages.
	 * @param json
	 */
	public synchronized boolean add(JsonObject json) {
		JsonObject manifest = this.store.get(this.name + keySeparator + "Manifest");
		
		if (manifest == null) {
			manifest = new JsonObject();
			
			//manifest.put("currentPageIndex", 1);
			//manifest.put("currentPageId", 1);
			manifest.put("itemCount", 0);
		}
		
		int itemCount = manifest.getInt("itemCount");
		int currentPageId = (itemCount / this.pageSize) + 1;
		
		JsonObject currentPage = this.store.get(this.name + keySeparator + "Page" + keySeparator + currentPageId);
		if (currentPage == null) {
			currentPage = new JsonObject();
			currentPage.put("items", new JsonArray());
		}
		
		JsonArray items = currentPage.getJsonArray("items");
		items.put(json);
		
		this.store.put(this.name + keySeparator + "Page" + keySeparator + currentPageId, currentPage);
		
		manifest.put("itemCount", itemCount + 1);
		
		this.store.put(this.name + keySeparator + "Manifest", manifest);
		
		return true;
	}
	
	public synchronized void removeDuplicates() {
		JsonObject manifest = this.store.get(this.name + keySeparator + "Manifest");
		
		if (manifest == null) {
			return;
		}
		
		int itemCount = manifest.getInt("itemCount");
		int pageCount = itemCount / this.pageSize;
		if (pageCount * this.pageSize < itemCount) {
			pageCount++;
		}
		
		ArrayList<String> allItems = new ArrayList<String>(50000);
		
		this.statusService.startAction("KeyValueList(" + this.name + ").removeDuplicates.buildList", pageCount);
		for (int i = 0; i < pageCount; i++) {
			List<JsonObject> items = this.getItemsFromBeginning(i * this.pageSize, this.pageSize);
			
			for (JsonObject item : items) {
				allItems.add(item.toSortedString());
			}
			
			this.statusService.onItemCompleted("KeyValueList(" + this.name + ").removeDuplicates.buildList");
		}
		
		ArrayList<Integer> indexesToRemove = new ArrayList<Integer>();
		for (int i = (allItems.size() - 1); i >= 0; i--) {
			String item = allItems.get(i);
			
			int firstIndex = allItems.indexOf(item);
			if (firstIndex < i) {
				indexesToRemove.add(i);
			}
		}
		
		this.statusService.startAction("KeyValueList(" + this.name + ").removeDuplicates.removeItems", indexesToRemove.size());
		for (Integer index : indexesToRemove) {
			this.remove(index);
			
			this.statusService.onItemCompleted("KeyValueList(" + this.name + ").removeDuplicates.removeItems");
		}
	}
	
	public synchronized void remove(JsonObject json) {
		if (this.getItemCount() > 0) {
			Iterator<JsonObject> iterator = this.iterator();
			
			int index = 0;
			while (iterator.hasNext()) {
				JsonObject testJson = iterator.next();
				
				if (json.toString().equals(testJson.toString())) {
					break;
					
				} else {
					index++;
				}
			}
			
			if (index < this.getItemCount()) {
				this.remove(index);
			}
		}
	}
	
	public synchronized void remove(final int index) {
		this.statusService.startAction("KeyValueList(" + this.name + ").remove(" + index + ")", 1);
		
		try {
			int pageIndex = index / this.pageSize;
			
			JsonObject manifest = this.store.get(this.name + keySeparator + "Manifest");
			
			if (manifest == null) {
				return;
			}
			
			int itemCount = manifest.getInt("itemCount");
			int currentPageId = (itemCount / this.pageSize) + 1;//manifest.getInt("currentPageId");
			
			if (currentPageId == (pageIndex + 1)) {
				// removing item from last page, no need to shift items
				JsonObject page = store.get(name + keySeparator + "Page" + keySeparator + currentPageId);
				
				if ((index % this.pageSize) >= page.getJsonArray("items").length()) {
					logger.error("Error removing item from KeyValueList(" + this.name + "), invalid item index: list=" + this.name + ", length=" + this.getItemCount() + ", index=" + index);
					
					throw new IllegalArgumentException("Error removing item from KeyValueList(" + this.name + "), invalid item index");
				}
				
				page.getJsonArray("items").removeByIndex(index % this.pageSize);
				
				if (page.getJsonArray("items").length() == 0) {
					this.store.remove(name + keySeparator + "Page" + keySeparator + currentPageId);
					
				} else {
					this.store.put(name + keySeparator + "Page" + keySeparator + currentPageId, page);
				}
			} else {
				
				final CountDownLatch latch = new CountDownLatch(currentPageId - pageIndex + 1);
				
				this.statusService.startAction("KeyValueList(" + this.name + ").remove(" + index + ").rebuildPages", (currentPageId - pageIndex + 1));
				
				final HashMap<Integer, JsonObject> pages = new HashMap<Integer, JsonObject>();
				for (int i = pageIndex; i <= currentPageId; i++) {
					final int x = i;
					
					this.executorService.execute(new Runnable() {
						public void run() {
							try {
								JsonObject page = store.get(name + keySeparator + "Page" + keySeparator + x);
								
								if (page != null) {
									pages.put(x, page);
								}
							} catch (Throwable t) {
								exceptionHandler.handleException(t);
								
							} finally {
								latch.countDown();
								
								statusService.onItemCompleted("KeyValueList(" + name + ").remove(" + index + ").rebuildPages");
							}
						}
					});
				}
				
				try {
					latch.await();
					
				} catch (InterruptedException ie) {
					this.exceptionHandler.handleException(ie);
				}
				
				if (!pages.containsKey(pageIndex + 1)) {
					logger.error("Error removing item from KeyValueList(" + this.name + "), invalid item index: list=" + this.name + ", length=" + this.getItemCount() + ", index=" + index);
					
					throw new IllegalArgumentException("Error removing item from KeyValueList(" + this.name + "), invalid item index");
				}
				
				JsonArray activeArray = pages.get(pageIndex + 1).getJsonArray("items");
				
				activeArray.removeByIndex(index % this.pageSize);
				
				for (int i = pageIndex; i <= currentPageId; i++) {
					if (pages.containsKey(i) && pages.containsKey(i + 1)) {
						JsonObject page1 = pages.get(i);
						JsonObject page2 = pages.get(i + 1);
						
						JsonArray array1 = page1.getJsonArray("items");
						JsonArray array2 = page2.getJsonArray("items");
						
						if (array1.length() < this.pageSize && array2.length() > 0) {
							JsonObject firstItemIn2 = array2.getJsonObject(0);
							
							array1.put(firstItemIn2);
							array2.remove(firstItemIn2);
						}
					}
				}
				
				final CountDownLatch latch2 = new CountDownLatch(pages.size());
				
				for (final Map.Entry<Integer, JsonObject> entry : pages.entrySet()) {
					this.executorService.execute(new Runnable() {
						public void run() {
							try {
								store.put(name + keySeparator + "Page" + keySeparator + entry.getKey(), entry.getValue());
								
							} catch (Throwable t) {
								exceptionHandler.handleException(t);
								
							} finally {
								latch2.countDown();
							}
						}
					});
				}
				
				try {
					latch2.await();
					
				} catch (InterruptedException ie) {
					this.exceptionHandler.handleException(ie);
				}
			}
			
			manifest.put("itemCount", manifest.getInt("itemCount") - 1);
			
			this.store.put(name + keySeparator + "Manifest", manifest);
			
		} finally {
			this.statusService.onItemCompleted("KeyValueList(" + this.name + ").remove(" + index + ")");
		}
	}
	
	public synchronized void updateItem(int index, JsonObject json) {
		int page = index / this.pageSize + 1;
		
		JsonObject pageJson = this.store.get(this.name + keySeparator + "Page" + keySeparator + page);
		JsonArray itemsArray = pageJson.getJsonArray("items");
		
		ArrayList<JsonObject> itemsList = new ArrayList<JsonObject>();
		for (int i = 0; i < itemsArray.length(); i++) {
			if (i == (index % this.pageSize)) {
				itemsList.add(json);
				
			} else {
				itemsList.add(itemsArray.getJsonObject(i));
			}
		}
		
		JsonArray newItemsArray = new JsonArray();
		for (JsonObject item : itemsList) {
			newItemsArray.put(item);
		}
		
		pageJson.put("items", newItemsArray);
		
		this.store.put(this.name + keySeparator + "Page" + keySeparator + page, pageJson);
	}
	
	public List<JsonObject> getItems(final Page page) {
		if (page.getSortFields().isEmpty() || page.getSortFields().get(0).getSortDirection() == SortDirection.Descending) {
			JsonObject manifest = this.store.get(this.name + keySeparator + "Manifest");
			
			if (manifest == null) {
				return new ArrayList<JsonObject>();
			}
			
			int itemCount = manifest.getInt("itemCount");
			
			int translatedStart = itemCount - page.getStart() - page.getCount();
			int translatedCount = page.getCount();
			if (translatedStart < 0) {
				translatedCount += translatedStart;
				translatedStart = 0;
			}
			
			if (translatedCount < 1) {
				return new ArrayList<JsonObject>();
			}
			
			List<JsonObject> items = this.getItemsFromBeginning(translatedStart, translatedCount);
			
			Collections.reverse(items);
			
			return items;
			
		} else {
			return this.getItemsFromBeginning(page.getStart(), page.getCount());
		}
	}
	
	public List<JsonObject> getItems(final int start, int count, SortOrder sortOrder) {
		if (sortOrder == SortOrder.Descending) {
			JsonObject manifest = this.store.get(this.name + keySeparator + "Manifest");
			
			if (manifest == null) {
				return new ArrayList<JsonObject>();
			}
			
			int itemCount = manifest.getInt("itemCount");
			
			int translatedStart = itemCount - start - count;
			int translatedCount = count;
			if (translatedStart < 0) {
				translatedCount += translatedStart;
				translatedStart = 0;
			}
			
			if (translatedCount < 1) {
				return new ArrayList<JsonObject>();
			}
			
			List<JsonObject> items = this.getItemsFromBeginning(translatedStart, translatedCount);
			
			Collections.reverse(items);
			
			return items;
			
		} else {
			return this.getItemsFromBeginning(start, count);
		}
	}
	
	private List<JsonObject> getItemsFromBeginning(final int start, int count) {
		if (count > this.getItemCount()) {
			count = this.getItemCount();
		}
		
		int firstPage = start / this.pageSize + 1;
		int pageCount = (count / this.pageSize + (count % this.pageSize == 0 ? 0 : 1) + (start % this.pageSize == 0 ? 0 : 1));
		
		final CountDownLatch latch = new CountDownLatch(pageCount);
		
		final HashMap<Integer, List<JsonObject>> pages = new HashMap<Integer, List<JsonObject>>();
		for (int i = firstPage; i < (firstPage + pageCount); i++) {
			final int pageIndex = i;
			
			pages.put(i, new ArrayList<JsonObject>());
			
			this.executorService.execute(new Runnable() {
				public void run() {
					try {
						JsonObject page = store.get(name + keySeparator + "Page" + keySeparator + pageIndex);
						if (page != null) {
							JsonArray items = page.getJsonArray("items");
							
							for (int j = 0; j < items.length(); j++) {
								pages.get(pageIndex).add(items.getJsonObject(j));
							}
						}
					} catch (Throwable t) {
						exceptionHandler.handleException(t);
						
					} finally {
						latch.countDown();
					}
				}
			});
		}
		
		try {
			latch.await(this.timeout, TimeUnit.MILLISECONDS);
			
		} catch (InterruptedException ie) {
			throw new RuntimeException(ie);
		}
		
		ArrayList<JsonObject> items = new ArrayList<JsonObject>((int) count);
		for (int i = firstPage; i < (firstPage + pageCount); i++) {
			if (i == firstPage) {
				for (int j = (start % this.pageSize); j < pages.get(i).size(); j++) {
					items.add(pages.get(i).get(j));
				}
			} else {
				items.addAll(pages.get(i));
			}
		}
		
		while (items.size() > count) {
			items.remove(items.size() - 1);
		}
		
		return items;
	}
	
	public List<Integer> indexesOf(JsonObject json) {
		ArrayList<Integer> indexes = new ArrayList<Integer>();
		
		JsonObject manifest = this.store.get(this.name + keySeparator + "Manifest");
		
		if (manifest == null) {
			return new ArrayList<Integer>();
		}
		
		int itemCount = manifest.getInt("itemCount");
		int pageCount = itemCount / this.pageSize;
		if (pageCount * this.pageSize < itemCount) {
			pageCount++;
		}
		
		String statusKey = "KeyValueList(" + this.name + ").indexesOf_" + RandomUtil.randomInt();
		
		this.statusService.startAction(statusKey, pageCount);
		
		int index = 0;
		for (int i = 0; i < pageCount; i++) {
			List<JsonObject> items = this.getItemsFromBeginning(i * this.pageSize, this.pageSize);
			
			for (JsonObject item : items) {
				if (item.toString().equals(json.toString())) {
					indexes.add(index);
				}
				
				index++;
			}
			
			this.statusService.onItemCompleted(statusKey);
		}
		
		return indexes;
	}
	
	public boolean contains(JsonObject json) {
		JsonObject manifest = this.store.get(this.name + keySeparator + "Manifest");
		
		if (manifest == null) {
			return false;
		}
		
		int itemCount = manifest.getInt("itemCount");
		int pageCount = itemCount / this.pageSize;
		if (pageCount * this.pageSize < itemCount) {
			pageCount++;
		}
		
		for (int i = 0; i < pageCount; i++) {
			List<JsonObject> items = this.getItemsFromBeginning(i * this.pageSize, this.pageSize);
			
			for (JsonObject item : items) {
				if (item.toString().equals(json.toString())) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	public Iterator<JsonObject> iterator() {
		return new KeyValueListIterator(this);
	}
	
	public void setStore(KeyValueStore<JsonObject> store) {
		this.store = store;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setStatusService(StatusService statusService) {
		this.statusService = statusService;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}
	
	public int getPageSize() {
		return this.pageSize;
	}
	
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	public int getPageCount() {
		JsonObject manifest = this.store.get(this.name + keySeparator + "Manifest");
		
		if (manifest == null) {
			return 0;
			
		} else {
			return manifest.getInt("currentPageId");
		}
	}
	
	public int getItemCount() {
		JsonObject manifest = this.store.get(this.name + keySeparator + "Manifest");
		
		if (manifest == null) {
			return 0;
			
		} else {
			return manifest.getInt("itemCount");
		}
	}
	
	private static class KeyValueListIterator implements Iterator<JsonObject> {
		
		private KeyValueList keyValueList;
		
		private Iterator<JsonObject> internalIterator;
		
		private int itemCount;
		
		private int index;
		
		public KeyValueListIterator(KeyValueList keyValueList) {
			this.keyValueList = keyValueList;
			this.itemCount = this.keyValueList.getItemCount();
			
			if (this.itemCount < 20000) {
				List<JsonObject> allItems = this.keyValueList.getItems(0, this.itemCount, SortOrder.Ascending);
				
				this.internalIterator = allItems.iterator();
			}
		}
		
		public boolean hasNext() {
			if (this.internalIterator != null) {
				return this.internalIterator.hasNext();
				
			} else {
				return (this.index < this.itemCount);
			}
		}
		
		public JsonObject next() {
			if (this.internalIterator != null) {
				return this.internalIterator.next();
				
			} else {
				try {
					return this.keyValueList.getItems(this.index++, 1, SortOrder.Ascending).get(0);
					
				} catch (Throwable t) {
					System.out.println("haha");
					
					throw new RuntimeException(t);
				}
			}
		}
		
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}
}