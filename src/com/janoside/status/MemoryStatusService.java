package com.janoside.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.stats.EventTracker;

public class MemoryStatusService implements StatusService, ExceptionHandlerAware {
	
	private ExceptionHandler exceptionHandler;
	
	private Map<String, EventTracker> actionEventTrackers;
	
	private Map<String, AtomicInteger> actionCounters;
	
	private Map<String, AtomicInteger> actionItemCounts;
	
	private Map<String, Long> actionStartTimes;
	
	private List<ActionObserver> observers;
	
	public MemoryStatusService() {
		this.actionEventTrackers = new ConcurrentHashMap<String, EventTracker>();
		this.actionCounters = new ConcurrentHashMap<String, AtomicInteger>();
		this.actionItemCounts = new ConcurrentHashMap<String, AtomicInteger>();
		this.actionStartTimes = new ConcurrentHashMap<String, Long>();
		this.observers = new ArrayList<ActionObserver>();
	}
	
	public List<Action> getActiveActions() {
		ArrayList<Action> activeActions = new ArrayList<Action>(this.actionCounters.size());
		
		for (String key : this.actionCounters.keySet()) {
			activeActions.add(this.getStatus(key));
		}
		
		return activeActions;
	}
	
	public Action getStatus(String name) {
		if (!this.actionStartTimes.containsKey(name)) {
			return null;
		}
		
		Action action = new Action();
		action.setName(name);
		action.setStartTime(this.actionStartTimes.get(name));
		action.setElapsedTime(System.currentTimeMillis() - action.getStartTime());
		action.setItemCount(this.actionItemCounts.get(name).get());
		action.setCurrentItem(this.actionItemCounts.get(name).get() - this.actionCounters.get(name).get());
		action.setCompletionRate(this.actionEventTrackers.get(name).getRate());
		
		return action;
	}
	
	public boolean isActive(String actionName) {
		return this.actionStartTimes.containsKey(actionName);
	}
	
	public void startAction(String name, int itemCount) {
		if (itemCount > 0 && !this.actionStartTimes.containsKey(name)) {
			this.actionEventTrackers.put(name, new EventTracker());
			this.actionCounters.put(name, new AtomicInteger(itemCount));
			this.actionItemCounts.put(name, new AtomicInteger(itemCount));
			this.actionStartTimes.put(name, System.currentTimeMillis());
			
			for (ActionObserver observer : this.observers) {
				try {
					observer.onActionStarted(name, this.getStatus(name));
					
				} catch (Throwable t) {
					this.exceptionHandler.handleException(t);
				}
			}
		}
	}
	
	public void onItemCompleted(final String name) {
		this.actionCounters.get(name).getAndDecrement();
		
		if (this.actionCounters.get(name).get() <= 0) {
			this.actionEventTrackers.remove(name);
			this.actionCounters.remove(name);
			this.actionItemCounts.remove(name);
			this.actionStartTimes.remove(name);
			
			for (ActionObserver observer : this.observers) {
				try {
					observer.onActionCompleted(name, this.getStatus(name));
					
				} catch (Throwable t) {
					this.exceptionHandler.handleException(t);
				}
			}
		} else {
			this.actionEventTrackers.get(name).increment();
		}
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
}