package com.janoside.status;

import java.util.List;

public interface StatusService {
	
	List<Action> getActiveActions();
	
	Action getStatus(String actionName);
	
	boolean isActive(String actionName);
	
	void onItemCompleted(String actionName);
	
	void startAction(String actionName, int itemCount);
}