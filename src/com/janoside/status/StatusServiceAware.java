package com.janoside.status;

public interface StatusServiceAware {
	
	void setStatusService(StatusService statusService);
}