package com.janoside.hash;

import com.lambdaworks.crypto.SCryptUtil;

public class SCryptPasswordHasher implements Hasher<String, String>, HashVerifier<String> {
	
	private int cpuCostParameter;
	
	private int memoryCostParameter;
	
	private int parallelizationParameter;
	
	public SCryptPasswordHasher() {
		this.cpuCostParameter = 256;
		this.memoryCostParameter = 50;
		this.parallelizationParameter = 1;
	}
	
	public String hash(String input) {
		return SCryptUtil.scrypt(
				input,
				this.cpuCostParameter,
				this.memoryCostParameter,
				this.parallelizationParameter);
	}
	
	public boolean verify(String input, String hash) {
		return SCryptUtil.check(input, hash);
	}
	
	public int getCpuCostParameter() {
		return cpuCostParameter;
	}
	
	public void setCpuCostParameter(int cpuCostParameter) {
		this.cpuCostParameter = cpuCostParameter;
	}
	
	public int getMemoryCostParameter() {
		return memoryCostParameter;
	}
	
	public void setMemoryCostParameter(int memoryCostParameter) {
		this.memoryCostParameter = memoryCostParameter;
	}
	
	public int getParallelizationParameter() {
		return parallelizationParameter;
	}
	
	public void setParallelizationParameter(int parallelizationParameter) {
		this.parallelizationParameter = parallelizationParameter;
	}
}