package com.janoside.thread;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.stats.BlackHoleStatTracker;
import com.janoside.stats.StatTracker;
import com.janoside.stats.StatTrackerAware;

public class SafeThread extends Thread implements StatTrackerAware, ExceptionHandlerAware {
	
	private Runnable runnable;
	
	private StatTracker statTracker;
	
	private ExceptionHandler exceptionHandler;
	
	private String statsName;
	
	public SafeThread() {
		super();
		
		this.statTracker = new BlackHoleStatTracker();
		this.statsName = "safe-thread";
	}
	
	public SafeThread(Runnable runnable) {
		super(runnable);
		
		this.runnable = runnable;
		this.statTracker = new BlackHoleStatTracker();
		this.statsName = "safe-thread";
	}
	
	public void run() {
		try {
			this.statTracker.trackThreadPerformanceStart(this.statsName);
			
			this.runInternal();
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(new RuntimeException("Error occurred in SafeThread: " + this.statsName, t));
			
		} finally {
			this.statTracker.trackThreadPerformanceStart(this.statsName);
		}
	}
	
	protected void runInternal() throws Exception {
		this.runnable.run();
	}
	
	public void setStatTracker(StatTracker statTracker) {
		this.statTracker = statTracker;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setStatsName(String statsName) {
		this.statsName = statsName;
	}
}