package com.janoside.thread;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class CountdownLatchJoinable implements Joinable {
	
	private CountDownLatch latch;
	
	public CountdownLatchJoinable(CountDownLatch latch) {
		this.latch = latch;
	}
	
	public void join(long waitMillis) {
		try {
			this.latch.await(waitMillis, TimeUnit.MILLISECONDS);
			
		} catch (InterruptedException ie) {
			throw new RuntimeException(ie);
		}
	}
	
	public void join() {
		try {
			this.latch.await();
			
		} catch (InterruptedException ie) {
			throw new RuntimeException(ie);
		}
	}
}