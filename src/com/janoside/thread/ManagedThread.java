package com.janoside.thread;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.util.StringUtils;

import com.janoside.criteria.Criteria;
import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.stats.EventTracker;
import com.janoside.stats.StatTracker;
import com.janoside.stats.StatTrackerAware;

public abstract class ManagedThread extends Thread implements ExceptionHandlerAware, StatTrackerAware {
	
	public enum ManagedThreadState {
		Unstarted,
		Started,
		InactiveStartWait,
		StartWait,
		InactiveRunningWait,
		BeforeLoop,
		RunLoop,
		AfterLoop,
		Sleep,
		ExceptionSleep
	}
	
	private static final Logger logger = LoggerFactory.getLogger(ManagedThread.class);
	
	private static final long MINIMUM_PERIOD = 25;
	
	private List<Criteria> executeCriteria;
	
	private List<ManagedThreadObserver> managedThreadObservers;
	
	private EventTracker runAttemptTracker;
	
	private EventTracker runTracker;
	
	private EventTracker blockTracker;
	
	private EventTracker failureTracker;
	
	protected ExceptionHandler exceptionHandler;
	
	protected StatTracker statTracker;
	
	private ManagedThreadState state;
	
	private long period;
	
	private long startWait;

	private boolean running;
	
	private boolean active;
	
	protected boolean allowSleep;
	
	public ManagedThread() {
		this.executeCriteria = new ArrayList<Criteria>();
		this.managedThreadObservers = new ArrayList<ManagedThreadObserver>();
		
		this.runAttemptTracker = new EventTracker();
		this.runTracker = new EventTracker();
		this.blockTracker = new EventTracker();
		this.failureTracker = new EventTracker();
		
		this.state = ManagedThreadState.Unstarted;
		this.period = MINIMUM_PERIOD;
		this.running = false;
		this.active = true;
		this.allowSleep = true;
	}
	
	public synchronized void run() {
		logger.info(String.format("ManagedThread[%s] started", this.getName()));
		
		this.state = ManagedThreadState.Started;
		
		this.onStarted();
		for (ManagedThreadObserver observer : this.managedThreadObservers) {
			observer.onStarted(this);
		}
		
		while (!this.active) {
			this.state = ManagedThreadState.InactiveStartWait;
			this.safeSleep(250);
		}
		
		if (this.startWait > 0) {
			logger.info(String.format("ManagedThread[%s] waiting for %s ms before beginning", this.getName(), this.startWait));
			
			this.state = ManagedThreadState.StartWait;
			this.safeSleep(this.startWait);
		}

		this.running = true;
		
		while (this.running) {
			try {
				while (!this.active) {
					this.state = ManagedThreadState.InactiveRunningWait;
					this.safeSleep(250);
				}
				
				this.runAttemptTracker.increment();
				
				boolean executionAllowed = true;
				for (Criteria executeCriteria : this.executeCriteria) {
					if (!executeCriteria.isMet()) {
						executionAllowed = false;
						
						break;
					}
				}
				
				if (executionAllowed) {
					this.state = ManagedThreadState.BeforeLoop;
					this.beforeLoop();
					
					this.state = ManagedThreadState.RunLoop;
					this.runInternal();
					
					this.state = ManagedThreadState.AfterLoop;
					this.afterLoop();
					
				} else {
					this.blockTracker.increment();
					
					for (ManagedThreadObserver observer : this.managedThreadObservers) {
						observer.onBlocked(this);
					}
				}
				
				if (this.period > 0 && this.allowSleep) {
					this.state = ManagedThreadState.Sleep;
					this.safeSleep(this.period);
				}
			} catch (Throwable t) {
				this.failureTracker.increment();
				
				this.exceptionHandler.handleException(t);
				
				// sleep even though we failed, prevents runaway thread when implementation's xLoop() methods fail
				if (this.period > 0 && this.allowSleep) {
					this.state = ManagedThreadState.ExceptionSleep;
					this.safeSleep(this.period);
				}
			}
		}
		
		this.onShutdown();
		for (ManagedThreadObserver observer : this.managedThreadObservers) {
			observer.onShutdown(this);
		}
	}
	
	protected abstract void runInternal() throws Exception;
	
	protected void beforeLoop() {
		for (ManagedThreadObserver observer : this.managedThreadObservers) {
			observer.beforeLoop(this);
		}
	}
	
	protected void afterLoop() {
		this.runTracker.increment();
		
		for (ManagedThreadObserver observer : this.managedThreadObservers) {
			observer.afterLoop(this);
		}
	}
	
	protected void onStarted() {
	}
	
	protected void onShutdown() {
	}
	
	@ManagedOperation
	public synchronized void runNow() {
		this.notifyAll();
	}
	
	private void safeSleep(long time) {
		try {
			this.wait(time);
			
		} catch (InterruptedException ie) {
			this.exceptionHandler.handleException(ie);
		}
	}
	
	public synchronized void setManagedThreadObservers(List<ManagedThreadObserver> managedThreadObservers) {
		this.managedThreadObservers = managedThreadObservers;
	}
	
	public void addExecuteCriteria(Criteria executeCriteria) {
		if (!this.executeCriteria.contains(executeCriteria)) {
			this.executeCriteria.add(executeCriteria);
		}
	}
	
	public void setExecuteCriteria(List<Criteria> executeCriteria) {
		List<Criteria> previousExecuteCriteriaList = this.executeCriteria;
		
		this.executeCriteria = new ArrayList<Criteria>(executeCriteria);
		
		// add back any we had before
		for (Criteria previousExecuteCriteria : previousExecuteCriteriaList) {
			if (!this.executeCriteria.contains(previousExecuteCriteria)) {
				this.executeCriteria.add(previousExecuteCriteria);
			}
		}
	}
	
	public synchronized void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setStatTracker(StatTracker statTracker) {
		this.statTracker = statTracker;
	}
	
	@ManagedOperation
	public List<String> viewExecuteCriteria() {
		ArrayList<String> executeCriteriaStrings = new ArrayList<String>(this.executeCriteria.size());
		
		for (Criteria criteria : this.executeCriteria) {
			executeCriteriaStrings.add(criteria.toString() + "-" + criteria.isMet());
		}
		
		return executeCriteriaStrings;
	}
	
	@ManagedAttribute
	public synchronized String getManagedThreadState() {
		return this.state.toString();
	}
	
	@ManagedAttribute
	public long getSecondsSinceRun() {
		return this.runTracker.getSecondsSinceLastOccurrence();
	}
	
	@ManagedAttribute
	public long getSecondsSinceBlock() {
		return this.blockTracker.getSecondsSinceLastOccurrence();
	}
	
	@ManagedAttribute
	public long getSecondsSinceFailure() {
		return this.failureTracker.getSecondsSinceLastOccurrence();
	}
	
	@ManagedAttribute
	public synchronized long getPeriod() {
		return this.period;
	}

	@ManagedAttribute
	public synchronized void setPeriod(long period) {
		// if new period less than old period runNow()
		if (period < this.period) {
			this.runNow();
		}
		
		if (period < MINIMUM_PERIOD) {
			logger.warn("Attempt to set ManagedThread period lower than minimum period: " + period + "ms");
			
			this.period = MINIMUM_PERIOD;
			
		} else {
			this.period = period;
		}
	}
	
	@ManagedAttribute
	public synchronized long getStartWait() {
		return this.startWait;
	}
	
	public synchronized void setStartWait(long startWait) {
		this.startWait = startWait;
	}
	
	@ManagedAttribute
	public long getRunCount() {
		return this.runTracker.getCount();
	}
	
	@ManagedAttribute
	public long getBlockCount() {
		return this.blockTracker.getCount();
	}
	
	@ManagedAttribute
	public synchronized long getSecondsToNextRun() {
		if (this.runAttemptTracker.getLastOccurenceTime() > 0) {
			return (this.period / 1000) - this.runAttemptTracker.getSecondsSinceLastOccurrence();
			
		} else {
			return -1;
		}
	}
	
	@ManagedAttribute
	public synchronized boolean isRunning() {
		return this.running;
	}

	@ManagedAttribute
	public synchronized void setRunning(boolean running) {
		this.running = running;
	}

	@ManagedAttribute
	public boolean isActive() {
		return this.active;
	}

	@ManagedAttribute
	public void setActive(boolean active) {
		this.active = active;
	}
	
	@ManagedAttribute
	public boolean getAllowSleep() {
		return this.allowSleep;
	}
	
	@ManagedAttribute
	public void setAllowSleep(boolean allowSleep) {
		this.allowSleep = allowSleep;
	}
	
	@Override
	public String toString() {
		if (StringUtils.hasText(this.getName())) {
			return "ManagedThread(" + this.getName() + ")";
			
		} else {
			return super.toString();
		}
	}
}