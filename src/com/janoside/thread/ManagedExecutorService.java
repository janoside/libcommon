package com.janoside.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.stats.EventTracker;

@ManagedResource(objectName = "ManagedExecutorService")
public class ManagedExecutorService {
	
	private ExecutorService executorService;
	
	private EventTracker eventTracker;
	
	private int activeThreadCount;
	
	private int maxThreadCount;
	
	public ManagedExecutorService() {
		this.executorService = Executors.newCachedThreadPool();
		this.eventTracker = new EventTracker();
		this.maxThreadCount = Integer.MAX_VALUE;
		this.activeThreadCount = 0;
	}
	
	public void execute(final Runnable runnable) {
		this.executorService.execute(new Runnable() {
			public synchronized void run() {
				activeThreadCount++;
				eventTracker.increment();
				
				try {
					runnable.run();
					
				} finally {
					activeThreadCount--;
				}
			}
		});
	}
	
	@ManagedAttribute
	public long getExecutedThreadCount() {
		return this.eventTracker.getCount();
	}
	
	@ManagedAttribute
	public int getMaxThreadCount() {
		return this.maxThreadCount;
	}
	
	public void setMaxThreadCount(int maxThreadCount) {
		this.maxThreadCount = maxThreadCount;
		
		this.executorService.shutdown();
		this.executorService = Executors.newFixedThreadPool(maxThreadCount);
	}
	
	@ManagedAttribute
	public synchronized int getActiveThreadCount() {
		return this.activeThreadCount;
	}
	
	@ManagedAttribute
	public synchronized int getIdleThreadCount() {
		return (this.maxThreadCount - this.activeThreadCount);
	}
	
	@ManagedAttribute
	public float getExecutedThreadRate() {
		return this.eventTracker.getRate();
	}
}