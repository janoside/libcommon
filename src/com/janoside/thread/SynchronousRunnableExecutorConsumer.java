package com.janoside.thread;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.queue.ObjectConsumer;

public class SynchronousRunnableExecutorConsumer implements ObjectConsumer<Runnable>, ExceptionHandlerAware {
	
	private ExceptionHandler exceptionHandler;
	
	public void consume(Runnable runnable) {
		try {
			runnable.run();
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
		}
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
}