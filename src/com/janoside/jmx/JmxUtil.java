package com.janoside.jmx;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanInfo;
import javax.management.MBeanServer;
import javax.management.ObjectName;

public class JmxUtil {
	
	public static List<String> getObjectNamesByPattern(Pattern p) {
		MBeanServer localJmx = ManagementFactory.getPlatformMBeanServer();
		Set<ObjectName> objectNames = localJmx.queryNames(null, null);
		
		ArrayList<String> names = new ArrayList<String>();
		for (ObjectName objectName : objectNames) {
			String name = objectName.toString();
			
			if (p.matcher(name).matches()) {
				names.add(name);
			}
		}
		
		return names;
	}
	
	public static List<String> getObjectAttributeNamesByPattern(String objectNameString, Pattern p) {
		try {
			MBeanServer localJmx = ManagementFactory.getPlatformMBeanServer();
			
			ObjectName objectName = new ObjectName(objectNameString);
			
			MBeanInfo info = localJmx.getMBeanInfo(objectName);
			
			MBeanAttributeInfo[] attribs = info.getAttributes();
			
			ArrayList<String> results = new ArrayList<String>();
			for (MBeanAttributeInfo attrib : attribs) {
				if (p.matcher(attrib.getName()).matches()) {
					results.add(attrib.getName());
				}
			}
			
			return results;
			
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}
	
	public static Map<String, Map<String, Object>> getAttributeValues(Map<String, Set<String>> attributeNamesByObjectName) {
		try {
			MBeanServer localJmx = ManagementFactory.getPlatformMBeanServer();
			
			HashMap<String, Map<String, Object>> results = new HashMap<String, Map<String, Object>>();
			
			for (Map.Entry<String, Set<String>> entry : attributeNamesByObjectName.entrySet()) {
				ObjectName objectName = new ObjectName(entry.getKey());
				
				String[] attributeNames = entry.getValue().toArray(new String[]{});
				AttributeList attributeList = localJmx.getAttributes(objectName, attributeNames);
				
				HashMap<String, Object> values = new HashMap<String, Object>();
				for (Object attributeObject : attributeList) {
					Attribute attribute = (Attribute) attributeObject;
					
					values.put(attribute.getName(), attribute.getValue());
				}
				
				results.put(entry.getKey(), values);
			}
			
			return results;
			
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}
	
	public static Object getAttributeValue(final String objectNameString, final String attributeName) {
		return JmxUtil.getAttributeValues(new HashMap<String, Set<String>>() {{
			put(objectNameString, new HashSet<String>() {{
				add(attributeName);
			}});
		}}).get(objectNameString).get(attributeName);
	}
}