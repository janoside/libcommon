package com.janoside.transaction;

public interface TransactionManagerAware {
	
	void setTransactionManager(TransactionManager transactionManager);
}