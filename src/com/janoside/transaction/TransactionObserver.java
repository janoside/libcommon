package com.janoside.transaction;

public interface TransactionObserver {
	
	void onTransactionBegun(String name);
	
	void onTransactionCommitted(String name);
}