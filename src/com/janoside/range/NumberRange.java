package com.janoside.range;

public class NumberRange {
	
	private Number min;
	
	private Number max;
	
	public Number getMin() {
		return min;
	}
	
	public void setMin(Number min) {
		this.min = min;
	}
	
	public Number getMax() {
		return max;
	}
	
	public void setMax(Number max) {
		this.max = max;
	}
	
	public boolean contains(Number number) {
		return (this.min.doubleValue() < number.doubleValue() && this.max.doubleValue() > number.doubleValue());
	}
	
	public boolean isOverlappedBy(NumberRange numberRange) {
		if (this.min.doubleValue() > numberRange.getMin().doubleValue() && this.min.doubleValue() < numberRange.getMax().doubleValue()) {
			return true;
		}
		
		if (this.max.doubleValue() > numberRange.getMin().doubleValue() && this.max.doubleValue() < numberRange.getMax().doubleValue()) {
			return true;
		}
		
		if (this.min.doubleValue() < numberRange.getMin().doubleValue() && this.max.doubleValue() > numberRange.getMax().doubleValue()) {
			return true;
		}
		
		return false;
	}
	
	public String toString() {
		return "NumberRange(min=" + min + ", max=" + max + ")";
	}
	
	public int hashCode() {
		return this.toString().hashCode();
	}
	
	public boolean equals(Object o) {
		if (o == null) {
			return false;
			
		} else if (o instanceof NumberRange) {
			NumberRange other = (NumberRange) o;
			
			return (other.getMin().equals(this.min) && other.getMax().equals(this.max));
			
		} else {
			return false;
		}
	}
}