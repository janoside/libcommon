package com.janoside.range;

import java.util.ArrayList;
import java.util.List;

public class LongRange {
	
	private long min;
	
	private long max;
	
	public LongRange() {
		this.min = 0;
		this.max = 0;
	}
	
	public LongRange(long min, long max) {
		this.min = min;
		this.max = max;
	}
	
	public long getMin() {
		return min;
	}
	
	public void setMin(long min) {
		this.min = min;
	}
	
	public long getMax() {
		return max;
	}
	
	public void setMax(long max) {
		this.max = max;
	}
	
	public List<LongRange> chunk(long maxRangeSpan) {
		ArrayList<LongRange> longRangeList = new ArrayList<LongRange>();
		
		long start = this.min;
		
		while (start + maxRangeSpan < this.max) {
			longRangeList.add(new LongRange(start, start + maxRangeSpan));
			
			start += (maxRangeSpan + 1);
		}
		
		longRangeList.add(new LongRange(start, this.max));
		
		return longRangeList;
	}
	
	public int hashCode() {
		return this.toString().hashCode();
	}
	
	public boolean equals(Object o) {
		if (o instanceof LongRange) {
			LongRange lr = (LongRange) o;
			
			return (lr.getMin() == this.min && lr.getMax() == this.max);
			
		} else {
			return false;
		}
	}
	
	public String toString() {
		return "LongRange(" + this.min + "," + this.max + ")";
	}
}