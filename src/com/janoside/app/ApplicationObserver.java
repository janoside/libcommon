package com.janoside.app;

public interface ApplicationObserver {
	
	void onApplicationInitialized(Application application);
}