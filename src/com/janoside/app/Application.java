package com.janoside.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

@ManagedResource(objectName = "Janoside:name=Application")
public class Application implements ApplicationListener {
	
	private static Application instance;
	
	private static final Logger logger = LoggerFactory.getLogger(Application.class);
	
	public static Application getInstance() {
		return Application.instance;
	}
	
	private ExecutorService executorService;
	
	private List<ApplicationObserver> applicationObservers;
	
	private List<Runnable> runnables;
	
	private String version;
	
	private String type;
	
	private String name;
	
	private long id;
	
	private boolean initialized;
	
	private boolean autoInitialize;
	
	public Application() {
		if (Application.instance == null) {
			Application.instance = this;
		}
		
		logger.info("Application: initializing...");
		
		this.executorService = Executors.newCachedThreadPool();
		this.runnables = new ArrayList<Runnable>();
		this.applicationObservers = new ArrayList<ApplicationObserver>();
		this.id = 0;
		this.initialized = false;
		this.autoInitialize = false;
	}
	
	public void onApplicationEvent(ApplicationEvent event) {
		if (event instanceof ContextRefreshedEvent && !this.initialized && this.autoInitialize) {
			this.setInitialized(true);
		}
	}
	
	private void onInitialized() {
		logger.info("Application initialized! - Starting runnables...");
		
		HashMap<String, Integer> runnableTypeCounts = new HashMap<String, Integer>();
		
		for (Runnable runnable : this.runnables) {
			logger.info("Starting application-managed runnable: " + runnable.getClass().getSimpleName());
			
			if (runnable instanceof Thread) {
				this.executorService.execute(runnable);
				
			} else {
				String typeName = runnable.getClass().getSimpleName();
				
				if (!runnableTypeCounts.containsKey(typeName)) {
					runnableTypeCounts.put(typeName, 0);
				}
				
				Thread runnableThread = new Thread(runnable);
				
				runnableThread.setName(typeName + "-" + runnableTypeCounts.get(typeName));
				
				runnableTypeCounts.put(typeName, runnableTypeCounts.get(typeName) + 1);
				
				this.executorService.execute(runnableThread);
			}
		}
	}
	
	@ManagedAttribute
	public List<String> getRunnableNames() {
		ArrayList<String> runnableNames = new ArrayList<String>(this.runnables.size());
		
		for (Runnable runnable : this.runnables) {
			if (runnable instanceof Thread) {
				runnableNames.add(((Thread) runnable).getName());
				
			} else {
				runnableNames.add(runnable.toString());
			}
		}
		
		return runnableNames;
	}
	
	@ManagedAttribute
	public int getRunnableCount() {
		return this.runnables.size();
	}
	
	public void addRunnable(Runnable runnable) {
		this.runnables.add(runnable);
	}
	
	public void addApplicationObserver(ApplicationObserver applicationObserver) {
		this.applicationObservers.add(applicationObserver);
	}
	
	public String getVersion() {
		return this.version;
	}
	
	public void setVersion(String version) {
		this.version = version;
	}
	
	@ManagedAttribute
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	@ManagedAttribute
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@ManagedAttribute
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	@ManagedAttribute
	public boolean isInitialized() {
		return this.initialized;
	}
	
	public void setInitialized(boolean initialized) {
		boolean previouslyInitialized = this.initialized;
		this.initialized = initialized;
		
		if (!previouslyInitialized && this.initialized) {
			this.onInitialized();
			
			for (ApplicationObserver observer : this.applicationObservers) {
				observer.onApplicationInitialized(this);
			}
		}
	}
	
	public void setAutoInitialize(boolean autoInitialize) {
		this.autoInitialize = autoInitialize;
	}
}