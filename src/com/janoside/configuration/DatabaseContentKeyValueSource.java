package com.janoside.configuration;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.janoside.keyvalue.KeyValueSource;

public class DatabaseContentKeyValueSource extends HibernateDaoSupport implements KeyValueSource<String> {
	
	@SuppressWarnings("unchecked")
	public String get(final String name) {
		HibernateCallback callback = new HibernateCallback() {
			public Object doInHibernate(Session session) throws HibernateException, SQLException {
				SQLQuery sqlQuery = session.createSQLQuery("select content from content_elements where name=:name");
				
				sqlQuery.setString("name", name);
				sqlQuery.setMaxResults(1);
				
				sqlQuery.addScalar("content", Hibernate.STRING);
				
				return sqlQuery.list();
			}
		};
		
		List resultList = (List) this.getHibernateTemplate().execute(callback);
		
		if (resultList.size() > 0) {
			return (String) resultList.get(0);
			
		} else {
			return null;
		}
	}
}