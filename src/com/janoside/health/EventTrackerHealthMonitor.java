package com.janoside.health;

import com.janoside.stats.EventTracker;

public class EventTrackerHealthMonitor implements HealthMonitor {
	
	private EventTracker eventTracker;
	
	private int errorThreshold;
	
	public void reportInternalErrors(ErrorTracker errorTracker) {
		if (this.eventTracker.getCount() > this.errorThreshold) {
			errorTracker.trackError("Event count for '" + this.eventTracker.getName() + "' is above " + this.errorThreshold);
		}
		
		this.eventTracker.reset();
	}
	
	public void setEventTracker(EventTracker eventTracker) {
		this.eventTracker = eventTracker;
	}
	
	public void setErrorThreshold(int errorThreshold) {
		this.errorThreshold = errorThreshold;
	}
}