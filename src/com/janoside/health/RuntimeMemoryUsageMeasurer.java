package com.janoside.health;

public class RuntimeMemoryUsageMeasurer implements MemoryUsageMeasurer {
	
	public long getMaxMemory() {
		Runtime runtime = Runtime.getRuntime();
		
		return runtime.maxMemory();
	}
	
	public long getUsedMemory() {
		Runtime runtime = Runtime.getRuntime();
		
		return (this.getMaxMemory() - runtime.freeMemory());
	}
}