package com.janoside.health;

import java.util.Set;

public interface HealthAlertChangeObserver {
	
	void handleNewHealthAlerts(Set<String> newAlerts);
	
	void handleResolvedHealthAlerts(Set<String> resolvedAlerts);
}