package com.janoside.health;

public class ActiveThreadCountHealthMonitor implements HealthMonitor {
	
	private int errorThreshold;
	
	public void reportInternalErrors(ErrorTracker errorTracker) {
		if (Thread.activeCount() > this.errorThreshold) {
			errorTracker.trackError("More than " + this.errorThreshold + " active threads");
		}
	}
	
	public void setErrorThreshold(int errorThreshold) {
		this.errorThreshold = errorThreshold;
	}
}