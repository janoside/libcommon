package com.janoside.health;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class MemoryErrorTracker implements ErrorTracker {
	
	private Set<String> errors;
	
	public MemoryErrorTracker() {
		this.errors = Collections.synchronizedSet(new HashSet<String>());
	}
	
	public Set<String> getErrors() {
		return Collections.unmodifiableSet(this.errors);
	}
	
	public void trackError(String error, Object... context) {
		ArrayList<Object> objects = new ArrayList<Object>(context.length);
		for (Object o : context) {
			objects.add(o);
		}
		
		if (objects.isEmpty()) {
			this.errors.add(error);
			
		} else {
			this.errors.add(error + ", context=" + objects);
		}
	}
	
	public void reset() {
		this.errors.clear();
	}
}