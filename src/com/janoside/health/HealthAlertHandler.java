package com.janoside.health;

public interface HealthAlertHandler {
	
	void handleHealthAlerts(ErrorTracker errorTracker);
}