package com.janoside.health;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.net.JavaNetUrlDownloader;
import com.janoside.net.UrlDownloader;
import com.janoside.stats.StatTracker;
import com.janoside.stats.StatTrackerAware;
import com.janoside.util.UrlUtil;

public class NetworkHealthMonitor implements Runnable, HealthMonitor, ExceptionHandlerAware, StatTrackerAware, InitializingBean {
	
	private static final Logger logger = LoggerFactory.getLogger(NetworkHealthMonitor.class);
	
	private UrlDownloader urlDownloader;
	
	private Timer schedulingTimer;
	
	private ExceptionHandler exceptionHandler;
	
	private StatTracker statTracker;
	
	private Set<String> urls;
	
	private TimerTask timerTask;
	
	private AtomicInteger failureCounter;
	
	private long period;
	
	private int alertFailureThreshold;
	
	public NetworkHealthMonitor() {
		this.urlDownloader = new JavaNetUrlDownloader();
		this.schedulingTimer = new Timer();
		
		this.urls = new HashSet<String>() {{
			add("https://www.google.com/robots.txt");
			add("https://www.twitter.com/robots.txt");
			add("https://www.facebook.com/robots.txt");
			add("https://www.apple.com/robots.txt");
		}};
		
		this.failureCounter = new AtomicInteger(0);
		
		this.period = 2000;
		this.alertFailureThreshold = 2;
	}
	
	public void afterPropertiesSet() {
		this.scheduleExecution(15000);
	}
	
	public void reportInternalErrors(ErrorTracker errorTracker) {
		int failures = this.failureCounter.getAndSet(0);
		
		if (failures > this.alertFailureThreshold) {
			errorTracker.trackError("Network health is lookin' janky.");
		}
	}
	
	public void run() {
		boolean atLeastOneSuccess = false;
		
		List<String> shuffledUrls = new ArrayList<String>(this.urls);
		Collections.shuffle(shuffledUrls);
		
		for (String url : shuffledUrls) {
			String domain = UrlUtil.getDomain(url).toLowerCase().replace('.', '-');
			
			this.statTracker.trackThreadPerformanceStart("network-health-monitor.check-url");
			this.statTracker.trackThreadPerformanceStart("network-health-monitor.check-url." + domain);
			
			try {
				String contentType = this.urlDownloader.getContentType(url);
				
				if (contentType != null) {
					atLeastOneSuccess = true;
					
					break;
					
				} else {
					this.statTracker.trackEvent("network-health-monitor.zero-last-modified." + domain);
				}
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
				
				this.statTracker.trackEvent("network-health-monitor.error." + domain);
				
			} finally {
				this.statTracker.trackThreadPerformanceEnd("network-health-monitor.check-url");
				this.statTracker.trackThreadPerformanceEnd("network-health-monitor.check-url." + domain);
			}
		}
		
		if (!atLeastOneSuccess) {
			this.failureCounter.getAndIncrement();
		}
	}
	
	private void scheduleExecution(long delay) {
		if (this.timerTask != null) {
			this.timerTask.cancel();
		}
		
		this.timerTask = new TimerTask() {
			public void run() {
				try {
					NetworkHealthMonitor.this.run();
					
				} catch (Throwable t) {
					exceptionHandler.handleException(t);
				}
			}
		};
		
		this.schedulingTimer.scheduleAtFixedRate(
				this.timerTask,
				delay,
				this.period);
	}
	
	public void setUrlDownloader(UrlDownloader urlDownloader) {
		this.urlDownloader = urlDownloader;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setStatTracker(StatTracker statTracker) {
		this.statTracker = statTracker;
	}
	
	public void setUrls(Set<String> urls) {
		this.urls = urls;
	}
	
	public long getPeriod() {
		return this.period;
	}
	
	public void setPeriod(long period) {
		this.period = period;
	}
	
	public int getAlertFailureThreshold() {
		return this.alertFailureThreshold;
	}
	
	public void setAlertFailureThreshold(int alertFailureThreshold) {
		this.alertFailureThreshold = alertFailureThreshold;
	}
}