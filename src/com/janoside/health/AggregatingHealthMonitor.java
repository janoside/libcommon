package com.janoside.health;

import java.util.Set;

public class AggregatingHealthMonitor implements ErrorTracker, HealthMonitor {
	
	private MemoryErrorTracker internalErrorTracker;
	
	public AggregatingHealthMonitor() {
		this.internalErrorTracker = new MemoryErrorTracker();
	}
	
	public Set<String> getErrors() {
		return this.internalErrorTracker.getErrors();
	}
	
	public void trackError(String error, Object... context) {
		this.internalErrorTracker.trackError(error, context);
	}
	
	public void reportInternalErrors(ErrorTracker errorTracker) {
		for (String error : this.internalErrorTracker.getErrors()) {
			errorTracker.trackError(error);
		}
		
		this.internalErrorTracker.reset();
	}
}