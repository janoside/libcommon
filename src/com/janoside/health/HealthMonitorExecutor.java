package com.janoside.health;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;

/**
 * Responsible for periodically checking all HealthMonitor instances for errors
 * and notifying when there are some. This enables proactive health checking.
 * 
 * @author janoside
 */
public final class HealthMonitorExecutor implements ExceptionHandlerAware, InitializingBean {
	
	private static final Logger logger = LoggerFactory.getLogger(HealthMonitorExecutor.class);
	
	private List<HealthMonitor> healthMonitors;
	
	private List<HealthAlertHandler> healthAlertHandlers;
	
	private Timer schedulingTimer;
	
	private ExceptionHandler exceptionHandler;
	
	private ErrorTracker currentErrorTracker;
	
	private TimerTask timerTask;
	
	private Date lastExecutionDate;
	
	private long period;
	
	private long startupDelay;
	
	public HealthMonitorExecutor() {
		this.healthMonitors = new ArrayList<HealthMonitor>();
		this.healthAlertHandlers = new ArrayList<HealthAlertHandler>();
		
		this.schedulingTimer = new Timer();
		
		this.currentErrorTracker = new MemoryErrorTracker();
		
		this.lastExecutionDate = new Date();
		
		this.period = 60000;
		this.startupDelay = 120000;
	}
	
	public void afterPropertiesSet() {
		this.scheduleExecution(this.startupDelay);
	}
	
	public void reset() {
		this.scheduleExecution(0);
	}
	
	private void scheduleExecution(long delay) {
		if (this.timerTask != null) {
			this.timerTask.cancel();
		}
		
		this.timerTask = new TimerTask() {
			public void run() {
				try {
					HealthMonitorExecutor.this.run();
					
				} catch (Throwable t) {
					exceptionHandler.handleException(t);
				}
			}
		};
		
		this.schedulingTimer.scheduleAtFixedRate(
				this.timerTask,
				delay,
				this.period);
	}
	
	private void run() {
		try {
			logger.debug("Executing " + this.healthMonitors.size() + " HealthMonitors");
			
			MemoryErrorTracker errorTracker = new MemoryErrorTracker();
			
			for (HealthMonitor healthMonitor : this.healthMonitors) {
				try {
					healthMonitor.reportInternalErrors(errorTracker);
					
				} catch (Throwable t) {
					this.exceptionHandler.handleException(t);
				}
			}
			
			for (HealthAlertHandler healthAlertHandler : this.healthAlertHandlers) {
				try {
					healthAlertHandler.handleHealthAlerts(errorTracker);
					
				} catch (Throwable t) {
					this.exceptionHandler.handleException(t);
				}
			}
			
			this.currentErrorTracker = errorTracker;
			
			this.lastExecutionDate = new Date();
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
		}
	}
	
	public void addHealthMonitor(HealthMonitor healthMonitor) {
		this.healthMonitors.add(healthMonitor);
	}
	
	public void addHealthAlertHandler(HealthAlertHandler healthAlertHandler) {
		this.healthAlertHandlers.add(healthAlertHandler);
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public Set<String> getCurrentErrors() {
		return new HashSet<String>(this.currentErrorTracker.getErrors());
	}
	
	public Date getLastExecutionDate() {
		return this.lastExecutionDate;
	}
	
	public void setPeriod(long period) {
		this.period = period;
	}
	
	public void setStartupDelay(long startupDelay) {
		this.startupDelay = startupDelay;
	}
}