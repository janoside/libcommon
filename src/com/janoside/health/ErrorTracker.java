package com.janoside.health;

import java.util.Set;

public interface ErrorTracker {
	
	Set<String> getErrors();
	
	void trackError(String error, Object... context);
}