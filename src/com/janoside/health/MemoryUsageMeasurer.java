package com.janoside.health;

public interface MemoryUsageMeasurer {
	
	long getMaxMemory();
	
	long getUsedMemory();
}