package com.janoside.health;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;

public class ManagementMemoryUsageMeasurer implements MemoryUsageMeasurer {
	
	public long getMaxMemory() {
		MemoryMXBean memoryMXBean = ManagementFactory.getMemoryMXBean();
		
		return memoryMXBean.getHeapMemoryUsage().getMax();
	}
	
	public long getUsedMemory() {
		MemoryMXBean memoryMXBean = ManagementFactory.getMemoryMXBean();
		
		return memoryMXBean.getHeapMemoryUsage().getUsed();
	}
}