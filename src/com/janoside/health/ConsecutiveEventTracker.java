package com.janoside.health;

import java.util.concurrent.atomic.AtomicInteger;

public class ConsecutiveEventTracker {
	
	private AtomicInteger counter;
	
	public ConsecutiveEventTracker() {
		this.counter = new AtomicInteger(0);
	}
	
	public int getCount() {
		return this.counter.get();
	}
	
	public void increment() {
		this.counter.getAndIncrement();
	}
	
	public void reset() {
		this.counter.getAndSet(0);
	}
}