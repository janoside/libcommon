package com.janoside.health;

public interface HealthMonitor {
	
	void reportInternalErrors(ErrorTracker errorTracker);
}