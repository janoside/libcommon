package com.janoside.health;

import com.janoside.stats.SuccessTracker;

/**
 * HealthMonitor implementation that tracks the success rate of a
 * <code>SuccessTracker</code>. If the successRate:
 * 
 * successRate = (successCount / (successCount + failureCount))
 * 
 * falls below the error threshold an error will be returned.
 * Generally, the threshold values should be configured such that:
 * 
 * 0.0f <= errorThreshold <= 1.0f
 * 
 * Note: SuccessTrackers have a minimum totalCount:
 * 
 * totalCount = successCount + failureCount
 * 
 * under which they will not "publish" an accurate successRate; this
 * HealthMonitor implementation does not change that and therefore
 * will return no errors until that threshold is met.
 * 
 * @author janoside
 */
public class SuccessTrackerHealthMonitor implements HealthMonitor {
	
	private SuccessTracker successTracker;
	
	private float errorThreshold;
	
	public SuccessTrackerHealthMonitor() {
		this.errorThreshold = 0.5f;
	}
	
	public void reportInternalErrors(ErrorTracker errorTracker) {
		if (this.successTracker.getTotalCount() >= this.successTracker.getMinPublishCount()) {
			if (this.successTracker.getSuccessRate() < this.errorThreshold) {
				errorTracker.trackError("Success rate for '" + this.successTracker.getName() + "' is below " + this.errorThreshold);
			}
			
			this.successTracker.reset();
		}
	}
	
	public void setSuccessTracker(SuccessTracker successTracker) {
		this.successTracker = successTracker;
	}
	
	public void setErrorThreshold(float errorThresold) {
		this.errorThreshold = errorThresold;
	}
}