package com.janoside.health;

import com.janoside.exception.ExceptionHandler;
import com.janoside.stats.EventTracker;

public class ExceptionVolumeHealthMonitor implements HealthMonitor, ExceptionHandler {
	
	private EventTracker eventTracker;
	
	private int errorThreshold;
	
	public ExceptionVolumeHealthMonitor() {
		this.eventTracker = new EventTracker();
		this.errorThreshold = 100;
	}
	
	public void handleException(Throwable t) {
		this.eventTracker.increment();
	}
	
	public void reportInternalErrors(ErrorTracker errorTracker) {
		if (this.eventTracker.getCount() > this.errorThreshold) {
			errorTracker.trackError("More than " + this.errorThreshold + " exceptions since last check");
		}
		
		this.eventTracker.reset();
	}
	
	public void setErrorThreshold(int errorThreshold) {
		this.errorThreshold = errorThreshold;
	}
}