package com.janoside.storage;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class MultithreadedKeyValueStoreTest {
	
	@Test
	@SuppressWarnings("serial")
	public void testIt() {
		MemoryKeyValueStore<String> memoryStore = new MemoryKeyValueStore<String>();
		MultithreadedKeyValueStore<String> store = new MultithreadedKeyValueStore<String>();
		store.setStore(memoryStore);
		
		store.put("one", "one");
		store.put("two", "two");
		
		store.putAll(new HashMap<String, String>() {{
			put("three", "three");
			put("four", "four");
		}});
		
		Assert.assertNull(store.get("five"));
		Assert.assertEquals("one", store.get("one"));
		
		Map<String, String> map = store.getAll(Arrays.asList("two", "three", "four"));
		
		Assert.assertEquals(3, map.size());
		Assert.assertEquals("two", map.get("two"));
		Assert.assertEquals("three", map.get("three"));
		Assert.assertEquals("four", map.get("four"));
	}
}