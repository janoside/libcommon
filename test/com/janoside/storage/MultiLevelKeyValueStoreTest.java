package com.janoside.storage;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class MultiLevelKeyValueStoreTest {
	
	@Test
	public void testIt() {
		KeyValueStore<String> s1 = new MemoryKeyValueStore<String>();
		KeyValueStore<String> s2 = new MemoryKeyValueStore<String>();
		KeyValueStore<String> s3 = new MemoryKeyValueStore<String>();
		
		MultiLevelKeyValueStore<String> store = new MultiLevelKeyValueStore<String>();
		store.setStores(Arrays.asList(s1, s2, s3));
		
		store.put("one", "one");
		
		Assert.assertNotNull(s1.get("one"));
		Assert.assertNull(s2.get("one"));
		Assert.assertNull(s3.get("one"));
		
		s2.put("two", "two");
		
		Assert.assertNull(s1.get("two"));
		
		// this get() call will auto-copy the value for "two" into s1
		Assert.assertEquals("two", store.get("two"));
		
		Assert.assertNotNull(s1.get("two"));
		
		s3.put("three", "three");
		
		Assert.assertNull(s1.get("three"));
		Assert.assertNull(s2.get("three"));
		
		// this get() call will auto-copy the value for "three" into s1
		Assert.assertEquals("three", store.get("three"));
		
		Assert.assertNotNull(s1.get("three"));
		Assert.assertNull(s2.get("three"));
		Assert.assertNotNull(s3.get("three"));
	}
}