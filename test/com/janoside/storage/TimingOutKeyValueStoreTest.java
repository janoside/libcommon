package com.janoside.storage;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.stats.MemoryStats;
import com.janoside.stats.MemoryStatsStatTracker;

public class TimingOutKeyValueStoreTest {
	
	@Test
	public void testTimeout() {
		MemoryStats memoryStats = new MemoryStats();
		
		MemoryStatsStatTracker statTracker = new MemoryStatsStatTracker();
		statTracker.setMemoryStats(memoryStats);
		
		TimingOutKeyValueStore<String> store = new TimingOutKeyValueStore<String>();
		store.setStatTracker(statTracker);
		store.setTimeout(1000);
		store.setName("test");
		store.setStore(new KeyValueStore<String>() {
			
			public String get(String key) {
				try {
					Thread.sleep(2000);
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				return UUID.randomUUID().toString();
			}
			
			public void put(String key, String value) {
			}
			
			public void remove(String key) {
			}
		});
		
		long start = System.currentTimeMillis();
		
		String value = store.get("something");
		
		long time = System.currentTimeMillis() - start;
		
		Assert.assertTrue(time < 1500);
		Assert.assertNull(value);
		Assert.assertEquals(1, memoryStats.getEvents().get("store.test.timeout").getCount());
	}
	
	@Test
	public void testNoTimeout() {
		MemoryStats memoryStats = new MemoryStats();
		
		MemoryStatsStatTracker statTracker = new MemoryStatsStatTracker();
		statTracker.setMemoryStats(memoryStats);
		
		TimingOutKeyValueStore<String> store = new TimingOutKeyValueStore<String>();
		store.setStatTracker(statTracker);
		store.setTimeout(1000);
		store.setStore(new KeyValueStore<String>() {
			
			public String get(String key) {
				return UUID.randomUUID().toString();
			}
			
			public void put(String key, String value) {
			}
			
			public void remove(String key) {
			}
		});
		
		long start = System.currentTimeMillis();
		
		String value = store.get("something");
		
		long time = System.currentTimeMillis() - start;
		
		Assert.assertTrue(time < 500);
		Assert.assertNotNull(value);
		Assert.assertTrue(memoryStats.getEvents().isEmpty());
	}
}