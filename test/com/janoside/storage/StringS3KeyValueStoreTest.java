package com.janoside.storage;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.StandardErrorExceptionHandler;
import com.janoside.s3.Jets3tS3Bucket;
import com.janoside.stats.MemoryStats;

public class StringS3KeyValueStoreTest {
	
	private static boolean skip = true;
	
	@Test
	public void testIt() {
		if (skip) {
			return;
		}
		
		String uuid = UUID.randomUUID().toString();
		
		Jets3tS3Bucket bucket = new Jets3tS3Bucket();
		bucket.setAccessKey("AKIAJ6KG2RA4GA4RRPGQ");
		bucket.setSecretKey("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");
		bucket.setBucketName(uuid);
		
		bucket.initialize();
		
		StringS3KeyValueStore store = new StringS3KeyValueStore();
		store.setS3Bucket(bucket);
		
		Assert.assertNull(store.get("test"));
		
		store.put("test", "value");
		
		Assert.assertEquals("value", store.get("test"));
		store.remove("test");
		Assert.assertNull(store.get("test"));
		
		bucket.deleteSelf();
	}
	
	@Test
	public void testPerformance() {
		if (skip) {
			return;
		}
		
		String bucketName = UUID.randomUUID().toString();
		
		Jets3tS3Bucket bucket = new Jets3tS3Bucket();
		bucket.setAccessKey("AKIAJ6KG2RA4GA4RRPGQ");
		bucket.setSecretKey("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");
		bucket.setBucketName(bucketName);
		
		bucket.initialize();
		
		final StringS3KeyValueStore store = new StringS3KeyValueStore();
		store.setS3Bucket(bucket);
		
		final MemoryStats stats = new MemoryStats();
		final ExecutorService executor = Executors.newFixedThreadPool(3);
		final ExceptionHandler exceptionHandler = new StandardErrorExceptionHandler();
		
		int count = 5;
		
		final CountDownLatch latch = new CountDownLatch(count);
		
		ArrayList<String> uuids = new ArrayList<String>(count);
		for (int i = 0; i < count; i++) {
			final String uuid = UUID.randomUUID().toString();
			uuids.add(uuid);
			
			executor.execute(new Runnable() {
				public void run() {
					try {
						stats.countPerformanceStart("put");
						
						store.put(uuid, uuid);
						
						stats.countPerformanceEnd("put");
						
					} catch (Throwable t) {
						exceptionHandler.handleException(t);
						
					} finally {
						latch.countDown();
					}
				}
			});
		}
		
		try {
			latch.await();
			
		} catch (InterruptedException ie) {
			exceptionHandler.handleException(ie);
		}
		
		final CountDownLatch latch3 = new CountDownLatch(count);
		
		for (final String uuid : uuids) {
			executor.execute(new Runnable() {
				public void run() {
					try {
						System.out.println("getting " + uuid);
						
						stats.countPerformanceStart("get");
						
						store.get(uuid);
						
						stats.countPerformanceEnd("get");
						
					} catch (Throwable t) {
						exceptionHandler.handleException(t);
						
					} finally {
						latch3.countDown();
					}
				}
			});
		}
		
		try {
			latch3.await();
			
		} catch (InterruptedException ie) {
			exceptionHandler.handleException(ie);
		}
		
		final CountDownLatch latch2 = new CountDownLatch(count);
		
		for (final String uuid : uuids) {
			executor.execute(new Runnable() {
				public void run() {
					try {
						stats.countPerformanceStart("remove");
						
						store.remove(uuid);
						
						stats.countPerformanceEnd("remove");
						
						System.out.println("removed " + uuid);
						
					} catch (Throwable t) {
						exceptionHandler.handleException(t);
						
					} finally {
						latch2.countDown();
					}
				}
			});
		}
		
		try {
			latch2.await();
			
		} catch (InterruptedException ie) {
			exceptionHandler.handleException(ie);
		}
		
		System.out.println(stats.printPerformanceReport());
		
		Assert.assertNull(store.get("test2"));
		
		store.put("test2", "value");
		
		Assert.assertEquals("value", store.get("test2"));
		store.remove("test2");
		Assert.assertNull(store.get("test2"));
		
		bucket.deleteSelf();
	}
}