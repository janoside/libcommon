package com.janoside.storage;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.exception.StandardErrorExceptionHandler;

public class RetryingKeyValueStoreTest {
	
	@Test
	public void testIt() {
		TestKeyValueStore innerStore = new TestKeyValueStore();
		
		RetryingKeyValueStore<String> store = new RetryingKeyValueStore<String>();
		store.setStore(innerStore);
		store.setExceptionHandler(new StandardErrorExceptionHandler());
		store.setAttemptCount(3);
		
		Assert.assertEquals("one", store.get("one"));
		Assert.assertEquals(1, innerStore.tryCount);
		
		int exceptionCount = 0;
		
		try {
			innerStore.success = false;
			innerStore.tryCount = 0;
			
			store.get("two");
			
		} catch (Throwable t) {
			exceptionCount++;
		}
		
		Assert.assertEquals(1, exceptionCount);
		Assert.assertEquals(3, innerStore.tryCount);
	}
	
	private static class TestKeyValueStore implements KeyValueStore<String> {
		
		public int tryCount = 0;
		
		public boolean success = true;
		
		public void put(String key, String value) {
			tryCount++;
			
			if (!success) {
				throw new RuntimeException("failure");
			}
		}
		
		public String get(String key) {
			tryCount++;
			
			if (success) {
				return key;
				
			} else {
				throw new RuntimeException("failure");
			}
		}
		
		public void remove(String key) {
			tryCount++;
			
			if (!success) {
				throw new RuntimeException("failure");
			}
		}
	}
}