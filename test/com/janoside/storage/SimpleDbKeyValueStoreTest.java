package com.janoside.storage;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.security.encryption.JasyptStrongEncryptor;
import com.janoside.simpledb.SimpleDbConnection;
import com.janoside.stats.MemoryStats;
import com.janoside.stats.MemoryStatsStatTracker;

public class SimpleDbKeyValueStoreTest {
	
	private static boolean skip = true;
	
	@Test
	public void testIt() throws Exception {
		if (skip) {
			return;
		}
		
		MemoryStats memoryStats = new MemoryStats();
		
		MemoryStatsStatTracker statTracker = new MemoryStatsStatTracker();
		statTracker.setMemoryStats(memoryStats);
		
		SimpleDbConnection sdbc = new SimpleDbConnection();
		sdbc.setEncryptor(new JasyptStrongEncryptor());
		sdbc.setEncryptedAwsAccessId("nothing");
		sdbc.setEncryptedAwsSecretKey("nothing");
		sdbc.setSecure(true);
		sdbc.afterPropertiesSet();
		
		SimpleDbKeyValueStore<String> keyValueStore = new SimpleDbKeyValueStore<String>();
		keyValueStore.setSimpleDbConnection(sdbc);
		keyValueStore.setStatTracker(statTracker);
		keyValueStore.setDomainName("test");
		
		String id = UUID.randomUUID().toString();
		
		keyValueStore.put("test:" + id, id);
		
		Thread.sleep(2000);
		
		String retrieved = keyValueStore.get("test:" + id);
		
		keyValueStore.remove("test:" + id);
		
		Assert.assertEquals(id, retrieved);

		// Wait for AWS to actual complete the remove operation
		Thread.sleep(2000);
		
		retrieved = keyValueStore.get("test:" + id);
		
		Assert.assertNull(retrieved);
		
		System.out.println(memoryStats.printPerformanceReport());
	}
}
