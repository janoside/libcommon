package com.janoside.storage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import org.jets3t.service.model.S3Object;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import com.janoside.exception.StandardOutExceptionHandler;
import com.janoside.s3.Jets3tS3Bucket;
import com.janoside.security.encryption.JasyptStrongEncryptor;
import com.janoside.stats.BlackHoleStatTracker;

public class S3KeyValueStoreTest {
	
	private static final String accessKey = "nothing";
	
	private static final String secretKey = "nothing";
	
	@Test
	@Ignore
	public void testIt() {
		String uuid = UUID.randomUUID().toString();
		
		Jets3tS3Bucket bucket = new Jets3tS3Bucket();
		bucket.setEncryptor(new JasyptStrongEncryptor());
		bucket.setAccessKey(accessKey);
		bucket.setSecretKey(secretKey);
		bucket.setBucketName(uuid);
		
		bucket.initialize();
		
		S3KeyValueStore<String> store = new S3KeyValueStore<String>();
		store.setS3Bucket(bucket);
		store.setExceptionHandler(new StandardOutExceptionHandler());
		store.setStatTracker(new BlackHoleStatTracker());
		
		Assert.assertNull(store.get("test"));
		
		store.put("test", "value");
		
		Assert.assertEquals("value", store.get("test"));
		store.remove("test");
		Assert.assertNull(store.get("test"));
		
		bucket.deleteSelf();
	}
	
	@Test
	@Ignore
	public void testPerformance() {
		String uuid = UUID.randomUUID().toString();
		
		Jets3tS3Bucket bucket = new Jets3tS3Bucket();
		bucket.setEncryptor(new JasyptStrongEncryptor());
		bucket.setAccessKey(accessKey);
		bucket.setSecretKey(secretKey);
		bucket.setBucketName(uuid);
		
		bucket.initialize();
		
		S3KeyValueStore<String> store = new S3KeyValueStore<String>();
		store.setS3Bucket(bucket);
		store.setExceptionHandler(new StandardOutExceptionHandler());
		store.setStatTracker(new BlackHoleStatTracker());
		
		Assert.assertNull(store.get("test"));
		
		store.put("test", "value");
		
		Assert.assertEquals("value", store.get("test"));
		store.remove("test");
		Assert.assertNull(store.get("test"));
		
		bucket.deleteSelf();
	}
	
	@Test
	@Ignore
	public void testDeleteAllKeys() throws InterruptedException {
		List<String> bucketNames = Arrays.asList();
		
		final AtomicInteger counter = new AtomicInteger(0);
		
		for (String bucketName : bucketNames) {
			final Jets3tS3Bucket bucket = new Jets3tS3Bucket();
			bucket.setEncryptor(new JasyptStrongEncryptor());
			bucket.setAccessKey(accessKey);
			bucket.setSecretKey(secretKey);
			bucket.setBucketName(bucketName);
			
			bucket.initialize();
			
			ExecutorService exec = Executors.newFixedThreadPool(25);
			
			List<S3Object> objects = bucket.getAllFiles();
			
			final CountDownLatch latch = new CountDownLatch(objects.size());
			
			for (final S3Object object : objects) {
				exec.execute(new Runnable() {
					public void run() {
						try {
							bucket.deleteFile(object.getKey());
							
							counter.getAndIncrement();
							
						} catch (Throwable t) {
							t.printStackTrace();
							
						} finally {
							latch.countDown();
						}
					}
				});
			}
			
			latch.await();
			
			System.out.println("Deleted " + counter.get() + " keys from " + bucketName);
		}
	}
	
	@Test
	@Ignore
	public void testClosingStreams() throws InterruptedException {
		String uuid = UUID.randomUUID().toString();
		
		final Jets3tS3Bucket bucket = new Jets3tS3Bucket();
		bucket.setEncryptor(new JasyptStrongEncryptor());
		bucket.setAccessKey(accessKey);
		bucket.setSecretKey(secretKey);
		bucket.setBucketName(uuid);
		
		bucket.initialize();
		
		final S3KeyValueStore<String> store = new S3KeyValueStore<String>();
		store.setS3Bucket(bucket);
		store.setExceptionHandler(new StandardOutExceptionHandler());
		store.setStatTracker(new BlackHoleStatTracker());
		
		ArrayList<String> uuids = new ArrayList<String>();
		for (int i = 0; i < 100; i++) {
			uuids.add(UUID.randomUUID().toString());
		}
		
		ExecutorService exec = Executors.newFixedThreadPool(50);
		
		final CountDownLatch latch = new CountDownLatch(10000);
		
		for (int i = 0; i < 10000; i++) {
			exec.execute(new Runnable() {
				public void run() {
					try {
						String id = UUID.randomUUID().toString();
						
						store.put(id, id);
						System.out.println(store.get(id));
						
					} catch (Throwable t) {
						t.printStackTrace();
						
					} finally {
						latch.countDown();
					}
				}
			});
		}
		
		latch.await();
	}
}