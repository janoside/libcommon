package com.janoside.storage;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.stats.BlackHoleStatTracker;
import com.janoside.stats.StatTracker;

public class MigrationKeyValueStoreTest {
	
	@Test
	public void testIt() {
		StatTracker statTracker1 = new BlackHoleStatTracker();
		
		ManagedKeyValueStore<String> source = new ManagedKeyValueStore<String>();
		source.setStatTracker(statTracker1);
		source.setStore(new MemoryKeyValueStore<String>());
		
		StatTracker statTracker2 = new BlackHoleStatTracker();
		
		ManagedKeyValueStore<String> destination = new ManagedKeyValueStore<String>();
		destination.setStatTracker(statTracker2);
		destination.setStore(new MemoryKeyValueStore<String>());
		
		MigrationKeyValueStore<String> store = new MigrationKeyValueStore<String>();
		store.setSource(source);
		store.setDestination(destination);
		
		store.put("one", "one");
		
		Assert.assertNotNull(destination.get("one"));
		Assert.assertNull(source.get("one"));
		
		Assert.assertEquals("one", store.get("one"));
		
		source.put("two", "two");
		
		Assert.assertNotNull(source.get("two"));
		Assert.assertNull(destination.get("two"));
		
		store.get("two");
		
		Assert.assertNotNull(destination.get("two"));
	}
}