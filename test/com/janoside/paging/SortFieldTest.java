package com.janoside.paging;

import org.junit.Assert;
import org.junit.Test;

public class SortFieldTest {
	
	@Test
	public void testBasic() {
		SortField sortField = new SortField();
		sortField.setName("cheese");
		sortField.setSortDirection(SortDirection.Descending);
		
		Assert.assertEquals("cheese", sortField.getName());
		Assert.assertEquals(SortDirection.Descending, sortField.getSortDirection());
	}
	
	@Test
	public void testToString() {
		SortField sortField = new SortField();
		sortField.setName("cheese");
		sortField.setSortDirection(SortDirection.Descending);
		
		Assert.assertEquals("SortField(cheese, Descending)", sortField.toString());
		
		sortField.setSortDirection(SortDirection.Ascending);
		
		Assert.assertEquals("SortField(cheese, Ascending)", sortField.toString());
	}
	
	@Test
	public void testHashCode() {
		SortField sortField = new SortField();
		sortField.setName("cheese");
		sortField.setSortDirection(SortDirection.Descending);
		
		SortField sortField2 = new SortField();
		sortField2.setName("cheese");
		sortField2.setSortDirection(SortDirection.Descending);
		
		Assert.assertTrue(sortField.hashCode() == sortField2.hashCode());
		
		sortField.setSortDirection(SortDirection.Ascending);
		
		Assert.assertFalse(sortField.hashCode() == sortField2.hashCode());
	}
	
	@Test
	public void testEquals() {
		SortField sortField = new SortField();
		sortField.setName("cheese");
		sortField.setSortDirection(SortDirection.Descending);
		
		SortField sortField2 = new SortField();
		sortField2.setName("cheese");
		sortField2.setSortDirection(SortDirection.Descending);
		
		Assert.assertTrue(sortField.equals(sortField2));
		
		sortField.setSortDirection(SortDirection.Ascending);
		
		Assert.assertFalse(sortField.equals(sortField2));
		
		Assert.assertFalse(sortField.equals(null));
		Assert.assertFalse(sortField.equals(new Object()));
	}
	
	@Test
	public void testFullConstructor() {
		SortField sortField = new SortField("cheese", SortDirection.Descending);
		
		Assert.assertEquals("SortField(cheese, Descending)", sortField.toString());
		
		sortField = new SortField("monkey", SortDirection.Ascending);
		
		Assert.assertEquals("SortField(monkey, Ascending)", sortField.toString());
	}
}