package com.janoside.health;

import org.junit.Test;

public class FreeDiskSpaceHealthMonitorTest {
	
	@Test
	public void testIt() {
		FreeDiskSpaceHealthMonitor monitor = new FreeDiskSpaceHealthMonitor();
		
		MemoryErrorTracker et = new MemoryErrorTracker();
		
		monitor.reportInternalErrors(et);
		
		System.out.println(et.getErrors());
	}
}