package com.janoside.health;

import java.util.Arrays;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.exception.StandardErrorExceptionHandler;
import com.janoside.util.ManualTimeSource;

public class HealthAlertChangeMonitorTest {
	
	@Test
	public void testIt() {
		int N = 4;
		
		ManualTimeSource ts = new ManualTimeSource();
		
		HealthAlertChangeMonitor hacm = new HealthAlertChangeMonitor();
		hacm.setExceptionHandler(new StandardErrorExceptionHandler());
		hacm.setTimeSource(ts);
		hacm.setConsecutiveUpChecksForAlertResolution(N);
		
		Assert.assertEquals(N, hacm.getConsecutiveUpChecksForAlertResolution());
		
		final AtomicInteger counter1 = new AtomicInteger(0);
		final AtomicInteger counter2 = new AtomicInteger();
		HealthAlertChangeObserver observer = new HealthAlertChangeObserver() {
			public void handleNewHealthAlerts(Set<String> newAlerts) {
				counter1.getAndAdd(newAlerts.size());
			}
			
			public void handleResolvedHealthAlerts(Set<String> resolvedAlerts) {
				counter2.getAndAdd(resolvedAlerts.size());
			}
		};
		
		// this fool verifies that one failing observer doesn't prevent others from getting notified
		HealthAlertChangeObserver explodingObserver = new HealthAlertChangeObserver() {
			public void handleNewHealthAlerts(Set<String> newAlerts) {
				throw new RuntimeException("BOOM!");
			}
			
			public void handleResolvedHealthAlerts(Set<String> resolvedAlerts) {
				throw new RuntimeException("KA-POW!");
			}
		};
		
		hacm.addHealthAlertChangeObserver(observer);
		hacm.addHealthAlertChangeObserver(observer); // dumb conditional check
		
		hacm.addHealthAlertChangeObserver(explodingObserver);
		
		hacm.setObservers(Arrays.asList(explodingObserver, observer));
		
		MemoryErrorTracker et1 = new MemoryErrorTracker();
		et1.trackError("error");
		
		MemoryErrorTracker et0 = new MemoryErrorTracker();
		
		// new error, observer should be notified
		hacm.handleHealthAlerts(et1);
		Assert.assertEquals(1, counter1.get());
		
		// error already exists, observer should NOT be notified
		hacm.handleHealthAlerts(et1);
		Assert.assertEquals(1, counter1.get());
		
		// error was cleared, but only once (need 2)
		hacm.handleHealthAlerts(et0);
		Assert.assertEquals(1, counter1.get());
		
		// error already exists, observer should NOT be notified
		hacm.handleHealthAlerts(et1);
		Assert.assertEquals(1, counter1.get());
		
		for (int i = 0; i < N - 1; i++) {
			// error was absent for N-1 consecutive checks, should still NOT be notified
			hacm.handleHealthAlerts(et0);
			Assert.assertEquals(1, counter1.get());
		}
		
		// error was absent for N consecutive checks, should be notified!
		hacm.handleHealthAlerts(et0);
		Assert.assertEquals(1, counter2.get());
	}
}