package com.janoside.health;

import org.junit.Test;

public class LoadAverageHealthMonitorTest {
	
	@Test
	public void testIt() {
		LoadAverageHealthMonitor laha = new LoadAverageHealthMonitor();
		
		MemoryErrorTracker et = new MemoryErrorTracker();
		
		laha.reportInternalErrors(et);
		
		for (String error : et.getErrors()) {
			System.out.println("Error: " + error);
		}
	}
}