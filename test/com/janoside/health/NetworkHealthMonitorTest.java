package com.janoside.health;

import java.util.Date;
import java.util.HashSet;

import org.junit.Test;

import com.janoside.exception.StandardErrorExceptionHandler;
import com.janoside.net.UrlDownloader;
import com.janoside.stats.BlackHoleStatTracker;

public class NetworkHealthMonitorTest {
	
	@Test
	public void testIt() {
		NetworkHealthMonitor nhm = new NetworkHealthMonitor();
		nhm.setExceptionHandler(new StandardErrorExceptionHandler());
		nhm.setStatTracker(new BlackHoleStatTracker());
		
		for (int i = 0; i < 10; i++) {
			nhm.run();
		}
	}
	
	@Test
	public void testIt2() {
		NetworkHealthMonitor nhm = new NetworkHealthMonitor();
		nhm.setExceptionHandler(new StandardErrorExceptionHandler());
		nhm.setStatTracker(new BlackHoleStatTracker());
		nhm.setUrls(new HashSet<String>() {{
			add("http://aiofgeygf03grgsyifgsidygfsdyifg.com/robots.txt");
		}});
		
		for (int i = 0; i < 3; i++) {
			nhm.run();
		}
	}
	
	@Test
	public void testExplosion() {
		NetworkHealthMonitor nhm = new NetworkHealthMonitor();
		nhm.setExceptionHandler(new StandardErrorExceptionHandler());
		nhm.setStatTracker(new BlackHoleStatTracker());
		nhm.setUrls(new HashSet<String>() {{
			add("http://aiofgeygf03grgsyifgsidygfsdyifg.com/robots.txt");
		}});
		nhm.setUrlDownloader(new UrlDownloader() {
			
			public Date getLastModified(String url) {
				return null;
			}
			
			public byte[] getData(String url) {
				return null;
			}
			
			public String getContentType(String url) {
				throw new RuntimeException("AHHHHH");
			}
		});
		
		for (int i = 0; i < 3; i++) {
			nhm.run();
		}
	}
}