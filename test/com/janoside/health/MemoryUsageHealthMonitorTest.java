package com.janoside.health;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.util.RandomUtil;

public class MemoryUsageHealthMonitorTest {
	
	@Test
	public void testIt() {
		ManagementMemoryUsageMeasurer memUsage = new ManagementMemoryUsageMeasurer();
		
		MemoryErrorTracker errorTracker = new MemoryErrorTracker();
		
		MemoryUsageHealthMonitor muhm = new MemoryUsageHealthMonitor();
		muhm.setMemoryUsageRatioThreshold(0.2f);
		muhm.setDataWindowSize(10);
		muhm.reportInternalErrors(errorTracker);
		
		Assert.assertTrue(errorTracker.getErrors().isEmpty());
		
		ArrayList<String> list = new ArrayList<String>();
		
		AtomicInteger counter = new AtomicInteger(0);
		for (int i = 0; i < 50000000; i++) {
			try {
				list.add(RandomUtil.randomWord(20));
				
				counter.getAndIncrement();
				
				if (memUsage.getUsedMemory() / (float) memUsage.getMaxMemory() > 0.9) {
					break;
				}
			} catch (OutOfMemoryError oome) {
				// expected
				break;
			}
		}
		
		for (int i = 0; i < 10; i++) {
			muhm.reportInternalErrors(errorTracker);
		}
		
		muhm.reportInternalErrors(errorTracker);
		
		Assert.assertFalse(errorTracker.getErrors().isEmpty());
		
		System.out.println("Got to " + counter.get());
	}
}