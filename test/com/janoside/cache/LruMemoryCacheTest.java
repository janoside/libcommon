package com.janoside.cache;

import java.util.ArrayList;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

public class LruMemoryCacheTest {
	
	@Test
	public void testMaxSize() {
		LruMemoryCache cache = new LruMemoryCache<String>();
		cache.setMaxSize(1000);
		
		for (int i = 0; i < 1000; i++) {
			cache.put(UUID.randomUUID().toString(), UUID.randomUUID().toString());
		}
		
		Assert.assertEquals(1000, cache.getSize());
		
		for (int i = 0; i < 1000; i++) {
			cache.put(UUID.randomUUID().toString(), UUID.randomUUID().toString());
			
			Assert.assertEquals(1000, cache.getSize());
		}
	}
	
	@Test
	public void testLruAlgorithm() {
		LruMemoryCache cache = new LruMemoryCache<String>();
		cache.setMaxSize(1000);
		
		ArrayList<String> firstBatch = new ArrayList<String>();
		for (int i = 0; i < 1000; i++) {
			String key = UUID.randomUUID().toString();
			
			firstBatch.add(key);
			
			cache.put(key, UUID.randomUUID().toString());
		}
		
		Assert.assertEquals(1000, cache.getSize());
		
		for (int i = 0; i < 1000; i++) {
			cache.put(UUID.randomUUID().toString(), UUID.randomUUID().toString());
			
			Assert.assertEquals(1000, cache.getSize());
		}
		
		for (String key : firstBatch) {
			Assert.assertNull(cache.get(key));
		}
	}
	
	@Test
	public void testLruAlgorithm2() {
		LruMemoryCache cache = new LruMemoryCache<String>();
		cache.setMaxSize(1000);
		
		ArrayList<String> firstHalfOfFirstBatch = new ArrayList<String>();
		for (int i = 0; i < 1000; i++) {
			String key = UUID.randomUUID().toString();
			
			if (i < 500) {
				firstHalfOfFirstBatch.add(key);
			}
			
			cache.put(key, UUID.randomUUID().toString());
		}
		
		Assert.assertEquals(1000, cache.getSize());
		
		for (String key : firstHalfOfFirstBatch) {
			cache.get(key);
		}
		
		for (int i = 0; i < 500; i++) {
			cache.put(UUID.randomUUID().toString(), UUID.randomUUID().toString());
			
			Assert.assertEquals(1000, cache.getSize());
		}
		
		for (String key : firstHalfOfFirstBatch) {
			Assert.assertNotNull(cache.get(key));
		}
	}
}