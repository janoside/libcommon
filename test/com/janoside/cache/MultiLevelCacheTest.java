package com.janoside.cache;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

public class MultiLevelCacheTest {
	
	@Test
	public void testCopyUpward() throws InterruptedException {
		MemoryCache<String> level1 = new MemoryCache<String>();
		
		MemoryCache<String> level2 = new MemoryCache<String>();
		
		ArrayList<ObjectCache<String>> caches = new ArrayList<ObjectCache<String>>();
		caches.add(level1);
		caches.add(level2);
		
		MultiLevelCache<String> cache = new MultiLevelCache<String>();
		cache.setInternalCaches(caches);
		
		cache.put("one", "one");
		
		Assert.assertEquals("one", cache.get("one"));
		Assert.assertNull(cache.get("two"));
		
		level2.put("two", "two");
		
		// value is in the "slower" cache only, this call finds it, and copies it into the "faster" cache
		Assert.assertEquals("two", cache.get("two"));
		
		Assert.assertEquals("two", level1.get("two"));
	}
}