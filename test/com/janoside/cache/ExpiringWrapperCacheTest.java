package com.janoside.cache;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.util.DateUtil;
import com.janoside.util.ManualTimeSource;

public class ExpiringWrapperCacheTest {
	
	@Test
	public void testIt() {
		ManualTimeSource ts = new ManualTimeSource();
		
		MemoryCache mc = new MemoryCache();
		
		ExpiringWrapperCache c = new ExpiringWrapperCache();
		c.setInternalCache(mc);
		c.setTimeSource(ts);
		
		ts.setCurrentTime(DateUtil.parseSimple("2013-05-01 12:00:00").getTime());
		
		c.put("abc", "def", 10000);
		
		ts.setCurrentTime(DateUtil.parseSimple("2013-05-01 12:00:09").getTime());
		
		Assert.assertNotNull(c.get("abc"));
		
		ts.setCurrentTime(DateUtil.parseSimple("2013-05-01 12:00:11").getTime());
		
		Assert.assertNull(c.get("abc"));
		
		Assert.assertNull(c.get("nonexistent-key"));
		
		
		
		ts.setCurrentTime(DateUtil.parseSimple("2013-05-01 12:00:00").getTime());
		
		c.put("abc", "def", 10000);
		Assert.assertEquals(1, c.getSize());
		
		c.clear();
		
		ts.setCurrentTime(DateUtil.parseSimple("2013-05-01 12:00:09").getTime());
		Assert.assertNull(c.get("abc"));
		Assert.assertEquals(0, c.getSize());
		
		
		
		c.put("abcd", "efgh");
		
		ts.setCurrentTime(DateUtil.parseSimple("3013-05-01 12:00:09").getTime());
		
		Assert.assertNotNull(c.get("abcd"));
		Assert.assertEquals(1, c.getSize());
	}
}