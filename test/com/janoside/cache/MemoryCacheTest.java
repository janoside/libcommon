package com.janoside.cache;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("unchecked")
public class MemoryCacheTest {
	
	@Test
	public void testContains() {
		MemoryCache cache = new MemoryCache();
		cache.put("test", "haha");
		
		Assert.assertNotNull(cache.get("test"));
	}
	
	@Test
	public void testPut() {
		MemoryCache cache = new MemoryCache();
		cache.put("test", "haha");
		
		Assert.assertNotNull(cache.get("test"));
	}
	
	@Test
	public void testPutExpire() throws InterruptedException {
		MemoryCache cache = new MemoryCache();
		cache.put("test", "haha", 50);
		
		Thread.sleep(100);
		
		Assert.assertNull(cache.get("test"));
	}
	
	@Test
	public void testExpireWithNoGet() throws InterruptedException {
		MemoryCache cache = new MemoryCache();
		cache.put("test", "haha", 50);
		
		Thread.sleep(100);
		
		Assert.assertEquals(0, cache.getSize());
	}
	
	@Test
	public void testGet() {
		MemoryCache cache = new MemoryCache();
		cache.put("test", "haha");
		
		Assert.assertNotNull(cache.get("test"));
		
		Object value = cache.get("test");
		
		Assert.assertTrue(value instanceof String);
		Assert.assertTrue(value.equals("haha"));
	}
	
	@Test
	public void testRemove() {
		MemoryCache cache = new MemoryCache();
		cache.put("test", "haha");
		
		Assert.assertNotNull(cache.get("test"));
		
		cache.remove("test");
		
		Assert.assertNull(cache.get("test"));
	}
	
	@Test
	public void testClear() {
		MemoryCache cache = new MemoryCache();
		cache.put("test", "haha");
		cache.put("test2", "haha2");
		
		Assert.assertNotNull(cache.get("test"));
		Assert.assertNotNull(cache.get("test2"));
		
		cache.clear();
		
		Assert.assertNull(cache.get("test"));
		Assert.assertNull(cache.get("test2"));
	}
	
	@Test
	public void testGetSize() {
		MemoryCache cache = new MemoryCache();
		cache.put("test", "haha");
		cache.put("test2", "haha2");
		
		Assert.assertTrue(cache.getSize() == 2);
		
		cache.put("test3", "haha3");
		
		Assert.assertTrue(cache.getSize() == 3);
		
		cache.put("test4", "haha3");
		
		Assert.assertTrue(cache.getSize() == 4);
		
		cache.remove("test4");
		
		Assert.assertTrue(cache.getSize() == 3);
	}
	
	@Test
	public void testGetSizeWithExpirations() throws Exception {
		MemoryCache cache = new MemoryCache();
		cache.put("1", "1", 100);
		cache.put("2", "2", 200);
		cache.put("3", "3", 300);
		
		Assert.assertEquals(3, cache.getSize());
		
		Thread.sleep(110);
		
		Assert.assertEquals(2, cache.getSize());
		
		Thread.sleep(110);
		
		Assert.assertEquals(1, cache.getSize());
		
		Thread.sleep(110);
		
		Assert.assertEquals(0, cache.getSize());
	}
	
	@Test
	@SuppressWarnings("serial")
	public void testSetValues() {
		MemoryCache<String> cache = new MemoryCache<String>();
		cache.setValues(new HashMap<String, String>() {{
			put("one", "one");
			put("two", "three");
		}});
		
		Assert.assertEquals("one", cache.get("one"));
		Assert.assertEquals("three", cache.get("two"));
		Assert.assertNull(cache.get("three"));
	}
}