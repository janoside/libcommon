package com.janoside.keyvalue;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.json.JsonObject;
import com.janoside.paging.Page;
import com.janoside.paging.SortDirection;
import com.janoside.paging.SortOrder;
import com.janoside.storage.KeyValueStore;
import com.janoside.storage.MemoryKeyValueStore;

public class KeyValueListTest {
	
	@Test
	public void testAdd() {
		KeyValueStore<JsonObject> store = new MemoryKeyValueStore<JsonObject>();
		
		KeyValueList list = new KeyValueList();
		list.setStore(store);
		list.setName("cheese");
		list.setPageSize(5);
		
		KeyValueList.keySeparator = ":";
		
		list.add(new JsonObject().put("id", 1));
		
		Assert.assertEquals("{\"itemCount\":1}", store.get("cheese:Manifest").toString());
		
		list.add(new JsonObject().put("id", 2));
		
		Assert.assertEquals("{\"itemCount\":2}", store.get("cheese:Manifest").toString());
		
		list.add(new JsonObject().put("id", 3));
		
		Assert.assertEquals("{\"itemCount\":3}", store.get("cheese:Manifest").toString());
		
		list.add(new JsonObject().put("id", 4));
		
		Assert.assertEquals("{\"itemCount\":4}", store.get("cheese:Manifest").toString());
		
		list.add(new JsonObject().put("id", 5));
		
		Assert.assertEquals("{\"itemCount\":5}", store.get("cheese:Manifest").toString());
		
		list.add(new JsonObject().put("id", 6));
		
		Assert.assertEquals("{\"itemCount\":6}", store.get("cheese:Manifest").toString());
		
		Assert.assertEquals("{\"items\":[{\"id\":6}]}", store.get("cheese:Page:2").toString());
		Assert.assertEquals("{\"items\":[{\"id\":1},{\"id\":2},{\"id\":3},{\"id\":4},{\"id\":5}]}", store.get("cheese:Page:1").toString());
	}
	
	@Test
	public void testGetItemsDescending() {
		KeyValueStore<JsonObject> store = new MemoryKeyValueStore<JsonObject>();
		
		KeyValueList list = new KeyValueList();
		list.setStore(store);
		list.setName("cheese");
		list.setPageSize(5);
		
		KeyValueList.keySeparator = ":";
		
		Assert.assertEquals(0, list.getItems(0, 5, SortOrder.Descending).size());
		
		list.add(new JsonObject().put("id", 1));
		
		Assert.assertEquals(1, list.getItems(0, 5, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":1}]", list.getItems(0, 5, SortOrder.Descending).toString());
		Assert.assertEquals(1, list.getItems(0, 1, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":1}]", list.getItems(0, 1, SortOrder.Descending).toString());
		
		list.add(new JsonObject().put("id", 2));
		
		Assert.assertEquals(2, list.getItems(0, 5, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":2}, {\"id\":1}]", list.getItems(0, 5, SortOrder.Descending).toString());
		Assert.assertEquals(2, list.getItems(0, 2, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":2}, {\"id\":1}]", list.getItems(0, 2, SortOrder.Descending).toString());
		Assert.assertEquals(1, list.getItems(0, 1, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":2}]", list.getItems(0, 1, SortOrder.Descending).toString());
		Assert.assertEquals(1, list.getItems(1, 1, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":1}]", list.getItems(1, 1, SortOrder.Descending).toString());
		
		list.add(new JsonObject().put("id", 3));
		
		Assert.assertEquals(3, list.getItems(0, 5, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":3}, {\"id\":2}, {\"id\":1}]", list.getItems(0, 5, SortOrder.Descending).toString());
		Assert.assertEquals(3, list.getItems(0, 3, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":3}, {\"id\":2}, {\"id\":1}]", list.getItems(0, 3, SortOrder.Descending).toString());
		Assert.assertEquals(2, list.getItems(0, 2, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":3}, {\"id\":2}]", list.getItems(0, 2, SortOrder.Descending).toString());
		Assert.assertEquals(1, list.getItems(0, 1, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":3}]", list.getItems(0, 1, SortOrder.Descending).toString());
		Assert.assertEquals(1, list.getItems(1, 1, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":2}]", list.getItems(1, 1, SortOrder.Descending).toString());
		Assert.assertEquals(1, list.getItems(2, 1, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":1}]", list.getItems(2, 1, SortOrder.Descending).toString());
		Assert.assertEquals(2, list.getItems(0, 2, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":3}, {\"id\":2}]", list.getItems(0, 2, SortOrder.Descending).toString());
		Assert.assertEquals(2, list.getItems(1, 2, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":2}, {\"id\":1}]", list.getItems(1, 2, SortOrder.Descending).toString());
		
		list.add(new JsonObject().put("id", 4));
		
		Assert.assertEquals(4, list.getItems(0, 5, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":4}, {\"id\":3}, {\"id\":2}, {\"id\":1}]", list.getItems(0, 5, SortOrder.Descending).toString());
		Assert.assertEquals(4, list.getItems(0, 4, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":4}, {\"id\":3}, {\"id\":2}, {\"id\":1}]", list.getItems(0, 4, SortOrder.Descending).toString());
		Assert.assertEquals(3, list.getItems(0, 3, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":4}, {\"id\":3}, {\"id\":2}]", list.getItems(0, 3, SortOrder.Descending).toString());
		Assert.assertEquals(2, list.getItems(0, 2, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":4}, {\"id\":3}]", list.getItems(0, 2, SortOrder.Descending).toString());
		Assert.assertEquals(1, list.getItems(0, 1, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":4}]", list.getItems(0, 1, SortOrder.Descending).toString());
		Assert.assertEquals(3, list.getItems(1, 5, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":3}, {\"id\":2}, {\"id\":1}]", list.getItems(1, 5, SortOrder.Descending).toString());
		Assert.assertEquals(2, list.getItems(2, 5, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":2}, {\"id\":1}]", list.getItems(2, 5, SortOrder.Descending).toString());
		
		list.add(new JsonObject().put("id", 5));
		
		Assert.assertEquals(5, list.getItems(0, 5, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":5}, {\"id\":4}, {\"id\":3}, {\"id\":2}, {\"id\":1}]", list.getItems(0, 5, SortOrder.Descending).toString());
		Assert.assertEquals(0, list.getItems(5, 5, SortOrder.Descending).size());
		Assert.assertEquals(4, list.getItems(1, 5, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":4}, {\"id\":3}, {\"id\":2}, {\"id\":1}]", list.getItems(1, 5, SortOrder.Descending).toString());
		Assert.assertEquals(3, list.getItems(2, 5, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":3}, {\"id\":2}, {\"id\":1}]", list.getItems(2, 5, SortOrder.Descending).toString());
		Assert.assertEquals(1, list.getItems(2, 1, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":3}]", list.getItems(2, 1, SortOrder.Descending).toString());
		
		list.add(new JsonObject().put("id", 6));
		
		Assert.assertEquals(5, list.getItems(0, 5, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":6}, {\"id\":5}, {\"id\":4}, {\"id\":3}, {\"id\":2}]", list.getItems(0, 5, SortOrder.Descending).toString());
		Assert.assertEquals(1, list.getItems(5, 5, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":1}]", list.getItems(5, 5, SortOrder.Descending).toString());
		Assert.assertEquals(6, list.getItems(0, 6, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":6}, {\"id\":5}, {\"id\":4}, {\"id\":3}, {\"id\":2}, {\"id\":1}]", list.getItems(0, 6, SortOrder.Descending).toString());
		Assert.assertEquals(4, list.getItems(2, 4, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":4}, {\"id\":3}, {\"id\":2}, {\"id\":1}]", list.getItems(2, 4, SortOrder.Descending).toString());
		Assert.assertEquals(4, list.getItems(2, 6, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":4}, {\"id\":3}, {\"id\":2}, {\"id\":1}]", list.getItems(2, 6, SortOrder.Descending).toString());
		Assert.assertEquals(4, list.getItems(2, 4, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":4}, {\"id\":3}, {\"id\":2}, {\"id\":1}]", list.getItems(2, 4, SortOrder.Descending).toString());
		
		for (int i = 7; i <= 10; i++) {
			list.add(new JsonObject().put("id", i));
		}
		
		Assert.assertEquals(5, list.getItems(0, 5, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":10}, {\"id\":9}, {\"id\":8}, {\"id\":7}, {\"id\":6}]", list.getItems(0, 5, SortOrder.Descending).toString());
		Assert.assertEquals(5, list.getItems(5, 5, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":5}, {\"id\":4}, {\"id\":3}, {\"id\":2}, {\"id\":1}]", list.getItems(5, 5, SortOrder.Descending).toString());
		Assert.assertEquals(0, list.getItems(10, 5, SortOrder.Descending).size());
		
		list.add(new JsonObject().put("id", 11));
		
		Assert.assertEquals(5, list.getItems(0, 5, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":11}, {\"id\":10}, {\"id\":9}, {\"id\":8}, {\"id\":7}]", list.getItems(0, 5, SortOrder.Descending).toString());
		Assert.assertEquals(5, list.getItems(5, 5, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":6}, {\"id\":5}, {\"id\":4}, {\"id\":3}, {\"id\":2}]", list.getItems(5, 5, SortOrder.Descending).toString());
		Assert.assertEquals(1, list.getItems(10, 5, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":1}]", list.getItems(10, 5, SortOrder.Descending).toString());
		Assert.assertEquals(11, list.getItems(0, 11, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":11}, {\"id\":10}, {\"id\":9}, {\"id\":8}, {\"id\":7}, {\"id\":6}, {\"id\":5}, {\"id\":4}, {\"id\":3}, {\"id\":2}, {\"id\":1}]", list.getItems(0, 11, SortOrder.Descending).toString());
		Assert.assertEquals(10, list.getItems(1, 11, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":10}, {\"id\":9}, {\"id\":8}, {\"id\":7}, {\"id\":6}, {\"id\":5}, {\"id\":4}, {\"id\":3}, {\"id\":2}, {\"id\":1}]", list.getItems(1, 11, SortOrder.Descending).toString());
		
		Assert.assertEquals(1, list.getItems(1, 1, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":10}]", list.getItems(1, 1, SortOrder.Descending).toString());
		Assert.assertEquals(1, list.getItems(1, 1, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":10}]", list.getItems(1, 1, SortOrder.Descending).toString());
		Assert.assertEquals(1, list.getItems(3, 1, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":8}]", list.getItems(3, 1, SortOrder.Descending).toString());
		Assert.assertEquals(1, list.getItems(5, 1, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":6}]", list.getItems(5, 1, SortOrder.Descending).toString());
		Assert.assertEquals(1, list.getItems(6, 1, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":5}]", list.getItems(6, 1, SortOrder.Descending).toString());
		Assert.assertEquals(1, list.getItems(9, 1, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":2}]", list.getItems(9, 1, SortOrder.Descending).toString());
		Assert.assertEquals(1, list.getItems(10, 1, SortOrder.Descending).size());
		Assert.assertEquals("[{\"id\":1}]", list.getItems(10, 1, SortOrder.Descending).toString());
	}
	
	@Test
	public void testGetItemsAscending() {
		KeyValueStore<JsonObject> store = new MemoryKeyValueStore<JsonObject>();
		
		KeyValueList list = new KeyValueList();
		list.setStore(store);
		list.setName("cheese");
		list.setPageSize(5);
		
		KeyValueList.keySeparator = ":";
		
		Assert.assertEquals(0, list.getItems(0, 5, SortOrder.Ascending).size());
		
		list.add(new JsonObject().put("id", 1));
		
		Assert.assertEquals(1, list.getItems(0, 5, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":1}]", list.getItems(0, 5, SortOrder.Ascending).toString());
		Assert.assertEquals(1, list.getItems(0, 1, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":1}]", list.getItems(0, 1, SortOrder.Ascending).toString());
		Assert.assertEquals(0, list.getItems(1, 1, SortOrder.Ascending).size());
		
		list.add(new JsonObject().put("id", 2));
		
		Assert.assertEquals(2, list.getItems(0, 5, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":1}, {\"id\":2}]", list.getItems(0, 5, SortOrder.Ascending).toString());
		Assert.assertEquals(1, list.getItems(0, 1, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":1}]", list.getItems(0, 1, SortOrder.Ascending).toString());
		Assert.assertEquals(2, list.getItems(0, 2, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":1}, {\"id\":2}]", list.getItems(0, 2, SortOrder.Ascending).toString());
		Assert.assertEquals(1, list.getItems(1, 1, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":2}]", list.getItems(1, 1, SortOrder.Ascending).toString());
		Assert.assertEquals(1, list.getItems(1, 2, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":2}]", list.getItems(1, 2, SortOrder.Ascending).toString());
		
		list.add(new JsonObject().put("id", 3));
		
		Assert.assertEquals(3, list.getItems(0, 5, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":1}, {\"id\":2}, {\"id\":3}]", list.getItems(0, 5, SortOrder.Ascending).toString());
		Assert.assertEquals(1, list.getItems(0, 1, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":1}]", list.getItems(0, 1, SortOrder.Ascending).toString());
		Assert.assertEquals(2, list.getItems(0, 2, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":1}, {\"id\":2}]", list.getItems(0, 2, SortOrder.Ascending).toString());
		Assert.assertEquals(3, list.getItems(0, 3, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":1}, {\"id\":2}, {\"id\":3}]", list.getItems(0, 3, SortOrder.Ascending).toString());
		
		list.add(new JsonObject().put("id", 4));
		
		Assert.assertEquals(4, list.getItems(0, 5, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":1}, {\"id\":2}, {\"id\":3}, {\"id\":4}]", list.getItems(0, 5, SortOrder.Ascending).toString());
		Assert.assertEquals(1, list.getItems(0, 1, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":1}]", list.getItems(0, 1, SortOrder.Ascending).toString());
		Assert.assertEquals(2, list.getItems(0, 2, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":1}, {\"id\":2}]", list.getItems(0, 2, SortOrder.Ascending).toString());
		Assert.assertEquals(3, list.getItems(0, 3, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":1}, {\"id\":2}, {\"id\":3}]", list.getItems(0, 3, SortOrder.Ascending).toString());
		Assert.assertEquals(4, list.getItems(0, 4, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":1}, {\"id\":2}, {\"id\":3}, {\"id\":4}]", list.getItems(0, 4, SortOrder.Ascending).toString());
		
		list.add(new JsonObject().put("id", 5));
		
		Assert.assertEquals(5, list.getItems(0, 5, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":1}, {\"id\":2}, {\"id\":3}, {\"id\":4}, {\"id\":5}]", list.getItems(0, 5, SortOrder.Ascending).toString());
		Assert.assertEquals(0, list.getItems(5, 5, SortOrder.Ascending).size());
		
		list.add(new JsonObject().put("id", 6));
		
		Assert.assertEquals(5, list.getItems(0, 5, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":1}, {\"id\":2}, {\"id\":3}, {\"id\":4}, {\"id\":5}]", list.getItems(0, 5, SortOrder.Ascending).toString());
		Assert.assertEquals(1, list.getItems(5, 5, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":6}]", list.getItems(5, 5, SortOrder.Ascending).toString());
		Assert.assertEquals(6, list.getItems(0, 6, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":1}, {\"id\":2}, {\"id\":3}, {\"id\":4}, {\"id\":5}, {\"id\":6}]", list.getItems(0, 6, SortOrder.Ascending).toString());
		Assert.assertEquals(5, list.getItems(1, 6, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":2}, {\"id\":3}, {\"id\":4}, {\"id\":5}, {\"id\":6}]", list.getItems(1, 6, SortOrder.Ascending).toString());
		Assert.assertEquals(4, list.getItems(2, 6, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":3}, {\"id\":4}, {\"id\":5}, {\"id\":6}]", list.getItems(2, 6, SortOrder.Ascending).toString());
		Assert.assertEquals(2, list.getItems(2, 2, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":3}, {\"id\":4}]", list.getItems(2, 2, SortOrder.Ascending).toString());
		
		for (int i = 7; i <= 10; i++) {
			list.add(new JsonObject().put("id", i));
		}
		
		Assert.assertEquals(5, list.getItems(0, 5, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":1}, {\"id\":2}, {\"id\":3}, {\"id\":4}, {\"id\":5}]", list.getItems(0, 5, SortOrder.Ascending).toString());
		Assert.assertEquals(5, list.getItems(5, 5, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":6}, {\"id\":7}, {\"id\":8}, {\"id\":9}, {\"id\":10}]", list.getItems(5, 5, SortOrder.Ascending).toString());
		Assert.assertEquals(6, list.getItems(0, 6, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":1}, {\"id\":2}, {\"id\":3}, {\"id\":4}, {\"id\":5}, {\"id\":6}]", list.getItems(0, 6, SortOrder.Ascending).toString());
		Assert.assertEquals(0, list.getItems(10, 5, SortOrder.Ascending).size());
		
		list.add(new JsonObject().put("id", 11));
		
		Assert.assertEquals(5, list.getItems(0, 5, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":1}, {\"id\":2}, {\"id\":3}, {\"id\":4}, {\"id\":5}]", list.getItems(0, 5, SortOrder.Ascending).toString());
		Assert.assertEquals(5, list.getItems(5, 5, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":6}, {\"id\":7}, {\"id\":8}, {\"id\":9}, {\"id\":10}]", list.getItems(5, 5, SortOrder.Ascending).toString());
		Assert.assertEquals(1, list.getItems(10, 5, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":11}]", list.getItems(10, 5, SortOrder.Ascending).toString());
		
		Assert.assertEquals(1, list.getItems(9, 1, SortOrder.Ascending).size());
		Assert.assertEquals("[{\"id\":10}]", list.getItems(9, 1, SortOrder.Ascending).toString());
	}
	
	@Test
	public void testIteration() {
		KeyValueStore<JsonObject> store = new MemoryKeyValueStore<JsonObject>();
		
		KeyValueList list = new KeyValueList();
		list.setStore(store);
		list.setName("cheese");
		list.setPageSize(5);
		
		KeyValueList.keySeparator = ":";
		
		list.add(new JsonObject().put("id", 1));
		
		ArrayList<JsonObject> items = new ArrayList<JsonObject>();
		for (JsonObject jsonObject : list) {
			items.add(jsonObject);
		}
		
		Assert.assertEquals("[{\"id\":1}]", items.toString());
		
		list.add(new JsonObject().put("id", 2));
		
		items = new ArrayList<JsonObject>();
		for (JsonObject jsonObject : list) {
			items.add(jsonObject);
		}
		
		Assert.assertEquals("[{\"id\":1}, {\"id\":2}]", items.toString());
		
		list.add(new JsonObject().put("id", 7));
		
		items = new ArrayList<JsonObject>();
		for (JsonObject jsonObject : list) {
			items.add(jsonObject);
		}
		
		Assert.assertEquals("[{\"id\":1}, {\"id\":2}, {\"id\":7}]", items.toString());
		
		list.add(new JsonObject().put("id", 10));
		list.add(new JsonObject().put("id", 11));
		list.add(new JsonObject().put("id", 1));
		list.add(new JsonObject().put("id", 800));
		
		items = new ArrayList<JsonObject>();
		for (JsonObject jsonObject : list) {
			items.add(jsonObject);
		}
		
		Assert.assertEquals("[{\"id\":1}, {\"id\":2}, {\"id\":7}, {\"id\":10}, {\"id\":11}, {\"id\":1}, {\"id\":800}]", items.toString());
	}
	
	@Test
	public void testRemoveOnSinglePage() {
		KeyValueStore<JsonObject> store = new MemoryKeyValueStore<JsonObject>();
		
		KeyValueList list = new KeyValueList();
		list.setStore(store);
		list.setName("cheese");
		list.setPageSize(5);
		
		KeyValueList.keySeparator = ":";
		
		for (int i = 0; i < 3; i++) {
			list.add(new JsonObject().put("id", i));
		}
		
		list.remove(new JsonObject().put("id", 1));
		
		Assert.assertEquals("[{\"id\":0}, {\"id\":2}]", list.getItems(0, 10000, SortOrder.Ascending).toString());
		Assert.assertEquals("[{\"id\":2}, {\"id\":0}]", list.getItems(0, 10000, SortOrder.Descending).toString());
	}
	
	@Test
	public void testRemoveOnTwoPages() {
		KeyValueStore<JsonObject> store = new MemoryKeyValueStore<JsonObject>();
		
		KeyValueList list = new KeyValueList();
		list.setStore(store);
		list.setName("cheese");
		list.setPageSize(5);
		
		KeyValueList.keySeparator = ":";
		
		for (int i = 0; i < 7; i++) {
			list.add(new JsonObject().put("id", i));
		}
		
		list.remove(new JsonObject().put("id", 1));
		
		Assert.assertEquals("[{\"id\":0}, {\"id\":2}, {\"id\":3}, {\"id\":4}, {\"id\":5}, {\"id\":6}]", list.getItems(0, 10000, SortOrder.Ascending).toString());
		Assert.assertEquals("[{\"id\":6}, {\"id\":5}, {\"id\":4}, {\"id\":3}, {\"id\":2}, {\"id\":0}]", list.getItems(0, 10000, SortOrder.Descending).toString());
	}
	
	@Test
	public void testRemoveOnThreePages() {
		KeyValueStore<JsonObject> store = new MemoryKeyValueStore<JsonObject>();
		
		KeyValueList list = new KeyValueList();
		list.setStore(store);
		list.setName("cheese");
		list.setPageSize(5);
		
		KeyValueList.keySeparator = ":";
		
		for (int i = 0; i < 13; i++) {
			list.add(new JsonObject().put("id", i));
		}
		
		list.remove(new JsonObject().put("id", 1));
		
		Assert.assertEquals("[{\"id\":0}, {\"id\":2}, {\"id\":3}, {\"id\":4}, {\"id\":5}, {\"id\":6}, {\"id\":7}, {\"id\":8}, {\"id\":9}, {\"id\":10}, {\"id\":11}, {\"id\":12}]", list.getItems(0, 10000, SortOrder.Ascending).toString());
		Assert.assertEquals("[{\"id\":12}, {\"id\":11}, {\"id\":10}, {\"id\":9}, {\"id\":8}, {\"id\":7}, {\"id\":6}, {\"id\":5}, {\"id\":4}, {\"id\":3}, {\"id\":2}, {\"id\":0}]", list.getItems(0, 10000, SortOrder.Descending).toString());
		
		Assert.assertEquals("{\"items\":[{\"id\":0},{\"id\":2},{\"id\":3},{\"id\":4},{\"id\":5}]}", store.get("cheese:Page:1").toString());
		Assert.assertEquals("{\"items\":[{\"id\":6},{\"id\":7},{\"id\":8},{\"id\":9},{\"id\":10}]}", store.get("cheese:Page:2").toString());
		Assert.assertEquals("{\"items\":[{\"id\":11},{\"id\":12}]}", store.get("cheese:Page:3").toString());
	}
	
	@Test
	public void testIndexesOf() {
		KeyValueStore<JsonObject> store = new MemoryKeyValueStore<JsonObject>();
		
		KeyValueList list = new KeyValueList();
		list.setStore(store);
		list.setName("cheese");
		list.setPageSize(5);
		
		KeyValueList.keySeparator = ":";
		
		for (int i = 0; i < 13; i++) {
			list.add(new JsonObject().put("id", i));
		}
		
		for (int i = 0; i < 13; i++) {
			list.add(new JsonObject().put("id", i));
		}
		
		for (int i = 0; i < 13; i++) {
			Assert.assertEquals("[" + i + ", " + (i + 13) +"]", list.indexesOf(new JsonObject().put("id", i)).toString());
		}
	}
	
	@Test
	public void testRemoveDuplicates() {
		KeyValueStore<JsonObject> store = new MemoryKeyValueStore<JsonObject>();
		
		KeyValueList list = new KeyValueList();
		list.setStore(store);
		list.setName("cheese");
		list.setPageSize(5);
		
		KeyValueList.keySeparator = ":";
		
		for (int i = 0; i < 13; i++) {
			list.add(new JsonObject().put("id", i));
		}
		
		list.add(new JsonObject().put("id", 0));
		for (int i = 0; i < 13; i++) {
			list.add(new JsonObject().put("id", i));
		}
		
		list.removeDuplicates();
		
		Assert.assertEquals(13, list.getItemCount());
		
		for (int i = 0; i < 13; i++) {
			Assert.assertEquals("[" + i + "]", list.indexesOf(new JsonObject().put("id", i)).toString());
		}
	}
	
	@Test
	public void testUpdateItem() {
		KeyValueStore<JsonObject> store = new MemoryKeyValueStore<JsonObject>();
		
		KeyValueList list = new KeyValueList();
		list.setStore(store);
		list.setName("cheese");
		list.setPageSize(5);
		
		KeyValueList.keySeparator = ":";
		
		for (int i = 0; i < 4; i++) {
			list.add(new JsonObject().put("id", i));
		}
		
		Assert.assertEquals("[{\"id\":0}, {\"id\":1}, {\"id\":2}, {\"id\":3}]", list.getItems(new Page("createdAt", SortDirection.Ascending, 0, 5)).toString());
		
		list.updateItem(2, new JsonObject().put("id", 4));
		
		Assert.assertEquals("[{\"id\":0}, {\"id\":1}, {\"id\":4}, {\"id\":3}]", list.getItems(new Page("createdAt", SortDirection.Ascending, 0, 5)).toString());
		
		list.updateItem(2, new JsonObject().put("id", 2));
		
		for (int i = 4; i < 13; i++) {
			list.add(new JsonObject().put("id", i));
		}
		
		Assert.assertEquals("[{\"id\":0}, {\"id\":1}, {\"id\":2}, {\"id\":3}, {\"id\":4}, {\"id\":5}, {\"id\":6}, {\"id\":7}, {\"id\":8}, {\"id\":9}, {\"id\":10}, {\"id\":11}, {\"id\":12}]", list.getItems(new Page("createdAt", SortDirection.Ascending, 0, 20)).toString());
		
		list.updateItem(5, new JsonObject().put("id", 50));
		
		Assert.assertEquals("[{\"id\":0}, {\"id\":1}, {\"id\":2}, {\"id\":3}, {\"id\":4}, {\"id\":50}, {\"id\":6}, {\"id\":7}, {\"id\":8}, {\"id\":9}, {\"id\":10}, {\"id\":11}, {\"id\":12}]", list.getItems(new Page("createdAt", SortDirection.Ascending, 0, 20)).toString());
		
		list.updateItem(9, new JsonObject().put("id", 90));
		
		Assert.assertEquals("[{\"id\":0}, {\"id\":1}, {\"id\":2}, {\"id\":3}, {\"id\":4}, {\"id\":50}, {\"id\":6}, {\"id\":7}, {\"id\":8}, {\"id\":90}, {\"id\":10}, {\"id\":11}, {\"id\":12}]", list.getItems(new Page("createdAt", SortDirection.Ascending, 0, 20)).toString());
		
		list.updateItem(12, new JsonObject().put("id", 120));
		
		Assert.assertEquals("[{\"id\":0}, {\"id\":1}, {\"id\":2}, {\"id\":3}, {\"id\":4}, {\"id\":50}, {\"id\":6}, {\"id\":7}, {\"id\":8}, {\"id\":90}, {\"id\":10}, {\"id\":11}, {\"id\":120}]", list.getItems(new Page("createdAt", SortDirection.Ascending, 0, 20)).toString());
	}
	
	@Test
	public void testRemoveNonexistentItem() {
		KeyValueStore<JsonObject> store = new MemoryKeyValueStore<JsonObject>();
		
		KeyValueList list = new KeyValueList();
		list.setStore(store);
		list.setName("cheese");
		list.setPageSize(5);
		
		KeyValueList.keySeparator = ":";
		
		for (int i = 0; i < 4; i++) {
			list.add(new JsonObject().put("id", i));
		}
		
		int exceptions = 0;
		try {
			list.remove(7);
			
		} catch (IllegalArgumentException e) {
			exceptions++;
		}
		
		Assert.assertEquals(1, exceptions);
		
		exceptions = 0;
		try {
			list.remove(new JsonObject().put("id", 8));
			
		} catch (IllegalArgumentException e) {
			exceptions++;
		}
		
		Assert.assertEquals(0, exceptions);
	}
}