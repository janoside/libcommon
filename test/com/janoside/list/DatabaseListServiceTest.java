package com.janoside.list;

import java.util.HashMap;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.paging.Page;
import com.janoside.paging.SortDirection;

public class DatabaseListServiceTest {
	
	@Test
	public void testIt() {
		DatabaseListService ls = new DatabaseListService();
		List<ListItem> items = ls.getItems(
				"Vehicle",
				new HashMap<String, String>() {{
					put("dealerId", "norm-reeves-honda-superstore-cerritos-ca");
					put("priceRange", "2");
				}},
				new Page(
						"createdAt",
						SortDirection.Descending,
						0,
						3));
		
		Assert.assertNull(items);
	}
}