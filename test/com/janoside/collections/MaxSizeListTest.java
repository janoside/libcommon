package com.janoside.collections;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class MaxSizeListTest {
	
	@Test
	public void testAdd() {
		MaxSizeList<String> list = new MaxSizeList<String>(2);
		list.add("one");
		
		Assert.assertEquals(1, list.size());
		
		list.add("two");
		
		Assert.assertEquals(2, list.size());
		
		list.add("three");
		
		Assert.assertEquals(2, list.size());
		Assert.assertEquals("two", list.get(0));
		Assert.assertEquals("three", list.get(1));
	}
	
	@Test
	public void testAddAll() {
		MaxSizeList<String> list = new MaxSizeList<String>(2);
		list.addAll(Arrays.asList("one", "two", "three"));
		
		Assert.assertEquals(2, list.size());
		Assert.assertEquals("two", list.get(0));
		Assert.assertEquals("three", list.get(1));
	}
	
	@Test
	public void testAddAllAtIndex() {
		MaxSizeList<String> list = new MaxSizeList<String>(2);
		
		list.add("one");
		
		list.addAll(1, Arrays.asList("two", "three", "four"));
		
		Assert.assertEquals(2, list.size());
		Assert.assertEquals("three", list.get(0));
		Assert.assertEquals("four", list.get(1));
	}
}