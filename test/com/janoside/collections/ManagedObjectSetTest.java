package com.janoside.collections;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("unchecked")
public class ManagedObjectSetTest {
	
	@Test
	public void testAddContains() {
		Object obj = new Object();
		
		ManagedMemorySet set = new ManagedMemorySet();
		set.add(obj);
		
		Assert.assertTrue(set.contains(obj));
	}
	
	@Test
	public void testRemove() {
		Object obj = new Object();
		
		ManagedMemorySet set = new ManagedMemorySet();
		set.add(obj);
		
		Assert.assertTrue(set.contains(obj));
		
		set.remove(obj);
		
		Assert.assertFalse(set.contains(obj));
	}
	
	@Test
	public void testSize() {
		ManagedMemorySet set = new ManagedMemorySet();
		
		for (int i = 0; i < 100; i++) {
			set.add(new Object());
			Assert.assertEquals(i + 1, set.getSize());
		}
	}
	
	@Test
	public void testIterator() {
		ManagedMemorySet set = new ManagedMemorySet();
		
		for (int i = 0; i < 100; i++) {
			set.add(new Object());
			Assert.assertEquals(i + 1, set.getSize());
		}
		
		int count = 0;
		for (Object o : set) {
			if (o != null) {
				count++;
			}
		}
		
		Assert.assertEquals(100, count);
	}
	
	@Test
	public void testClear() {
		ManagedMemorySet set = new ManagedMemorySet();
		
		for (int i = 0; i < 100; i++) {
			set.add(new Object());
			Assert.assertEquals(i + 1, set.getSize());
		}
		
		set.clear();
		
		Assert.assertEquals(0, set.getSize());
		
		int count = 0;
		Iterator iterator = set.iterator();
		while (iterator.hasNext()) {
			iterator.next();
			count++;
		}
		
		Assert.assertEquals(0, count);
	}
	
	@Test
	public void testMultithread() throws InterruptedException {
		for (int trial = 0; trial < 100; trial++) {
			final ManagedMemorySet set = new ManagedMemorySet();
			
			ExecutorService executor = Executors.newFixedThreadPool(100);
			
			for (int i = 0; i < 15; i++) {
				executor.execute(new Runnable() {
					public void run() {
						try {
							for (int i = 0; i < 100; i++) {
								set.add(new Object());
							}
						} catch (Throwable t) {
							t.printStackTrace();
						}
					}
				});
			}
			
			executor.shutdown();
			
			while (!executor.isTerminated()) {
				Thread.sleep(10);
			}
			
			Assert.assertEquals("Failure on attempt " + (trial + 1), 1500, set.getSize());
		}
	}
	
	@Test
	public void testViewValues() {
		ManagedMemorySet set = new ManagedMemorySet();
		
		for (int i = 0; i < 10; i++) {
			set.add(Long.valueOf(i));
		}
		
		List<String> values = set.viewValues();
		for (int i = 0; i < values.size(); i++) {
			Assert.assertEquals(Long.toString(i), values.get(i));
		}
	}
}