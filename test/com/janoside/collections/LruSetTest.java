package com.janoside.collections;

import org.junit.Assert;
import org.junit.Test;

public class LruSetTest {
	
	@Test
	public void testIt() {
		int max = 3000;
		
		LruSet<Integer> set = new LruSet<Integer>(max);
		
		for (int i = 0; i < max; i++) {
			set.add(i);
		}
		
		for (int i = 0; i < max; i++) {
			Assert.assertTrue(set.contains(i));
		}
		
		int overage = 1000;
		
		for (int i = 0; i < max + overage; i++) {
			set.add(i);
		}
		
		for (int i = 0; i < max + 1; i++) {
			if (i < overage) {
				Assert.assertFalse(set.contains(i));
				
			} else {
				Assert.assertTrue(set.contains(i));
			}
		}
	}
}