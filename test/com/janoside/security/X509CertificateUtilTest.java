package com.janoside.security;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.ConnectException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.Date;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;

import org.junit.Test;

import com.janoside.util.DateUtil;

public class X509CertificateUtilTest {
	
	@Test
	public void testX509Certificate() throws Exception {
		String keystorePassword = "keystorePassword";
		String keyDataPassword = "keyDataPassword";
		
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
		keyPairGenerator.initialize(1024);
		KeyPair keyPair = keyPairGenerator.generateKeyPair();
		
		X509Certificate cert = X509CertificateUtil.generateSelfSignedX509Certificate(
                keyPair,
                "CN=astarsoftware.com",
                "CN=junit",
                new Date(System.currentTimeMillis() - 1000),
                new Date(System.currentTimeMillis() + 10 * DateUtil.YearMillis));
		
		final byte[] keystore = SecurityUtil.generateKeystore(
				keyPair,
				cert,
				keystorePassword,
				keyDataPassword);
		
		InputStream inputStream = new ByteArrayInputStream(keystore);
		
		SSLContext sslContext = SSLContext.getInstance("TLS");
		KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
		KeyStore keyStore = KeyStore.getInstance("PKCS12");
		
		keyStore.load(inputStream, keystorePassword.toCharArray());
		keyManagerFactory.init(keyStore, keyDataPassword.toCharArray());
		sslContext.init(keyManagerFactory.getKeyManagers(), null, null);
		
		javax.net.ssl.SSLSocketFactory socketFactory = sslContext.getSocketFactory();
		
		try {
			SSLSocket socket = (SSLSocket) socketFactory.createSocket("localhost", 44300);
			socket.getOutputStream().write("cheese".getBytes());
			
			throw new Exception();
			
		} catch (ConnectException ce) {
			// expected
		}
	}
	
}
