package com.janoside.security.encryption;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.junit.Assert;
import org.junit.Test;

public class JasyptStrongEncryptorTest {
	
	@Test
	public void testEncrypt() {
		JasyptStrongEncryptor encryptor = new JasyptStrongEncryptor();
		
		Assert.assertNotNull(encryptor.encrypt("haha"));
	}
	
	@Test
	public void testDecrypt() {
		JasyptStrongEncryptor e = new JasyptStrongEncryptor("ahhhhhh");
		
		System.out.println(e.encrypt("abcdef"));
		
		Assert.assertEquals("abcdef", e.decrypt("LWhqh2mB2yZsM7m+tuBj+Q=="));
	}
	
	@Test
	public void testEncryptDecrypt() {
		JasyptStrongEncryptor encryptor = new JasyptStrongEncryptor();
		
		Random random = new Random();
		
		String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		
		for (int i = 0; i < 100; i++) {
			int length = random.nextInt(25);
			
			StringBuilder buffer = new StringBuilder(length);
			for (int j = 0; j < length; j++) {
				buffer.append(chars.charAt(random.nextInt(chars.length())));
			}
			
			String output = encryptor.encrypt(buffer.toString());
			
			Assert.assertEquals("original cleartext vs. decrypted ciphertext", buffer.toString(), encryptor.decrypt(output));
		}
	}
	
	@Test
	public void testDifferentEncrypt() {
		JasyptStrongEncryptor encryptor = new JasyptStrongEncryptor();
		
		String input = "haha";
		
		String output1 = encryptor.encrypt(input);
		String output2 = encryptor.encrypt(input);
		
		Assert.assertNotNull(output1);
		Assert.assertNotNull(output2);
		Assert.assertFalse("differentEncryptedValues", output1.equals(output2));
	}
	
	@Test
	public void testDecryptFailure() {
		JasyptStrongEncryptor encryptor = new JasyptStrongEncryptor();
		
		int exceptionCount = 0;
		try {
			encryptor.decrypt("abc");
			
		} catch (EncryptionOperationNotPossibleException eonpe) {
			exceptionCount++;
		}
		
		Assert.assertEquals(1, exceptionCount);
		Assert.assertEquals(1, encryptor.getFailureCount());
		Assert.assertEquals(0, encryptor.getSuccessCount());
		Assert.assertEquals(0.0f, encryptor.getSuccessRate(), 0);
	}
	
	@Test
	public void testProductionPassword() {
		JasyptStrongEncryptor encryptor = new JasyptStrongEncryptor("abc");
		
		System.out.println("------------");
		System.out.println(encryptor.encrypt("abc"));
	}
	
	@Test
	public void testStuff() {
		this.encrypt("");
	}
	
	private void decrypt(String s) {
		JasyptStrongEncryptor e = new JasyptStrongEncryptor();
		
		System.out.println("decrypt(" + s + ") -> " + e.decrypt(s));
	}
	
	private void encrypt(String s) {
		JasyptStrongEncryptor e = new JasyptStrongEncryptor();
		
		System.out.println("encrypt(" + s + ") -> " + e.encrypt(s));
	}
	
	@Test
	public void testPerformance() {
		JasyptStrongEncryptor encryptor = new JasyptStrongEncryptor();
		
		int count = 1000;
		
		ArrayList<String> things = new ArrayList<String>(count);
		ArrayList<String> things2 = new ArrayList<String>(count);
		
		for (int i = 0; i < count; i++) {
			things.add(UUID.randomUUID().toString());
		}
		
		long startTime = System.currentTimeMillis();
		
		for (String thing : things) {
			things2.add(encryptor.encrypt(thing));
		}
		
		System.out.println("Encrypt: " + (System.currentTimeMillis() - startTime));
		startTime = System.currentTimeMillis();
		
		for (String thing : things2) {
			encryptor.decrypt(thing);
		}
		
		System.out.println("Decrypt: " + (System.currentTimeMillis() - startTime));
		startTime = System.currentTimeMillis();
	}
}