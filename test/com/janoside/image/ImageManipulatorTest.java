package com.janoside.image;

import java.io.File;
import java.io.FileOutputStream;

import org.junit.Test;

import com.janoside.http.BasicHttpClient;
import com.janoside.http.HttpClient;
import com.janoside.stats.BlackHoleStatTracker;

public class ImageManipulatorTest {
	
	@Test
	public void testResize() throws Exception {
		HttpClient httpClient = new BasicHttpClient();
		
		byte[] bytes = httpClient.getBytes("http://i.imgur.com/5FSWHpV.png", 0);
		
		ImageManipulator im = new ImageManipulator();
		im.setStatTracker(new BlackHoleStatTracker());
		im.setImageCreator(new ImageCreator());
		
		byte[] data = im.resize(bytes, 300, 200, ResizeMode.Crop, ImageFileType.Jpeg, 1.0f);
		
		FileOutputStream fos = new FileOutputStream(new File("/tmp/im.jpg"));
		
		fos.write(data);
		
		fos.close();
	}
}