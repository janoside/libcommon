package com.janoside.image;

import java.awt.image.BufferedImage;
import java.util.UUID;

import org.junit.Test;

import com.janoside.http.BasicHttpClient;
import com.janoside.util.FileUtil;
import com.thebuzzmedia.imgscalr.Scalr;

public class ImgScalrTest {
	
	@Test
	public void testIt() {
		ImageCreator imageCreator = new ImageCreator();
		
		BasicHttpClient http = new BasicHttpClient();
		
		byte[] bytes = http.getBytes("https://s3.amazonaws.com/astar-content/irobot-100.jpeg", 0);
		
		BufferedImage bi = imageCreator.createImageFromByteArray(bytes);
		
		bi = Scalr.resize(bi, 150);
		
		bytes = imageCreator.createImageFile(bi, ImageFileType.Png, 1.0f);
		
		FileUtil.writeBytes("/tmp/" + UUID.randomUUID().toString() + ".png", bytes);
	}
}