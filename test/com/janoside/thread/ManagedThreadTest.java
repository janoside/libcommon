package com.janoside.thread;

import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Assert;
import org.junit.Test;

public class ManagedThreadTest {
	
	@Test
	public void testBasic() throws InterruptedException {
		final AtomicInteger intValue = new AtomicInteger(0);
		
		ManagedThread thread = new ManagedThread() {
			protected void runInternal() throws Exception {
				for (int i = 0; i < 100; i++) {
					intValue.getAndIncrement();
				}
			}
		};
		
		thread.start();
		
		Thread.sleep(10);
		
		thread.setRunning(false);
		
		while (thread.isAlive()) {
			Thread.sleep(5);
		}
		
		Assert.assertEquals(100, intValue.get());
	}
	
	@Test
	public void testRunNow() throws InterruptedException {
		final AtomicInteger intValue = new AtomicInteger(0);
		
		ManagedThread thread = new ManagedThread() {
			protected void runInternal() throws Exception {
				intValue.getAndIncrement();
			}
		};
		thread.setPeriod(10000);
		
		thread.start();
		
		Thread.sleep(100);
		
		thread.runNow();
		
		Thread.sleep(100);
		
		Assert.assertEquals(2, intValue.get());
	}
	
	@Test
	public void testWakeUpOnSetPeriod() throws InterruptedException {
		final AtomicInteger intValue = new AtomicInteger(0);
		
		ManagedThread thread = new ManagedThread() {
			protected void runInternal() throws Exception {
				intValue.getAndIncrement();
			}
		};
		thread.setPeriod(10000);
		
		thread.start();
		
		Thread.sleep(100);
		
		Assert.assertEquals(1, intValue.get());
		
		// notifyAll() is only called when newPeriod < oldPeriod
		thread.setPeriod(10000);
		thread.setPeriod(15000);
		
		Thread.sleep(100);
		
		Assert.assertEquals(1, intValue.get());
		
		thread.setPeriod(9000);
		
		Thread.sleep(100);
		
		Assert.assertEquals(2, intValue.get());
	}
}