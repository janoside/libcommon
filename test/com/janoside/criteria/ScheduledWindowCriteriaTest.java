package com.janoside.criteria;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.util.DateUtil;
import com.janoside.util.ManualTimeSource;

public class ScheduledWindowCriteriaTest {
	
	@Test
	public void testMet() throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		ManualTimeSource timeSource = new ManualTimeSource();
		
		ScheduledWindowCriteria criteria = new ScheduledWindowCriteria();
		criteria.setTimeSource(timeSource);
		criteria.setWindowStartTime("09:00:00");
		criteria.setWindowEndTime("10:00:00");
		
		String startDateString = dateFormat.format(DateUtil.getToday());
		
		long startDateTime = dateTimeFormat.parse(startDateString + " 09:00:00").getTime();
		long endDateTime = dateTimeFormat.parse(startDateString + " 10:00:00").getTime();
		
		timeSource.setCurrentTime(startDateTime - 50000);
		Assert.assertFalse(criteria.isMet());
		
		timeSource.setCurrentTime(endDateTime + 50000);
		Assert.assertFalse(criteria.isMet());
		
		timeSource.setCurrentTime(startDateTime + (endDateTime - startDateTime) / 2);
		Assert.assertTrue(criteria.isMet());
	}
	
	@Test
	public void testMetNextDay() throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		ManualTimeSource timeSource = new ManualTimeSource();
		
		ScheduledWindowCriteria criteria = new ScheduledWindowCriteria();
		criteria.setTimeSource(timeSource);
		criteria.setWindowStartTime("09:00:00");
		criteria.setWindowEndTime("04:00:00");
		
		String startDateString = dateFormat.format(DateUtil.getToday());
		
		long startDateTime = dateTimeFormat.parse(startDateString + " 09:00:00").getTime();
		long endDateTime = dateTimeFormat.parse(startDateString + " 04:00:00").getTime() + DateUtil.DayMillis;
		
		timeSource.setCurrentTime(startDateTime - 50000);
		Assert.assertFalse(criteria.isMet());
		
		timeSource.setCurrentTime(endDateTime + 50000);
		Assert.assertFalse(criteria.isMet());
		
		timeSource.setCurrentTime(startDateTime + (endDateTime - startDateTime) / 2);
		Assert.assertTrue(criteria.isMet());
	}
	
	@Test
	public void testEarlyTomorrow() throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		ManualTimeSource timeSource = new ManualTimeSource();
		
		ScheduledWindowCriteria criteria = new ScheduledWindowCriteria();
		criteria.setTimeSource(timeSource);
		criteria.setWindowStartTime("01:00:00");
		criteria.setWindowEndTime("02:00:00");
		
		String startDateString = dateFormat.format(DateUtil.getToday());
		
		long startDateTime = dateTimeFormat.parse(startDateString + " 01:00:00").getTime();
		long endDateTime = dateTimeFormat.parse(startDateString + " 02:00:00").getTime();
		
		timeSource.setCurrentTime(startDateTime - DateUtil.DayMillis - 50000);
		Assert.assertFalse(criteria.isMet());
		
		timeSource.setCurrentTime(endDateTime + 50000);
		Assert.assertFalse(criteria.isMet());
		
		timeSource.setCurrentTime(startDateTime + (endDateTime - startDateTime) / 2);
		Assert.assertTrue(criteria.isMet());
	}
	
	@Test
	public void testInspectionTimers() throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		ManualTimeSource timeSource = new ManualTimeSource();
		
		ScheduledWindowCriteria criteria = new ScheduledWindowCriteria();
		criteria.setTimeSource(timeSource);
		criteria.setWindowStartTime("09:00:00");
		criteria.setWindowEndTime("10:00:00");
		
		String startDateString = dateFormat.format(DateUtil.getToday());
		
		long startDateTime = dateTimeFormat.parse(startDateString + " 09:00:00").getTime();
		long endDateTime = dateTimeFormat.parse(startDateString + " 10:00:00").getTime();
		
		timeSource.setCurrentTime(startDateTime - 50000);
		Assert.assertEquals("secondsUntilWindowStart", 50, criteria.getSecondsUntilWindowStart());
		
		timeSource.setCurrentTime(startDateTime + (endDateTime - startDateTime) / 2);
		Assert.assertEquals("secondsUntilWindowEnd", (endDateTime - startDateTime) / 2000, criteria.getSecondsUntilWindowEnd());
	}
	
	@Test
	public void testInspectionTimersNextDay() throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		ManualTimeSource timeSource = new ManualTimeSource();
		
		ScheduledWindowCriteria criteria = new ScheduledWindowCriteria();
		criteria.setTimeSource(timeSource);
		criteria.setWindowStartTime("09:00:00");
		criteria.setWindowEndTime("04:00:00");
		
		String startDateString = dateFormat.format(DateUtil.getToday());
		
		long startDateTime = dateTimeFormat.parse(startDateString + " 09:00:00").getTime();
		long endDateTime = dateTimeFormat.parse(startDateString + " 04:00:00").getTime() + DateUtil.DayMillis;
		
		timeSource.setCurrentTime(startDateTime - 50000);
		Assert.assertEquals("secondsUntilWindowStart", 50, criteria.getSecondsUntilWindowStart());
		
		timeSource.setCurrentTime(startDateTime + (endDateTime - startDateTime) / 2);
		Assert.assertEquals("secondsUntilWindowEnd", (endDateTime - startDateTime) / 2000, criteria.getSecondsUntilWindowEnd());
	}
}