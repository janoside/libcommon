package com.janoside.criteria;

import org.junit.Assert;
import org.junit.Test;

public class BooleanCriteriaTest {
	
	@Test
	public void testDefault() {
		BooleanCriteria criteria = new BooleanCriteria();
		
		Assert.assertFalse(criteria.isMet());
	}
	
	@Test
	public void testMet() {
		BooleanCriteria criteria = new BooleanCriteria();
		criteria.setMet(true);
		
		Assert.assertTrue(criteria.isMet());
	}
}