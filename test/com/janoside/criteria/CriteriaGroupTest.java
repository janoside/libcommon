package com.janoside.criteria;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class CriteriaGroupTest {
	
	@Test
	public void testTrue() {
		BooleanCriteria crit1 = new BooleanCriteria();
		crit1.setMet(true);
		
		BooleanCriteria crit2 = new BooleanCriteria();
		crit2.setMet(true);
		
		CriteriaGroup group = new CriteriaGroup();
		group.setInternalCriteria(Arrays.asList(crit1, crit2));
		
		Assert.assertTrue(group.isMet());
	}
	
	@Test
	public void testFalse() {
		BooleanCriteria crit1 = new BooleanCriteria();
		crit1.setMet(true);
		
		BooleanCriteria crit2 = new BooleanCriteria();
		crit2.setMet(false);
		
		CriteriaGroup group = new CriteriaGroup();
		group.setInternalCriteria(Arrays.asList(crit1, crit2));
		
		Assert.assertFalse(group.isMet());
	}
}