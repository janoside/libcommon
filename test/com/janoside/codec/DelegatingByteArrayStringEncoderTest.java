package com.janoside.codec;

import java.util.Arrays;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.util.RandomUtil;

public class DelegatingByteArrayStringEncoderTest {
	
	@Test
	public void testIt() {
		DelegatingByteArrayStringEncoder enc = new DelegatingByteArrayStringEncoder();
		enc.setEncoder(new Base16Encoder());
		
		Assert.assertEquals("bf5844e015ca9c4734528f2f1741c91454ce8362483ad1389335386ec68083d95bd2d6cde5ff03e288571cfbaa4c4884ea57dafa501b9fb0c6c977cfc7c6269d558b6b0230cd524844ddc62262506aef33e2a83f699709953623c76e018ba082570bec49", enc.encode(RandomUtil.randomByteArray(100, new Random(5))));
		Assert.assertEquals("[-65, 88, 68, -32, 21, -54, -100, 71, 52, 82, -113, 47, 23, 65, -55, 20, 84, -50, -125, 98, 72, 58, -47, 56, -109, 53, 56, 110, -58, -128, -125, -39, 91, -46, -42, -51, -27, -1, 3, -30, -120, 87, 28, -5, -86, 76, 72, -124, -22, 87, -38, -6, 80, 27, -97, -80, -58, -55, 119, -49, -57, -58, 38, -99, 85, -117, 107, 2, 48, -51, 82, 72, 68, -35, -58, 34, 98, 80, 106, -17, 51, -30, -88, 63, 105, -105, 9, -107, 54, 35, -57, 110, 1, -117, -96, -126, 87, 11, -20, 73]", Arrays.toString(enc.decode("bf5844e015ca9c4734528f2f1741c91454ce8362483ad1389335386ec68083d95bd2d6cde5ff03e288571cfbaa4c4884ea57dafa501b9fb0c6c977cfc7c6269d558b6b0230cd524844ddc62262506aef33e2a83f699709953623c76e018ba082570bec49")));
	}
}