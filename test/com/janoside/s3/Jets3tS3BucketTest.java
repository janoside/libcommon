package com.janoside.s3;

import org.junit.Test;

public class Jets3tS3BucketTest {
	
	private static final String AccessKey = "";
	
	private static final String SecretKey = "";
	
	@Test
	public void testListKeysWithPrefix() throws Exception {
		String bucketName = "";
		
		if ("".equals(AccessKey)) {
			return;
		}
		
		Jets3tS3Bucket bucket = new Jets3tS3Bucket();
		bucket.setAccessKey(AccessKey);
		bucket.setSecretKey(SecretKey);
		bucket.setBucketName(bucketName);
		bucket.afterPropertiesSet();
		
		System.out.println(bucket.getKeys(
				"janoside/ClusterService/RegistrationsByType/GameServer/",
				true,
				true));
	}
}