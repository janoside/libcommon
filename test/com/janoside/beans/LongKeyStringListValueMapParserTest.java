package com.janoside.beans;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class LongKeyStringListValueMapParserTest {
	
	@Test
	@SuppressWarnings("unchecked")
	public void testIt() {
		LongKeyStringListValueMapParser parser = new LongKeyStringListValueMapParser();
		
		Assert.assertTrue(mapIsValid(parser.parse("0=[\"abc\",\"def\"],5=[\"haha\",\"monkey\",\"whoa\"]"),         new long[] {0, 5},   new List[] {Arrays.asList("abc", "def"), Arrays.asList("haha", "monkey", "whoa")}));
		Assert.assertTrue(mapIsValid(parser.parse("1 = [\"abc\",\"def\"],  204 = [\"haha\",\"monkey\",\"whoa\"]"), new long[] {1, 204}, new List[] {Arrays.asList("abc", "def"), Arrays.asList("haha", "monkey", "whoa")}));
		Assert.assertTrue(mapIsValid(parser.parse("0=[\"abc\",\"def\"],5=[\"haha\",\"\",\"monkey\",\"whoa\"]"),        new long[] {0, 5},   new List[] {Arrays.asList("abc", "def"), Arrays.asList("haha", "", "monkey", "whoa")}));
		Assert.assertTrue(mapIsValid(parser.parse("0=[\"[abc\",\"def\"],5=[\"haha\",\"monkey\",\"whoa\"]"),        new long[] {0, 5},   new List[] {Arrays.asList("[abc", "def"), Arrays.asList("haha", "monkey", "whoa")}));
		
		Assert.assertTrue(mapIsValid(parser.parse("0=[\"[abc\",\"def]\"],5=[\"haha\",\"monkey\",\"whoa\"]"),       new long[] {0, 5},   new List[] {Arrays.asList("[abc", "def]"), Arrays.asList("haha", "monkey", "whoa")}));
		Assert.assertTrue(mapIsValid(parser.parse("0=[\"[abc\",\"[def]\",\"\"],5=[\"haha\",\"monkey\",\"whoa\"]"),   new long[] {0, 5},   new List[] {Arrays.asList("[abc", "[def]", ""), Arrays.asList("haha", "monkey", "whoa")}));
		
		Assert.assertTrue(mapIsValid(parser.parse("0=[\"[abc\",\"[def]\"],5=[\"haha\",\"monkey\",\"whoa\"]"),      new long[] {0, 5},   new List[] {Arrays.asList("[abc", "[def]"), Arrays.asList("haha", "monkey", "whoa")}));
		Assert.assertTrue(mapIsValid(parser.parse("0=[\"[abc\",\"[def]=\"],5=[\"haha\",\"monkey\",\"whoa\"]"),     new long[] {0, 5},   new List[] {Arrays.asList("[abc", "[def]="), Arrays.asList("haha", "monkey", "whoa")}));
		
		Assert.assertTrue(mapIsValid(parser.parse("0=[\"[abc\", \"[def]=\"],5=[\"haha\", \"monkey\",\"whoa\"], 7=[\"hello\"]"),     new long[] {0, 5, 7},   new List[] {Arrays.asList("[abc", "[def]="), Arrays.asList("haha", "monkey", "whoa"), Arrays.asList("hello")}));
		
		int exceptionCount = 0;
		try {
			mapIsValid(parser.parse("0=[\"abc\",\"\",\"],5=[\"haha\",\"monkey\",\"whoa\"]"),         new long[] {0, 5},   new List[] {Arrays.asList("abc", "def"), Arrays.asList("haha", "monkey", "whoa")});
			
		} catch (IllegalArgumentException iae) {
			exceptionCount++;
		}
		
		Assert.assertEquals("Invalid format should have failed but no exceptions caught", 1, exceptionCount);
	}
	
	private boolean mapIsValid(Map<Long, List<String>> map, long[] keys, List<String>[] values) {
		for (int i = 0; i < keys.length; i++) {
			if (!map.containsKey(keys[i])) {
				return false;
			}
			
			for (int j = 0; j < values[i].size(); j++) {
				if (!map.get(keys[i]).get(j).equals(values[i].get(j))) {
					return false;
				}
			}
		}
		
		return true;
	}
}