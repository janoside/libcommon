package com.janoside.beans;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.junit.Assert;
import org.junit.Test;

public class CalendarParserTest {
	
	@Test
	public void testBasic() {
		CalendarParser calendarParser = new CalendarParser();
		calendarParser.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
		
		Calendar calendar = calendarParser.parse("2009-07-24");
		
		Assert.assertEquals(2009, calendar.get(Calendar.YEAR));
		Assert.assertEquals(6, calendar.get(Calendar.MONTH));
		Assert.assertEquals(24, calendar.get(Calendar.DAY_OF_MONTH));
	}
	
	@Test
	public void testExplosion() {
		CalendarParser calendarParser = new CalendarParser();
		calendarParser.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
		
		int exceptionCount = 0;
		
		try {
			calendarParser.parse("47");
			
		} catch (RuntimeException re) {
			exceptionCount++;
		}
		
		Assert.assertEquals(1, exceptionCount);
	}
}