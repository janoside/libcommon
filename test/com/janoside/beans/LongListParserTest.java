package com.janoside.beans;

import org.junit.Assert;
import org.junit.Test;

public class LongListParserTest {
	
	@Test
	public void testIt() {
		Assert.assertEquals("[1, 2, 3]", new LongListParser().parse("[1, 2, 3]").toString());
	}
}