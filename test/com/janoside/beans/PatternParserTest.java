package com.janoside.beans;

import java.util.regex.Pattern;

import org.junit.Assert;
import org.junit.Test;

public class PatternParserTest {
	
	@Test
	public void testIt() {
		Pattern p = new PatternParser().parse("abc");
		Assert.assertTrue(p.matcher("abc").matches());
		Assert.assertFalse(p.matcher("ab").matches());
	}
}