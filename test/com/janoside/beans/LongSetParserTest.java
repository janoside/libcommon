package com.janoside.beans;

import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class LongSetParserTest {
	
	@Test
	public void testBasic() {
		LongSetParser parser = new LongSetParser();
		
		Set<Long> set = parser.parse("3,4,5");
		
		Assert.assertEquals(3, set.size());
		Assert.assertTrue(set.contains(3L));
		Assert.assertTrue(set.contains(4L));
		Assert.assertTrue(set.contains(5L));
	}
	
	@Test
	public void testBlank() {
		LongSetParser parser = new LongSetParser();
		
		Set<Long> set = parser.parse("");
		
		Assert.assertEquals(0, set.size());
	}
	
	@Test
	public void testBlank2() {
		LongSetParser parser = new LongSetParser();
		
		Set<Long> set = parser.parse(",,,,,,,");
		
		Assert.assertEquals(0, set.size());
	}
	
	@Test
	public void testBlankEntry() {
		LongSetParser parser = new LongSetParser();
		
		Set<Long> set = parser.parse("3,,4,5");
		
		Assert.assertEquals(3, set.size());
		Assert.assertTrue(set.contains(3L));
		Assert.assertTrue(set.contains(4L));
		Assert.assertTrue(set.contains(5L));
	}
	
	@Test
	public void testInvalidEntry() {
		LongSetParser parser = new LongSetParser();
		
		int exceptionCount = 0;
		
		try {
			parser.parse("3,four,5");
			
		} catch (RuntimeException re) {
			exceptionCount++;
		}
		
		Assert.assertEquals(1, exceptionCount);
	}
}