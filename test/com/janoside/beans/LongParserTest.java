package com.janoside.beans;

import org.junit.Assert;
import org.junit.Test;

public class LongParserTest {
	
	@Test
	public void testIt() {
		Assert.assertEquals(5, new LongParser().parse("5").intValue());
	}
}