package com.janoside.beans;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

public class DateParserTest {
	
	@Test
	public void testBasic() {
		DateParser dateParser = new DateParser();
		dateParser.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
		
		Date date = dateParser.parse("2009-07-24");
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		
		Assert.assertEquals(2009, calendar.get(Calendar.YEAR));
		Assert.assertEquals(6, calendar.get(Calendar.MONTH));
		Assert.assertEquals(24, calendar.get(Calendar.DAY_OF_MONTH));
	}
	
	@Test
	public void testExplosion() {
		DateParser dateParser = new DateParser();
		dateParser.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
		
		int exceptionCount = 0;
		
		try {
			dateParser.parse("47");
			
		} catch (RuntimeException re) {
			exceptionCount++;
		}
		
		Assert.assertEquals(1, exceptionCount);
	}
}