package com.janoside.http;

import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.junit.Assert;
import org.junit.Test;

import com.janoside.cache.MemoryCache;
import com.janoside.cache.ObjectCache;
import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.LoggingExceptionHandler;

public class CachingHttpClientTest {
	
	private ObjectCache<String> httpCache;
	
	private BasicHttpClient httpClient;
	
	private ExceptionHandler exceptionHandler;
	
	public CachingHttpClientTest() {
		this.httpCache = new MemoryCache<String>();
		this.exceptionHandler = new LoggingExceptionHandler();
		
		org.apache.commons.httpclient.HttpClient internalHttpClient = new org.apache.commons.httpclient.HttpClient();
		MultiThreadedHttpConnectionManager manager = new MultiThreadedHttpConnectionManager();
		HttpConnectionManagerParams params = new HttpConnectionManagerParams();
		params.setDefaultMaxConnectionsPerHost(10);
		params.setMaxTotalConnections(50);
		manager.setParams(params);
		internalHttpClient.setHttpConnectionManager(manager);
		this.httpClient = new BasicHttpClient();
		this.httpClient.setHttpClient(internalHttpClient);
	}
	
	@Test
	public void testCachingGet() {
		this.httpCache.clear();
		
		CachingHttpClient client = new CachingHttpClient();
		client.setCache(this.httpCache);
		client.setExceptionHandler(this.exceptionHandler);
		client.setHttpClient(this.httpClient);
		
		String unCached = client.get("http://www.monkey.com/");
		String cached = client.get("http://www.monkey.com/");
		
		if (!unCached.isEmpty()) {
			Assert.assertTrue("Cache is empty", this.httpCache.getSize() > 0);
			Assert.assertEquals("Expected cached get, received uncached get", unCached, cached);
		}
	}
	
	@Test
	public void testCachingGetTimeout() {
		this.httpCache.clear();
		
		CachingHttpClient client = new CachingHttpClient();
		client.setCache(this.httpCache);
		client.setExceptionHandler(this.exceptionHandler);
		client.setHttpClient(this.httpClient);
		
		String unCached = client.get("http://www.monkey.com/", 5000);
		String cached = client.get("http://www.monkey.com/", 5000);
		
		if (!unCached.isEmpty()) {
			Assert.assertTrue("Cache is empty", this.httpCache.getSize() > 0);
			Assert.assertEquals("Expected cached get, received uncached get", unCached, cached);
		}
	}
}