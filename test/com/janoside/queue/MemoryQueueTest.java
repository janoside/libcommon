package com.janoside.queue;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class MemoryQueueTest {
	
	@Test
	public void testEnqueue() {
		MemoryQueue<Object> queue = new MemoryQueue<Object>();
		
		queue.enqueue(new Object());
		
		Assert.assertEquals("size", 1, queue.getSize());
	}
	
	@Test
	public void testDequeue() {
		MemoryQueue<Object> queue = new MemoryQueue<Object>();
		
		Object input = new Object();
		
		queue.enqueue(input);
		
		Assert.assertEquals("size", 1, queue.getSize());
		
		List<Object> outputList = queue.dequeue(1);
		
		Assert.assertSame(input, outputList.get(0));
	}
	
	@Test
	public void testClear() {
		MemoryQueue<Object> queue = new MemoryQueue<Object>();
		
		queue.enqueue(new Object());
		
		Assert.assertEquals("size", 1, queue.getSize());
		
		queue.clear();
		
		Assert.assertEquals("size", 0, queue.getSize());
	}
	
	@Test
	public void testSize() {
		MemoryQueue<Object> queue = new MemoryQueue<Object>();
		
		for (int i = 0; i < 250; i++) {
			queue.enqueue(new Object());
			
			Assert.assertEquals("size", i + 1, queue.getSize());
		}
		
		for (int i = 0; i < 250; i++) {
			queue.dequeue(1);
			
			Assert.assertEquals("size", 250 - (i + 1), queue.getSize());
		}
	}
	
	@Test
	public void testDequeueTooMany() {
		MemoryQueue<Object> queue = new MemoryQueue<Object>();
		
		queue.enqueue(new Object());
		
		Assert.assertEquals("size", 1, queue.getSize());
		
		List<Object> outputList = queue.dequeue(10);
		
		Assert.assertEquals("outputListSize", 1, outputList.size());
	}
	
	@Test
	public void testDequeueEmpty() {
		MemoryQueue<Object> queue = new MemoryQueue<Object>();
		
		List<Object> outputList = queue.dequeue(1);
		
		Assert.assertEquals("outputListSize", 0, outputList.size());
	}
	
	@Test
	public void testDequeueNegative() {
		MemoryQueue<Object> queue = new MemoryQueue<Object>();
		
		List<Object> outputList = queue.dequeue(-5);
		
		Assert.assertEquals("outputListSize", 0, outputList.size());
	}
	
	@Test
	public void testDequeueZero() {
		MemoryQueue<Object> queue = new MemoryQueue<Object>();
		
		List<Object> outputList = queue.dequeue(0);
		
		Assert.assertEquals("outputListSize", 0, outputList.size());
	}
}