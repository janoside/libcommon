package com.janoside.queue;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class TransformPassthroughQueueTest {
	
	@Test
	public void testEnqueue() {
		StringTestPassthroughQueue queue = new StringTestPassthroughQueue();
		
		MemoryQueue<String> stringQueue = new MemoryQueue<String>();
		queue.setInternalQueue(stringQueue);
		
		queue.enqueue("test");
		
		List<String> stringQueueContent = stringQueue.dequeue(1);
		Assert.assertEquals(1, stringQueueContent.size());
		Assert.assertTrue(stringQueueContent.get(0).equalsIgnoreCase("test-transformed"));
	}
	
	@Test
	public void testDequeue() {
		StringTestPassthroughQueue queue = new StringTestPassthroughQueue();
		
		MemoryQueue<String> stringQueue = new MemoryQueue<String>();
		queue.setInternalQueue(stringQueue);
		
		queue.enqueue("test");
		
		int exceptionCount = 0;
		
		try {
			queue.dequeue(1);
			
		} catch (UnsupportedOperationException uoe) {
			exceptionCount = 1;
		}
		
		Assert.assertEquals("TransformPassthroughQueues should not support dequeue", 1, exceptionCount);
	}
	
	@Test
	public void testClear() {
		StringTestPassthroughQueue queue = new StringTestPassthroughQueue();
		
		MemoryQueue<String> stringQueue = new MemoryQueue<String>();
		queue.setInternalQueue(stringQueue);
		
		queue.enqueue("test");
		
		int exceptionCount = 0;
		
		try {
			queue.clear();
			
		} catch (UnsupportedOperationException uoe) {
			exceptionCount = 1;
		}
		
		Assert.assertEquals("TransformPassthroughQueues should not support clear", 1, exceptionCount);
	}
	
	@Test
	public void testSize() {
		StringTestPassthroughQueue queue = new StringTestPassthroughQueue();
		
		MemoryQueue<String> stringQueue = new MemoryQueue<String>();
		queue.setInternalQueue(stringQueue);
		
		queue.enqueue("test");
		
		Assert.assertEquals("TransformPassthroughQueues should always have size 0", 0, queue.getSize());
	}
	
	private static class StringTestPassthroughQueue extends TransformPassthroughQueue<String, String> {
		
		protected String transform(String input) {
			return input.toString() + "-transformed";
		}
	}
}