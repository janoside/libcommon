package com.janoside.queue;

import java.util.List;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

public class FileQueueTest {
	
	@Test
	public void testIt() {
		FileQueue<String> fq = new FileQueue<String>();
		fq.setDirectory("/tmp/" + UUID.randomUUID().toString() + "/");
		
		fq.enqueue("abc");
		fq.enqueue("def");
		
		List<String> data = fq.dequeue(2);
		
		Assert.assertEquals(2, data.size());
		Assert.assertEquals("abc", data.get(0));
		Assert.assertEquals("def", data.get(1));
		
		data = fq.dequeue(2);
		
		Assert.assertTrue(data.isEmpty());
	}
}