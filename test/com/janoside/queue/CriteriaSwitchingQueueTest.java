package com.janoside.queue;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.criteria.BooleanCriteria;

@SuppressWarnings("unchecked")
public class CriteriaSwitchingQueueTest {
	
	@Test
	public void testEnqueue() {
		BooleanCriteria criteria = new BooleanCriteria();
		
		MemoryQueue q1 = new MemoryQueue();
		MemoryQueue q2 = new MemoryQueue();
		
		CriteriaSwitchingQueue q = new CriteriaSwitchingQueue();
		q.setCriteria(criteria);
		q.setCriteriaMetQueue(q1);
		q.setCriteriaUnmetQueue(q2);
		
		q.enqueue(new Object());
		
		Assert.assertEquals(1, q2.getSize());
		Assert.assertEquals(0, q1.getSize());
		
		criteria.setMet(true);
		
		q.enqueue(new Object());
		
		Assert.assertEquals(1, q2.getSize());
		Assert.assertEquals(1, q1.getSize());
	}
	
	@Test
	public void testEnqueueNull() {
		BooleanCriteria criteria = new BooleanCriteria();
		criteria.setMet(true);
		
		MemoryQueue q1 = new MemoryQueue();
		MemoryQueue q2 = new MemoryQueue();
		
		CriteriaSwitchingQueue q = new CriteriaSwitchingQueue();
		
		int exceptionCount = 0;
		try {
			q.enqueue(new Object());
			
		} catch (NullPointerException npe) {
			exceptionCount = 1;
		}
		
		Assert.assertEquals("No default Criteria should be configured, this should have failed with NPE", 1, exceptionCount);
		
		q.setCriteria(criteria);
		
		exceptionCount = 0;
		try {
			q.enqueue(new Object());
			
		} catch (NullPointerException npe) {
			exceptionCount = 1;
		}
		
		Assert.assertEquals("No default criteriaMetQueue should be configured, this should have failed with NPE", 1, exceptionCount);
		
		q.setCriteriaMetQueue(q1);
		criteria.setMet(false);
		
		exceptionCount = 0;
		try {
			q.enqueue(new Object());
			
		} catch (NullPointerException npe) {
			exceptionCount = 1;
		}
		
		Assert.assertEquals("No default criteriaUnmetQueue should be configured, this should have failed with NPE", 1, exceptionCount);
		
		q.setCriteriaUnmetQueue(q2);
		
		exceptionCount = 0;
		try {
			q.enqueue(new Object());
			
		} catch (NullPointerException npe) {
			exceptionCount = 1;
		}
		
		Assert.assertEquals("Everything is configured, should have been good", 0, exceptionCount);
	}
	
	@Test
	public void testDequeue() {
		BooleanCriteria criteria = new BooleanCriteria();
		criteria.setMet(true);
		
		MemoryQueue q1 = new MemoryQueue();
		MemoryQueue q2 = new MemoryQueue();
		
		CriteriaSwitchingQueue q = new CriteriaSwitchingQueue();
		q.setCriteria(criteria);
		q.setCriteriaMetQueue(q1);
		q.setCriteriaUnmetQueue(q2);
		
		q.enqueue(new Object());
		
		List list = q.dequeue(1);
		
		Assert.assertFalse(list.isEmpty());
		
		q.enqueue(new Object());
		
		list = q1.dequeue(1);
		
		Assert.assertFalse(list.isEmpty());
		Assert.assertEquals(0, q.getSize());
		Assert.assertEquals(0, q1.getSize());
		
		criteria.setMet(false);
		q.enqueue(new Object());
		
		list = q2.dequeue(1);
		
		Assert.assertFalse(list.isEmpty());
		Assert.assertEquals(0, q.getSize());
		Assert.assertEquals(0, q1.getSize());
	}
	
	@Test
	public void testClear() {
		BooleanCriteria criteria = new BooleanCriteria();
		criteria.setMet(true);
		
		MemoryQueue q1 = new MemoryQueue();
		MemoryQueue q2 = new MemoryQueue();
		
		CriteriaSwitchingQueue q = new CriteriaSwitchingQueue();
		q.setCriteria(criteria);
		q.setCriteriaMetQueue(q1);
		q.setCriteriaUnmetQueue(q2);
		
		q.enqueue(new Object());
		
		Assert.assertEquals(1, q.getSize());
		Assert.assertEquals(1, q1.getSize());
		Assert.assertEquals(0, q2.getSize());
		
		q.clear();
		
		Assert.assertEquals(0, q.getSize());
		Assert.assertEquals(0, q1.getSize());
		Assert.assertEquals(0, q2.getSize());
		
		criteria.setMet(false);
		
		q.enqueue(new Object());
		
		Assert.assertEquals(1, q.getSize());
		Assert.assertEquals(0, q1.getSize());
		Assert.assertEquals(1, q2.getSize());
		
		q.clear();
		
		Assert.assertEquals(0, q.getSize());
		Assert.assertEquals(0, q1.getSize());
		Assert.assertEquals(0, q2.getSize());
	}
}