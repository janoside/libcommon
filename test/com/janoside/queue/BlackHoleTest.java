package com.janoside.queue;

import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("unchecked")
public class BlackHoleTest {
	
	@Test
	public void testEnqueue() {
		BlackHole blackHole = new BlackHole();
		
		blackHole.enqueue(new Object());
	}
	
	@Test
	public void testDequeue() {
		BlackHole blackHole = new BlackHole();
		
		blackHole.enqueue(new Object());
		
		int exceptionCount = 0;
		
		try {
			blackHole.dequeue(1);
			
		} catch (PhysicsException pe) {
			exceptionCount++;
		}
		
		Assert.assertEquals(1, exceptionCount);
	}
	
	@Test
	public void testClear() {
		BlackHole blackHole = new BlackHole();
		
		blackHole.enqueue(new Object());
		
		blackHole.clear();
	}
	
	@Test
	public void testSize() {
		BlackHole blackHole = new BlackHole();
		
		for (int i = 0; i < 1000000; i++) {
			blackHole.enqueue(new Object());
		}
		
		Assert.assertEquals(0, blackHole.getSize());
	}
}