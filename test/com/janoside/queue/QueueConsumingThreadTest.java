package com.janoside.queue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Assert;
import org.junit.Test;

public class QueueConsumingThreadTest {
	
	@Test
	public void testBasic() throws InterruptedException {
		ManagedMemoryQueue<Object> q = new ManagedMemoryQueue<Object>();
		TimeTrackingConsumer c = new TimeTrackingConsumer();
		
		QueueConsumingThread<Object> t = new QueueConsumingThread<Object>();
		t.setQueue(q);
		q.addManagedObjectQueueObserver(t);
		t.setConsumer(c);
		
		new Thread(t).start();
		
		ArrayList<Long> times = new ArrayList<Long>();
		
		Thread.sleep(200);
		
		times.add(System.currentTimeMillis());
		q.enqueue(new Object());
		
		Thread.sleep(100);
		
		times.add(System.currentTimeMillis());
		q.enqueue(new Object());
		
		Thread.sleep(150);
		
		times.add(System.currentTimeMillis());
		q.enqueue(new Object());
		
		Thread.sleep(200);
		
		times.add(System.currentTimeMillis());
		q.enqueue(new Object());
	
		Thread.sleep(100);
		
		Assert.assertEquals(times.size(), c.times.size());
		
		for (int i = 0; i < times.size(); i++) {
			Assert.assertTrue((c.times.get(i) - times.get(i)) < 10);
		}
	}
	
	@Test
	public void testConcurrent() throws InterruptedException {
		for (int trial = 0; trial < 10; trial++) {
			final ManagedMemoryQueue<Object> q = new ManagedMemoryQueue<Object>();
			TimeTrackingConsumer c = new TimeTrackingConsumer();
			
			QueueConsumingThread<Object> t = new QueueConsumingThread<Object>();
			t.setQueue(q);
			q.addManagedObjectQueueObserver(t);
			t.setConsumer(c);
			
			new Thread(t).start();
			
			ExecutorService executor = Executors.newFixedThreadPool(20);
			
			for (int i = 0; i < 15; i++) {
				final int x = i;
				executor.execute(new Runnable() {
					public void run() {
						try {
							for (int j = 0; j < 100; j++) {
								q.enqueue("obj-" + x + "-" + j);
							}
						} catch (Throwable t) {
							t.printStackTrace();
						}
					}
				});
			}
			
			// make sure all tasks are started
			Thread.sleep(250);
			
			executor.shutdown();
			
			while (!executor.isTerminated() && q.getSize() > 0) {
				Thread.sleep(10);
			}
			
			Assert.assertEquals(1500, c.objects.size());
			for (int i = 0; i < 15; i++) {
				for (int j = 0; j < 100; j++) {
					Assert.assertTrue(c.objects.contains("obj-" + i + "-" + j));
				}
			}
		}
	}
	
	private static class TimeTrackingConsumer implements ObjectConsumer<Object> {
		
		public Set<Object> objects = Collections.synchronizedSet(new HashSet<Object>());
		
		public ArrayList<Long> times = new ArrayList<Long>();
		
		public void consume(Object object) {
			this.objects.add(object);
			this.times.add(System.currentTimeMillis());
		}
	}
}