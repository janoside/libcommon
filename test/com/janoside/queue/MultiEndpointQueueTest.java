package com.janoside.queue;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("unchecked")
public class MultiEndpointQueueTest {
	
	@Test
	public void testEnqueue() {
		ObjectQueue q1 = new MemoryQueue();
		ObjectQueue q2 = new MemoryQueue();
		ObjectQueue q3 = new MemoryQueue();
		
		MultiEndpointQueue multiEndpointQueue = new MultiEndpointQueue();
		multiEndpointQueue.setInternalQueues(Arrays.asList(q1, q2, q3));
		
		Object object = new Object();
		
		multiEndpointQueue.enqueue(object);
		
		Assert.assertSame(object, q1.dequeue(1).get(0));
		Assert.assertSame(object, q2.dequeue(1).get(0));
		Assert.assertSame(object, q3.dequeue(1).get(0));
	}
	
	@Test
	public void testDequeue() {
		ObjectQueue q1 = new MemoryQueue();
		ObjectQueue q2 = new MemoryQueue();
		ObjectQueue q3 = new MemoryQueue();
		
		MultiEndpointQueue multiEndpointQueue = new MultiEndpointQueue();
		multiEndpointQueue.setInternalQueues(Arrays.asList(q1, q2, q3));
		
		Object object = new Object();
		
		multiEndpointQueue.enqueue(object);
		
		int exceptionCount = 0;
		
		try {
			multiEndpointQueue.dequeue(1);
			
		} catch (UnsupportedOperationException uoe) {
			exceptionCount++;
		}
		
		Assert.assertEquals(1, exceptionCount);
	}
	
	@Test
	public void testClear() {
		ObjectQueue q1 = new MemoryQueue();
		ObjectQueue q2 = new MemoryQueue();
		ObjectQueue q3 = new MemoryQueue();
		
		MultiEndpointQueue multiEndpointQueue = new MultiEndpointQueue();
		multiEndpointQueue.setInternalQueues(Arrays.asList(q1, q2, q3));
		
		Object object = new Object();
		
		multiEndpointQueue.enqueue(object);
		
		int exceptionCount = 0;
		
		try {
			multiEndpointQueue.clear();
			
		} catch (UnsupportedOperationException uoe) {
			exceptionCount++;
		}
		
		Assert.assertEquals(1, exceptionCount);
	}
	
	@Test
	public void testEnqueueWithNoInternalQueues() {
		MultiEndpointQueue multiEndpointQueue = new MultiEndpointQueue();
		
		multiEndpointQueue.enqueue(new Object());
	}
}