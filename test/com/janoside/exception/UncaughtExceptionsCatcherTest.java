package com.janoside.exception;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Assert;
import org.junit.Test;

public class UncaughtExceptionsCatcherTest {
	
	@Test
	public void testIt() throws Exception {
		final AtomicInteger counter = new AtomicInteger(0);
		
		ExceptionHandler exceptionHandler = new ExceptionHandler() {
			public void handleException(Throwable t) {
				counter.getAndIncrement();
			}
		};
		
		UncaughtExceptionsCatcher catcher = new UncaughtExceptionsCatcher();
		catcher.setExceptionHandler(exceptionHandler);
		catcher.afterPropertiesSet();
		
		int threadCount = 100;
		ExecutorService executor = Executors.newFixedThreadPool(10);
		final CountDownLatch latch = new CountDownLatch(threadCount);
		
		for (int i = 0; i < threadCount; i++) {
			executor.execute(new Runnable() {
				public void run() {
					try {
						throw new RuntimeException();
						
					} finally {
						latch.countDown();
					}
				}
			});
		}
		
		latch.await();
		
		// default uncaught exception handling is apparently not synchronous, so wait just a moment
		Thread.sleep(250);
		
		Assert.assertEquals(threadCount, counter.get());
	}
}