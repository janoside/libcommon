package com.janoside.csv;

import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class CsvFieldExtractorTest {
	
	@Test
	public void testNoQuotes() {
		CsvFieldExtractor cfe = new CsvFieldExtractor();
		
		List<Map<String, String>> values = cfe.getCsvFields("one,two,three\n1,2,3", ",", false, false);
		Assert.assertEquals(1, values.size());
		Assert.assertEquals(3, values.get(0).size());
		Assert.assertEquals("1", values.get(0).get("one"));
		Assert.assertEquals("2", values.get(0).get("two"));
		Assert.assertEquals("3", values.get(0).get("three"));
	}
	
	@Test
	public void testTitleQuotes() {
		CsvFieldExtractor cfe = new CsvFieldExtractor();
		
		List<Map<String, String>> values = cfe.getCsvFields("\"one\",\"two\",\"three\"\n1,2,3", ",", true, false);
		Assert.assertEquals(1, values.size());
		Assert.assertEquals(3, values.get(0).size());
		Assert.assertEquals("1", values.get(0).get("one"));
		Assert.assertEquals("2", values.get(0).get("two"));
		Assert.assertEquals("3", values.get(0).get("three"));
	}
	
	@Test
	public void testTitleAndValueQuotes() {
		CsvFieldExtractor cfe = new CsvFieldExtractor();
		
		List<Map<String, String>> values = cfe.getCsvFields("\"one\",\"two\",\"three\"\n\"1\",\"2\",\"3\"", ",", true, true);
		Assert.assertEquals(1, values.size());
		Assert.assertEquals(3, values.get(0).size());
		Assert.assertEquals("1", values.get(0).get("one"));
		Assert.assertEquals("2", values.get(0).get("two"));
		Assert.assertEquals("3", values.get(0).get("three"));
	}
	
	@Test
	public void testNoQuotesTab() {
		CsvFieldExtractor cfe = new CsvFieldExtractor();
		
		List<Map<String, String>> values = cfe.getCsvFields("one\ttwo\tthree\n1\t2\t3", "\t", false, false);
		Assert.assertEquals(1, values.size());
		Assert.assertEquals(3, values.get(0).size());
		Assert.assertEquals("1", values.get(0).get("one"));
		Assert.assertEquals("2", values.get(0).get("two"));
		Assert.assertEquals("3", values.get(0).get("three"));
	}
	
	@Test
	public void testTitleQuotesTab() {
		CsvFieldExtractor cfe = new CsvFieldExtractor();
		
		List<Map<String, String>> values = cfe.getCsvFields("\"one\"	\"two\"	\"three\"\n1\t2\t3", "\t", true, false);
		Assert.assertEquals(1, values.size());
		Assert.assertEquals(3, values.get(0).size());
		Assert.assertEquals("1", values.get(0).get("one"));
		Assert.assertEquals("2", values.get(0).get("two"));
		Assert.assertEquals("3", values.get(0).get("three"));
	}
	
	@Test
	public void testTitleAndValueQuotesTab() {
		CsvFieldExtractor cfe = new CsvFieldExtractor();
		
		List<Map<String, String>> values = cfe.getCsvFields("\"one\"\t\"two\"\t\"three\"\n\"1\"\t\"2\"\t\"3\"", "	", true, true);
		Assert.assertEquals(1, values.size());
		Assert.assertEquals(3, values.get(0).size());
		Assert.assertEquals("1", values.get(0).get("one"));
		Assert.assertEquals("2", values.get(0).get("two"));
		Assert.assertEquals("3", values.get(0).get("three"));
	}
	
	@Test
	public void testSomething() {
		CsvFieldExtractor cfe = new CsvFieldExtractor();
		
		String s = "\"StockNumber\",\"VIN\",\"Year\",\"Make\",\"Model\",\"Trim\",\"Engine\",\"Tramsission\",\"BodyStyle\",\"NewUsed\",\"Certified\",\"Price\",\"Mileage\",\"Color\",\"InteriorColor\",\"Description\",\"Options\",\"PhotoURLs\"\n";
		s += "\"Q9492A\",\"JHMCP26399C011322\",\"2009\",\"HONDA\",\"ACCORD\",\"LX\",\"DOHC i-VTEC 16V I-4\",\"Automatic\",\"4DR\",\"Used\",\"Y\",\"19279\",\"36172\",\"GRAY\",\"BLACK\",\"CLEAN CARFAX, 1 OWNER, HONDA CERTIFIED,LX trim.\"\"\"\" 4 NEW TIRES\"\"\"\" EPA 30 MPG Hwy/21 MPG City! Warranty 7yrs/100K Miles - Drivetrain Warranty;COMPLIMENTARY MAINT. Edmunds.com explains \"\"Upscale feel.\"\", Consumer Guide Best Buy Car, Steel Wheels, CD Player, Overhead Airbag, iPod/MP3 Input AND MORE!         ======SHOP WITH CONFIDENCE :  CARFAX 1-Owner. ======KEY FEATURES: iPod/MP3 Input,CD Player,Steel Wheels :  MP3 Player,Remote Trunk Release,Keyless Entry,Child Safety Locks,Steering Wheel Controls. ======EXPERTS ARE SAYING :  Car and Driver Editors Choice.  \"\"The Honda Accord offers an appealing combination of spaciousness, a relatively upscale feel and a reputation for reliability.\"\" -Edmunds.com.  5 Star Driver Front Crash Rating. 5 Star Driver Side Crash Rating.  Great Gas Mileage: 30 MPG Hwy. ======WHO WE ARE :  Pompano Honda is a premier Honda franchised dealership of the Fort Lauderdale - Miami area. Celebrating 33 years of dedicated service to the Ft. Lauderdale Florida marketplace. The only South Florida Dealer that is a 7 time winner of the very highly acclaimed 'Honda Presidents Award.'  Fuel economy calculations based on original manufacturer data for trim engine configuration. Please confirm the accuracy of the included equipment by calling us prior to purchase.\",\"Child Protection Door Locks,Driver & Front Passenger Active Head Restraints,Dual-Chamber Front Seat Side Airbags,Dual-Stage Dual-Threshold Frontal Airbags,Emergency Interior Trunk Opener,Front Passenger Occupant Detection System,LATCH Child Safety Seat Anchors,Side Curtain Airbags,Theft-Deterrent Immobilizer System,12-Volt Auxiliary Power Outlets,160-Watt AM/FM Stereo w/CD/MP3 Player,6-Speaker Audio System,Air Conditioning w/Filtration System,Cruise Control,Map Lights,Power Door Locks,Rear Window Defroster,Remote Fuel Door/Trunk Releases,Steering Wheel-Mounted Audio Controls,Tilt/Telescopic Steering Column,Daytime Running Lights,Power Exterior Mirrors,Remote Entry System w/Window Control,Variable Intermittent Windshield Wipers,Advanced Compatibility Engineering Body,Drive-By-Wire Throttle System,Electronic Brake Distribution & Brake Assist,Power 4-Wheel Disc Anti-Lock Brakes,Tachometer,Tire Pressure Monitoring System,Variable Power Rack & Pinion Steering,Vehicle Stability Assist & Traction Control\",\"http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-o1.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p2.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p3.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p4.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p5.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p6.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p7.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p8.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p9.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p10.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p11.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p12.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p13.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p14.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p15.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p16.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p17.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p18.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p19.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p20.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p21.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p22.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p23.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p24.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p25.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p26.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p27.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p28.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p29.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p30.jpg?dt=20110818060026,http://www.netlook.com/auto/USFLFTL/pic/11986575/AT11527240-640px-p31.jpg?dt=20110818060026\"";
		
		List<Map<String, String>> data = cfe.getCsvFields(s, ",", true, true);
		
		System.out.println(data.get(0).get("Description"));
		System.out.println(data.get(0).get("Options"));
	}
}