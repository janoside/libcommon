package com.janoside.csv;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.exception.StandardErrorExceptionHandler;

public class CsvFieldQuotingTextTransformerTest {
	
	@Test
	public void testIt() {
		CsvFieldQuotingTextTransformer t = new CsvFieldQuotingTextTransformer();
		t.setExceptionHandler(new StandardErrorExceptionHandler());
		
		Assert.assertEquals("\"1\",\"2\",\"3\"", t.transform("1,\"2\",3"));
		Assert.assertEquals("\"one\",\"two\",\"three\"", t.transform("\"one\",\"two\",three"));
		Assert.assertEquals("\"one\",\"two\",\"three\"", t.transform("\"one\",\"two\",\"three\""));
		Assert.assertEquals("\"one\",\"two\"", t.transform("\"one\",\"two\""));
		Assert.assertEquals("\"one\",\"two,three\",\"four\",\"five\",\"six\",\"seven\"", t.transform("one,\"two,three\",four,five,\"six\",seven"));
		Assert.assertEquals("\"one\",\"abc|two, three|haha\",\"four\",\"five\",\"six\",\"seven\"", t.transform("one,\"abc|two, three|haha\",four,five,\"six\",seven"));
		
		Assert.assertEquals("\"one\",\"two\",\"three\",\"four\",\"five,six\",\"seven\"", t.transform("one,\"two\",three,\"four\",\"five,six\",\"seven\""));
	}
	
	@Test
	public void test2() {
		CsvFieldQuotingTextTransformer t = new CsvFieldQuotingTextTransformer();
		t.setExceptionHandler(new StandardErrorExceptionHandler());
		
		String output = t.transform("abc,Red,Red,,,\"Fuel Consumption: City: 16|Fuel Consumption: Highway: 22|Memorized Settings including door mirror(s)|Memorized Settings including steering wheel|Memorized Settings for 2 drivers|Driver seat memory|Remote engine start|Remote power door locks|Power windows|Cruise controls on steering wheel|Cruise control|4-wheel ABS Brakes|Front Ventilated disc brakes|1st, 2nd and 3rd row head airbags|Passenger Airbag|Curb weight: 4,980|Gross vehicle weight: 6,459|Overall Length: 201.8\",,06/22/2010,04/29/2011,Y,\"Littleton Chevrolet\",\"851 Meadow Street\",Littleton,NH,03561,603-444-5678,,http://autobooknow.images.dmotorworks.com/ADPDMBU4899_5555/B1021_1.jpg|http://autobooknow.images.dmotorworks.com/ADPDMBU4899_5555/B1021_2.jpg|http://autobooknow.images.dmotorworks.com/ADPDMBU4899_5555/B1021_3.jpg|http://autobooknow.images.dmotorworks.com/ADPDMBU4899_5555/B1021_4.jpg|http://autobooknow.images.dmotorworks.com/ADPDMBU4899_5555/B1021_5.jpg|http://autobooknow.images.dmotorworks.com/ADPDMBU4899_5555/B1021_6.jpg|http://autobooknow.images.dmotorworks.com/ADPDMBU4899_5555/B1021_7.jpg|http://autobooknow.images.dmotorworks.com/ADPDMBU4899_5555/B1021_8.jpg|http://autobooknow.images.dmotorworks.com/ADPDMBU4899_5555/B1021_9.jpg|http://autobooknow.images.dmotorworks.com/ADPDMBU4899_5555/B1021_10.jpg|http://autobooknow.images.dmotorworks.com/ADPDMBU4899_5555/B1021_11.jpg|http://autobooknow.images.dmotorworks.com/ADPDMBU4899_5555/B1021_12.jpg|http://autobooknow.images.dmotorworks.com/ADPDMBU4899_5555/B1021_13.jpg|http://autobooknow.images.dmotorworks.com/ADPDMBU4899_5555/B1021_14.jpg");
		
		System.out.println(output);
		
		CsvFieldExtractor fe = new CsvFieldExtractor();
		System.out.println(fe.getCsvFields(output, ",", true, true));
	}
}