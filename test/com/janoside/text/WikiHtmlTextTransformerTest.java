package com.janoside.text;

import org.junit.Assert;
import org.junit.Test;

public class WikiHtmlTextTransformerTest {
	
	@Test
	public void testLinks() {
		WikiHtmlTextTransformer tt = new WikiHtmlTextTransformer();
		
		Assert.assertEquals("<p><a href=\"http://cheese.com\">awesome stuff</a></p>", tt.transform("[http://cheese.com|awesome stuff]"));
		Assert.assertEquals("<p><a href=\"http://cheese.com\">awesome stuff</a>.</p>", tt.transform("[http://cheese.com|awesome stuff]."));
		Assert.assertEquals("<p>1.<a href=\"http://cheese.com\">awesome stuff</a> something</p>", tt.transform("1.[http://cheese.com|awesome stuff] something"));
	}
	
	@Test
	public void testBoldItalic() {
		WikiHtmlTextTransformer tt = new WikiHtmlTextTransformer();
		
		Assert.assertEquals("<p><strong><i>Cheese</i></strong></p>", tt.transform("'''''Cheese'''''"));
		Assert.assertEquals("<p><strong><i>Cheese</i></strong> Gorilla</p>", tt.transform("'''''Cheese''''' Gorilla"));
		Assert.assertEquals("<p><strong><i>Cheese's</i></strong> Gorilla</p>", tt.transform("'''''Cheese's''''' Gorilla"));
		Assert.assertEquals("<p><strong><i>Cheese's</i></strong> <strong><i>Gorilla</i></strong></p>", tt.transform("'''''Cheese's''''' '''''Gorilla'''''"));
		Assert.assertEquals("<p><strong><i>Cheese's</i></strong> Gorilla'''''</p>", tt.transform("'''''Cheese's''''' Gorilla'''''"));
	}
	
	@Test
	public void testBold() {
		WikiHtmlTextTransformer tt = new WikiHtmlTextTransformer();
		
		Assert.assertEquals("<p><strong>Cheese</strong></p>", tt.transform("'''Cheese'''"));
		Assert.assertEquals("<p><strong>Cheese</strong> Gorilla</p>", tt.transform("'''Cheese''' Gorilla"));
		Assert.assertEquals("<p><strong>Cheese's</strong> Gorilla</p>", tt.transform("'''Cheese's''' Gorilla"));
		Assert.assertEquals("<p><strong>Cheese's</strong> <strong>Gorilla</strong></p>", tt.transform("'''Cheese's''' '''Gorilla'''"));
		Assert.assertEquals("<p><strong>Cheese's</strong> Gorilla'''</p>", tt.transform("'''Cheese's''' Gorilla'''"));
	}
	
	@Test
	public void testItalic() {
		WikiHtmlTextTransformer tt = new WikiHtmlTextTransformer();
		
		Assert.assertEquals("<p><i>Cheese</i></p>", tt.transform("''Cheese''"));
		Assert.assertEquals("<p><i>Cheese</i> Gorilla</p>", tt.transform("''Cheese'' Gorilla"));
		Assert.assertEquals("<p><i>Cheese's</i> Gorilla</p>", tt.transform("''Cheese's'' Gorilla"));
		Assert.assertEquals("<p><i>Cheese's</i> Gorilla''</p>", tt.transform("''Cheese's'' Gorilla''"));
	}
	
	@Test
	public void testHeadings() {
		WikiHtmlTextTransformer tt = new WikiHtmlTextTransformer();
		
		Assert.assertEquals("<h2 id=\"Cheese\">Cheese</h2>", tt.transform("==Cheese=="));
		Assert.assertEquals("<h2 id=\"Cheese_2\">Cheese 2</h2>", tt.transform("==Cheese 2=="));
		
		Assert.assertEquals("<h3 id=\"Cheese_2\">Cheese 2</h3>", tt.transform("===Cheese 2==="));
	}
	
	@Test
	public void testList() {
		WikiHtmlTextTransformer tt = new WikiHtmlTextTransformer();
		
		Assert.assertEquals("<ul><li>Cheese</li><li>Cheese 2</li><li>Cheese 3</li></ul>", tt.transform("* Cheese\n* Cheese 2\n* Cheese 3"));
	}
}